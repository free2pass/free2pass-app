# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added 

- Automated builds for iOS
- API for appointments
- Animation for user change
- Animation for certificate status change
- Beta flavor for independent testing
- Animation to predicted home tab after scan
- Filters and search for locations
- Implemented digital vaccination certificate
- Visual handling of offline devices 
- Optional push provider for appointments and certificates
- Language code to certificate data for manual exports

### Changed

- Redirecting to check in or certificate scan if tapping the wrong button
- Icons for the location types
- Cleaned source code
- Fusioned check in and certificate scan pages
- PDF verification mechanism

### Deprecated

- 

### Removed

- 

### Fixed

- Refactored code
- Dispose in phone number verification
- Disabled photo selector as soon as another one is active

### Security

- 

## [Release-1.4.8+32] - 2021-05-29

### Added 

- Version information for web 
- Crypto concept for contact tracing without health department
- Check-In for private Occasions
- Creation of private Occasions
- Deep links to app
- Friendly notice for pentesters
- Dialog on max registration attempts

### Changed

- Reverted internally modified dependency `flutter_map_location` to the upstream now patched
- Renamed GitLab entrypoint config for web
- Flutter engine version used
- Migrated to null safety. Blocking: [flutter_map#829](https://github.com/fleaflet/flutter_map/issues/829)
- Expiring focus from inputs after activating other UI elements

### Deprecated

- Use of `package_info`

### Removed

- 

### Fixed

- Home page layout on tablet-size devices 
- Fixed overflow on pin verification view
- iOS preventing from entering a `-` in names
- Cleaned source code
- Certificate countdown timer for certificates with a validity of more than 24 hours

### Security

- Added random entropy to all encrypted personal data to avoid binary pattern detection
- Allow signature verification fo PDF-exported certificates

## [Release-1.4.7+31] - 2021-05-18 [YANKED]

### Security

- Critical error displaying the wrong user on an exported PDF

## [Release-1.4.7+30] - 2021-05-18

### Added 

- Added this changelog to ensure easy understanding of changes at a glace
- Added button to contact issuer on positive tests

### Security

- Removed possibility to override user image by creating a new profile

## [Release-1.4.6+29] - 2021-05-18

### Added

- The latest release without proper changelog in git

[unreleased]: https://gitlab.com/free2pass/free2pass-app/-/compare/Release-1.4.8+32...main
[Release-1.4.8+32]: https://gitlab.com/free2pass/free2pass-app/-/tags/Release-1.4.8+32
[Release-1.4.7+31]: https://gitlab.com/free2pass/free2pass-app/-/tags/Release-1.4.7+31
[Release-1.4.7+30]: https://gitlab.com/free2pass/free2pass-app/-/tags/Release-1.4.7+30
[Release-1.4.6+29]: https://gitlab.com/free2pass/free2pass-app/-/tags/Release-1.4.6+29

