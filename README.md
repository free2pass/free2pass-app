# free2**pass**

[![Current version](https://img.shields.io/badge/dynamic/yaml?label=Current%20version&query=version&url=https%3A%2F%2Fgitlab.com%2Ffree2pass%2Ffree2pass-app%2Fraw%2Fmain%2Fpubspec.yaml%3Finline%3Dfalse&style=for-the-badge&logo=flutter&logoColor=white)](https://gitlab.com/free2pass/free2pass-app/-/tags)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/free2pass/free2pass-app/main?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/free2pass/free2pass-app/-/pipelines)
[![Google Play](https://img.shields.io/endpoint?color=689f38&url=https%3A%2F%2Fplayshields.herokuapp.com%2Fplay%3Fi%3Dcom.hanntech.free2pass%26l%3DGoogle-Play%26m%3D%24version&style=for-the-badge&logo=google-play&logoColor=white)](https://play.google.com/store/apps/details?id=com.hanntech.free2pass)
[![Apple App Store](https://img.shields.io/badge/Apple%20App%20Store-available-lightgrey?style=for-the-badge&logo=apple&logoColor=white)](https://apps.apple.com/de/app/free2pass/id1561574735)
[![F-Droid](https://img.shields.io/f-droid/v/com.hanntech.free2pass?logo=f-droid&style=for-the-badge)](https://f-droid.org/packages/com.hanntech.free2pass/)

A project fighting against the Covid pandemics.

## Technology

This app is developed using the [Flutter SDK](https://flutter.dev/) and
the [Dart programming language](https://dart.dev/).

Aside if the SDK itself, we rely on some important Dart and Flutter packages, such as:

- [Hive](https://pub.dev/packages/hive) - an encrypted, in-memory no-SQL database we use to store your personal data,
  recent Covid certificates and your location history
- [animations](https://pub.dev/packages/animations) - a feature-rich library providing smooth and friendly animations
- [geolocator](https://pub.dev/packages/geolocator) - a location processing plugin we use to locally verify your
  check-ins with
- [flutter_map](https://pub.dev/packages/flutter_map) - the Flutter leaflet implementation providing privacy-friendly
  OpenStreetMap data we use to display participating locations
- [qr_code_scanner](https://pub.dev/packages/qr_code_scanner) - an implementation for high-level QR code processing we
  use to exchange data during the check-ins and Covid tests
- [crypto](https://pub.dev/packages/crypto), [encrypt](https://pub.dev/packages/encrypt)
  , [pointycastle](https://pub.dev/packages/pointycastle) & [openpgp_flutter](https://pub.dev/packages/openpgp_flutter)
  four crypto libraries we encrypt your user data with

Parts of our code are outsourced into our [support library](https://gitlab.com/free2pass/lib-free2pass-dart). Of course,
aside of these core dependencies, we use plenty other packages too. The full list of dependencies we have can be found
in the `pubspec.yaml` of this repo and in the support library.

## Download

- **Android**
    - download release APKs at the [release page](https://gitlab.com/free2pass/free2pass-app/-/tags/)
    - download from [Google Play Store](https://play.google.com/store/apps/details?id=com.hanntech.free2pass)
    - download from [F-Droid](https://f-droid.org/packages/com.hanntech.free2pass/)
    - download from [HUAWEI AppGallery](https://appgallery.huawei.com/#/app/C104190677)
    - download [continuous builds](https://gitlab.com/free2pass/free2pass-app/-/jobs/artifacts/main/browse?job=build%3Alibre)
    - download [nightly testing builds](https://gitlab.com/free2pass/free2pass-app/-/jobs/artifacts/main/browse?job=build%3Anightly)
      *(Not meant for production: Cannot communicate with real-world infrastructure)*
- **iOS**
    - download from [Apple App Store](https://apps.apple.com/de/app/free2pass/id1561574735)
    - download [continuous builds](https://gitlab.com/free2pass/free2pass-app/-/jobs/artifacts/main/browse?job=build%3Aios)
- **Web** _(testing)_
    - [*Beta*](https://free2pass.gitlab.io/free2pass-app) _(These builds are really **very** unstable and not at all
      tested.)_
- *[Code analysis](https://gitlab.com/free2pass/free2pass-app/-/jobs/artifacts/main/browse?job=analyze%3Aflutter)*

## Getting started

If you like to build the app yourself, you need to install Flutter on your development machine. Afterwards, follow these
steps to build your local copy of the app:

- run `flutter pub get`
- in case you hacked around in `lib/enums` or `lib/models`, run `flutter pub run build_runner build`
- build your app using
    - build the standard flavors
        - `git checkout origin/play`
        - `git rebase main`
        - now either
            - `flutter build ipa -t lib/entrypoints/apple.entrypoint.dart` for iOS
            - or `flutter build apk --flavor play -t lib/entrypoints/play.entrypoint.dart`
    - build the FLOSS flavors
        - `flutter build apk --flavor $FLAVOR -t lib/entrypoints/$FLAVOR.entrypoint.dart` for
          Android, whereas `FLAVOR`
          can be `fdroid`, `huawei`, `amazon`, `gitlab` or `nightly`

As of now, flavors `fdroid`, `amazon`, `huawei`, `gitlab` and `nightly` are not different except of the update download
URL. Due to technical limitations of the Flutter framework, builds for `play` or `apple` flavor require the separate
`play` branch. Please ensure you always rebase the `play` branch before building.

If you'd like to get some test data in your own build, you can uncomment the corresponding lines
in `lib/utils/hive.util.dart`.

Contributions are to the project are very welcome. Simply open an issue in the GitLab repository. There, we may discuss
your ideas and find solutions.

## State of the art

:snail:

As you may expect for such a short development time as we were faced with, we were forced to code very quickly - maybe a
bit too quickly. Our source code itself is only coarsely documented hence. We hope you may understand these
circumstances, but we'll try to document the entire source code as soon as possible.

This is what we currently work on:

- [x] Advanced encryption
- [x] Dynamic data changes using listenables
- [x] i18n for German and English
- [x] Offline history
- [x] Offline AES Cipher transfer
- [x] Design working on small devices
- [ ] Design working on big devices
- [ ] Bluetooth Exposure Notification

## Style & assets

This is our applications style:

- Primary Color: `#00c58d`
- Accent Color: `#3d5ba0`
- Alert Color `#aa003c`
- Background: `#dae3ff66`
- Text Color `#3c3c3b`

We use `Quicksand` (OFL) as font.

Our icons can be found in `assets/logos/`. To get the proper PNG from the plain SVG, you can use the following commands:

```shell
inkscape -o assets/logos/white-transparent.png -w 2048 -h 2048 assets/logos/white-transparent.svg
convert -background transparent assets/logos/white-transparent.png -gravity center -extent 3328x3328 assets/logos/white-transparent.png
flutter pub run flutter_launcher_icons:main
```

## Security

Health data is highly sensitive information. We hence use an advanced combination of decentralized, offline AES, RSA and
PGP encryption to provide real end-to-end-encryption. In case of any data leak, one would need to get at least three
secret keys at once to access any information.

Want to learn more? Read our [documentation on security](SECURITY.md) _(German only)_ yourself.

If you don't believe us, fell free to inspect our source code ;-).

## CCC contact tracing touchstone compliance

**Are you complying with the Covid contact tracing touchstones of the Chaos Computer Club?**

Yes and no. The touchstones of the German CCC were developed for pure contact tracing as e.g. the Corona Warn App does.
Aside of contact tracing, we provide features such as Covid-19 test attestation too. We have other requirements hence.

To be clear: We comply with most of the touchstones of the CCC but actively decided against complying with some. For
those we ignore, we provide a detailed explanation in our [documentation on security](SECURITY.md) _(German only)_.

## API authentication

Theoretically, our API is open for access. Anyway, sensitive requests, such as the status of the last Covid-19 test
result are signed. This verifies, the request ist really sent by the user it belongs to.

The signature works the following way:
To avoid abuse of the power we provide to our users by doing lots of security and crypto stuff on their own devices, a
secret authentication in very important.

For the transparency, we would like to explain how this authentication works.

As we want to proof, the request belongs to us, and we are authorized to access the information within, we take a
blowfish hash of our personal data.

Now, we imagine adding this hash to the end of the request string consisting of the url and the parameters and take a
hash of this constructed string. This hash is added as a request header.

Ready to go :tada:!

## Legal information

This work is owned by free2pass GmbH and published under the terms and conditions of the AGPL 3.0. Further information
can be found in [LICENSE](LICENSE).

- [Legal notice](https://free2pass.de/impressum/)
- [Privacy policy](https://free2pass.de/app-privacy-policy/)
- [Terms of use](https://free2pass.de/terms-of-use/)
