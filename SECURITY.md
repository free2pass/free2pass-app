# free2**pass** Sicherheitskonzept

## Inhalt

- [free2**pass** Sicherheitskonzept](#free2pass-sicherheitskonzept)
    - [Inhalt](#inhalt)
    - [Präambel](#präambel)
    - [Bezug auf die 10 Prüfsteine für die Beurteilung von "Corona-Tracing"-Apps](#bezug-auf-die-10-prüfsteine-für-die-beurteilung-von-corona-tracing-apps)
        - [Risiko](#risiko)
        - [10. & 9. Unbeobachtbarkeit der Kommunikation und Unverkettbarkeit](#10--9-unbeobachtbarkeit-der-kommunikation-und-unverkettbarkeit)
        - [8. Kein Aufbau von zentralen Bewegungs- und Kontaktprofilen](#8-kein-aufbau-von-zentralen-bewegungs--und-kontaktprofilen)
        - [7. Anonymität](#7-anonymität)
        - [6. Datensparsamkeit](#6-datensparsamkeit)
        - [5. Unabhängigkeit von Vertrauen](#5-unabhängigkeit-von-vertrauen)
        - [4. & 3. Transparenz und Prüfbarkeit](#4--3-transparenz-und-prüfbarkeit)
        - [2. & 1. Freiwilligkeit, Diskriminierungsfreiheit und Zweckgebundenheit](#2--1-freiwilligkeit-diskriminierungsfreiheit-und-zweckgebundenheit)
        - [Schlussstrich](#schlussstrich)
    - [Verschlüsselung](#verschlüsselung)
        - [Vorwort](#vorwort)
        - [Teil 1 - Wie die Verschlüsselung beginnt](#teil-1---wie-die-verschlüsselung-beginnt)
        - [Teil 2 - Die teilnehmenden Locations](#teil-2---die-teilnehmenden-locations)
        - [Teil 3 - Freiwillige Kontaktnachverfolgung im privaten Umfeld](#teil-3---freiwillige-kontaktnachverfolgung-im-privaten-umfeld)
        - [Teil 4 - Dezentraler Transport von Keys](#teil-4---dezentraler-transport-von-keys)
        - [Teil 5 - Covid-19-Tests](#teil-5---covid-19-tests)
        - [Teil 6 - Entschlüsselung im Gesundheitsamt](#teil-6---entschlüsselung-im-gesundheitsamt)
            - [Falls das Gesundheitsamt sichere Übertragung unterstützt](#falls-das-gesundheitsamt-sichere-übertragung-unterstützt)
            - [Falls das Gesundheitsamt ***keine*** sichere Übertragung unterstützt](#falls-das-gesundheitsamt-keine-sichere-übertragung-unterstützt)
            - [In beiden Fällen](#in-beiden-fällen)
        - [Teil 7 - Federation zwischen Gesundheitsämtern](#teil-7---federation-zwischen-gesundheitsämtern)
        - [Teil 8 - Fallback für User\*innen ohne Smartphone](#teil-8---fallback-für-userinnen-ohne-smartphone)
        - [Und jetzt? Ist das sicher? Gibt es eine Schwachstelle?](#und-jetzt-ist-das-sicher-gibt-es-eine-schwachstelle)
    - [Metadaten](#metadaten)
    - [Weitere Sicherheitsmechanismen](#weitere-sicherheitsmechanismen)
    - [Weiternutzung](#weiternutzung)

## Präambel

Der Schutz von den sensibelsten Daten, nämlich den Gesundheits- und Bewegungsdaten, ist essenziell, das haben wir aus
den Problemen von vergangenen digitalen Ansätzen der Pandemie-Bekämpfung gelernt. Um so mehr Energie haben wir in das
Konzept zur Verschlüsselung der User\*innendaten bei free2**pass** hineingesteckt.

In diesem Dokument möchten wir unser Verschlüsselungskonzept so einfach und zugänglich, wie möglich, darstellen.

## Bezug auf die 10 Prüfsteine für die Beurteilung von "Corona-Tracing"-Apps

Im Folgenden möchten wir auf die vom Chaos Computer Club
veröffentlichten [Prüfsteine zur Beurteilung von "Contact Tracing"-Apps](https://www.ccc.de/de/updates/2020/contact-tracing-requirements)
eingehen.

Ziel hiervon ist es, maximale Transparenz für unser Modell von free2**pass** zu schaffen. Wir möchten vor allem eines
sein: realistisch. Wir sind uns darüber im Klaren, dass wir *nicht* alle genannten Prüfsteine erfüllen - und haben uns
auch teils trotz prinzipieller technischer Umsetzbarkeit bewusst dagegen entschieden. Wir bitten daher alle technisch
interessierten menschen, sich selbst ein Urteil zu schaffen, inwiefern unser Konzept legitim ist.

### Risiko

Contact-Tracing halten wir für enorm riskant. Jedes Datum, dass in falsche Hände gerät, das nicht verschlüsselt ist, das
nicht fälschungssicher übermittelt wird, ist ein Risiko. Die jüngsten Ansätze von Contact-Tracing-Apps haben jedoch
gezeigt, dass diese Awareness leider unter den Entwickler\*innen nicht selbstverständlich ist.

### 10. & 9. Unbeobachtbarkeit der Kommunikation und Unverkettbarkeit

Um eine Metadatenanalyse zu erschweren, versuchen wir, so wenig, wie möglich, Informationen zuortbar zu machen. Leider
scheitert das gänzliche Anonymisieren an Mechanismen zur Fälschungsprüfung.

Alle sicherheitsrelevanten Anfragen an unsere Server, wie z. B. die Abfrage von Covid-19 Testergebnissen, sind mit einem
Hash der Daten der abfragenden User\*in signiert. Da wir die Rohdaten dieses Hashes (13-facher Blowfisch) selbst nicht
kennen, können wir zwar die Autorisierung der Anfragen prüfen, jedoch nicht ermitteln, welche Requests von welchen
Clients kommen.

An den gespeicherten Daten auf unseren Servern ist nicht erkennbar, welche User\*innenkennung hinter z. B. Check-Ins
steht. Die Daten werden in diesem Falle verschlüsselt gespeichert, ohne eine Zuordnung zum Clients vorzunehmen (was wir
technisch gar nicht könnten). Somit wäre nur erkennbar, dass in Zeitfenster X gleichzeitig Y Personen in einer
bestimmten Location waren. Es wäre unmöglich zu erkennen, welche User\*innen das waren, oder wo diese sich vorher /
nachher aufgehalten haben.

### 8. Kein Aufbau von zentralen Bewegungs- und Kontaktprofilen

Unser System erlaubt es nicht, GPS-Daten, Telefonnummern o. Ä. zueinander zuzuordnen. Positionsdaten werden nur lokal
verarbeitet, um zu überprüfen, ob die User\*in sich tatsächlich in der Nähe der Location befindet, wo sie sich
einchecken will.

Da wir von den einzelnen Requests nicht wissen, wer diese sendet, ist es uns technisch nicht möglich, Metadaten zu
analysieren. IP-Adressen werden von unseren Servern nicht gespeichert. Wir suchen momentan nach einer Lösung, diese
bereits vor dem Erreichen usnserer Server von einem Proxy o. Ä. anonymisieren zu lassen.

### 7. Anonymität

Anonymität möchten wir nur bis zu einem bestimmten Punkt gewährleisten.

Ziel unserer App ist es, den Gesundheitsämtern Daten zu geben, umd Kontaktketten nachzuverfolgen. Somit können wir nicht
einfach anonym Beacons sammeln - wir müssen User\*innendaten verarbeiten. Unsere Idee ist es, die Verarbeitung dieser
Daten so zu gestalten, dass es ohne einen Positivbefund technisch nicht möglich ist, die Daten zu entschlüsseln. Dies
wird erreicht, indem die Schüssel zu den Daten bis zu einem Covid-19-Testzentrum offline transportiert werden und dort
nur im Falle eines Positivbefundes weiterzuverarbeiten. Dass dies der Fall ist, lässt sich problemlos durch einen
Source-Code-Audit überprüfen.

Somit sind die User\*innendaten von der Eingabe bis zum Entschlüsseln beim Gesundheitsamt Ende-zu-Ende verschlüsselt und
lassen sich nur mit vier (eher drei; einer ist berechenbar / im Klartext gespeichert) unabhängigen Schlüsseln
entschlüsseln: Dem Location-AES-Cipher, dem Master-Private-PGP-Key der Teststation und dem Master-Private-PGP-Key des
Gesundheitsamtes, auch, wenn diese Keys und die verschlüsselten Daten zwischendurch immer weiter um Schichten ver- und
entschlüsselt werden.

### 6. Datensparsamkeit

Wir versuchen, fortlaufen so wenig daten, wie möglich zu speichern und die Daten, die wir speichern, so zu
verschlüsseln, dass sie nach dem Zero-Trust-Prinzip für keine der beteiligten Parteien zugänglich sind.

Es kann uns jedoch dennoch vorgeworfen werden, dass wir Kontaktnachverfolgungsdaten pauschal speichern. Das stimmt,
jedoch sind diese mehrfach verschlüsselt, so, dass u. A. der offline transportierte Location-AES-Cipher benötigt wird,
um diese zu entschlüsseln. Auch hier wären insgesamt drei private Schlüssel notwendig, um die Daten zu entschlüsseln.

Würden wir die Daten nicht speichern, wäre es technisch nicht möglich, persönliche Kontaktnachverfolgung für das
Gesundheitsamt zu ermöglichen.

### 5. Unabhängigkeit von Vertrauen

Alle Daten, die bei uns verarbeitet werden, sind Ende-zu-Ende verschlüsselt. Daher ist es nicht notwendig, uns als
Betreiber\*innen des Dienstes zu vertrauen. Alle personenbezogenen Daten oder Private-Keys werden bereits vor Verlassen
des Endgerätes verschlüsselt, sodass uns nicht vertraut werden muss und auch im Falle eines Datenlecks keine
personenbezogenen Daten verfügbar wären. Die Verschlüsselung kann im Quellcode unserer Endanwendungen verifiziert
werden.

### 4. & 3. Transparenz und Prüfbarkeit

Der Quellcode unserer Endanwendungen ist frei im Sinne von Open Source und Contributions sind willkommen. Alle
User\*innen können diesen einsehen. Momentan sind unsere Builds nicht replizierbar, für Android ist dies jedoch bald
geplant. Bis dato können jedoch immernoch eigene Builds von dem Quellcode erstellt werden.

Die Serverinfrastruktur ist nicht open-source, da diese nicht sicherheitsrelevant ist und jegliche Verschlüsselung auf
den Endgeräten geschieht.

Die einfachste Transparenz, die wir bieten, ist jedoch dieses Dokument. Hier erläutern wir kritisch unser Konzept,
zeigen potentielle Schwachstellen transparent auf.

Falls ihr den Quellcode selbst nicht versteht, könnt ihr also sicher sein, dass andere Menschen unseren Quellcode
gelesen und überprüft haben und im Zweifel dieses Dokument angepasst hätten.

### 2. & 1. Freiwilligkeit, Diskriminierungsfreiheit und Zweckgebundenheit

Unser System lässt sich auch 100 % offline verwenden. Es wird niemensch verpflichtet, unsere App zu verwenden oder uns
zu vertrauen. Es ist ebenso möglich, Covid-19-Tests als Ausdruck zu erhalten und mit diesen in Locations einzuchecken.
Es ist ebenfalls überprüfbar, dass wir außer den zur Kontaktverfolgung notwendigen Daten nicht erfassen. Wir sammeln
keine Metadaten, keine Muster, gar nichts.

### Schlussstrich

Wir versuchen, an jeder Stelle die Prüfsteine des CCC zu respektieren und umzusetzen. Wenn wir das nicht getan haben,
haben wir euch erklärt, weshalb wir dies für besser halten.

**Falls euch Fehler in diesem Dokument auffallen, öffnet gerne einen Issue - und wir korrigieren diesen. Wir sind auch
nur Menschen.**

## Verschlüsselung

Wie in den Antworten zu den Prüfsteinen bereits häufig genannt, spielt Verschlüsselung bei free2**pass** eine enorme
Rolle. Im Folgenden möchten wir ausführlich erläutern, wie wir unsere User\*innendaten verschlüsseln.

### Vorwort

In einer früheren Version dieses Konzeptes ist die Verschlüsselung sehr viel effektiver und vielschichtiger gewesen. Für
jede art von Entschlüsselung mussten mehrere Parteien involviert sein. Die aufmerksamen Leser\*innen mögen sich
wahrscheinlich wundern, warum z. B. die gesamte Kryptographie um die Gesundheitsämter optional geworden ist. Dies hat
leider einen so einfachen, wie beschämenden Grund: Die deutschen Gesundheitsämter sind nicht bereit, sichere Software
für die Kontaktnachverfolgung zu verwenden, sondern bestehen auf einfache Dateien, die sie zugesandt bekommen. Trotz
dieser schwierigen Anforderung, dabei noch Datensicherheit zu garantieren, haben wir uns ein Konzept erdacht, bei
welchem auch ohne Beteiligung der Gesundheitsämter die Daten der Nutzer\*innen ende-zu-ende-verschlüsselt übermittelt
werden können.

Das umgearbeitete Konzept fungiert als Drop-In-Alternative, welche nicht verhindert, dass Gesundheitsämter, die sich un
Zukunft digitalisieren möchten, sichere Übermittlungswege verwenden.

### Teil 1 - Wie die Verschlüsselung beginnt

Zu Beginn muss die Verschlüsselung für das Gesundheitsamt eingerichtet werden. Diese besteht aus:

- einem sog. Authn-PGP-Key-Pair, das lokal in dem Frontend des Gesundheitsamtes generiert wird
    - der Authn-Private-PGP-Key wir mit einer nutzer\*indefinierten Passphrase AES verschlüsselt
    - der verschlüsselte Authn-Private-PGP-Key und der klartext Authn-Public-PGP-Key werden auf dem Server gespeichert,
      um Zugang von verschiedenen Geräten des Amtes zu ermöglichen
- einem sog. Master-Private-PGP-Key, der auch lokal generiert wird
    - dieser wird zuerst mit dem Authn-Public-PGP-Key verschlüsselt
    - der verschlüsselte Master-Private-PGP-Key wird an unseren Server gesandt
    - dort angekommen wird dieser mit einer zufälligen, sog. Master-Server-AES-Passphrase von 256-Bit länge
      verschlüsselt
    - der Master-Private-PGP-Key des Gesundheitsamtes ist somit weder für die Serverbetreiber\*innen, noch für das
      Gesundheitsamt verfügbar
- der Master-Public-PGP-Key wird im Klartext auf unseren Servern gespeichert

---

- die genannte Master-Server-AES-Passphrase wird am ersten Tag mit einer zufälligen sog. Daily-Server-AES-Passphrase
  verschlüsselt. Diese wird im *Klartext* bei uns auf dem Server gespeichert. Diese ist für die Serverbetreiber\*innen
  nicht sicherheitsrelevant, sondern dient nur als Access-Token, dem Gesundheitsamt Zugriff auf die verschlüsselten
  Daten zu geben. Wir könnten natürlich auch einfach normale Zugangsdaten verwenden, wie sie überall auf der Welt
  verwendet werden, jedoch halten wir es für sicherer, wenn wir uns technisch nicht auf Authentifizierungsmechanismen
  auf Nutzer\*innen-Logindaten-Basis verlassen müssen, weshalb wir die Architektur von Server-AES-Ciphern eingeführt
  haben. Somit ist es kein Sicherheitsproblem, diese im Klartext zu speichern.
- der weitere Nutzen dieser Passphrase wird in [Teil 6](#Teil-5---Entschlüsselung-im-Gesundheitsamt) weiter beschrieben.

---

- jeden Tag wird für jedes Gesundheitsamt ein Daily-PGP-Keypair generiert
- der Daily-Private-PGP-Key wird zuerst mit dem Authn-Public-Key verschlüsselt,
- danach wird dieser mit dem Master-PGP-Public-Key verschlüsselt
- somit ist der Daily-key erstmal nicht mehr erreichbar.

Diese Keys müssen leider auf dem Server generiert werden, was potentiell unsicher ist, da die Gesundheitsämter uns
kommuniziert haben, dass wir nicht davon ausgehen können, dass sie täglich die Infektionsdaten abrufen - und wir
dementsprechend nicht täglich client-side neue Keys generieren können

Für den Fall, dass die Serverbetreiber\*innen hier un vertrauenswürdig sind, wäre jedoch eine Entschlüsselung von
Kontaktnachverfolgungsdaten immer noch nicht möglich. Hierzu wäre der Master-Private-Key des Gesundheitsamtes notwendig,
welcher niemals im Klartext auf dem Server verfügbar ist.

---

- somit haben wir jetzt eine Architektur, mit der niemensch alleine an irgendeinen Schlüssel gelangen kann.

---

Falls das Gesundheitsamt der beteiligten Kommune keine extra-sicher verschlüsselte Übertragung akzeptiert, wird dieser
Schritt übersprungen.

### Teil 2 - Die teilnehmenden Locations

Die Schlüssel von Locations werden ausschließlich offline transportiert. Dies verhindert, dass durch ein Datenleck die
Schlüssel zu personenbezogenen Daten erlangt werden können.

---

- Bei der Registrierung einer Location wird ein zufälliger 256-Bit-AES-Key im Endgerät generiert, den wir
  Location-AES-Cipher nennen.
- Darauffolgend wird ein Master-Location-PGP-Keypair generiert.
- Der Master-Location-Private-PGP-Key wird zuerst mit dem Location-AES-Cipher verschlüsselt.
- Nun unterscheiden sich die Schritte je nachdem, ob das Gesundheitsamt sichere Übertragung akzeptiert:
    - Falls ja:
        - Daraufhin wird der verschlüsselte Master-Location-Private-PGP-Key mit dem Master-Public-PGP-Key des
          Gesundheitsamtes verschlüsselt
        - Der zweifach verschlüsselte Master-Location-Private-PGP-Key wird auf dem Server gespeichert.
    - Falls nicht, wird der einfach verschlüsselte Key auf dem Server gespeichert
- Der Master-Location-Public-PGP-Key wird im Klartext auf dem Server gespeichert.

---

- Der Location-AES-Cipher wird zusammen mit der Location-ID in einen QR-Code geschrieben. Den QR-Code nennen wir
  Location-Crypto-QR.
- Nun gibt es geht zweierlei:
    - Die Location druckt direkt die Plakate aus
    - Die Location erhält zusätzlich einen Link mit den Plakaten
        - Der Location-AES-Cipher wird mit dem user\*innengewählten Passwort, welches zur Anmeldung erforderlich ist,
          verschlüsselt
        - Der verschlüsselte Cipher wird mit einer zufälligen AES-Passphrase verschlüsselt. Diese nennen wir
          Widerherstellungscode.
        - Der Wiederherstellungscode in eine Email geschrieben und der Location zugesandt.
        - Kann nun mit eigenem Passwort und Widerherstellungscode der Location-AES-Cipher entschlüsselt werden.
        - Die Plakate werden direkt als PDF angezeigt

---

- Jeden Tag wird ein neues Daily-Location-RSA-Keypair generiert
    - Der Daily-Location-Private-RSA-Key wird mit dem Master-Location-Public-PGP-Key verschlüsselt
    - Danach wird der mit bereits verschlüsselte Daily-Location-Private-RSA-Key mit dem Daily-Public-PGP-Key des
      Gesundheitsamtes verschlüsselt
    - Dieser zweifach verschlüsselte Key wird bei uns auf dem Server gespeichert.
- Der Daily-Location-Public-RSA-Key wird im Klartext auf dem Server gespeichert.

---

Nun haben wir eine Infrastruktur geschaffen, bei der eine Überträge\*in den Location-AES-Cipher *offline* dem
Gesundheitsamt zukommen lassen müsste, damit der Master-Location-Private-PGP-Key entschlüsselt werden kann - damit die
tagesaktuellen Daily-Location-RSA-Keys entschlüsselt werden können. Zusätzlich braucht das Gesundheitsamt, wie
unter [Teil 1](#Teil-1---Wie-die-Verschl%C3%BCsselung-beginnt) bereits erklärt, die Daily-Server-AES-Passphrase, um
überhaupt an die eigenen Schlüssel zu gelangen.

### Teil 3 - Freiwillige Kontaktnachverfolgung im privaten Umfeld

Neben registrierten Locations ist auch ein Check-In-Verfahren im privaten Umfeld vorgesehen. Hierdurch wird auch bei
Treffen mit Familie und Bekannten qualitative Kontaktnachverfolgung ermöglicht. Im Verglich zu teilnehmenden Locations
ist das Verfahren hier etwas einfacher, da ein Check-In-Code nur für eine Occasion gilt – und nicht automatisiert
tagesaktuelle Public- und Private-Keypairs generiert werden müssen.

Eine User\*in möchte eine Occasion mit Check-In veranstalten.

- Das Device generiert einen zufälligen Occasion-AES-Cipher. Dieser hat dieselbe Funktion, wie ein Location-AES-Cipher.
- Das Device generiert ein Occasion-RSA-Keypair, dessen Occasion-Private-RSA-Key mit dem Occasion-AES-Cipher
  verschlüsselt wird.
- Das Device registriert am Server eine Occasion und erhält:
    - eine Occasion-ID, mit welcher die Occasion identifiziert werden kann
    - den Daily-Public-PGP-Key des Gesundheitsamtes, falls das Amt sichere Übertragung unterstützt:
        - Der AES-verschlüsselte Occasion-Private-RSA-Key wird mit dem Daily-Public-PGP-Key des Gesundheitsamtes
          verschlüsselt und an den Server gesandt
    - andernfalls wird der Occasion-Private-RSA-Key einfach verschlüsselt an den Server gesandt
- Das Device generiert einen Crypto-QR-Code bestehend aus der Occasion-ID, sowie dem Occasion-Public-PGP-Key und dem
  Occasion-AES-Cipher
    - Der Occasion-Crypto-QR kann entweder auf dem Smartphone angezeigt werden oder als PDF exportiert bzw. gedruckt
      werden
- Das Device checkt seine Besitzer\*in direkt in die Occasion ein, um menschlichem Vergessen vorzubeugen. Dies kann
  deaktiviert werden.

---

Bei einem Occasion-Check-In wird nun der erstellte QR-Code gescanned. Ähnlich, wie
in [Teil 4](#teil-4---dezentraler-transport-von-key) für Locations beschrieben, werden auch hier die personenbezogenen
Daten verschlüsselt. Der Unterschied ist lediglich, dass der Public-Key nicht vom Server abgefragt werden muss.

### Teil 4 - Dezentraler Transport von Keys

Wie in [Teil 2](#Teil-2---Die-teilnehmenden-Locations) beschrieben, wird ein ausschließlich offline transportierter
Location-AES-Cipher benötigt, um an die Private-Keys (sowohl Master-PGP als auch Daily-RSA) der Locations zu kommen.
Diese werden offline von den User\*innen auf den Geräten transportiert.

- Beim Check-In in eine Location wird der Location-Crypto-QR gelesen.
    - In diesem sind die ID der Location, sowie der Location-AES-Cipher enthalten. Diese beiden Daten werden lokal auf
      dem Gerät in einer 256-Bin-AES-verschlüsselten Hive-Datenbank gespeichert.
    - Die Location-ID wird nun an den Server gesandt un der Daily-Location-Public-RSA-Key zurückgegeben.
    - Die personenbezogenen Daten der User\*in werden mit Zufallsdaten angereichert, um Mustererkennung vorzubeugen
    - diese Daten werden nun mit dem Daily-Location-Public-RSA-Key verschlüsselt und auf dem Server gespeichert. Diese
      werden ohne Angabe einer User\*innen-ID o. Ä. gespeichert.
    - die Location-ID wird hierbei mit dem Location-AES-Cipher verschlüsselt, um Metadaten auf dem Server zu vermeiden.
      Dies verhindert, dass der Server weiß, welche tatsächliche Location hinter einem Check-In steht.
        - Das Einzige, was der Server also weiß, ist, dass eine (noch) unbekannte Person zu einer bestimmten Zeit in
          einer Location war.

---

Dieses Offline-Gerüst erlaubt es uns, später, dei einem Covid-19-Positivbefund zusammen mit dem Gesundheitsamt die
Location-Daten zu entschlüsseln. Ansonsten bleibt dies unmöglich, da die Location-AES-Ciphers ausschließlich offline auf
den Crypto-QRs und den Geräten der User\*innen gespeichert werden.

### Teil 5 - Covid-19-Tests

Bei einem Covid-19-Test müssen die personenbezogenen Daten der Getesteten verschlüsselt an die Teststation übertragen
werden, damit diese im Falle eines Positiv-Befundes mitsamt den vergangenen Locations und deren Location-AES-Ciphern
verschlüsselt an das Gesundheitsamt übertragen werden. Dies ist einer der kompliziertesten Vorgänge, da alle genannten
Daten *nur* im Falle eines Positivbefundes entschlüsselt werden können dürfen.

---

- Die Station denkt sich einen 256-Bit langen Station-AES-Cipher aus
- Das Master-Station-PGP-Keypair wird generiert
- Der Master-Station-PGP-Key wird mit dem Station-AES-Cipher verschlüsselt und auf dem Server gespeichert.
- Der Station-AES-Cipher wird in der Teststation behalten
- Der Master-Station-Public-PGP-Key wird auf dem Server gespeichert.

---

- Für jeden Covid-19-Test werden zwei neue RSA-Keypairs generiert:
    - das Certificate-RSA-Keypair
        - Der Certificate-Private-RSA-Key wird mit dem Master-Station-PGP-Key verschlüsselt und auf dem Server
          gespeichert.
        - Der Certificate-Public-RSA-Key wird im Klartext auf dem Server gespeichert
    - das Session-RSA-Keypair
        - Der Session-Private-RSA-Key wird mit einer zufälligen Passphrase verschlüsselt
        - Der verschlüsselte Session-Private-RSA-Key wird auf dem Server gespeichert
        - Der Session-Public-RSA-Key wird zusammen mit der serverinternen UUID des auszustellenden Covid-19-Certificates
          in einen Crypto-QR geschrieben
        - die zufällige Passphrase wird mit dem Master-Public-PGP-Key der Teststation verschlüsselt und auf dem Server
          gespeichert. Dies erlaubt auch anderen Clients des Testzentrums, später an die Daten zu gelangen.
- Beim Starten des Covid-19-Tests scannt die Nutzer\*in den Crypto-QR und erhält hierdurch:
    - Die Test UUID
    - Den Session-Public-RSA-Key
- Mit der Test UUID wird der Certificate-Public-RSA-Key vom Server angefordert und mit diesem werden die
  personenbezogenen Daten der User\*in werden mit einem zufälligen String angereichert, um später Mustererkennung in den
  verschlüsselten Daten zu erschweren
- die Daten mit Random-Entropy werden verschlüsselt
- Die verschlüsselten User\*innendaten werden auf dem Server gespeichert
- Nun werden die Kontaktverfolgungsdaten verschlüsselt
    - Die in der Vergangenheit auf dem Gerät gesammelten Location-IDs und Location-AES-Ciphers werden lokal
      entschlüsselt. Diese Daten nennen wir Location-Data.
    - Diese Daten werden nun mit einer zufälligen 256-Bit-AES-Passphrase, die wir Tracing-AES-Passphrase nennen,
      verschlüsselt.
    - Die verschlüsselten Daten werden an den Server gesandt.
    - Die Tracing-AES-Passphrase wird mit dem Session-Public-RSA-Key verschlüsselt und an den Server gesandt

---

Nun wird der Covid-19-Test durchgeführt.

- Im Falle eines Negativbefundes löscht der Server:
    - Den verschlüsselten Session-Private-RSA-Key
    - Den Certificate-Private- und Public-RSA-Key
    - Die verschlüsselte Tracing-AES-Passphrase
    - Das verschlüsselte Location-Data
- Im falle eines Positivbefundes:

---

- Ab hier beginnt der Entschlüsselungsprozess
    - Der Master-Private-PGP-Key der Teststation wird verwendet, um die Passphrase des Session-Private-RSA-Keys zu
      entschlüsseln.
    - Der Session-Private-RSA-Key wird nun verwendet, um die mit dem Session-Public-RSA-Key verschlüsselte
      Tracing-AES-Passphrase entschlüsselt werden kann.
    - Die Tracing-AES-Passphrase wird verwendet, um das Location-Data zu entschlüsseln
    - falls das Gesundheitsamt sichere Übertragung unterstützt:
        - Die personenbezogenen Daten, die die Teststation vorliegen hat, sowie das Location-Data werden mit dem
          Daily-Public-PGP-Key des Gesundheitsamtes verschlüsselt und an den Server gesandt
    - falls nicht:
        - Die personenbezogenen Daten, die die Teststation vorliegen hat, sowie das Location-Data werden mit einem
          zufälligen Transfer-AES-Cipher für das Gesundheitsamt verschlüsselt und an den Server gesandt
        - der Transfer-AES-Cipher wird für den Tagesabschlussbericht lokal aufbewahrt

---

Leider müssen wir personenbezogene Daten pauschal übertragen, auch, wenn kein Positivbefund vorliegt. Es wäre
prinzipiell erdenklich, die personenbezogenen Daten der User\*innen erst bei einem Positivbefund vom Device an den
Server zu senden. Dies halten wir jedoch für zu Fehleranfällig. So könnten die Daten von den User\*innen einfach
unterdrückt werden, die App könnte deinstalliert werden oder das Device offline sein.

Auch, wenn die Daten bereits im Voraus übertragen sind, sind diese erstmal so verschlüsselt, dass diese nur in der
Teststation entschlüsselt werden können. Dies wäre bei analogen Covid-19-Tests auch der Fall: Dort muss ebenfalls der
Personalausweis vorgezeigt werden. Daher halten wir diese Lösung für datensparsamer.

### Teil 6 - Entschlüsselung im Gesundheitsamt

#### Falls das Gesundheitsamt sichere Übertragung unterstützt

An dieser Stelle möchten wir zurück zu den Daily-Server-AES-Passphrases kommen. Mit diesen, sowie dem
Authn-Private-PGP-Key des Gesundheitsamtes können Positivbefunde nachverfolgt werden.

- Das Gesundheitsamt bekommt einmal täglich einen Link zugesandt, mit welchem die aktuellen Infektionsdaten erfasst
  werden können
- Dieser Link enthält die Daily-Server-AES-Passphrase, jedoch nicht in dem Link als Parameter, sondern als
  Hypertext-Target (`https://link.com/location/#AES_PASSPHRASE`)
- Nun beginnt die Authentifizierung für den Hauptentschlüsselungszyklus:
    - das Gesundheitsamt fragt im Browser nach einem FIDO2-Stick
    - Die Signatur des FIDO2-Sticks wird auf dem Server überprüft
    - es wird nach der Passphrase des Authn-Private-PGP-Keys gefragt
    - es wird ein zufälliger Referenztext angefordert, der auf Serverseite mit dem Authn-Public-PGP-Key verschlüsselt
      wird
    - es wird versucht, diesen mit dem Authn-Private-PGP-Key zu entschlüsseln und das Resultat wird zurück an den Server
      geschickt, wo es mit dem zwei Minuten lang gecacheden Klartext verglichen wird
    - der zweifach verschlüsselte Master-Private-Key des Gesundheitsamtes wird bei Erfolg zurückgegeben
    - mit dem Authn-Private-PGP-Key kann eine Verschlüsselungsstufe hiervon entfernt werden
    - das Gesundheitsamt fragt den aktuellen Daily-Private-PGP-Key ab
        - hierfür muss die gegebene Daily-Server-AES-Passphrase, sowie der um einen Layer entschlüsselte
          Master-Private-PGP-Key an den Server gesandt werden
        - mit der Daily-Server-AES-Passphrase kann die Master-Server-AES-Passphrase entschlüsselt werden
        - mit dieser kann der Master-Private-PGPKey entschlüsselt werden
        - mit diesem kann ein Layer des Daily-Private-PGP-Keys des Amtes entschlüsselt werden.
        - der nunmehr nur noch einmal verschlüsselte Daily-Private-Key wird zurückgegeben.
    - dieser kann mit dem Authn-Private-Key entschlüsselt werden und liegt nun im Klartext auf dem Endgerät vor

---

Nun ist das Amt authentifiziert und kann mit dem Daily-Private-PGP-Key die tagesaktuellen Daten abrufen

---

- das Gesundheitsamt fragt die Liste der Positivbefunde ab.
- dabei bekommt das Gesundheitsamt:
    - die mit dem Daily-Public-PGP-Key verschlüsselten Location-Data-Sets
- diese werden mit dem Daily-Private-PGP-Key entschlüsselt

---

Das Amt hat nun die Locations und kann die Daten dazu abrufen.  
Im Folgenden wird der Einfachheit halber einfach nur von den Daily-Location-RSA-Keypairs gesprochen. Tatsächlich sind
hermit jeweils *die* Daily-Location-RSA-Keypairs gemeint, die im Frontend des Gesundheitsamtes ausgewählt werden, um die
Daten eines entsprechenden Tages abzurufen. Dies betrifft den Fall, dass einige Tage im Nachhinein erst ein
Positivbefund festgestellt wird und dann immer noch die Kontaktnachverfolgungsdaten erreicht werden müssen.

---

- für die nun bekannten Locations zur Kontaktverfolgung wird jetzt:
    - der zweifach verschlüsselte Master-Location-PGP-Key angefordert
    - dieser wird mit dem aus dem Location-Data bekannten Location-AES-Cipher einen Layer entschlüsselt
- alle nunmehr einfach verschlüsselten Master-Location-PGP-Keys werden zusammen mit dem nunmehr einfach verschlüsselte
  Master-Private-Key des Gesundheitsamtes und der Daily-Server-AES-Passphrase zurück an den Server gesandt
- auf dem Server wird nun eine weitere Verschlüsselungsstufe aufgelöst
    - mit der Daily-Server-AES-Passphrase wird die Master-Server-AES-Passphrase entschlüsselt
    - mit dieser kann nun der mitgesandte Master-Private-PGP-Key des Amtes zum Klartext entschlüsselt werden
    - mit diesem wird nun dreierlei entschlüsselt:
        - die personenbezogenen Daten der direkt positiv getesteten User\*innen
        - die bereits um eine Stufe entschlüsselten Master-Location-Private-RSA-Keys
        - mit diesen werden die Daily-Location-Private-RSA-Keys nun um die erste Stufe entschlüsselt, sodass diese nur
          noch mit dem Daily-Private-PGP-Key des Amtes verschlüsselt sind
        - der Daily-Private-PGP-Key des Amtes, sodass dieser nur noch mit dem Authn-Private-PGP-Key verschlüsselt ist
    - Die direkt positiv getesteten User\*innendaten, die verschlüsselten Daily-Location-Private-RSA-Keys und der
      Daily-Private-PGP-Key des Amtes werden an das Amt zurückgesandt
    - die dazugehörigen verschlüsselten Kontaktnachverfolgungsdaten der Location werden mit dem Location-AES-Cipher und
      der ID abgerufen, da die Location-ID, unter welcher die Kontaktnachverfolgungsdaten gespeichert wird, zur
      Metadatenvermeidung selbst verschlüsselt ist.
    - danach beginnt auf dem Server der Prozess, um die Daily-Server-AES-Passphrase zu tauschen
        - die momentane Daily-Server-AES-Passphrase befindet sich ja gerade im Memory. Wir nennen diese in den folgenden
          Zeilen "alte Passphrase"
        - es wird eine neue 256-Bit AES-Passphrase generiert. Wir nennen diese "neue Passphrase"
        - Mit der alten Passphrase ist momentan die Master-Server-AES-Passphrase verschlüsselt
        - Die Master-Server-AES-Passphrase wird mit der alten Passphrase entschlüsselt, danach mit der neuen Passphrase
          verschlüsselt und *zusätzlich* gespeichert.
            - *Ansonsten kann das Amt später am Tag nicht nochmal die Daten aufrufen*
        - die neue Passphrase wird als neue Daily-Server-AES-Passphrase gespeichert
        - die alte Passphrase wird so lange gespeichert, bis die erste Abfrage (vermutlich am Folgetag) mit der neuen
          Passphrase eingeht
            - die alte Passphrase wird nun gelöscht und der Zyklus beginnt von vorne

---

#### Falls das Gesundheitsamt ***keine*** sichere Übertragung unterstützt

Die Teststationen können alle positiven Befunde eines tages (oder auch mehrmals am Tag) auf einen Klick per Email an das
gesundheitsamt senden.

Die Email enthält einen Link, der die gesamten Informationen, die zur Entschlüsselung benötigt werden, als
Hypertext-Target enthält. Hiermit können die im nächsten Abschnitt beschriebenen Daten übermittelt werden, ohne, dass
diese an unsere Server gesandt werden.

- für jede in den Location-Data enthaltene ID:
    - das Amt ruft den Master-Location-PGP-Key, und die benötigten Daily-Location-Private-RSA-Keys
    - die dazugehörigen verschlüsselten Kontaktnachverfolgungsdaten der Location werden mit dem Location-AES-Cipher und
      der ID abgerufen, da die Location-ID, unter welcher die Kontaktnachverfolgungsdaten gespeichert wird, zur
      Metadatenvermeidung selbst verschlüsselt ist.
    - mit dem im Location-Data enthaltenen Location-AES-Ciphers wird der Location-Master-PGP-Key entschlüsselt
    - mit diesem werden die Daily-Location-Private-RSA-Keys entschlüsselt werden

#### In beiden Fällen

Jetzt beginnt die Entschlüsselung der Klardaten im Gesundheitsamt

---

- das Amt hat nun
    - die direkt positiv getesteten User\*innendaten
    - die verschlüsselten Daily-Location-Private-RSA-Keys
    - die mit dem Daily-Location-Public-RSA-Keys verschlüsselten Kontaktnachverfolgungsdaten aus den Locations
    - und - falls unterstützt - von vorher den Daily-Private-PGP-Key des Amtes
    - Mit den Daily-Location-Private-RSA-Keys können alle User\*innendaten abgerufen werden, die zur Kontaktverfolgung
      notwendig sein

---

Here you are. Die Kontaktverfolgung kann beginnen. Glaubt ihr uns jetzt, dass wir anständig Ende-zu-Ende verschlüsseln?

---

### Teil 7 - Federation zwischen Gesundheitsämtern

In Deutschland gibt es mehr, als nur ein Gesundheitsamt. Daher sehen wir auch einen Datenaustausch zwischen
Gesundheitsämtern verschiedener Ortschaften vor.  
Jeder Zuständigkeitsbereich eines Gesundheitsamtes (also jede Kommune) bekommt eine komplett dezidierte Datenbank auf
unseren Servern.  
Die Location-IDs und die Testzentren-IDs enthalten einen Teil, der sie einem Gesundheitsamt zuordnet.

---

Das Gesundheitsamt **Eins**enhausen bekommt die Liste von Infektionsdaten, wie
in [Teil 6](#Teil-5---Entschl%C3%BCsselung-im-Gesundheitsamt) beschrieben, und kann die Location-IDs, die zur
Kontaktnachverfolgung notwendig sind, entschlüsseln.

Wenn hier nun eine Location-ID enthalten ist, zur Stadt **Zwei**enberg gehört, wird im Frontend des Gesundheitsamtes
eine Umverschlüsselung vorgenommen:

- (Das Gesundheitsamt aus) Einsenhausen hat eine Location-ID und den dazugehörigen Location-AES-Cipher (also ein
  Location-Data-Set) aus Zweienberg
- Einsenhausen erfragt den Daily-Public-PGP-Key aus Zweienberg beim Server
- Einsenhausen verschlüsselt das Location-Data-Set mit dem Zweienberger Daily-Public-PGP-Key
- das verschlüsselte Location-Data-Set wird an den Server gesandt und in der Datenbank für Zweienberg abgespeichert
- der Abruf der zweienberger Daten kann nun stattfinden, als wären die Daten direkt in Zweienberg gesammelt worden.

### Teil 8 - Fallback für User\*innen ohne Smartphone

Im Testzentrum:

---

Nutzer\*innen ohne Smartphone können sich ebenfalls in den Teststationen testen lassen.

- die Teststation beginnt einen neuen Test und hat somit den Certificate-Public-RSA-Key
- falls vorhanden wird nach dem letzten Testpass gefragt
- ansonsten werden die Kontakt- und Personendaten erfragt
- die Station erstellt einen Random-Entropy-String
- Station erstellt einen 11-fachen B-Crypt des User-Datas
- der Hash wird an den Server gesandt und von diesem signiert
- das User-Data wird um die Signatur und den Random-Entropy-String ergänzt
- die Daten werden JSON-encodiert und als Hypertext-Target an einen Link angefügt
- unter der URL können alle Menschen, die den QR scannen, kryptografisch prüfen, ob die Signatur korrekt ist - und das
  Zertifikat entsprechend unverändert und gültig ist -, ohne, dass jemals sensible Daten auf dem Server landen

---

Fall eines Positivbefundes:

---

- Die User\*innendaten werden mit dem Daily-Public-PGP-Key des Amtes verschlüsselt
- Die Teststation nimmt - falls vorhanden - den letzten Testpass der User\*in
- falls dieser nicht vorhanden ist, wird nach allen besuchten Orten gefragt
- die nun bekannten Locations werden entweder telefonisch oder vom Server via Push kontaktiert
    - Die Locations haben entweder den Location-AES-Cipher schon auf ihrem Gerät
    - die Locations werden aufgefordert, den eigenen Crypto-QR zu scannen
        - die Locations müssen den QR-Code bei sich scannen.
        - Dadurch hat die Location das eigene Location-Data-Set.
- Dieses wird mit dem Daily-Public-PGP-Key des Amtes verschlüsselt
- Demnach ist Kontaktverfolgung wie gehabt möglich.

---

In Locations:

---

Beim Check-In in Locations muss die Offline-User\*in das Testdokument zusammen mit dem Personalausweis vorzeigen.

- Die Türsteher\*in überprüft die Identität
- und scannt den QR-Code auf dem Testpass
    - Falls es der erste Offline-Scan ist, kommt eine verständliche Aufforderung an die Location, dass der eigene
      Check-In-Crypto-QR gescanned werden muss
- Das Gerät der Türsteher\*in erfragt den Daily-Location-Public-RSA-Key
- Die User\*innendaten werden mit diesem verschlüsselt
- Die verschlüsselten Daten werden an den Server gesandt
- Die Türsteher\*in trägt die ID der Location in der dafür vorgesehenen Liste auf der Rückseite ein

### Und jetzt? Ist das sicher? Gibt es eine Schwachstelle?

Das kommt darauf an, wie mensch *Schwachstelle* definiert:

Die schwächste Stelle in unserer Verschlüsselung ist wohl die Tatsache, dass wir eine tagesaktuelle AES-Passphrase im
Klartext speichern, mit der

- Identifikationsnummern von Locations, bei denen Covid-19-Positiv getestete Menschen waren und
- eine Verschlüsselungsstufe des Master-Private-PGP-Keys des Gesundheitsamtes

verschlüsselt sind. Wir halten das dennoch nicht für allzu kritisch, da hiermit nur bereits mehrfach verschlüsselte
Daten um eine Stufe entschlüsselt werden können. Wir hätten die Server-AES-Passphrasen natürlich auch gänzlich weglassen
können, jedoch hielten wir jegliche Form von Verschlüsselung für besser, als reine
Zugangs-Authentifizierungsmechanismen.

*Egal, welche Daten und an welcher Stelle; um irgendwelche unverschlüsselten, zuordbaren Information zu erlangen, ist
paralleler Zugriff auf drei Stellen im Ökosystem notwendig. Und selbst dann benutzen wir für alle Prozesse entweder
Einmal-Schlüssel oder täglich wechselnde Schlüssel. Somit können maximal Daten von einem Tag geklaut werden.*

---

Eine weitere Schwachstelle kann sein, dass sich User\*innen die App selbst kompilieren und die Überprüfung der Location
herausnehmen. Dies ist jedoch nur bedingt risikoreich, da User\*innen auch unter Android z. B. eine sog.
Mock-Location-App verwenden könnten, die diese Mechanismen sowieso umgehen könnte. Um dennoch Missbrauch vorzubeugen,
werden die Check-Ins pro IP-Adresse limitiert.

Außerdem sind wir uns der Tatsache bewusst, dass auch bei einer noch so abgesicherten API erfahrene Nutzer\*innen die
Möglichkeit hätten, aus den Binaries die entsprechende Architektur zum Ansprechen der API zu extrahieren. Daher haben
wir uns an dieser Stelle für ein offenes Modell entschieden.

## Metadaten

Unsere Anwendungsarchitektur vermeidet Konstrukte, wie Nutzer\*innenaccounts, da hierdurch ungewollt viele Metadaten
anfallen würden. Obwohl wir uns Mühe geben, alle Informationen mit möglichst wenig Metadaten zu übertragen, sind einige
Daten an gewissen Stellen nicht vermeidbar.

Metadaten, die anfallen sind z. B.

- die Anzahl an Check-Ins in unbekannten Locations zu bestimmten Uhrzeiten
    - die Locations, die zu einem Check-In gehört, ist uns *nicht* bekannt, da die IDs der Locations beim Check-In
      verschlüsselt sind.
- welche User\*innen-Hashs nach welchem Muster Covid-19-Tests tätigen. Dies fällt an, weil wir mit Bulk-Anfragen den
  Status mehrerer Tests auf einmal verarbeiten.
- die Anzahl an privaten Occasions

## Weitere Sicherheitsmechanismen

Neben reiner Verschlüsselung ist Fälschungssicherheit und Missbrauchsschutz ebenfalls wichtig. Dies beinhaltet sichere
Client-Server-Kommunikation, sowie den Missbrauchsschutz von falschen Check-Ins in Locations.

---

Einer unserer Mechanismen besteht darin, die lokalen Nutzer\*innendaten zu verschlüsseln, sodass selbst auf einem
gerooteten / gejailbraikeden Gerät die Daten wie Name oder Adresse nicht im Nachhinein gefälscht werden können.
Insbesondere trifft das auch das Selfie zu, das quasi zur Personenprüfung verwendet wird. Dieses wird aus
Effizienzgründen nicht in der verschlüsselten Datenbank gespeichert, sondern im normalen Dateisystem. Um dennoch
fälschungssicher das Selfie abgleichen zu können, wird eine Prüfsumme von diesem in der (verschlüsselten) Datenbank
gespeichert und beim Öffnen der App mit der Datei aus dem Dateisystem verglichen.

---

Außerdem wird beim Check-In in locations wird der ungefähre Standort der User\*in lokal auf dem Gerät mit dem erwarteten
Standort der Location abgeglichen. Falls keine Übereinstimmung gefunden wird, wird der Check-In abgelehnt.

---

Ein weiterer Sicherheitsmechanismus ist eine Signatur an jedem sicherheitsrelevanten Request, den ein Gerät sendet.

Die Requests, die ein Covid-19 Zertifikat abzurufen, werden mit einer Prüfsumme der personenbezogenen Daten, die zum
Certificate gehören, signiert und können daher nur von der Besitzer\*in abgerufen werden.

## Weiternutzung

Dieses Verschlüsselungskonzept wurde von B'Elanna Michalke mit Unterstützung von Sean-André Steinke für die HannTech
GmbH entwickelt. Das Konzept darf weiterverwendet und adaptiert werden, jedoch ausschließlich unter strikter Nennung der
Urheber\*innen und der Auflage von nicht-kommerziellen Zwecken nach Definition in
der [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
