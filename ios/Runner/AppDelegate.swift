import UIKit
import Flutter
//import workmanager

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {

    // workmanager
//    UIApplication.shared.setMinimumBackgroundFetchInterval(TimeInterval(60*15))
//    WorkmanagerPlugin.setPluginRegistrantCallback { registry in
//      GeneratedPluginRegistrant.register(with: registry)
//    }

    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
