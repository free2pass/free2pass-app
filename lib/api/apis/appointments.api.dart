import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/enums/appointments.enum.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/appointment-change.dart';
import 'package:free2pass/push/push-adapter.dart';
import 'package:free2pass_support/free2pass_support.dart';

class AppointmentsApi extends ApiModel {
  static const base = 'appointments';
  final String languageCode;

  AppointmentsApi(F2PApi instance, this.languageCode);

  /// returns the id of a given appointment
  Future<ResponseModel<String>> create({
    required DateTime startTime,
    required int issuer,
    required String signature,
  }) async {
    return doPostRequest(apiUrl, base, params: {
      'fromTimestamp': (startTime.millisecondsSinceEpoch / 1000).round(),
      'issuerId': issuer,
      'signature': signature,
      'utcOffset': startTime.timeZoneOffset.inMinutes,
    });
  }

  Future<ResponseModel> cancel(
      {required String id,
      required AppointmentCancelReason reason,
      String? reasonString,
      required String signature}) async {
    final response = await doPostRequest(
      apiUrl,
      base + '/' + id + '/cancel',
      params: {
        'cancelReason': reason.index,
        if (reason == AppointmentCancelReason.OTHER && reasonString != null)
          'otherCancelReason': reasonString,
      },
      signRequest: signature,
    );
    if (response.status.success) {
      final appointment =
          appointments.values.firstWhere((element) => element.id == id);
      appointment.state = AppointmentState.CANCELED;
      appointment.otherCancelReason = reasonString;
      appointments.put(id, appointment);
      PushAdapter.unsubscribeAppointment(id);
    }
    return response;
  }

  Future<ResponseModel> check({
    required String id,
    required String signature,
  }) async {
    final response = await doGetRequest(
      apiUrl,
      base + '/' + id,
      signRequest: signature,
    );
    if (response.status.success) {
      final appointmentChange = AppointmentChange.fromJson(response.payload!);
      final actualAppointment = appointments.get(id)!;

      actualAppointment.state = appointmentChange.state;
      actualAppointment.revokeReason = appointmentChange.revokeReason;
      actualAppointment.cancelReason = appointmentChange.cancelReason;
      actualAppointment.otherRevokeReason = appointmentChange.otherRevokeReason;
      actualAppointment.otherCancelReason = appointmentChange.otherCancelReason;

      appointments.put(id, actualAppointment);
    }

    return response;
  }

  Future<ResponseModel> push({
    required String id,
    required String token,
  }) async {
    return doPostRequest(
      apiUrl,
      base + '/' + id + '/push',
      params: {'token': token},
    );
  }

  @override
  bool get isDebug => debug;
}
