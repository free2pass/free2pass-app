import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass_support/free2pass_support.dart';

class AuthRegisterApi extends ApiModel {
  AuthRegisterApi(F2PApi instance, this.languageCode);

  static const String base = 'auth/register';
  final String languageCode;

  Future<ResponseModel<Map<String, dynamic>>> user({
    required String phoneNumber,
    required String verifyCode,
  }) async {
    final ResponseModel<Map<String, dynamic>> response =
        await doPostRequest(apiUrl, base + '/user', params: <String, String>{
      'phoneNumber': phoneNumber,
      'verifyCode': verifyCode,
      'language': languageCode,
    });
    return response;
  }

  @override
  bool get isDebug => debug;
}
