import 'package:free2pass/api/apis/auth-register.api.dart';
import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/update-data.dart';
import 'package:free2pass_support/free2pass_support.dart';

class AuthApi extends ApiModel {
  static const base = 'auth';
  final String languageCode;

  AuthApi(F2PApi instance, this.languageCode) {
    register = AuthRegisterApi(instance, languageCode);
  }

  late AuthRegisterApi register;

  Future<ResponseModel<String>> verifySMS(
      {String? phoneNumber, String? appHash}) async {
    final ResponseModel<String> response =
        await doPostRequest(apiUrl, base + '/verifySMS',
            params: {
              "phoneNumber": phoneNumber,
              "language": languageCode,
              if (appHash != null) "appHash": appHash,
            },
            useToken: false);
    return response;
  }

  Future<ResponseModel<UpdateData>> shouldUpdate(
      {required Stores store, required int version}) async {
    final ResponseModel<Map<String, dynamic>> response =
        await doPostRequest(apiUrl, base + '/shouldUpdate', params: {
      "version": version,
      "system": store.index,
    });

    return ResponseModel(
      response.status.success && response.payload != null
          ? UpdateData.fromJson(response.payload!)
          : null,
      response.status,
    );
  }

  /// signs a given String to ensure data is not modified
  Future<ResponseModel<String>> signature({required String data}) async {
    return doGetRequest(apiUrl, base + '/signature', params: {
      "data": data,
    });
  }

  /// checks whether a network connection is available
  Future<bool> hello() async {
    try {
      final ResponseModel response = await doGetRequest(apiUrl, base + '/hello')
          .timeout(Duration(seconds: 10));

      return response.status.success;
    } catch (e) {
      return false;
    }
  }

  @override
  bool get isDebug => debug;
}
