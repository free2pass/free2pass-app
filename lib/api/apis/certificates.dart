import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/enums/certificate-status.enum.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/certificate.dart';
import 'package:free2pass/utils/update-certificate-in-database.dart';
import 'package:free2pass_support/free2pass_support.dart';

class CertificatesApi extends ApiModel {
  static const base = 'certificates';
  final String languageCode;

  CertificatesApi(F2PApi instance, this.languageCode);

  Future<ResponseModel> getBulk(
      {required List<String> ids, required String signature}) async {
    final ResponseModel<List> response = await doGetRequest(
      apiUrl,
      base + '/bulk',
      params: {
        "ids": ids,
        "language": languageCode,
      },
      signRequest: signature,
    );

    final List<String> returnedIds = [];
    final List<String> hashesPresent = [];

    for (var element in response.payload!) {
      final certificate = Certificate.fromJson(element);
      returnedIds.add(certificate.id);

      hashesPresent.add(await updateCertificateInDatabase(certificate));
    }
    // removing local certificate in case the server asks us to
    if (ids.length > returnedIds.length) {
      ids.forEach((element) {
        if (!returnedIds.contains(element)) {
          // in case the certificate is of a user processed previously
          if (hashesPresent.contains(certificates.get(element)!.userHash))
            // deleting
            certificates.delete(element);
        }
      });
    }

    return response;
  }

  Future<ResponseModel> swapSignature(
      {required String oldSignature, required String newSignature}) async {
    return doPostRequest(
      apiUrl,
      base + '/swapSignature',
      params: {
        "newSignature": newSignature,
        "oldSignature": oldSignature,
      },
      signRequest: oldSignature,
    );
  }

  Future<ResponseModel<String>> getPublicKey({required String id}) async {
    final ResponseModel<String> response =
        await doGetRequest(apiUrl, base + '/' + id + '/publicKey');
    return response;
  }

  Future<ResponseModel<CertificateStatusValidity>> getStatus({
    required String id,
    required String signature,
    bool skipCertificateUpdate = false,
  }) async {
    final ResponseModel<Map> response = await doGetRequest(
      apiUrl,
      base + '/' + id,
      params: {
        "language": languageCode,
      },
      signRequest: signature,
    );
    final certificate =
        Certificate.fromJson(response.payload as Map<String, dynamic>);

    if (!skipCertificateUpdate) await updateCertificateInDatabase(certificate);

    return ResponseModel(certificate.expectedValidity, response.status);
  }

  Future<ResponseModel<String>> setEncryptedData({
    required String id,
    required String encryptedUserData,
    required String verificationCode,
    required String aesCipher,
    required String locationCodes,
    required String signature,
  }) async {
    return doPostRequest(
      apiUrl,
      base + '/' + id + '/data',
      params: {
        "userData": encryptedUserData,
        "aesCipher": aesCipher,
        "verificationCode": verificationCode,
        "locationCodes": locationCodes,
        "signature": signature,
        "getSession": true,
        "language": languageCode,
      },
      signRequest: signature,
    );
  }

  Future<ResponseModel> push({
    required String id,
    required String token,
  }) async {
    return doPostRequest(
      apiUrl,
      base + '/' + id + '/push',
      params: {'token': token},
    );
  }

  @override
  bool get isDebug => debug;
}
