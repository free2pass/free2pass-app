import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/health-department.model.dart';
import 'package:free2pass_support/free2pass_support.dart';

class HealthDepartmentsApi extends ApiModel {
  static const base = 'health-departments';
  final String languageCode;

  HealthDepartmentsApi(F2PApi instance, this.languageCode);

  /// fetches the competent health department for a given zip code
  Future<ResponseModel<HealthDepartment>> competentDepartment(
      {required String zip}) async {
    final ResponseModel<Map<String, dynamic>> response = await doGetRequest(
      apiUrl,
      base + '/' + zip,
    );
    return ResponseModel(
        HealthDepartment.fromJson(response.payload!), response.status);
  }

  @override
  bool get isDebug => debug;
}
