import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/issuer-slot.dart';
import 'package:free2pass/models/issuer.dart';
import 'package:free2pass_support/free2pass_support.dart';

class IssuersApi extends ApiModel {
  static const base = 'issuers';
  final String languageCode;

  IssuersApi(F2PApi instance, this.languageCode);

  Future<ResponseModel<Issuer>> getData({required int id}) async {
    final ResponseModel response = await doGetRequest(
        apiUrl, base + '/' + id.toString(),
        params: {"language": languageCode});
    final location = Issuer.fromJson(response.payload);

    return ResponseModel(location, response.status);
  }

  Future<ResponseModel<IssuerSlot>> getSlot(
      {required int issuer, required DateTime day}) async {
    final ResponseModel<Map<String, dynamic>> response = await doGetRequest(
        apiUrl, base + '/' + issuer.toString() + '/slots',
        params: {
          'dayTimestamp':
              (day.millisecondsSinceEpoch / 1000).round().toString(),
          'utcOffset': day.timeZoneOffset.inMinutes.toString(),
          'language': languageCode,
        });

    return ResponseModel(
      IssuerSlot.fromJson(response.payload!),
      response.status,
    );
  }

  @override
  bool get isDebug => debug;
}
