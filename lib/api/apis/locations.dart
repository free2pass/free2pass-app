import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/location.dart';
import 'package:free2pass_support/free2pass_support.dart';

class LocationsApi extends ApiModel {
  static const base = 'locations';
  final String languageCode;

  LocationsApi(F2PApi instance, this.languageCode);

  Future<ResponseModel<Map<String, dynamic>>> getAll() async {
    final ResponseModel<Map<String, dynamic>> response =
        await doGetRequest(apiUrl, base, params: {
      // TODO: implement filters
      "language": languageCode
    });
    final ids = [];
    if (response.payload != null)
      for (int i = 0; i < response.payload!['locations']!.length; i++) {
        final location = Location.fromJson(response.payload!['locations'][i]);
        ids.add(location.id);
        response.payload!['locations'][i] = location;
        if (!cachedLocations.containsKey(location.id)) {
          cachedLocations.put(location.id, location);
        }
      }
    final idsToDelete =
        cachedLocations.keys.where((element) => !ids.contains(element));
    cachedLocations.deleteAll(idsToDelete);
    return response;
  }

  @override
  bool get isDebug => debug;
}
