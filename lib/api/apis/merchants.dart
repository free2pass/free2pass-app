import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/checkin-data.dart';
import 'package:free2pass/models/merchant.dart';
import 'package:free2pass_support/free2pass_support.dart';

class MerchantsApi extends ApiModel {
  static const base = 'merchants';
  final String languageCode;

  MerchantsApi(F2PApi instance, this.languageCode);

  Future<ResponseModel<Merchant>> getData(
      {required int id, bool addToVisitedLocations = false}) async {
    final ResponseModel response = await doGetRequest(
        apiUrl, base + '/' + id.toString(),
        params: {"language": languageCode});
    final location = Merchant.fromJson(response.payload);

    if (!cachedLocations.containsKey(location.id)) {
      cachedLocations.put(location.id, location);
    }

    if (addToVisitedLocations) visitedLocations.add(location);

    return ResponseModel(location, response.status);
  }

  Future<ResponseModel<CheckInData>> getCheckInData(
      {required String id}) async {
    final ResponseModel<Map<String, dynamic>> response =
        await doGetRequest(apiUrl, base + '/' + id + '/checkIn');

    return ResponseModel(
        CheckInData.fromJson(response.payload!), response.status);
  }

  Future<ResponseModel> setEncryptedUserData(
      {required String encryptedId, required String encryptedUserData}) async {
    return doPostRequest(apiUrl, base + '/' + encryptedId + '/data',
        params: {"data": encryptedUserData, "language": languageCode});
  }

  @override
  bool get isDebug => debug;
}
