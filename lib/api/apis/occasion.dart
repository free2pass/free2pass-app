import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/occasion-create-data.model.dart';
import 'package:free2pass_support/free2pass_support.dart';

class OccasionsApi extends ApiModel {
  static const base = 'occasions';
  final String languageCode;

  OccasionsApi(F2PApi instance, this.languageCode);

  Future<ResponseModel<bool>> available() async {
    return await doGetRequest(apiUrl, base + '/available');
  }

  Future<ResponseModel<OccasionCreationData>> create(
      {required String zip}) async {
    final response =
        await doGetRequest(apiUrl, base + '/create', params: {'zip': zip});
    if (!response.status.success) return ResponseModel(null, response.status);
    return ResponseModel(
        OccasionCreationData.fromJson(response.payload), response.status);
  }

  Future<ResponseModel> setKey(
      {required String id, String? encryptedPrivateRSAKey}) async {
    return await doPostRequest(apiUrl, base + '/' + id + '/key',
        params: {'encryptedPrivateRSAKey': encryptedPrivateRSAKey});
  }

  Future<ResponseModel> setEncryptedUserData(
      {required String id, required String encryptedUserData}) async {
    return doPostRequest(apiUrl, base + '/' + id + '/data',
        params: {"data": encryptedUserData, "language": languageCode});
  }

  @override
  bool get isDebug => debug;
}
