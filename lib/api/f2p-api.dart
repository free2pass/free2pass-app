import 'package:free2pass/api/apis/appointments.api.dart';
import 'package:free2pass/api/apis/auth.api.dart';
import 'package:free2pass/api/apis/certificates.dart';
import 'package:free2pass/api/apis/health-departments.dart';
import 'package:free2pass/api/apis/issuers.dart';
import 'package:free2pass/api/apis/locations.dart';
import 'package:free2pass/api/apis/merchants.dart';
import 'package:free2pass/api/apis/occasion.dart';
import 'package:free2pass_support/free2pass_support.dart';

class F2PApi extends BaseApi {
  static late F2PApi _api;
  final String languageCode;

  F2PApi._(this.languageCode) {
    auth = AuthApi(this, languageCode);
    certificates = CertificatesApi(this, languageCode);
    locations = LocationsApi(this, languageCode);
    merchants = MerchantsApi(this, languageCode);
    occasions = OccasionsApi(this, languageCode);
    appointments = AppointmentsApi(this, languageCode);
    issuers = IssuersApi(this, languageCode);
    healthDepartments = HealthDepartmentsApi(this, languageCode);
  }

  static F2PApi get instance {
    return _api;
  }

  static Future<bool> initialize({required String languageCode}) async {
    _api = F2PApi._(languageCode);
    return true;
  }

  late AuthApi auth;
  late CertificatesApi certificates;
  late LocationsApi locations;
  late MerchantsApi merchants;
  late OccasionsApi occasions;
  late AppointmentsApi appointments;
  late IssuersApi issuers;
  late HealthDepartmentsApi healthDepartments;
}

class F2PApiNotInitializedError extends Error {
  @override
  String toString() {
    return 'Error: F2PApi not initialized yet. Please run F2PApi.initialize() before any call of F2PApi.instance.';
  }
}
