/*
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:free2pass/enums/async-workers.enum.dart';
import 'package:free2pass/enums/notification-channels.enum.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/models/notification-data.dart';
import 'package:workmanager/workmanager.dart';

Future cancelWorkManager() {
  return Workmanager().cancelByUniqueName(
      (AsyncWorker.IMPORT_CERTIFICATE.index + 1).toString());
}

Future<FlutterLocalNotificationsPlugin>
    getCertificateAvailableNotification() async {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      new FlutterLocalNotificationsPlugin();
  var androidInitSettings =
      new AndroidInitializationSettings('@mipmap/launcher_icon');
  var iOSInitSettings = new IOSInitializationSettings();
  var initSettings = new InitializationSettings(
      android: androidInitSettings, iOS: iOSInitSettings);
  await flutterLocalNotificationsPlugin.initialize(
    initSettings,
    onSelectNotification: (_) async {},
  );
  return flutterLocalNotificationsPlugin;
}

Future showCertificateAvailableNotification(
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin,
    NotificationData notificationData) async {
  var android = new AndroidNotificationDetails(
    NotificationChannel.TEST_RESULTS.toString(),
    notificationData.channelName,
    notificationData.channelDetails,
    priority: Priority.defaultPriority,
    importance: Importance.defaultImportance,
  );
  var iOS = new IOSNotificationDetails();
  var platform = new NotificationDetails(android: android, iOS: iOS);
  return flutterLocalNotificationsPlugin.show(
    0,
    notificationData.notificationTitle,
    notificationData.notificationBody,
    platform,
    payload: 'AndroidCoding.in',
  );
}

void showWebSocketNotification(
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin, S s) {
  var android = new AndroidNotificationDetails(
    NotificationChannel.WEBSOCKET_OPEN.toString(),
    s.notificationChannelWebSocket,
    s.notificationChannelWebSocketLong,
    priority: Priority.min,
    importance: Importance.min,
    playSound: false,
    ongoing: true,
    fullScreenIntent: false,
    enableLights: false,
    enableVibration: false,
    indeterminate: true,
    autoCancel: false,
  );
  var iOS = new IOSNotificationDetails(
    presentAlert: false,
    presentBadge: false,
    presentSound: false,
  );
  var platform = new NotificationDetails(android: android, iOS: iOS);
  flutterLocalNotificationsPlugin.show(
      1, s.notificationSilentContent, '', platform,
      payload: 'AndroidCoding.in');
}
*/
