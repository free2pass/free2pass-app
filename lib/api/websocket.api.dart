import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/certificate.dart';
import 'package:free2pass/models/web-socket-controller.dart';
import 'package:free2pass/utils/update-certificate-in-database.dart';
import 'package:socket_io_client/socket_io_client.dart';

class WebSocketClient {
  static Future<WebSocketController> certificateProcess(
    String server,
    String token, {
    VoidCallback? onPinVerified,
    VoidCallback? onConnect,
    VoidCallback? onCertificateUpdated,
    VoidCallback? onDisconnect,
    VoidCallback? onError,
    //NotificationData notificationData,
    S? s,
  }) async {
    cache.clear();
    Socket socket = io(server + '?token=' + token, <String, dynamic>{
      'transports': ['websocket'],
    });

    /*FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
    if (notificationData != null) {
      try {
        flutterLocalNotificationsPlugin =
            await getCertificateAvailableNotification();
        showWebSocketNotification(flutterLocalNotificationsPlugin, s);
      } catch (e) {
        print(e);
      }
    }*/
    final connectedCompleter = Completer();
    final disconnector = Completer();
    socket.onConnect((_) {
      if (debug) print('Connected WebSocket');
      socket.emit('registerClient');
      if (onConnect != null) onConnect();
    });
    socket.on('clientRegistered', (_) => connectedCompleter.complete());
    socket.on('pinEntered', (_) {
      if (onPinVerified != null) onPinVerified();
    });
    socket.on('exception', (_) {
      if (onError != null) onError();
    });
    socket.onDisconnect((_) {
      if (onDisconnect != null) onDisconnect();
    });
    socket.on('certificateUpdated', (_) async {
      disconnector.complete();
      final certificate = Certificate.fromJson(_);
      updateCertificateInDatabase(certificate);
      if (debug) print('Imported certificate');

      /*if (notificationData != null) {
        try {
          await flutterLocalNotificationsPlugin.cancelAll();
          showCertificateAvailableNotification(
              flutterLocalNotificationsPlugin, notificationData);
        } catch (e) {
          print(e);
        }
      }
      if (!kIsWeb) cancelWorkManager();*/
      if (onCertificateUpdated != null) onCertificateUpdated();
    });

    disconnector.future.then((value) {
      socket.disconnect();
      socket.dispose();
    });
    return WebSocketController(
      connected: connectedCompleter.future,
      disconnect: disconnector,
    );
  }
}
/*

/// starts a background task using [Workmanager] to check whether
/// the certificate became valid
/// this is a kind of workaround to avoid push services to preserve the user's
/// privacy and our security requirements
Future waitForCertificateWorker() async {
  Workmanager().executeTask((task, encodedImportData) async {
    final decodedData =
        CertificateImportData.fromJson(jsonDecode(encodedImportData['data']));
    print(decodedData.signature);
    try {
      await F2PApi.initialize(languageCode: decodedData.language);
      final data = await F2PApi.instance.certificates.getStatus(
        id: decodedData.id,
        signature: decodedData.signature,
        skipCertificateUpdate: true,
      );
      if (data.payload != CertificateStatusValidity.PENDING) {
        cancelWorkManager();
        FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
            await getCertificateAvailableNotification();
        await flutterLocalNotificationsPlugin.cancelAll();
        showCertificateAvailableNotification(
            flutterLocalNotificationsPlugin, decodedData.notificationData);
        return true;
      }
    } catch (e) {
      print(e);
    }

    return true;
  });
}
*/
