import 'package:flutter/material.dart';

class DarkModeOverlay extends StatelessWidget {
  final Widget child;

  const DarkModeOverlay({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (Theme.of(context).brightness == Brightness.light)
      return child;
    else
      return ColorFiltered(
        colorFilter: ColorFilter.mode(Colors.grey[600]!, BlendMode.modulate),
        child: child,
      );
  }
}
