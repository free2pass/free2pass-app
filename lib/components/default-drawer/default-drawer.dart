import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/screens/expired-certificates/expired-certificates.screen.dart';
import 'package:free2pass/screens/home/components/views/decoration/user-photo-decoration.dart';
import 'package:free2pass/screens/home/home.dart';
import 'package:free2pass/screens/multi-user/multi-user.dart';
import 'package:free2pass/screens/recent-locations/recent-locations.screen.dart';
import 'package:free2pass/screens/settings/settings.screen.dart';
import 'package:free2pass/screens/vaccination-certificate/vaccination-certificate.dart';
import 'package:free2pass/utils/encode-personal-data.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:hive_flutter/hive_flutter.dart';

class DefaultDrawer extends StatefulWidget {
  @override
  _DefaultDrawerState createState() => _DefaultDrawerState();
}

class _DefaultDrawerState extends State<DefaultDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: green,
        child: SafeArea(
          child: SingleChildScrollView(
            child: Material(
              color: Colors.transparent,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  _DefaultDrawerHeader(),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      DrawerTile(
                        icon: Icons.home,
                        label: S.of(context).home,
                        routeName: HomeScreen.routeName,
                      ),
                      if (!kIsWeb)
                        DrawerTile(
                          icon: Icons.mediation,
                          label: S.of(context).vaccinationCertificate,
                          routeName: VaccinationCertificateScreen.routeName,
                        ),
                      DrawerTile(
                        icon: Icons.map,
                        label: S.of(context).recentLocations,
                        routeName: RecentLocationsScreen.routeName,
                      ),
                      DrawerTile(
                        icon: Icons.lock_clock,
                        label: S.of(context).expiredCertificates,
                        routeName: ExpiredCertificatesScreen.routeName,
                      ),
                      DrawerTile(
                        icon: Icons.supervisor_account,
                        label: S.of(context).multiUser,
                        routeName: MultiUserScreen.routeName,
                      ),
                    ],
                  ),
                  DrawerTile(
                    icon: Icons.settings,
                    label: S.of(context).settings,
                    routeName: SettingsScreen.routeName,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _DefaultDrawerHeader extends StatefulWidget {
  @override
  __DefaultDrawerHeaderState createState() => __DefaultDrawerHeaderState();
}

class __DefaultDrawerHeaderState extends State<_DefaultDrawerHeader> {
  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: data.listenable(),
        builder: (context, Box box, w) {
          final user = getCurrentUser();
          return UserAccountsDrawerHeader(
            currentAccountPicture: user.photoLocation != null
                ? UserPhotoDecoration(
                    tag: _DefaultDrawerHeader,
                  )
                : Icon(
                    Icons.person,
                    size: 64,
                    color: Colors.white,
                  ),
            accountName: Text(user.name, style: TextStyle(color: Colors.white)),
            accountEmail: Text(S.of(context).currentUser,
                style: TextStyle(color: Colors.white)),
            onDetailsPressed: _showDetails,
            arrowColor: Colors.transparent,
          );
        });
  }

  void _showDetails() {
    Navigator.of(context).pushNamed(MultiUserScreen.routeName);
  }
}
