import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/screens/scan-flow/decoration/scan-result-view.dart';
import 'package:free2pass_support/free2pass_support.dart';

class InvalidQrCodeView extends StatelessWidget {
  final VoidCallback onRetry;

  const InvalidQrCodeView({Key? key, required this.onRetry}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScanResultView(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: AlignedMarkdown(
            data: '# ' +
                S.of(context).invalidCryptoQr +
                '\n' +
                S.of(context).invalidCryptoQrLongFormatted,
            color: gray,
            alignment: CrossAxisAlignment.end,
          ),
        ),
        ElevatedButton(onPressed: onRetry, child: Text(S.of(context).retry))
      ],
    );
  }
}
