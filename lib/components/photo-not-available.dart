import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';

class PhotoNotAvailableNotice extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(
          maxWidth: MediaQuery.of(context).size.width * 1 / 3,
          maxHeight: MediaQuery.of(context).size.height * 1 / 3),
      child: AspectRatio(
        aspectRatio: 1,
        child: Tooltip(
          message: S.of(context).noPhotoAvailableLong,
          child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(32),
                side: BorderSide(color: Colors.white, width: 4)),
            child: Padding(
              padding: const EdgeInsets.all(4),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    S.of(context).noPhotoAvailable,
                    style: Theme.of(context).textTheme.headline6,
                    textAlign: TextAlign.center,
                  ),
                  if (MediaQuery.of(context).size.width > 400)
                    Text(
                      S.of(context).noPhotoAvailableLong,
                      textAlign: TextAlign.center,
                    ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
