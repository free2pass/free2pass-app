import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/push/push-adapter.dart';

class PushEnablerTile extends StatefulWidget {
  final ValueChanged<bool?> onChanged;

  const PushEnablerTile({Key? key, required this.onChanged}) : super(key: key);

  @override
  State<PushEnablerTile> createState() => _PushEnablerTileState();
}

class _PushEnablerTileState extends State<PushEnablerTile> {
  bool? _shouldReceivePush;

  @override
  void initState() {
    PushAdapter.instance.isActivated;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
      value: _shouldReceivePush,
      onChanged: _handlePushChange,
      tristate: true,
      title: Text(S.of(context).appointmentEnablePush),
      subtitle: Text(S.of(context).pushPrivacyNotice),
    );
  }

  void _handlePushChange(bool? value) {
    setState(() {
      _shouldReceivePush = value;
    });
    widget.onChanged(value);
  }
}
