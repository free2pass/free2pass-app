import 'dart:async';

import 'package:animations/animations.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/screens/scan-flow/decoration/scan-result-view.dart';
import 'package:free2pass_support/free2pass_support.dart';

class RequiresNetworkPage extends StatefulWidget {
  final Widget child;

  const RequiresNetworkPage({Key? key, required this.child}) : super(key: key);

  @override
  _RequiresNetworkPageState createState() => _RequiresNetworkPageState();
}

class _RequiresNetworkPageState extends State<RequiresNetworkPage> {
  final _controller = AxisTransitionController();

  late StreamSubscription<ConnectivityResult> _subscription;

  @override
  void initState() {
    _checkConnection();
    _subscription =
        Connectivity().onConnectivityChanged.listen(_handleConnectivityChange);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: AxisTransitionView(
        direction: SharedAxisTransitionType.scaled,
        controller: _controller,
        children: [
          widget.child,
          ScanResultView(
            children: [
              AlignedMarkdown(
                data: S.of(context).noConnectionLongFormatted,
                alignment: CrossAxisAlignment.end,
              ),
              ElevatedButton(
                  onPressed: _reconnect, child: Text(S.of(context).retry))
            ],
          ),
          Center(
            child: CircularProgressIndicator(),
          ),
        ],
      ),
    );
  }

  @override
  dispose() {
    _subscription.cancel();
    super.dispose();
  }

  Future<void> _checkConnection() async {
    var connectivityResult = await Connectivity().checkConnectivity();
    bool connection = false;
    if (connectivityResult != ConnectivityResult.none)
      connection = await F2PApi.instance.auth.hello();
    _controller.current = connection ? 0 : 1;
  }

  void _reconnect() {
    _controller.current = 2;
    _checkConnection();
  }

  void _handleConnectivityChange(ConnectivityResult event) {
    _checkConnection();
  }
}
