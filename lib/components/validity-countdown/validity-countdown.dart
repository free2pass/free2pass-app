import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/index.dart';

class ValidityCountdownWidget extends StatelessWidget {
  final int milliseconds;
  final Color? color;

  const ValidityCountdownWidget(
      {Key? key, required this.milliseconds, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CountdownTimer(
      endTime: milliseconds,
      widgetBuilder: (_, CurrentRemainingTime? time) {
        return DefaultTextStyle(
          style: Theme.of(context)
              .textTheme
              .headline5!
              .copyWith(color: color, fontWeight: FontWeight.bold),
          child: Text(_countdownFormatter(time)),
        );
      },
    );
  }

  String _countdownFormatter(CurrentRemainingTime? duration) {
    return makeTwoCharacters(
            (duration?.hours ?? 0) + 24 * (duration?.days ?? 0)) +
        'h ' +
        makeTwoCharacters(duration?.min) +
        'm ' +
        makeTwoCharacters(duration?.sec) +
        's';
  }
}

String makeTwoCharacters(input) {
  var text = '0' + (input ?? 0).toString();
  return text.substring(
    text.length - 2,
  );
}
