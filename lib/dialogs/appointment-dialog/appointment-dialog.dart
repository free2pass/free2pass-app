import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/components/push-enabler-tile.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/appointment.dart';
import 'package:free2pass/models/issuer-slot.dart';
import 'package:free2pass/models/issuer.dart';
import 'package:free2pass/push/push-adapter.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:intl/intl.dart';

class AppointmentsDialog extends StatefulWidget {
  final DateTime initialDate;
  final Issuer issuer;

  const AppointmentsDialog(
      {Key? key, required this.initialDate, required this.issuer})
      : super(key: key);

  @override
  State<AppointmentsDialog> createState() => _AppointmentsDialogState();
}

class _AppointmentsDialogState extends State<AppointmentsDialog> {
  late DateTime _day;
  bool _reversed = false;
  Slot? _slot;

  AxisTransitionController _controller = AxisTransitionController();

  bool? _shouldReceivePush;

  @override
  void initState() {
    _day = widget.initialDate;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(S.of(context).selectSlot),
      content: AxisTransitionView(
        controller: _controller,
        direction: SharedAxisTransitionType.vertical,
        children: [
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ButtonBar(
                alignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    onPressed: _previousDay,
                    icon: Icon(Icons.arrow_left),
                    tooltip: S.of(context).previousDay,
                  ),
                  Text(DateFormat.MMMEd().format(_day)),
                  IconButton(
                    onPressed: _nextDay,
                    icon: Icon(Icons.arrow_right),
                    tooltip: S.of(context).nextDay,
                  ),
                ],
              ),
              PageTransitionSwitcher(
                reverse: _reversed,
                transitionBuilder: (Widget child,
                    Animation<double> primaryAnimation,
                    Animation<double> secondaryAnimation) {
                  return SharedAxisTransition(
                    fillColor: Colors.transparent,
                    animation: primaryAnimation,
                    secondaryAnimation: secondaryAnimation,
                    transitionType: SharedAxisTransitionType.horizontal,
                    child: child,
                  );
                },
                child: FutureBuilder(
                  key: ValueKey(widget.issuer.id),
                  future: F2PApi.instance.issuers
                      .getSlot(issuer: widget.issuer.id, day: _day),
                  builder: (context,
                      AsyncSnapshot<ResponseModel<IssuerSlot>> snapshot) {
                    if (snapshot.hasData)
                      return Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            constraints: BoxConstraints(
                                maxHeight: (MediaQuery.of(context).size.height /
                                        2) -
                                    (PushAdapter.instance.isDummy ? 0 : 96)),
                            child: Container(
                              width: double.maxFinite,
                              height: double.maxFinite,
                              child: GridView.builder(
                                gridDelegate:
                                    SliverGridDelegateWithMaxCrossAxisExtent(
                                        maxCrossAxisExtent: 128,
                                        childAspectRatio: 5 / 2,
                                        crossAxisSpacing: 4,
                                        mainAxisSpacing: 4),
                                itemCount: snapshot
                                    .data!.payload!.availableSlots.length,
                                itemBuilder: (context, index) {
                                  final slot = snapshot
                                      .data!.payload!.availableSlots[index];
                                  return Chip(
                                    backgroundColor: _slot != null &&
                                            _slot!.fromTimestamp ==
                                                slot.fromTimestamp
                                        ? green
                                        : null,
                                    label: Text(
                                      DateFormat.Hm().format(slot.fromDateTime),
                                    ),
                                    deleteIcon: Icon(Icons.check_circle),
                                    deleteButtonTooltipMessage:
                                        S.of(context).selectThisSlot,
                                    onDeleted: () => _selectSlot(slot),
                                  );
                                },
                              ),
                            ),
                          ),
                          if (!PushAdapter.instance.isDummy)
                            PushEnablerTile(onChanged: _handlePushChange),
                        ],
                      );
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  },
                ),
              ),
            ],
          ),
          Container(
            width: 64,
            height: 64,
            alignment: Alignment.topCenter,
            child: CircularProgressIndicator(),
          ),
          Text(S.of(context).successfullyReservedSlot)
        ],
      ),
      actions: [
        TextButton(
          onPressed: Navigator.of(context).pop,
          child: Text(S.of(context).close),
        ),
        if (_controller.current != 2)
          TextButton(
            onPressed: (_slot != null &&
                    (PushAdapter.instance.isDummy ||
                        _shouldReceivePush != null) &&
                    _controller.current == 0)
                ? _createAppointment
                : null,
            child: Text(S.of(context).createAppointment),
          ),
      ],
    );
  }

  void _nextDay() {
    _day = _day.add(Duration(days: 1));
    setState(() {
      _reversed = false;
    });
  }

  void _previousDay() {
    _day = _day.subtract(Duration(days: 1));
    setState(() {
      _reversed = true;
    });
  }

  Future<void> _createAppointment() async {
    setState(() {
      _controller.next();
    });
    final signature = Crypto.createRandomString();

    final response = await F2PApi.instance.appointments.create(
      startTime:
          DateTime.fromMillisecondsSinceEpoch(_slot!.fromTimestamp * 1000),
      issuer: widget.issuer.id,
      signature: signature,
    );

    final appointment = Appointment(
      issuer: widget.issuer,
      startTime: _slot!.fromTimestamp,
      signature: signature,
      id: response.payload!,
    );

    if (_shouldReceivePush == true) {
      PushAdapter.instance.registerAppointment(appointment);
    }

    if (response.status.success) {
      await appointments.put(response.payload!, appointment);
      _controller.next();
    } else {
      Navigator.of(context).pop();
      Navigator.of(context).pop();

      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(S.of(context).errorCreatingAppointment)));
    }
  }

  _selectSlot(Slot e) {
    setState(() {
      _slot = e;
    });
  }

  void _handlePushChange(bool? value) {
    setState(() {
      _shouldReceivePush = value;
    });
  }
}
