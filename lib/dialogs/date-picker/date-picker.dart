import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:free2pass/generated/l10n.dart';

Future<DateTime?> showInputDatePicker({
  required BuildContext context,
  DateTime? initialDate,
}) async {
  final dayController = TextEditingController();
  final monthController = TextEditingController();
  final yearController = TextEditingController();

  final dayFocus = FocusNode();
  final monthFocus = FocusNode();
  final yearFocus = FocusNode();

  final formKey = GlobalKey<FormState>();

  if (initialDate != null) {
    dayController.text = initialDate.day.toString();
    dayController.selection =
        TextSelection(baseOffset: 0, extentOffset: dayController.text.length);
    monthController.text = initialDate.month.toString();
    monthController.selection =
        TextSelection(baseOffset: 0, extentOffset: monthController.text.length);
    yearController.text = initialDate.year.toString();
    yearController.selection =
        TextSelection(baseOffset: 2, extentOffset: yearController.text.length);
  }

  dayController.addListener(() {
    if (dayController.text.length >= 2 &&
        (dayController.text.endsWith('.') ||
            dayController.text.endsWith('/'))) {
      dayController.text =
          dayController.text.substring(0, dayController.text.length - 1);
      monthFocus.requestFocus();
    }
    try {
      if (RegExp(r'\D').hasMatch(dayController.text)) {
        dayController.value = NumbersOnlyFormatter()
            .formatEditUpdate(dayController.value, dayController.value);
        return;
      }

      final dayInt = int.parse(dayController.text);
      if (dayInt >= 4 && dayInt <= 31) {
        monthFocus.requestFocus();
      }
    } catch (e) {}
  });

  monthController.addListener(() {
    if (monthController.text.length >= 2 &&
        (monthController.text.endsWith('.') ||
            monthController.text.endsWith('/'))) {
      monthController.text =
          monthController.text.substring(0, monthController.text.length - 1);
      yearFocus.requestFocus();
    }
    try {
      if (RegExp(r'\D').hasMatch(monthController.text)) {
        monthController.value = NumbersOnlyFormatter()
            .formatEditUpdate(monthController.value, monthController.value);
        return;
      }
      final dayInt = int.parse(monthController.text);
      if (dayInt >= 2 && dayInt <= 12) {
        yearFocus.requestFocus();
      }
    } catch (e) {}
  });

  void submit() {
    if (formKey.currentState!.validate()) {
      try {
        final date = DateTime(int.parse(yearController.text),
            int.parse(monthController.text), int.parse(dayController.text));
        Navigator.of(context).pop(date);
      } catch (e) {}
    }
  }

  final DateTime? result = await showModal(
    context: context,
    builder: (context) => AlertDialog(
      title: Text(S.of(context).birthday),
      content: Form(
        autovalidateMode: AutovalidateMode.always,
        key: formKey,
        child: Wrap(
          alignment: WrapAlignment.spaceEvenly,
          runAlignment: WrapAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: 92,
              child: TextFormField(
                focusNode: dayFocus,
                controller: dayController,
                autofocus: true,
                keyboardType: TextInputType.number,
                validator: (day) {
                  try {
                    final dayInt = int.parse(day!);
                    if (dayInt >= 1 && dayInt <= 31) return null;
                  } catch (e) {}
                  return '';
                },
                maxLength: 2,
                maxLengthEnforcement: MaxLengthEnforcement.enforced,
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: S.of(context).day,
                ),
              ),
            ),
            SizedBox(
              width: 92,
              child: TextFormField(
                focusNode: monthFocus,
                controller: monthController,
                keyboardType: TextInputType.number,
                validator: (month) {
                  try {
                    final monthInt = int.parse(month!);
                    if (monthInt >= 1 && monthInt <= 12) return null;
                  } catch (e) {}
                  return '';
                },
                maxLength: 2,
                maxLengthEnforcement: MaxLengthEnforcement.enforced,
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: S.of(context).month,
                ),
              ),
            ),
            SizedBox(
              width: 128,
              child: TextFormField(
                focusNode: yearFocus,
                controller: yearController,
                keyboardType: TextInputType.number,
                validator: (year) {
                  try {
                    final yearInt = int.parse(year!);
                    if (yearInt >= 1910 && yearInt <= DateTime.now().year)
                      return null;
                  } catch (e) {}
                  return '';
                },
                maxLength: 4,
                maxLengthEnforcement: MaxLengthEnforcement.enforced,
                textInputAction: TextInputAction.go,
                onEditingComplete: submit,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: S.of(context).year,
                ),
              ),
            ),
          ]
              .map((e) => Padding(
                    padding: const EdgeInsets.all(2),
                    child: e,
                  ))
              .toList(),
        ),
      ),
      actions: [
        TextButton(
          onPressed: Navigator.of(context).pop,
          child: Text(S.of(context).cancel),
        ),
        TextButton(
          onPressed: submit,
          child: Text(S.of(context).save),
        ),
      ],
    ),
  );
  return result;
}

class NumbersOnlyFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    var newText = newValue.text.replaceAll(RegExp(r'\D'), '');
    return newValue.copyWith(
      text: newText,
      composing:
          newText != newValue.text ? TextRange.empty : newValue.composing,
    );
  }
}
