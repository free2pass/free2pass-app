import 'package:flutter/material.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/utils/hive.util.dart';
import 'package:free2pass_support/free2pass_support.dart';

class FullScreenUserImagePreview extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(),
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: MemoryImage(userImages[data.get(kCurrentUserKey)]!),
                fit: BoxFit.cover)),
      ),
    );
  }
}
