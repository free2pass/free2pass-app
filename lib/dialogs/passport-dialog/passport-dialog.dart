import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:free2pass/components/photo-not-available.dart';
import 'package:free2pass/components/validity-countdown/validity-countdown.dart';
import 'package:free2pass/enums/certificate-status.enum.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/certificate.dart';
import 'package:free2pass/screens/pdf-export/pdf-export.dart';
import 'package:free2pass/utils/encode-personal-data.dart';
import 'package:free2pass/utils/hive.util.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:intl/intl.dart';

class PassportDialog extends StatefulWidget {
  final Certificate certificate;
  final String label;

  const PassportDialog(
      {Key? key, required this.certificate, required this.label})
      : super(key: key);

  @override
  _PassportDialogState createState() => _PassportDialogState();
}

class _PassportDialogState extends State<PassportDialog> {
  @override
  Widget build(BuildContext context) {
    final user = getCurrentUser();
    return Scaffold(
      backgroundColor: (widget.certificate.expectedValidity ==
              CertificateStatusValidity.POSITIVE
          ? red
          : green),
      body: ListView(
        shrinkWrap: true,
        children: [
          SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    onPressed: Navigator.of(context).pop,
                    icon: Icon(Icons.close),
                    tooltip: S.of(context).close,
                    color: Colors.white,
                  ),
                  if (widget.certificate.expectedValidity ==
                          CertificateStatusValidity.NEGATIVE &&
                      !widget.certificate.issuer!.isPrivate)
                    TextButton(
                      onPressed: _sharePdf,
                      child: Row(
                        children: [
                          Text(
                            S.of(context).shareAsPdf + ' ',
                            style: TextStyle(color: Colors.white),
                          ),
                          Icon(
                            Icons.share,
                            color: Colors.white,
                          ),
                        ],
                      ),
                      //color: Colors.white,
                    ),
                ],
              ),
            ),
          ),
          AlignedMarkdown(
            color: Colors.white,
            data: '# ' +
                (widget.certificate.expectedValidity ==
                            CertificateStatusValidity.POSITIVE
                        ? S.of(context).positive
                        : S.of(context).negatie)
                    .toUpperCase(),
          ),
          if (widget.certificate.issuer!.isPrivate)
            Padding(
              padding: const EdgeInsets.only(top: 24.0),
              child: ListTile(
                tileColor: red,
                title: AlignedMarkdown(
                  data: '## ' + S.of(context).internalCertificateFormatted,
                  color: Colors.white,
                ),
              ),
            ),
          ResponsiveBox(
            child: Padding(
              padding: const EdgeInsets.all(32.0),
              child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(32)),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    children: [
                      (user.photoLocation != null)
                          ? _PassportPhoto(
                              color: (widget.certificate.expectedValidity ==
                                      CertificateStatusValidity.POSITIVE
                                  ? red
                                  : green),
                            )
                          : PhotoNotAvailableNotice(),
                      ListTile(
                        leading: Icon(Icons.person),
                        title: MarkdownBody(data: '# ' + user.name),
                      ),
                      ListTile(
                        leading: Icon(Icons.card_giftcard),
                        title: Text(
                            DateFormat.yMMMMd().format(user.birthdayDateTime)),
                      ),
                      ListTile(
                        leading: Icon(Icons.home),
                        title: Text(
                            user.street + ', ' + user.zip + ' ' + user.town),
                      ),
                      ListTile(
                        title:
                            AlignedMarkdown(data: '# **' + widget.label + '**'),
                      ),
                      if (widget.certificate.name != null)
                        ListTile(
                          leading: Icon(Icons.tag),
                          title: Text(
                            widget.certificate.name!,
                            style: widget.certificate.expectedValidity ==
                                    CertificateStatusValidity.PENDING
                                ? Theme.of(context).textTheme.headline3
                                : null,
                          ),
                          subtitle: Text(S.of(context).certificateId),
                        ),
                      if (widget.certificate.issuer!.isPrivate)
                        ListTile(
                          leading: widget.certificate.issuer!.logo != null
                              ? Image.network(
                                  widget.certificate.issuer!.logo!,
                                  width: 64,
                                  height: 64,
                                )
                              : null,
                          title: AlignedMarkdown(
                            data:
                                S.of(context).internalCertificateLongFormatted,
                            alignment: CrossAxisAlignment.start,
                          ),
                        ),
                      ListTile(
                        leading: Icon(Icons.healing),
                        title: Text(widget.certificate.issuer!.name),
                      ),
                      ListTile(
                        leading: Icon(Icons.calendar_today),
                        subtitle: Text(S.of(context).issued),
                        title: Text(localDateTimeString(S.of(context),
                            widget.certificate.resultDateTime)),
                      ),
                      ListTile(
                        leading: Icon(Icons.timer),
                        subtitle: Text(S.of(context).validity),
                        title: ValidityCountdownWidget(
                          milliseconds: widget.certificate.expiryDateTime
                              .millisecondsSinceEpoch,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  void _sharePdf() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => PdfExportScreen(certificate: widget.certificate),
        fullscreenDialog: true));
  }
}

String localDateTimeString(S s, DateTime dateTime) {
  return DateFormat.yMMMMd().format(dateTime) +
      ', ' +
      DateFormat.Hm().format(dateTime) +
      ' ' +
      s.oClock;
}

class _PassportPhoto extends StatelessWidget {
  final Color? color;

  const _PassportPhoto({Key? key, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints:
          BoxConstraints(maxWidth: MediaQuery.of(context).size.width * 2 / 3),
      padding: EdgeInsets.symmetric(vertical: 8),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(32),
        child: AspectRatio(
          aspectRatio: 1,
          child: Hero(
            tag: kPhotoLocationKey,
            child: Container(
              decoration: BoxDecoration(
                border: Border.all(color: color!, width: 4),
                borderRadius: BorderRadius.circular(32),
                image: DecorationImage(
                    image: MemoryImage(userImages[data.get(kCurrentUserKey)]!),
                    fit: BoxFit.cover),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
