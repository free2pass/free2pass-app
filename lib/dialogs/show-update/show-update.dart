import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/models/update-data.dart';
import 'package:url_launcher/url_launcher.dart';

void showUpdateDialog(BuildContext context,
    {bool forceUpdate = false, required Stores source}) {
  showDialog(
      context: context,
      builder: (context) => UpdateDialog(
            forceUpdate: forceUpdate,
            source: source,
          ),
      barrierDismissible: !forceUpdate);
}

class UpdateDialog extends StatefulWidget {
  final bool forceUpdate;
  final Stores? source;

  const UpdateDialog({Key? key, required this.forceUpdate, this.source})
      : super(key: key);

  @override
  _UpdateDialogState createState() => _UpdateDialogState();
}

class _UpdateDialogState extends State<UpdateDialog> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(S.of(context).updateAvailable),
      content: Text(widget.forceUpdate
          ? S.of(context).updateMandatoryLongFormatted
          : S.of(context).updateOptionalLongFormatted),
      actions: [
        TextButton(
          onPressed: _cancel,
          child: Text(
              widget.forceUpdate ? S.of(context).quitApp : S.of(context).later),
        ),
        ElevatedButton(
          onPressed: _update,
          child: Text(S.of(context).updateNow),
        ),
      ],
    );
  }

  void _update() {
    launch(storeUrls[widget.source!] ?? 'https://free2pass.de/download');
  }

  void _cancel() {
    if (widget.forceUpdate)
      SystemNavigator.pop();
    else
      Navigator.of(context).pop();
  }
}
