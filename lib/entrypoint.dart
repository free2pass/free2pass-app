import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/route-generator.dart';
import 'package:free2pass/utils/app_config.dart';
import 'package:free2pass/utils/hive.util.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:free2pass_support/generated/l10n.dart' as support;
import 'package:hive_flutter/hive_flutter.dart';

/// PLEASE NOTE: The `main()` function can be found in `entrypoints/`

/// entrypoint of our application
Future<void> entrypoint(AppConfig appConfig) async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  /// we first need to initialize [Hive]. Hive is used to store any
  /// kind of data such as settings, personal data, certificates etc.
  await initHive();

  runApp(appConfig);
}

/// The root application
///
/// Anything is a child of this app
class Free2PassApp extends StatefulWidget {
  @override
  _Free2PassAppState createState() => _Free2PassAppState();
}

class _Free2PassAppState extends State<Free2PassApp> {
  @override
  Widget build(BuildContext context) {
    /// We rely on [ValueListenableBuilder] Widgets all around our application
    /// They are used to efficiently handle new data or changes dependencies
    /// such as settings changes or new data from the API
    /// We usually listen for [Hive] [Box] Objects.
    return ValueListenableBuilder(
      valueListenable: settings.listenable(),
      builder: (context, Box box, w) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'free2𝗽𝗮𝘀𝘀',
          theme: lightTheme,
          darkTheme: darkTheme,
          themeMode: _getMode(box),
          initialRoute: '/',
          localizationsDelegates: [
            S.delegate,
            support.S.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          locale: _getLocale(box),
          supportedLocales: S.delegate.supportedLocales,
          onGenerateRoute: RouteGenerator.generateRoute,
        );
      },
    );
  }

  /// getting the requested [ThemeMode] out of our settings [Box]
  ThemeMode _getMode(Box box) {
    final data = box.get(kThemeKey, defaultValue: kDefaultTheme);
    switch (data) {
      case 1:
        return ThemeMode.light;
      case 2:
        return ThemeMode.dark;
      default:
        return ThemeMode.light;
    }
  }

  /// getting the expected [Locale] out of our settings [Box]
  Locale? _getLocale(Box box) {
    if (box.containsKey(kLocaleKey)) return Locale(box.get(kLocaleKey));
    return null;
  }
}
