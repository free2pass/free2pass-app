import 'package:free2pass/entrypoint.dart';
import 'package:free2pass/models/update-data.dart';
import 'package:free2pass/push/implementations/no-implementation.dart';
import 'package:free2pass/utils/app_config.dart';

void main() {
  entrypoint(AppConfig(
    source: Stores.FDroid,
    pushAdapter: NoPushAdapter(),
    child: Free2PassApp(),
  ));
}
