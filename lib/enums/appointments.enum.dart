import 'package:hive/hive.dart';

part 'appointments.enum.g.dart';

@HiveType(typeId: 14)
enum AppointmentCancelReason {
  @HiveField(0)
  OTHER,
  @HiveField(1)
  UNAVAILABLE,
  @HiveField(2)
  WRONG_TIME,
}

@HiveType(typeId: 15)
enum AppointmentRevokeReason {
  @HiveField(0)
  OTHER,
  @HiveField(1)
  ISSUER_CLOSED,
  @HiveField(2)
  TOO_MANY_APPOINTMENT,
  @HiveField(3)
  NO_MORE_TESTS_AVAILABLE,
  @HiveField(4)
  COULD_NOT_VERIFY,
}

@HiveType(typeId: 16)
enum AppointmentState {
  @HiveField(0)
  APPROVED,
  @HiveField(1)
  REVOKED,
  @HiveField(2)
  CANCELED,
}
