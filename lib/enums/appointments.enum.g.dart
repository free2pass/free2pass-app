// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appointments.enum.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AppointmentCancelReasonAdapter
    extends TypeAdapter<AppointmentCancelReason> {
  @override
  final int typeId = 14;

  @override
  AppointmentCancelReason read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return AppointmentCancelReason.OTHER;
      case 1:
        return AppointmentCancelReason.UNAVAILABLE;
      case 2:
        return AppointmentCancelReason.WRONG_TIME;
      default:
        return AppointmentCancelReason.OTHER;
    }
  }

  @override
  void write(BinaryWriter writer, AppointmentCancelReason obj) {
    switch (obj) {
      case AppointmentCancelReason.OTHER:
        writer.writeByte(0);
        break;
      case AppointmentCancelReason.UNAVAILABLE:
        writer.writeByte(1);
        break;
      case AppointmentCancelReason.WRONG_TIME:
        writer.writeByte(2);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppointmentCancelReasonAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class AppointmentRevokeReasonAdapter
    extends TypeAdapter<AppointmentRevokeReason> {
  @override
  final int typeId = 15;

  @override
  AppointmentRevokeReason read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return AppointmentRevokeReason.OTHER;
      case 1:
        return AppointmentRevokeReason.ISSUER_CLOSED;
      case 2:
        return AppointmentRevokeReason.TOO_MANY_APPOINTMENT;
      case 3:
        return AppointmentRevokeReason.NO_MORE_TESTS_AVAILABLE;
      case 4:
        return AppointmentRevokeReason.COULD_NOT_VERIFY;
      default:
        return AppointmentRevokeReason.OTHER;
    }
  }

  @override
  void write(BinaryWriter writer, AppointmentRevokeReason obj) {
    switch (obj) {
      case AppointmentRevokeReason.OTHER:
        writer.writeByte(0);
        break;
      case AppointmentRevokeReason.ISSUER_CLOSED:
        writer.writeByte(1);
        break;
      case AppointmentRevokeReason.TOO_MANY_APPOINTMENT:
        writer.writeByte(2);
        break;
      case AppointmentRevokeReason.NO_MORE_TESTS_AVAILABLE:
        writer.writeByte(3);
        break;
      case AppointmentRevokeReason.COULD_NOT_VERIFY:
        writer.writeByte(4);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppointmentRevokeReasonAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class AppointmentStateAdapter extends TypeAdapter<AppointmentState> {
  @override
  final int typeId = 16;

  @override
  AppointmentState read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return AppointmentState.APPROVED;
      case 1:
        return AppointmentState.REVOKED;
      case 2:
        return AppointmentState.CANCELED;
      default:
        return AppointmentState.APPROVED;
    }
  }

  @override
  void write(BinaryWriter writer, AppointmentState obj) {
    switch (obj) {
      case AppointmentState.APPROVED:
        writer.writeByte(0);
        break;
      case AppointmentState.REVOKED:
        writer.writeByte(1);
        break;
      case AppointmentState.CANCELED:
        writer.writeByte(2);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppointmentStateAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
