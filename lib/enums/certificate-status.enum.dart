import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'certificate-status.enum.g.dart';

@HiveType(typeId: 2)
enum CertificateStatusValidity {
  @HiveField(0)
  @JsonKey(name: 'negative')
  NEGATIVE,
  @HiveField(1)
  @JsonKey(name: 'positive')
  POSITIVE,
  @HiveField(2)
  @JsonKey(name: 'pending')
  PENDING,
  @HiveField(3)
  @JsonKey(ignore: true)
  EMPTY,
  @HiveField(4)
  @JsonKey(name: 'revoked')
  REVOKED,
}
