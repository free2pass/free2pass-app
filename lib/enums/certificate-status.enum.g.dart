// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'certificate-status.enum.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CertificateStatusValidityAdapter
    extends TypeAdapter<CertificateStatusValidity> {
  @override
  final int typeId = 2;

  @override
  CertificateStatusValidity read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return CertificateStatusValidity.NEGATIVE;
      case 1:
        return CertificateStatusValidity.POSITIVE;
      case 2:
        return CertificateStatusValidity.PENDING;
      case 3:
        return CertificateStatusValidity.EMPTY;
      case 4:
        return CertificateStatusValidity.REVOKED;
      default:
        return CertificateStatusValidity.NEGATIVE;
    }
  }

  @override
  void write(BinaryWriter writer, CertificateStatusValidity obj) {
    switch (obj) {
      case CertificateStatusValidity.NEGATIVE:
        writer.writeByte(0);
        break;
      case CertificateStatusValidity.POSITIVE:
        writer.writeByte(1);
        break;
      case CertificateStatusValidity.PENDING:
        writer.writeByte(2);
        break;
      case CertificateStatusValidity.EMPTY:
        writer.writeByte(3);
        break;
      case CertificateStatusValidity.REVOKED:
        writer.writeByte(4);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CertificateStatusValidityAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
