import 'package:hive/hive.dart';

part 'location-types.enum.g.dart';

@HiveType(typeId: 9)
enum LocationType {
  @HiveField(0)
  ISSUER,

  @HiveField(1)
  MERCHANT,

  @HiveField(2)
  VACCINATOR,

  @HiveField(3)
  CLUSTER,

  @HiveField(4)
  OCCASION,
}
