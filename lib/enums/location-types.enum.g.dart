// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'location-types.enum.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class LocationTypeAdapter extends TypeAdapter<LocationType> {
  @override
  final int typeId = 9;

  @override
  LocationType read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return LocationType.ISSUER;
      case 1:
        return LocationType.MERCHANT;
      case 2:
        return LocationType.VACCINATOR;
      case 3:
        return LocationType.CLUSTER;
      case 4:
        return LocationType.OCCASION;
      default:
        return LocationType.ISSUER;
    }
  }

  @override
  void write(BinaryWriter writer, LocationType obj) {
    switch (obj) {
      case LocationType.ISSUER:
        writer.writeByte(0);
        break;
      case LocationType.MERCHANT:
        writer.writeByte(1);
        break;
      case LocationType.VACCINATOR:
        writer.writeByte(2);
        break;
      case LocationType.CLUSTER:
        writer.writeByte(3);
        break;
      case LocationType.OCCASION:
        writer.writeByte(4);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LocationTypeAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
