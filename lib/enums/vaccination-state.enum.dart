import 'package:hive/hive.dart';

part 'vaccination-state.enum.g.dart';

@HiveType(typeId: 42)
enum VaccinationState {
  @HiveField(0)
  NONE,

  @HiveField(1)
  PARTIAL,

  @HiveField(2)
  IMMUNIZED,

  @HiveField(3)
  UNKNOWN,
}

const vaccinationStateMap = {
  VaccinationState.NONE: 0,
  VaccinationState.PARTIAL: 1,
  VaccinationState.IMMUNIZED: 2,
  VaccinationState.UNKNOWN: 3,
};
