// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vaccination-state.enum.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class VaccinationStateAdapter extends TypeAdapter<VaccinationState> {
  @override
  final int typeId = 42;

  @override
  VaccinationState read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return VaccinationState.NONE;
      case 1:
        return VaccinationState.PARTIAL;
      case 2:
        return VaccinationState.IMMUNIZED;
      case 3:
        return VaccinationState.UNKNOWN;
      default:
        return VaccinationState.NONE;
    }
  }

  @override
  void write(BinaryWriter writer, VaccinationState obj) {
    switch (obj) {
      case VaccinationState.NONE:
        writer.writeByte(0);
        break;
      case VaccinationState.PARTIAL:
        writer.writeByte(1);
        break;
      case VaccinationState.IMMUNIZED:
        writer.writeByte(2);
        break;
      case VaccinationState.UNKNOWN:
        writer.writeByte(3);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is VaccinationStateAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
