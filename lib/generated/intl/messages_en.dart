// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String m0(x, y) => "Vaccination ${x} of ${y}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "aPhotoDepictingYourFaceYourCamera":
            MessageLookupByLibrary.simpleMessage(
                "A photo depicting your face / your camera"),
        "abortAndViewUsers":
            MessageLookupByLibrary.simpleMessage("Abort and view users"),
        "abortTest": MessageLookupByLibrary.simpleMessage("Abort test"),
        "abortTestHelper":
            MessageLookupByLibrary.simpleMessage("This can not be undone."),
        "abortTestLong": MessageLookupByLibrary.simpleMessage(
            "Are you sure to abort the test?"),
        "acceptDataCorrectFormatted": MessageLookupByLibrary.simpleMessage(
            "I hereby confirm all data I provide is correct. In case of invalid data, free2**pass** will not be able to attest Covid test results."),
        "acceptPrivacyPolicy": MessageLookupByLibrary.simpleMessage(
            "I accept the **privacy policy**"),
        "acceptTermsOfUseFormatted": MessageLookupByLibrary.simpleMessage(
            "I accept the **terms of use**"),
        "add": MessageLookupByLibrary.simpleMessage("Save"),
        "address": MessageLookupByLibrary.simpleMessage("Address"),
        "addressHelper": MessageLookupByLibrary.simpleMessage(
            "Government-known street and house number"),
        "addressValidator": MessageLookupByLibrary.simpleMessage(
            "Please provide a valid street and house number"),
        "advancedDangerous":
            MessageLookupByLibrary.simpleMessage("Advanced (Dangerous)"),
        "advancedEncryption": MessageLookupByLibrary.simpleMessage(
            "Your data is transferred using advanced encryption."),
        "advancedEncryptionLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Your local health department **supports advanced encryption**. Using our unique encryption technology, we can **cryptographically ensure** no one but the **competent health department** is able to access your data in **case of a** Covid-19 **infection**."),
        "appointmentCancelError": MessageLookupByLibrary.simpleMessage(
            "Error canceling this appointment"),
        "appointmentCancelSuccessful": MessageLookupByLibrary.simpleMessage(
            "Successfully canceled appointment"),
        "appointmentEnablePush":
            MessageLookupByLibrary.simpleMessage("Enable push notifications"),
        "areYouSure": MessageLookupByLibrary.simpleMessage("Are you sure?"),
        "areYourConfidentWithThePhoto": MessageLookupByLibrary.simpleMessage(
            "Are your confident with the photo?"),
        "back": MessageLookupByLibrary.simpleMessage("Back"),
        "birthday": MessageLookupByLibrary.simpleMessage("Birthday"),
        "birthdayValidator":
            MessageLookupByLibrary.simpleMessage("Please provide a birthday."),
        "cancel": MessageLookupByLibrary.simpleMessage("Cancel"),
        "cancelAppointment":
            MessageLookupByLibrary.simpleMessage("Cancel appointment"),
        "cancelOtherReason":
            MessageLookupByLibrary.simpleMessage("Other reason"),
        "cancelReasonLong": MessageLookupByLibrary.simpleMessage(
            "Why would you like to cancel the appointment?"),
        "cancelUnavailable": MessageLookupByLibrary.simpleMessage(
            "I am unexpectedly busy at that time"),
        "cancelWrongTime": MessageLookupByLibrary.simpleMessage(
            "I chose the wrong date or time"),
        "certificate": MessageLookupByLibrary.simpleMessage("Certificate"),
        "certificateId": MessageLookupByLibrary.simpleMessage("Certificate ID"),
        "certificateInvalid":
            MessageLookupByLibrary.simpleMessage("Certificate invalid"),
        "certificateInvalidLong": MessageLookupByLibrary.simpleMessage(
            "This certificate does\nnot seem to be valid\nfor you."),
        "certificateIssuer":
            MessageLookupByLibrary.simpleMessage("Certificate issuer"),
        "certificateRefreshError": MessageLookupByLibrary.simpleMessage(
            "Error loading certificates. Are you sure you have a valid internet connection?"),
        "certificateRevoked": MessageLookupByLibrary.simpleMessage(
            "Your last test\'s result\nhas been **marked invalid**."),
        "certificateRevokedLong": MessageLookupByLibrary.simpleMessage(
            "Your last certificate has been revoked"),
        "changeData": MessageLookupByLibrary.simpleMessage("Change data"),
        "changePersonalData":
            MessageLookupByLibrary.simpleMessage("Change personal data"),
        "changeUser": MessageLookupByLibrary.simpleMessage("Change user"),
        "checkInIntroLongFormatted":
            MessageLookupByLibrary.simpleMessage("Some intro..."),
        "checkInLongFormatted": MessageLookupByLibrary.simpleMessage(
            "To enable **contact tracing**,\nyou can **simply scan a qr code**\nin participating locations\nin case you have a negative Covid-19 certificate."),
        "checkInNow": MessageLookupByLibrary.simpleMessage("**Check-in** now"),
        "checkInSuccessful":
            MessageLookupByLibrary.simpleMessage("Check-In successful"),
        "checkOut": MessageLookupByLibrary.simpleMessage("Check-Out"),
        "checkin": MessageLookupByLibrary.simpleMessage("Check-In"),
        "checkinSuccessfulLong": MessageLookupByLibrary.simpleMessage(
            "You can see all\ncheck-ins of the last\n14 days in the navigation drawer."),
        "chosePassportVerification": MessageLookupByLibrary.simpleMessage(
            "Okay, you chose to use your passport. Let\'s proceed."),
        "clear": MessageLookupByLibrary.simpleMessage("Clear"),
        "close": MessageLookupByLibrary.simpleMessage("Close"),
        "closed": MessageLookupByLibrary.simpleMessage("closed"),
        "codeValidator": MessageLookupByLibrary.simpleMessage(
            "Please enter a valid verification code."),
        "contactIssuer": MessageLookupByLibrary.simpleMessage("Contact issuer"),
        "continueTest": MessageLookupByLibrary.simpleMessage("Continue test"),
        "continueText": MessageLookupByLibrary.simpleMessage("Continue"),
        "couldNotSavePhoto": MessageLookupByLibrary.simpleMessage(
            "We unfortunately could not save the selected image."),
        "couldNotVerify": MessageLookupByLibrary.simpleMessage(
            "They could not verify your appointment."),
        "createAnOccasion":
            MessageLookupByLibrary.simpleMessage("Create an occasion"),
        "createAppointment":
            MessageLookupByLibrary.simpleMessage("Create appointment"),
        "createNewOccasion":
            MessageLookupByLibrary.simpleMessage("Create new **Occasion**"),
        "createUser": MessageLookupByLibrary.simpleMessage("Create **user**"),
        "createUserDataLongFormatted": MessageLookupByLibrary.simpleMessage(
            "# Create user\nPlease provide the user\'s personal data"),
        "createUsersLongFormatted": MessageLookupByLibrary.simpleMessage(
            "For e. g. family members\nwithout smartphone, you\ncan create user profiles to\nmanage their test results\n and check ins."),
        "currentUser": MessageLookupByLibrary.simpleMessage("Current user"),
        "currentlyClosed":
            MessageLookupByLibrary.simpleMessage("Currently closed"),
        "currentlyOpened":
            MessageLookupByLibrary.simpleMessage("Currently opened"),
        "darkBeta": MessageLookupByLibrary.simpleMessage("Dark (beta)"),
        "dateOfBirth": MessageLookupByLibrary.simpleMessage("Date of birth"),
        "dateOfVaccination":
            MessageLookupByLibrary.simpleMessage("Date of vaccination"),
        "day": MessageLookupByLibrary.simpleMessage("Day"),
        "deleteCertificates":
            MessageLookupByLibrary.simpleMessage("Delete certificates"),
        "deleteUser": MessageLookupByLibrary.simpleMessage("Delete user"),
        "deleteUserLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Are you sure to delete the user and all their certificates? This cannot be undone."),
        "directions": MessageLookupByLibrary.simpleMessage("Directions"),
        "displayLocationLongFormatted": MessageLookupByLibrary.simpleMessage(
            "You can select to **display you location on the map** and therefore grant us **access to your coarse location**.\nThis can help you to find participating locations close to you. If you choose to grant us this permission, **location access is used additionally** to verify you are **present in locations during a check in**.\nBoth uses of location happen **only locally on your device** and under **no circumstances, you location is transferred** to anyone."),
        "dontImport": MessageLookupByLibrary.simpleMessage("Don\'t import"),
        "duplicateUser":
            MessageLookupByLibrary.simpleMessage("Duplicate user found"),
        "duplicateUserLong": MessageLookupByLibrary.simpleMessage(
            "For the data you just entered, another user is already present. Please provide other user data or return to the user list."),
        "editPhoneNumber":
            MessageLookupByLibrary.simpleMessage("Edit phone number"),
        "editProvidedData":
            MessageLookupByLibrary.simpleMessage("Edit provided data"),
        "editUser": MessageLookupByLibrary.simpleMessage("Edit user"),
        "emailAddress":
            MessageLookupByLibrary.simpleMessage("Email address (optional)"),
        "emailValidator": MessageLookupByLibrary.simpleMessage(
            "Optionally specify - This email address is invalid."),
        "encryptingUserData": MessageLookupByLibrary.simpleMessage(
            "Please wait, we encrypt your user data."),
        "encryption": MessageLookupByLibrary.simpleMessage("Encryption"),
        "end": MessageLookupByLibrary.simpleMessage("End"),
        "enterVerificationCodeLong": MessageLookupByLibrary.simpleMessage(
            "Please enter the verification code we sent you by SMS. Your code is valid for 10 minutes."),
        "errorContactingIssuer": MessageLookupByLibrary.simpleMessage(
            "We could not find any way to contact your certificate issuer."),
        "errorCreatingAppointment": MessageLookupByLibrary.simpleMessage(
            "Error creating the appointment."),
        "errorCreatingYourOccasion": MessageLookupByLibrary.simpleMessage(
            "Error creating your occasion.\nProbably, your community does not\nsupport this feature."),
        "errorLoadingLocationData": MessageLookupByLibrary.simpleMessage(
            "Error loading this location\'s data"),
        "errorNoExpiryProvided":
            MessageLookupByLibrary.simpleMessage("Error: No expiry provided."),
        "errorRequestingVerificationCode": MessageLookupByLibrary.simpleMessage(
            "We could not request a verification code. Are you sure you properly entered your phone number?"),
        "errorSavingUserData":
            MessageLookupByLibrary.simpleMessage("Error saving your user data"),
        "errorWringVerificationCode": MessageLookupByLibrary.simpleMessage(
            "We could not verify your code. Please check it again."),
        "expiredAt": MessageLookupByLibrary.simpleMessage("Expired at"),
        "expiredCertificates":
            MessageLookupByLibrary.simpleMessage("Past tests"),
        "filterLocations":
            MessageLookupByLibrary.simpleMessage("Filter locations"),
        "finish": MessageLookupByLibrary.simpleMessage("Finish"),
        "formatA4": MessageLookupByLibrary.simpleMessage("A4 (Default)"),
        "formatA5": MessageLookupByLibrary.simpleMessage("A5"),
        "formatUSLegal": MessageLookupByLibrary.simpleMessage("US Legal"),
        "formatUSLetter": MessageLookupByLibrary.simpleMessage("US Letter"),
        "friday": MessageLookupByLibrary.simpleMessage("Friday"),
        "from": MessageLookupByLibrary.simpleMessage("From"),
        "fullName": MessageLookupByLibrary.simpleMessage("Full name"),
        "fullNameHelper": MessageLookupByLibrary.simpleMessage(
            "Full name as noted on the identity card"),
        "fullNameValidator": MessageLookupByLibrary.simpleMessage(
            "Please provide both a given and a family name."),
        "fullyImmunized": MessageLookupByLibrary.simpleMessage(
            "Fully immunized for more than two weeks."),
        "generalVaccinationInformation":
            MessageLookupByLibrary.simpleMessage("General information"),
        "goBackHome": MessageLookupByLibrary.simpleMessage("Go back home"),
        "grant": MessageLookupByLibrary.simpleMessage("Grant"),
        "grantLocationAccess":
            MessageLookupByLibrary.simpleMessage("Grant location access"),
        "home": MessageLookupByLibrary.simpleMessage("Home"),
        "iUnderstood": MessageLookupByLibrary.simpleMessage("I understood"),
        "identityCheck": MessageLookupByLibrary.simpleMessage("Identity check"),
        "identityCheckLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Please show your\n**identity card** to\nthe test station staff."),
        "ifYouChangeYourPersonalDataAllPastCertificatesExpire":
            MessageLookupByLibrary.simpleMessage(
                "If you change your personal data, all past certificates expire."),
        "ifYouChoseToUseYourPassportInsteadOfA":
            MessageLookupByLibrary.simpleMessage(
                "If you chose to use your passport instead of a photo, you will need to show your passport every time you check in or do a Covid-19 test."),
        "ignore": MessageLookupByLibrary.simpleMessage("Ignore"),
        "imagePickError": MessageLookupByLibrary.simpleMessage(
            "I could not read the image. Maybe try another one."),
        "importAddressFromInitialUser": MessageLookupByLibrary.simpleMessage(
            "Import address from initial user"),
        "infoPrivateOccasion": MessageLookupByLibrary.simpleMessage(
            "This is a private occasion without address data"),
        "information": MessageLookupByLibrary.simpleMessage("Information"),
        "initialUser": MessageLookupByLibrary.simpleMessage("Initial user"),
        "injection":
            MessageLookupByLibrary.simpleMessage("Potentially malicious data"),
        "injectionLong": MessageLookupByLibrary.simpleMessage(
            "The data you entered looks like a so called injection, an attac commonly used by hackers to perform malicious actions on software. Please be aware of the fact that our software has been pentested and we normalize all data you input.\nIn case you are not a hacker, you can ignore this message."),
        "internalCertificateFormatted": MessageLookupByLibrary.simpleMessage(
            "**Attention:** internal certificate"),
        "internalCertificateLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Enterprise certificate\nwith **internal**\n**validity** only."),
        "intro1Bottom": MessageLookupByLibrary.simpleMessage(
            "**No more tiresome paperwork**\nin restaurant access,\nevent locations and other places!"),
        "intro1Head": MessageLookupByLibrary.simpleMessage(
            "free2**pass** your safe wallet\nfor **accessing Covid-19** test\ncertificates **anytime** right on\nyour smartphone."),
        "intro2Bottom": MessageLookupByLibrary.simpleMessage(
            "The remaining time permitted\nto enter locations using free2**pass**\nis displayed **at a glace**."),
        "intro2Head": MessageLookupByLibrary.simpleMessage(
            "Simpy scan the **qr code at**\n**participating issuers** and\none breath - your test result\nis right on your phone!"),
        "intro3Bottom": MessageLookupByLibrary.simpleMessage(
            "Just scan a qr code on site\nand in case of spreading infection, your data is\ntransferred to the competent authorities\nusing **end-to-end encryption**."),
        "intro3Head": MessageLookupByLibrary.simpleMessage(
            "You have to fill a contact tracing\nform in a restaurant, fitness center\nor at a hair cut?\n**Never ever with free2pass!**"),
        "intro4Bottom": MessageLookupByLibrary.simpleMessage(
            "Want to learn more?\nCheck our **FAQ**."),
        "intro4Head": MessageLookupByLibrary.simpleMessage(
            "In modern times, privacy is\na universal right.\nfree2**pass** is **100%**\n**GDPR compliant, tamper-proof**\nand multiple times **RSA encrypted.**"),
        "invalid": MessageLookupByLibrary.simpleMessage("Invalid"),
        "invalidCheckInCode":
            MessageLookupByLibrary.simpleMessage("Invalid Check-In code"),
        "invalidCheckInCodeLong": MessageLookupByLibrary.simpleMessage(
            "This check-in code\ndoes not seem to be\neligible for you."),
        "invalidCryptoQr":
            MessageLookupByLibrary.simpleMessage("Invalid Crypto QR"),
        "invalidCryptoQrLongFormatted": MessageLookupByLibrary.simpleMessage(
            "This does not seem to\nbe a valid free2**pass**\nCrypto QR code. Are you\nsure you scanned the right code?"),
        "invalidLocation": MessageLookupByLibrary.simpleMessage(
            "You are not at the location you want to check-in at."),
        "invalidLocationLongFormatted": MessageLookupByLibrary.simpleMessage(
            "To avoid abuse, we verify whether you are close to the location you want to check in at.."),
        "issued": MessageLookupByLibrary.simpleMessage("Issued"),
        "issuer": MessageLookupByLibrary.simpleMessage("Issuer"),
        "issuerClosed":
            MessageLookupByLibrary.simpleMessage("The issuer is closed."),
        "issuerRevokedAppointment": MessageLookupByLibrary.simpleMessage(
            "The issuer revoked this appointment."),
        "issuerVerifiedFormatted":
            MessageLookupByLibrary.simpleMessage("Next **step**"),
        "issuers": MessageLookupByLibrary.simpleMessage("Issuers"),
        "keep": MessageLookupByLibrary.simpleMessage("Keep"),
        "keepAvailable":
            MessageLookupByLibrary.simpleMessage("Keep available:"),
        "language": MessageLookupByLibrary.simpleMessage("Language"),
        "later": MessageLookupByLibrary.simpleMessage("Later"),
        "learnMore": MessageLookupByLibrary.simpleMessage("Learn more"),
        "legalNotice": MessageLookupByLibrary.simpleMessage("Legal notice"),
        "light": MessageLookupByLibrary.simpleMessage("Light"),
        "locateMe": MessageLookupByLibrary.simpleMessage("Locate me"),
        "locationRequired":
            MessageLookupByLibrary.simpleMessage("Location required"),
        "locationRequiredLongFormatted": MessageLookupByLibrary.simpleMessage(
            "To avoid abuse, location\naccess is required to verify\nwhether you are really present\nat the location you want\nto check in.\n\nYour location is\nbeing processed\nentirely locally."),
        "locationType": MessageLookupByLibrary.simpleMessage("Location type"),
        "locationTypes": MessageLookupByLibrary.simpleMessage("Location types"),
        "locationsInArea":
            MessageLookupByLibrary.simpleMessage("Locations in area:"),
        "lookingForQrCode":
            MessageLookupByLibrary.simpleMessage("Looking for QR code..."),
        "matchingLocations":
            MessageLookupByLibrary.simpleMessage("Matching locations"),
        "merchant": MessageLookupByLibrary.simpleMessage("Merchant"),
        "merchants": MessageLookupByLibrary.simpleMessage("Merchants"),
        "monday": MessageLookupByLibrary.simpleMessage("Monday"),
        "month": MessageLookupByLibrary.simpleMessage("Month"),
        "multiUser": MessageLookupByLibrary.simpleMessage("Multi-User"),
        "name": MessageLookupByLibrary.simpleMessage("Name"),
        "nameAlreadySaved": MessageLookupByLibrary.simpleMessage(
            "Seems link you already told us your name. Let\'s proceed."),
        "negatie": MessageLookupByLibrary.simpleMessage("Negative"),
        "negativeLong":
            MessageLookupByLibrary.simpleMessage("Tested negativ on Covid-19"),
        "newTestResultPending":
            MessageLookupByLibrary.simpleMessage("New test result pending"),
        "next": MessageLookupByLibrary.simpleMessage("Next"),
        "nextCertificate":
            MessageLookupByLibrary.simpleMessage("Next certificate"),
        "nextDay": MessageLookupByLibrary.simpleMessage("Next day"),
        "no": MessageLookupByLibrary.simpleMessage("No"),
        "noConnectionLongFormatted": MessageLookupByLibrary.simpleMessage(
            "# No internet connection\nIt seems like there is no internet\nconnection. To proceed, please\nensure you have a valid\ninternet connection."),
        "noCurrentCertificateLongFormatted":
            MessageLookupByLibrary.simpleMessage(
                "There is **no current**\n**test result** available."),
        "noExpiredCertificatesLongFormatted":
            MessageLookupByLibrary.simpleMessage(
                "Seems like\nyou have **no past**\n**certificates**."),
        "noInformation":
            MessageLookupByLibrary.simpleMessage("I prefer not to answer"),
        "noMoreTests": MessageLookupByLibrary.simpleMessage(
            "No more tests available for today."),
        "noPhotoAvailable":
            MessageLookupByLibrary.simpleMessage("No photo available"),
        "noPhotoAvailableLong": MessageLookupByLibrary.simpleMessage(
            "Please manually verify the user\'s identity card"),
        "noRecentLocationsLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Seems like\nyou have **no recent**\n**locations**."),
        "notAVaccinationCertificate": MessageLookupByLibrary.simpleMessage(
            "This does not seem to be a valid vaccination certificate."),
        "notFound": MessageLookupByLibrary.simpleMessage("404 - Not found"),
        "notificationChannelTestResults":
            MessageLookupByLibrary.simpleMessage("Test results"),
        "notificationChannelTestResultsDetails":
            MessageLookupByLibrary.simpleMessage(
                "Information on new Covid-19 test results"),
        "notificationChannelWebSocket":
            MessageLookupByLibrary.simpleMessage("Server connection"),
        "notificationChannelWebSocketLong": MessageLookupByLibrary.simpleMessage(
            "This silent notification is used to keep an internet connection while the app is closed."),
        "notificationResultAvailable": MessageLookupByLibrary.simpleMessage(
            "Your test result is available!"),
        "notificationSilentContent": MessageLookupByLibrary.simpleMessage(
            "Waiting for your test result..."),
        "oClock": MessageLookupByLibrary.simpleMessage("o\'clock"),
        "occasionAutoCheckIn": MessageLookupByLibrary.simpleMessage(
            "Automatically Check-In yourself"),
        "occasionNameLong": MessageLookupByLibrary.simpleMessage(
            "Please name this occasion. The name is displayed for all attendees but never transferred to the health department."),
        "occasionTime": MessageLookupByLibrary.simpleMessage(
            "Please select the time of your occasion"),
        "okay": MessageLookupByLibrary.simpleMessage("Okay"),
        "ongoingOccasions":
            MessageLookupByLibrary.simpleMessage("Your ongoing occasions:"),
        "onlyShowFree2passenablesLocations":
            MessageLookupByLibrary.simpleMessage(
                "Only show free2pass-enabled locations"),
        "openPrivacyPolicy":
            MessageLookupByLibrary.simpleMessage("Open privacy policy"),
        "openSourceLicenses":
            MessageLookupByLibrary.simpleMessage("Open source licenses"),
        "openUsagePolicy":
            MessageLookupByLibrary.simpleMessage("Open usage policy"),
        "openWebsite": MessageLookupByLibrary.simpleMessage("Open website"),
        "openingHours": MessageLookupByLibrary.simpleMessage("Opening hours"),
        "optional": MessageLookupByLibrary.simpleMessage("Optionally specify"),
        "otherUsers": MessageLookupByLibrary.simpleMessage("Other users"),
        "partiallyVaccinated": MessageLookupByLibrary.simpleMessage(
            "Only partially vaccinated yet."),
        "pass": MessageLookupByLibrary.simpleMessage("Pass"),
        "pdfBirth": MessageLookupByLibrary.simpleMessage("Birth:"),
        "pdfCertificateSigned": MessageLookupByLibrary.simpleMessage(
            "Scan the crypto QR code\nto verify whether the\ncertificate is authentic!"),
        "pdfLegalNoTransfer": MessageLookupByLibrary.simpleMessage(
            "Use of this document is only permitted for the given person. Any transfer or copy of this documents results an immediate expiry of this test certificate."),
        "pdfLegalNotice": MessageLookupByLibrary.simpleMessage(
            "free2pass GmbH | Hamburger Allee 2-4 | D-30161 Hannover\nhello@free2pass.de | freepass.de"),
        "pdfLegalSignature": MessageLookupByLibrary.simpleMessage(
            "This document hast been automatically created and is valid without signature."),
        "pdfNegativeLong":
            MessageLookupByLibrary.simpleMessage("Test result: NEGATIVE"),
        "pdfNegativeTestLegal": MessageLookupByLibrary.simpleMessage(
            "A negative test result cannot rule out a COVID-19 infection for sure. This document only certifies the health state at the time of creation."),
        "pdfNoPhoto":
            MessageLookupByLibrary.simpleMessage("No photo\nprovided"),
        "pdfOfficial": MessageLookupByLibrary.simpleMessage("Official"),
        "pdfPoCTest": MessageLookupByLibrary.simpleMessage(
            "PoC antigen test on SARS-CoV-2"),
        "pdfTestCertificate":
            MessageLookupByLibrary.simpleMessage("test certificate"),
        "pdfTestTime": MessageLookupByLibrary.simpleMessage("Test time: "),
        "pdfTestUsed": MessageLookupByLibrary.simpleMessage("Test used"),
        "pdfUnknownTestType":
            MessageLookupByLibrary.simpleMessage("Unknown test type"),
        "pdfValidUntil": MessageLookupByLibrary.simpleMessage("Valid until"),
        "pending": MessageLookupByLibrary.simpleMessage("Pending"),
        "pendingTestLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Your last test\'s result\nis **still pending**.\nWe inform you as soon as\nyour result is available!"),
        "pendingTestResult":
            MessageLookupByLibrary.simpleMessage("Test result pending"),
        "pendingTestResultLong": MessageLookupByLibrary.simpleMessage(
            "Your last test result\nis still pending.\nCome back later to\nsee whether your\nresult is available!"),
        "permanentlyRemove":
            MessageLookupByLibrary.simpleMessage("Permanently remove"),
        "personalDataLongFormatted": MessageLookupByLibrary.simpleMessage(
            "# Personal Data\nYour personal data is required to ensure a Covid-19 test certificate is only usable for the person it was issued to. Again, this data is only stored locally and is transferred to the Covid-19 test issuers using end-to-end encryption."),
        "phoneAlreadyVerifiedLong": MessageLookupByLibrary.simpleMessage(
            "Seems link you already verified your phone number. Let\'s proceed."),
        "phoneNumberInputHelper": MessageLookupByLibrary.simpleMessage(
            "You can optionally leave out the international part."),
        "phoneNumberLongFormatted": MessageLookupByLibrary.simpleMessage(
            "# Phone number\nWe need to verify your phone number once to ensure the health department can contact you. Your phone number will not be stored on our servers but only on your device. We are hence unable to match any information we have to your identity."),
        "phoneNumberLostDescription": MessageLookupByLibrary.simpleMessage(
            "Unfortunately, there was a bug in a previous version of free2pass, which could cause the phone number to be lost. You will need to re-verify your phone number in the next step."),
        "phoneNumberLostTitle":
            MessageLookupByLibrary.simpleMessage("Phone number lost"),
        "phoneNumberValidator": MessageLookupByLibrary.simpleMessage(
            "Please enter a valid phone number."),
        "photoAlreadySaved": MessageLookupByLibrary.simpleMessage(
            "Seems link you already saved a photo. Let\'s proceed."),
        "photoValidator":
            MessageLookupByLibrary.simpleMessage("Please select a photo"),
        "pin": MessageLookupByLibrary.simpleMessage("PIN"),
        "pleaseProvideAValidTown":
            MessageLookupByLibrary.simpleMessage("Please provide a valid town"),
        "pleaseProvideAValidZipCode": MessageLookupByLibrary.simpleMessage(
            "Please provide a valid ZIP code."),
        "pleaseTellMeYourName":
            MessageLookupByLibrary.simpleMessage("Please tell me your name."),
        "positive": MessageLookupByLibrary.simpleMessage("Positive"),
        "positiveComment":
            MessageLookupByLibrary.simpleMessage("Important notice"),
        "positiveLong":
            MessageLookupByLibrary.simpleMessage("Tested positiv on Covid-19"),
        "positiveTestNotice": MessageLookupByLibrary.simpleMessage(
            "Your test result is being transferred\n automatically to the competent health department.\n\n**Please respect the current**\n\n**[quarantine regulations](https://www.infektionsschutz.de/coronavirus/tests-auf-sars-cov-2/antigen-schnelltest.html#c14955)!**"),
        "poweredByOpenstreetmap":
            MessageLookupByLibrary.simpleMessage("Powered by OpenStreetMap"),
        "previousCertificate":
            MessageLookupByLibrary.simpleMessage("Previous certificate"),
        "previousDay": MessageLookupByLibrary.simpleMessage("Previous day"),
        "privacyPolicy": MessageLookupByLibrary.simpleMessage("Privacy policy"),
        "privateOccasion":
            MessageLookupByLibrary.simpleMessage("Private occasion"),
        "providePhoneNumberLong": MessageLookupByLibrary.simpleMessage(
            "Please provide your phone number. We will send you an SMS to verify your identity.\n**Please note**: Since the laste update, we improved our security mechanisms. You unfortunately need to verify your phone number again hence."),
        "pushNotifications":
            MessageLookupByLibrary.simpleMessage("Push notifications"),
        "pushNotificationsEnabledTapToDisable":
            MessageLookupByLibrary.simpleMessage(
                "Push notifications enabled. Tap to disable."),
        "pushPrivacyNotice": MessageLookupByLibrary.simpleMessage(
            "You can get notifications in case appointments are canceled or test results are available. Please note that this harms your privacy."),
        "quitApp": MessageLookupByLibrary.simpleMessage("Quit app"),
        "readyToGo": MessageLookupByLibrary.simpleMessage("Ready to go!"),
        "reason": MessageLookupByLibrary.simpleMessage("Reason"),
        "recentLocations": MessageLookupByLibrary.simpleMessage("Locations"),
        "refresh": MessageLookupByLibrary.simpleMessage("Refresh"),
        "remove": MessageLookupByLibrary.simpleMessage("Remove"),
        "removeVaccinationCertificate": MessageLookupByLibrary.simpleMessage(
            "Remove vaccination certificate"),
        "removeVaccinationCertificateLongFormatted":
            MessageLookupByLibrary.simpleMessage(
                "Are you sure to permanently remove the following vaccination certificate from you device:"),
        "requestNewCode":
            MessageLookupByLibrary.simpleMessage("Request new code"),
        "requestNewTestResult":
            MessageLookupByLibrary.simpleMessage("Request new test result"),
        "requestVerificationCode":
            MessageLookupByLibrary.simpleMessage("Request verification code"),
        "reset": MessageLookupByLibrary.simpleMessage("Reset"),
        "resetAppLongFormatted": MessageLookupByLibrary.simpleMessage(
            "This will reset the app preferences as well as your personal data. Moreover, **all certificates, locations and contacts** will be removed from your device."),
        "resetFilters": MessageLookupByLibrary.simpleMessage("Reset filters"),
        "resetFree2passFormatted":
            MessageLookupByLibrary.simpleMessage("Reset free2**pass**"),
        "resetRequired": MessageLookupByLibrary.simpleMessage("Reset required"),
        "restartSetup": MessageLookupByLibrary.simpleMessage("Restart setup"),
        "retry": MessageLookupByLibrary.simpleMessage("Retry"),
        "returnHome": MessageLookupByLibrary.simpleMessage("Return home"),
        "saturday": MessageLookupByLibrary.simpleMessage("Saturday"),
        "save": MessageLookupByLibrary.simpleMessage("Save"),
        "scanCheckInCode":
            MessageLookupByLibrary.simpleMessage("Scan check-in code"),
        "scanNewTest":
            MessageLookupByLibrary.simpleMessage("**Scan** new test"),
        "scanVaccinationCertificate": MessageLookupByLibrary.simpleMessage(
            "Scan an official vaccination certificate"),
        "search": MessageLookupByLibrary.simpleMessage("Search"),
        "seemsLikenyouHavenonpastCheckins":
            MessageLookupByLibrary.simpleMessage(
                "Seems like you have **no past Check-Ins**"),
        "selectPhoto": MessageLookupByLibrary.simpleMessage("Select photo"),
        "selectPhotoLongFormatted": MessageLookupByLibrary.simpleMessage(
            "# Select photo\nYou can provide a photo depicting your face to **simplify the identity control** at Covid-19 test certificate issuer or locations you want to check in. For this, we either require access to your **photo gallery**, a **file from the internal storage** or **access to your camera** to import a selfie. If you grant camera access, this enables you to **scan Covid-19 test and check in QR codes** too. Under **no circumstances**, the photo you choose is **transferred to anyone**. You can alternatively decide to use your **identity card for verification** in test centers and locations."),
        "selectSelfie":
            MessageLookupByLibrary.simpleMessage("Select selfie from Gallery"),
        "selectSlot": MessageLookupByLibrary.simpleMessage("Select slot"),
        "selectThisSlot":
            MessageLookupByLibrary.simpleMessage("Select this slot"),
        "settings": MessageLookupByLibrary.simpleMessage("Settings"),
        "shareAsPdf": MessageLookupByLibrary.simpleMessage("Share as PDF"),
        "showDeviceTestStation": MessageLookupByLibrary.simpleMessage(
            "Please show your device to the test station staff."),
        "showLastVisitedFormatted": MessageLookupByLibrary.simpleMessage(
            "**View all** places\nyou visited"),
        "showPassport": MessageLookupByLibrary.simpleMessage("Show passport"),
        "standardEncryption": MessageLookupByLibrary.simpleMessage(
            "Your data is transferred using standard encryption."),
        "standardEncryptionLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Unfortunately, some community\'s health departments **oppose any kind of data security**. In this case, we cannot use our advanced encryption technology. Even though the communication happens using one **layer of security less** than usually, your data remains **end-to-end encrypted**. Feel free to **appeal to your community** to enable advanced encryption."),
        "start": MessageLookupByLibrary.simpleMessage("Start"),
        "startScanVaccinationCertificate": MessageLookupByLibrary.simpleMessage(
            "Scan vaccination certificate"),
        "startTestNow": MessageLookupByLibrary.simpleMessage("Start test now"),
        "store": MessageLookupByLibrary.simpleMessage("Store"),
        "successfullyCheckedIn":
            MessageLookupByLibrary.simpleMessage("Successfully checked in."),
        "successfullyReservedSlot":
            MessageLookupByLibrary.simpleMessage("Successfully reserved slot!"),
        "sunday": MessageLookupByLibrary.simpleMessage("Sunday"),
        "takeSelfie": MessageLookupByLibrary.simpleMessage("Take a selfie"),
        "tapToEnlarge": MessageLookupByLibrary.simpleMessage("Tap to enlarge."),
        "tapToShowQr":
            MessageLookupByLibrary.simpleMessage("Tap to show Crypto-QR"),
        "termsOfService":
            MessageLookupByLibrary.simpleMessage("Terms of service"),
        "testCenter": MessageLookupByLibrary.simpleMessage("Test center"),
        "testedLong":
            MessageLookupByLibrary.simpleMessage("Tested on Covid-19"),
        "thatsNotAMatchingQrCode": MessageLookupByLibrary.simpleMessage(
            "That\'s not a matching QR code."),
        "theRequestedPageWasNotFound": MessageLookupByLibrary.simpleMessage(
            "The requested page was not found."),
        "theme": MessageLookupByLibrary.simpleMessage("Theme"),
        "themeDefault": MessageLookupByLibrary.simpleMessage("Default"),
        "thursday": MessageLookupByLibrary.simpleMessage("Thursday"),
        "time": MessageLookupByLibrary.simpleMessage("Time"),
        "timezone": MessageLookupByLibrary.simpleMessage("timezone"),
        "to": MessageLookupByLibrary.simpleMessage("To"),
        "today": MessageLookupByLibrary.simpleMessage("Today,"),
        "toggleFlash": MessageLookupByLibrary.simpleMessage("Toggle flash"),
        "tooManyAppointments":
            MessageLookupByLibrary.simpleMessage("Too many appointments."),
        "tooManyAttempts":
            MessageLookupByLibrary.simpleMessage("Too many attempts"),
        "tooManyAttemptsLong": MessageLookupByLibrary.simpleMessage(
            "To avoid abuse, registration attempts are limited. Please wait a minute or two and try again."),
        "town": MessageLookupByLibrary.simpleMessage("Town"),
        "tuesday": MessageLookupByLibrary.simpleMessage("Tuesday"),
        "tutorial":
            MessageLookupByLibrary.simpleMessage("That\'s how **it works**"),
        "uniqueCertificateIdentifier": MessageLookupByLibrary.simpleMessage(
            "Unique certificate identifier"),
        "unknownReason":
            MessageLookupByLibrary.simpleMessage("Unknown reason."),
        "unsupportedVaccinationCertificate":
            MessageLookupByLibrary.simpleMessage(
                "Unsupported version of the digital health certificate:"),
        "upcomingAppointment":
            MessageLookupByLibrary.simpleMessage("Upcoming appointment"),
        "updateAvailable":
            MessageLookupByLibrary.simpleMessage("Update available"),
        "updateMandatoryLongFormatted": MessageLookupByLibrary.simpleMessage(
            "This update is mandatory. You won\'t be able to use free2pass until you update."),
        "updateNow": MessageLookupByLibrary.simpleMessage("Update now"),
        "updateOptionalLongFormatted": MessageLookupByLibrary.simpleMessage(
            "We highly recommend to update. If you cannot do this right now, you can come back later and start the update."),
        "usePassport": MessageLookupByLibrary.simpleMessage("Use passport"),
        "usePassportLong": MessageLookupByLibrary.simpleMessage(
            "In this case, you need to show you passort every time you check in or do a Covid-19 test."),
        "vaccinatedQuestion": MessageLookupByLibrary.simpleMessage(
            "Have you been vaccinated against Covid-19?"),
        "vaccinatedQuestionExplanation": MessageLookupByLibrary.simpleMessage(
            "Your response won\'t give any advantage. We are required to ask you for statistical reasons only."),
        "vaccinationCenter":
            MessageLookupByLibrary.simpleMessage("Vaccination center"),
        "vaccinationCertificate":
            MessageLookupByLibrary.simpleMessage("Vaccination Certificate"),
        "vaccinationCertificateTitle": MessageLookupByLibrary.simpleMessage(
            "Official COVID-19 vaccination certificate"),
        "vaccinationCertificateVerificationNotice":
            MessageLookupByLibrary.simpleMessage(
                "Warning: This vaccination certificate is only valid in case your digitally verify it."),
        "vaccinationInformationParagraphs": MessageLookupByLibrary.simpleMessage(
            "Please note that a digital vaccination certificate does not automatically permit for traveling within the European Union. Further information on the ongoing Covid-19 spread as well as the current traveling restrictions in the European union can be found at [reopen.europa.eu](https://reopen.europa.eu/).  \nInformation on the digital vaccination certificate can be found at [www.digitaler-impfnachweis-app.de](https://www.digitaler-impfnachweis-app.de/)."),
        "vaccinationNumber": m0,
        "vaccinationStateImmunized":
            MessageLookupByLibrary.simpleMessage("**Twice**"),
        "vaccinationStateImmunizedLong":
            MessageLookupByLibrary.simpleMessage("I am **fully immunized**"),
        "vaccinationStateNone":
            MessageLookupByLibrary.simpleMessage("**Never**"),
        "vaccinationStateNoneLong": MessageLookupByLibrary.simpleMessage(
            "I have **not** been vaccinated **yet**"),
        "vaccinationStatePartial":
            MessageLookupByLibrary.simpleMessage("**Once**"),
        "vaccinationStatePartialLong": MessageLookupByLibrary.simpleMessage(
            "**Partial** - I have been vaccinated once and **wait for the second** vaccination"),
        "vaccinationStations":
            MessageLookupByLibrary.simpleMessage("Vaccination stations"),
        "validVaccinationCertificateFound":
            MessageLookupByLibrary.simpleMessage(
                "Valid vaccination certificate found."),
        "validatorOccasionName": MessageLookupByLibrary.simpleMessage(
            "Please create a sound name for your occasion."),
        "validatorOccasionRange": MessageLookupByLibrary.simpleMessage(
            "Please select the time of the occasion"),
        "validity": MessageLookupByLibrary.simpleMessage("Validity"),
        "verificationSuccessful":
            MessageLookupByLibrary.simpleMessage("Verification successful"),
        "verify": MessageLookupByLibrary.simpleMessage("Verify"),
        "version": MessageLookupByLibrary.simpleMessage("Version"),
        "waitingForFullProtection": MessageLookupByLibrary.simpleMessage(
            "Fully vaccinated but period of two weeks not passed yet."),
        "weEncryptYourData":
            MessageLookupByLibrary.simpleMessage("We encrypt your data..."),
        "webVaccinationCertificate": MessageLookupByLibrary.simpleMessage(
            "For technical reasons, the digital vaccination certificate is unfortunately only available in the native mobile app."),
        "website": MessageLookupByLibrary.simpleMessage("Website"),
        "wednesday": MessageLookupByLibrary.simpleMessage("Wednesday"),
        "welcomeToFree2pass":
            MessageLookupByLibrary.simpleMessage("Welcome to free2pass!"),
        "work": MessageLookupByLibrary.simpleMessage("Work"),
        "year": MessageLookupByLibrary.simpleMessage("Year"),
        "yes": MessageLookupByLibrary.simpleMessage("Yes"),
        "youCanceledAppointment": MessageLookupByLibrary.simpleMessage(
            "You canceled this appointment."),
        "youChangedImportantUserDataIfYouProceedAllCertificates":
            MessageLookupByLibrary.simpleMessage(
                "You changed important user data. If you proceed, all certificates for this user will expire. Are you sure to proceed?"),
        "yourPhoneNumber":
            MessageLookupByLibrary.simpleMessage("Your phone number"),
        "yourVerificationCode":
            MessageLookupByLibrary.simpleMessage("Your verification code"),
        "zipCode": MessageLookupByLibrary.simpleMessage("Zip code")
      };
}
