// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Your last test result\nis still pending.\nCome back later to\nsee whether your\nresult is available!`
  String get pendingTestResultLong {
    return Intl.message(
      'Your last test result\nis still pending.\nCome back later to\nsee whether your\nresult is available!',
      name: 'pendingTestResultLong',
      desc: '',
      args: [],
    );
  }

  /// `Looking for QR code...`
  String get lookingForQrCode {
    return Intl.message(
      'Looking for QR code...',
      name: 'lookingForQrCode',
      desc: '',
      args: [],
    );
  }

  /// `Toggle flash`
  String get toggleFlash {
    return Intl.message(
      'Toggle flash',
      name: 'toggleFlash',
      desc: '',
      args: [],
    );
  }

  /// `That's not a matching QR code.`
  String get thatsNotAMatchingQrCode {
    return Intl.message(
      'That\'s not a matching QR code.',
      name: 'thatsNotAMatchingQrCode',
      desc: '',
      args: [],
    );
  }

  /// `Back`
  String get back {
    return Intl.message(
      'Back',
      name: 'back',
      desc: '',
      args: [],
    );
  }

  /// `Next`
  String get next {
    return Intl.message(
      'Next',
      name: 'next',
      desc: '',
      args: [],
    );
  }

  /// `Finish`
  String get finish {
    return Intl.message(
      'Finish',
      name: 'finish',
      desc: '',
      args: [],
    );
  }

  /// `Select selfie from Gallery`
  String get selectSelfie {
    return Intl.message(
      'Select selfie from Gallery',
      name: 'selectSelfie',
      desc: '',
      args: [],
    );
  }

  /// `Take a selfie`
  String get takeSelfie {
    return Intl.message(
      'Take a selfie',
      name: 'takeSelfie',
      desc: '',
      args: [],
    );
  }

  /// `Seems link you already saved a photo. Let's proceed.`
  String get photoAlreadySaved {
    return Intl.message(
      'Seems link you already saved a photo. Let\'s proceed.',
      name: 'photoAlreadySaved',
      desc: '',
      args: [],
    );
  }

  /// `We unfortunately could not save the selected image.`
  String get couldNotSavePhoto {
    return Intl.message(
      'We unfortunately could not save the selected image.',
      name: 'couldNotSavePhoto',
      desc: '',
      args: [],
    );
  }

  /// `Please provide your phone number. We will send you an SMS to verify your identity.\n**Please note**: Since the laste update, we improved our security mechanisms. You unfortunately need to verify your phone number again hence.`
  String get providePhoneNumberLong {
    return Intl.message(
      'Please provide your phone number. We will send you an SMS to verify your identity.\n**Please note**: Since the laste update, we improved our security mechanisms. You unfortunately need to verify your phone number again hence.',
      name: 'providePhoneNumberLong',
      desc: '',
      args: [],
    );
  }

  /// `Your phone number`
  String get yourPhoneNumber {
    return Intl.message(
      'Your phone number',
      name: 'yourPhoneNumber',
      desc: '',
      args: [],
    );
  }

  /// `You can optionally leave out the international part.`
  String get phoneNumberInputHelper {
    return Intl.message(
      'You can optionally leave out the international part.',
      name: 'phoneNumberInputHelper',
      desc: '',
      args: [],
    );
  }

  /// `Request verification code`
  String get requestVerificationCode {
    return Intl.message(
      'Request verification code',
      name: 'requestVerificationCode',
      desc: '',
      args: [],
    );
  }

  /// `Please enter the verification code we sent you by SMS. Your code is valid for 10 minutes.`
  String get enterVerificationCodeLong {
    return Intl.message(
      'Please enter the verification code we sent you by SMS. Your code is valid for 10 minutes.',
      name: 'enterVerificationCodeLong',
      desc: '',
      args: [],
    );
  }

  /// `Your verification code`
  String get yourVerificationCode {
    return Intl.message(
      'Your verification code',
      name: 'yourVerificationCode',
      desc: '',
      args: [],
    );
  }

  /// `Verify`
  String get verify {
    return Intl.message(
      'Verify',
      name: 'verify',
      desc: '',
      args: [],
    );
  }

  /// `Seems link you already verified your phone number. Let's proceed.`
  String get phoneAlreadyVerifiedLong {
    return Intl.message(
      'Seems link you already verified your phone number. Let\'s proceed.',
      name: 'phoneAlreadyVerifiedLong',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a valid phone number.`
  String get phoneNumberValidator {
    return Intl.message(
      'Please enter a valid phone number.',
      name: 'phoneNumberValidator',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a valid verification code.`
  String get codeValidator {
    return Intl.message(
      'Please enter a valid verification code.',
      name: 'codeValidator',
      desc: '',
      args: [],
    );
  }

  /// `We could not request a verification code. Are you sure you properly entered your phone number?`
  String get errorRequestingVerificationCode {
    return Intl.message(
      'We could not request a verification code. Are you sure you properly entered your phone number?',
      name: 'errorRequestingVerificationCode',
      desc: '',
      args: [],
    );
  }

  /// `We could not verify your code. Please check it again.`
  String get errorWringVerificationCode {
    return Intl.message(
      'We could not verify your code. Please check it again.',
      name: 'errorWringVerificationCode',
      desc: '',
      args: [],
    );
  }

  /// `Please tell me your name.`
  String get pleaseTellMeYourName {
    return Intl.message(
      'Please tell me your name.',
      name: 'pleaseTellMeYourName',
      desc: '',
      args: [],
    );
  }

  /// `Full name`
  String get fullName {
    return Intl.message(
      'Full name',
      name: 'fullName',
      desc: '',
      args: [],
    );
  }

  /// `Full name as noted on the identity card`
  String get fullNameHelper {
    return Intl.message(
      'Full name as noted on the identity card',
      name: 'fullNameHelper',
      desc: '',
      args: [],
    );
  }

  /// `Save`
  String get save {
    return Intl.message(
      'Save',
      name: 'save',
      desc: '',
      args: [],
    );
  }

  /// `Seems link you already told us your name. Let's proceed.`
  String get nameAlreadySaved {
    return Intl.message(
      'Seems link you already told us your name. Let\'s proceed.',
      name: 'nameAlreadySaved',
      desc: '',
      args: [],
    );
  }

  /// `Please provide both a given and a family name.`
  String get fullNameValidator {
    return Intl.message(
      'Please provide both a given and a family name.',
      name: 'fullNameValidator',
      desc: '',
      args: [],
    );
  }

  /// `Welcome to free2pass!`
  String get welcomeToFree2pass {
    return Intl.message(
      'Welcome to free2pass!',
      name: 'welcomeToFree2pass',
      desc: '',
      args: [],
    );
  }

  /// `Keep available:`
  String get keepAvailable {
    return Intl.message(
      'Keep available:',
      name: 'keepAvailable',
      desc: '',
      args: [],
    );
  }

  /// `A photo depicting your face / your camera`
  String get aPhotoDepictingYourFaceYourCamera {
    return Intl.message(
      'A photo depicting your face / your camera',
      name: 'aPhotoDepictingYourFaceYourCamera',
      desc: '',
      args: [],
    );
  }

  /// `Terms of service`
  String get termsOfService {
    return Intl.message(
      'Terms of service',
      name: 'termsOfService',
      desc: '',
      args: [],
    );
  }

  /// `Legal notice`
  String get legalNotice {
    return Intl.message(
      'Legal notice',
      name: 'legalNotice',
      desc: '',
      args: [],
    );
  }

  /// `Privacy policy`
  String get privacyPolicy {
    return Intl.message(
      'Privacy policy',
      name: 'privacyPolicy',
      desc: '',
      args: [],
    );
  }

  /// `Ready to go!`
  String get readyToGo {
    return Intl.message(
      'Ready to go!',
      name: 'readyToGo',
      desc: '',
      args: [],
    );
  }

  /// `**Scan** new test`
  String get scanNewTest {
    return Intl.message(
      '**Scan** new test',
      name: 'scanNewTest',
      desc: '',
      args: [],
    );
  }

  /// `Pass`
  String get pass {
    return Intl.message(
      'Pass',
      name: 'pass',
      desc: '',
      args: [],
    );
  }

  /// `Check-In`
  String get checkin {
    return Intl.message(
      'Check-In',
      name: 'checkin',
      desc: '',
      args: [],
    );
  }

  /// `Negative`
  String get negatie {
    return Intl.message(
      'Negative',
      name: 'negatie',
      desc: '',
      args: [],
    );
  }

  /// `Positive`
  String get positive {
    return Intl.message(
      'Positive',
      name: 'positive',
      desc: '',
      args: [],
    );
  }

  /// `Pending`
  String get pending {
    return Intl.message(
      'Pending',
      name: 'pending',
      desc: '',
      args: [],
    );
  }

  /// `Invalid`
  String get invalid {
    return Intl.message(
      'Invalid',
      name: 'invalid',
      desc: '',
      args: [],
    );
  }

  /// `Tested negativ on Covid-19`
  String get negativeLong {
    return Intl.message(
      'Tested negativ on Covid-19',
      name: 'negativeLong',
      desc: '',
      args: [],
    );
  }

  /// `Tested positiv on Covid-19`
  String get positiveLong {
    return Intl.message(
      'Tested positiv on Covid-19',
      name: 'positiveLong',
      desc: '',
      args: [],
    );
  }

  /// `Tested on Covid-19`
  String get testedLong {
    return Intl.message(
      'Tested on Covid-19',
      name: 'testedLong',
      desc: '',
      args: [],
    );
  }

  /// `o'clock`
  String get oClock {
    return Intl.message(
      'o\'clock',
      name: 'oClock',
      desc: '',
      args: [],
    );
  }

  /// `Contact issuer`
  String get contactIssuer {
    return Intl.message(
      'Contact issuer',
      name: 'contactIssuer',
      desc: '',
      args: [],
    );
  }

  /// `We could not find any way to contact your certificate issuer.`
  String get errorContactingIssuer {
    return Intl.message(
      'We could not find any way to contact your certificate issuer.',
      name: 'errorContactingIssuer',
      desc: '',
      args: [],
    );
  }

  /// `Reason`
  String get reason {
    return Intl.message(
      'Reason',
      name: 'reason',
      desc: '',
      args: [],
    );
  }

  /// `Your last test's result\nhas been **marked invalid**.`
  String get certificateRevoked {
    return Intl.message(
      'Your last test\'s result\nhas been **marked invalid**.',
      name: 'certificateRevoked',
      desc: '',
      args: [],
    );
  }

  /// `Your test result is being transferred\n automatically to the competent health department.\n\n**Please respect the current**\n\n**[quarantine regulations](https://www.infektionsschutz.de/coronavirus/tests-auf-sars-cov-2/antigen-schnelltest.html#c14955)!**`
  String get positiveTestNotice {
    return Intl.message(
      'Your test result is being transferred\n automatically to the competent health department.\n\n**Please respect the current**\n\n**[quarantine regulations](https://www.infektionsschutz.de/coronavirus/tests-auf-sars-cov-2/antigen-schnelltest.html#c14955)!**',
      name: 'positiveTestNotice',
      desc: '',
      args: [],
    );
  }

  /// `Your last test's result\nis **still pending**.\nWe inform you as soon as\nyour result is available!`
  String get pendingTestLongFormatted {
    return Intl.message(
      'Your last test\'s result\nis **still pending**.\nWe inform you as soon as\nyour result is available!',
      name: 'pendingTestLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `There is **no current**\n**test result** available.`
  String get noCurrentCertificateLongFormatted {
    return Intl.message(
      'There is **no current**\n**test result** available.',
      name: 'noCurrentCertificateLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `That's how **it works**`
  String get tutorial {
    return Intl.message(
      'That\'s how **it works**',
      name: 'tutorial',
      desc: '',
      args: [],
    );
  }

  /// `Some intro...`
  String get checkInIntroLongFormatted {
    return Intl.message(
      'Some intro...',
      name: 'checkInIntroLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Scan check-in code`
  String get scanCheckInCode {
    return Intl.message(
      'Scan check-in code',
      name: 'scanCheckInCode',
      desc: '',
      args: [],
    );
  }

  /// `Location required`
  String get locationRequired {
    return Intl.message(
      'Location required',
      name: 'locationRequired',
      desc: '',
      args: [],
    );
  }

  /// `To avoid abuse, location\naccess is required to verify\nwhether you are really present\nat the location you want\nto check in.\n\nYour location is\nbeing processed\nentirely locally.`
  String get locationRequiredLongFormatted {
    return Intl.message(
      'To avoid abuse, location\naccess is required to verify\nwhether you are really present\nat the location you want\nto check in.\n\nYour location is\nbeing processed\nentirely locally.',
      name: 'locationRequiredLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Grant location access`
  String get grantLocationAccess {
    return Intl.message(
      'Grant location access',
      name: 'grantLocationAccess',
      desc: '',
      args: [],
    );
  }

  /// `You are not at the location you want to check-in at.`
  String get invalidLocation {
    return Intl.message(
      'You are not at the location you want to check-in at.',
      name: 'invalidLocation',
      desc: '',
      args: [],
    );
  }

  /// `To avoid abuse, we verify whether you are close to the location you want to check in at..`
  String get invalidLocationLongFormatted {
    return Intl.message(
      'To avoid abuse, we verify whether you are close to the location you want to check in at..',
      name: 'invalidLocationLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Successfully checked in.`
  String get successfullyCheckedIn {
    return Intl.message(
      'Successfully checked in.',
      name: 'successfullyCheckedIn',
      desc: '',
      args: [],
    );
  }

  /// `Close`
  String get close {
    return Intl.message(
      'Close',
      name: 'close',
      desc: '',
      args: [],
    );
  }

  /// `Certificate invalid`
  String get certificateInvalid {
    return Intl.message(
      'Certificate invalid',
      name: 'certificateInvalid',
      desc: '',
      args: [],
    );
  }

  /// `This certificate does\nnot seem to be valid\nfor you.`
  String get certificateInvalidLong {
    return Intl.message(
      'This certificate does\nnot seem to be valid\nfor you.',
      name: 'certificateInvalidLong',
      desc: '',
      args: [],
    );
  }

  /// `Test result pending`
  String get pendingTestResult {
    return Intl.message(
      'Test result pending',
      name: 'pendingTestResult',
      desc: '',
      args: [],
    );
  }

  /// `Retry`
  String get retry {
    return Intl.message(
      'Retry',
      name: 'retry',
      desc: '',
      args: [],
    );
  }

  /// `Invalid Check-In code`
  String get invalidCheckInCode {
    return Intl.message(
      'Invalid Check-In code',
      name: 'invalidCheckInCode',
      desc: '',
      args: [],
    );
  }

  /// `This check-in code\ndoes not seem to be\neligible for you.`
  String get invalidCheckInCodeLong {
    return Intl.message(
      'This check-in code\ndoes not seem to be\neligible for you.',
      name: 'invalidCheckInCodeLong',
      desc: '',
      args: [],
    );
  }

  /// `Return home`
  String get returnHome {
    return Intl.message(
      'Return home',
      name: 'returnHome',
      desc: '',
      args: [],
    );
  }

  /// `Check-In successful`
  String get checkInSuccessful {
    return Intl.message(
      'Check-In successful',
      name: 'checkInSuccessful',
      desc: '',
      args: [],
    );
  }

  /// `You can see all\ncheck-ins of the last\n14 days in the navigation drawer.`
  String get checkinSuccessfulLong {
    return Intl.message(
      'You can see all\ncheck-ins of the last\n14 days in the navigation drawer.',
      name: 'checkinSuccessfulLong',
      desc: '',
      args: [],
    );
  }

  /// `I could not read the image. Maybe try another one.`
  String get imagePickError {
    return Intl.message(
      'I could not read the image. Maybe try another one.',
      name: 'imagePickError',
      desc: '',
      args: [],
    );
  }

  /// `Please provide a valid street and house number`
  String get addressValidator {
    return Intl.message(
      'Please provide a valid street and house number',
      name: 'addressValidator',
      desc: '',
      args: [],
    );
  }

  /// `Address`
  String get address {
    return Intl.message(
      'Address',
      name: 'address',
      desc: '',
      args: [],
    );
  }

  /// `Government-known street and house number`
  String get addressHelper {
    return Intl.message(
      'Government-known street and house number',
      name: 'addressHelper',
      desc: '',
      args: [],
    );
  }

  /// `Next **step**`
  String get issuerVerifiedFormatted {
    return Intl.message(
      'Next **step**',
      name: 'issuerVerifiedFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure to abort the test?`
  String get abortTestLong {
    return Intl.message(
      'Are you sure to abort the test?',
      name: 'abortTestLong',
      desc: '',
      args: [],
    );
  }

  /// `This can not be undone.`
  String get abortTestHelper {
    return Intl.message(
      'This can not be undone.',
      name: 'abortTestHelper',
      desc: '',
      args: [],
    );
  }

  /// `Abort test`
  String get abortTest {
    return Intl.message(
      'Abort test',
      name: 'abortTest',
      desc: '',
      args: [],
    );
  }

  /// `Continue test`
  String get continueTest {
    return Intl.message(
      'Continue test',
      name: 'continueTest',
      desc: '',
      args: [],
    );
  }

  /// `free2**pass** your safe wallet\nfor **accessing Covid-19** test\ncertificates **anytime** right on\nyour smartphone.`
  String get intro1Head {
    return Intl.message(
      'free2**pass** your safe wallet\nfor **accessing Covid-19** test\ncertificates **anytime** right on\nyour smartphone.',
      name: 'intro1Head',
      desc: '',
      args: [],
    );
  }

  /// `Simpy scan the **qr code at**\n**participating issuers** and\none breath - your test result\nis right on your phone!`
  String get intro2Head {
    return Intl.message(
      'Simpy scan the **qr code at**\n**participating issuers** and\none breath - your test result\nis right on your phone!',
      name: 'intro2Head',
      desc: '',
      args: [],
    );
  }

  /// `You have to fill a contact tracing\nform in a restaurant, fitness center\nor at a hair cut?\n**Never ever with free2pass!**`
  String get intro3Head {
    return Intl.message(
      'You have to fill a contact tracing\nform in a restaurant, fitness center\nor at a hair cut?\n**Never ever with free2pass!**',
      name: 'intro3Head',
      desc: '',
      args: [],
    );
  }

  /// `In modern times, privacy is\na universal right.\nfree2**pass** is **100%**\n**GDPR compliant, tamper-proof**\nand multiple times **RSA encrypted.**`
  String get intro4Head {
    return Intl.message(
      'In modern times, privacy is\na universal right.\nfree2**pass** is **100%**\n**GDPR compliant, tamper-proof**\nand multiple times **RSA encrypted.**',
      name: 'intro4Head',
      desc: '',
      args: [],
    );
  }

  /// `**No more tiresome paperwork**\nin restaurant access,\nevent locations and other places!`
  String get intro1Bottom {
    return Intl.message(
      '**No more tiresome paperwork**\nin restaurant access,\nevent locations and other places!',
      name: 'intro1Bottom',
      desc: '',
      args: [],
    );
  }

  /// `The remaining time permitted\nto enter locations using free2**pass**\nis displayed **at a glace**.`
  String get intro2Bottom {
    return Intl.message(
      'The remaining time permitted\nto enter locations using free2**pass**\nis displayed **at a glace**.',
      name: 'intro2Bottom',
      desc: '',
      args: [],
    );
  }

  /// `Just scan a qr code on site\nand in case of spreading infection, your data is\ntransferred to the competent authorities\nusing **end-to-end encryption**.`
  String get intro3Bottom {
    return Intl.message(
      'Just scan a qr code on site\nand in case of spreading infection, your data is\ntransferred to the competent authorities\nusing **end-to-end encryption**.',
      name: 'intro3Bottom',
      desc: '',
      args: [],
    );
  }

  /// `Want to learn more?\nCheck our **FAQ**.`
  String get intro4Bottom {
    return Intl.message(
      'Want to learn more?\nCheck our **FAQ**.',
      name: 'intro4Bottom',
      desc: '',
      args: [],
    );
  }

  /// `# Phone number\nWe need to verify your phone number once to ensure the health department can contact you. Your phone number will not be stored on our servers but only on your device. We are hence unable to match any information we have to your identity.`
  String get phoneNumberLongFormatted {
    return Intl.message(
      '# Phone number\nWe need to verify your phone number once to ensure the health department can contact you. Your phone number will not be stored on our servers but only on your device. We are hence unable to match any information we have to your identity.',
      name: 'phoneNumberLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Verification successful`
  String get verificationSuccessful {
    return Intl.message(
      'Verification successful',
      name: 'verificationSuccessful',
      desc: '',
      args: [],
    );
  }

  /// `Zip code`
  String get zipCode {
    return Intl.message(
      'Zip code',
      name: 'zipCode',
      desc: '',
      args: [],
    );
  }

  /// `Town`
  String get town {
    return Intl.message(
      'Town',
      name: 'town',
      desc: '',
      args: [],
    );
  }

  /// `Birthday`
  String get birthday {
    return Intl.message(
      'Birthday',
      name: 'birthday',
      desc: '',
      args: [],
    );
  }

  /// `Please provide a valid town`
  String get pleaseProvideAValidTown {
    return Intl.message(
      'Please provide a valid town',
      name: 'pleaseProvideAValidTown',
      desc: '',
      args: [],
    );
  }

  /// `Please provide a valid ZIP code.`
  String get pleaseProvideAValidZipCode {
    return Intl.message(
      'Please provide a valid ZIP code.',
      name: 'pleaseProvideAValidZipCode',
      desc: '',
      args: [],
    );
  }

  /// `Please provide a birthday.`
  String get birthdayValidator {
    return Intl.message(
      'Please provide a birthday.',
      name: 'birthdayValidator',
      desc: '',
      args: [],
    );
  }

  /// `# Personal Data\nYour personal data is required to ensure a Covid-19 test certificate is only usable for the person it was issued to. Again, this data is only stored locally and is transferred to the Covid-19 test issuers using end-to-end encryption.`
  String get personalDataLongFormatted {
    return Intl.message(
      '# Personal Data\nYour personal data is required to ensure a Covid-19 test certificate is only usable for the person it was issued to. Again, this data is only stored locally and is transferred to the Covid-19 test issuers using end-to-end encryption.',
      name: 'personalDataLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `# Select photo\nYou can provide a photo depicting your face to **simplify the identity control** at Covid-19 test certificate issuer or locations you want to check in. For this, we either require access to your **photo gallery**, a **file from the internal storage** or **access to your camera** to import a selfie. If you grant camera access, this enables you to **scan Covid-19 test and check in QR codes** too. Under **no circumstances**, the photo you choose is **transferred to anyone**. You can alternatively decide to use your **identity card for verification** in test centers and locations.`
  String get selectPhotoLongFormatted {
    return Intl.message(
      '# Select photo\nYou can provide a photo depicting your face to **simplify the identity control** at Covid-19 test certificate issuer or locations you want to check in. For this, we either require access to your **photo gallery**, a **file from the internal storage** or **access to your camera** to import a selfie. If you grant camera access, this enables you to **scan Covid-19 test and check in QR codes** too. Under **no circumstances**, the photo you choose is **transferred to anyone**. You can alternatively decide to use your **identity card for verification** in test centers and locations.',
      name: 'selectPhotoLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Are your confident with the photo?`
  String get areYourConfidentWithThePhoto {
    return Intl.message(
      'Are your confident with the photo?',
      name: 'areYourConfidentWithThePhoto',
      desc: '',
      args: [],
    );
  }

  /// `No`
  String get no {
    return Intl.message(
      'No',
      name: 'no',
      desc: '',
      args: [],
    );
  }

  /// `Yes`
  String get yes {
    return Intl.message(
      'Yes',
      name: 'yes',
      desc: '',
      args: [],
    );
  }

  /// `Open usage policy`
  String get openUsagePolicy {
    return Intl.message(
      'Open usage policy',
      name: 'openUsagePolicy',
      desc: '',
      args: [],
    );
  }

  /// `Open privacy policy`
  String get openPrivacyPolicy {
    return Intl.message(
      'Open privacy policy',
      name: 'openPrivacyPolicy',
      desc: '',
      args: [],
    );
  }

  /// `I hereby confirm all data I provide is correct. In case of invalid data, free2**pass** will not be able to attest Covid test results.`
  String get acceptDataCorrectFormatted {
    return Intl.message(
      'I hereby confirm all data I provide is correct. In case of invalid data, free2**pass** will not be able to attest Covid test results.',
      name: 'acceptDataCorrectFormatted',
      desc: '',
      args: [],
    );
  }

  /// `I accept the **terms of use**`
  String get acceptTermsOfUseFormatted {
    return Intl.message(
      'I accept the **terms of use**',
      name: 'acceptTermsOfUseFormatted',
      desc: '',
      args: [],
    );
  }

  /// `I accept the **privacy policy**`
  String get acceptPrivacyPolicy {
    return Intl.message(
      'I accept the **privacy policy**',
      name: 'acceptPrivacyPolicy',
      desc: '',
      args: [],
    );
  }

  /// `**Check-in** now`
  String get checkInNow {
    return Intl.message(
      '**Check-in** now',
      name: 'checkInNow',
      desc: '',
      args: [],
    );
  }

  /// `**View all** places\nyou visited`
  String get showLastVisitedFormatted {
    return Intl.message(
      '**View all** places\nyou visited',
      name: 'showLastVisitedFormatted',
      desc: '',
      args: [],
    );
  }

  /// `To enable **contact tracing**,\nyou can **simply scan a qr code**\nin participating locations\nin case you have a negative Covid-19 certificate.`
  String get checkInLongFormatted {
    return Intl.message(
      'To enable **contact tracing**,\nyou can **simply scan a qr code**\nin participating locations\nin case you have a negative Covid-19 certificate.',
      name: 'checkInLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Show passport`
  String get showPassport {
    return Intl.message(
      'Show passport',
      name: 'showPassport',
      desc: '',
      args: [],
    );
  }

  /// `Validity`
  String get validity {
    return Intl.message(
      'Validity',
      name: 'validity',
      desc: '',
      args: [],
    );
  }

  /// `Issuer`
  String get issuer {
    return Intl.message(
      'Issuer',
      name: 'issuer',
      desc: '',
      args: [],
    );
  }

  /// `Expired at`
  String get expiredAt {
    return Intl.message(
      'Expired at',
      name: 'expiredAt',
      desc: '',
      args: [],
    );
  }

  /// `Seems like\nyou have **no past**\n**certificates**.`
  String get noExpiredCertificatesLongFormatted {
    return Intl.message(
      'Seems like\nyou have **no past**\n**certificates**.',
      name: 'noExpiredCertificatesLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Home`
  String get home {
    return Intl.message(
      'Home',
      name: 'home',
      desc: '',
      args: [],
    );
  }

  /// `Locations`
  String get recentLocations {
    return Intl.message(
      'Locations',
      name: 'recentLocations',
      desc: '',
      args: [],
    );
  }

  /// `Past tests`
  String get expiredCertificates {
    return Intl.message(
      'Past tests',
      name: 'expiredCertificates',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get settings {
    return Intl.message(
      'Settings',
      name: 'settings',
      desc: '',
      args: [],
    );
  }

  /// `Check-Out`
  String get checkOut {
    return Intl.message(
      'Check-Out',
      name: 'checkOut',
      desc: '',
      args: [],
    );
  }

  /// `Seems like\nyou have **no recent**\n**locations**.`
  String get noRecentLocationsLongFormatted {
    return Intl.message(
      'Seems like\nyou have **no recent**\n**locations**.',
      name: 'noRecentLocationsLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Open website`
  String get openWebsite {
    return Intl.message(
      'Open website',
      name: 'openWebsite',
      desc: '',
      args: [],
    );
  }

  /// `Issued`
  String get issued {
    return Intl.message(
      'Issued',
      name: 'issued',
      desc: '',
      args: [],
    );
  }

  /// `Directions`
  String get directions {
    return Intl.message(
      'Directions',
      name: 'directions',
      desc: '',
      args: [],
    );
  }

  /// `Seems like you have **no past Check-Ins**`
  String get seemsLikenyouHavenonpastCheckins {
    return Intl.message(
      'Seems like you have **no past Check-Ins**',
      name: 'seemsLikenyouHavenonpastCheckins',
      desc: '',
      args: [],
    );
  }

  /// `Powered by OpenStreetMap`
  String get poweredByOpenstreetmap {
    return Intl.message(
      'Powered by OpenStreetMap',
      name: 'poweredByOpenstreetmap',
      desc: '',
      args: [],
    );
  }

  /// `Language`
  String get language {
    return Intl.message(
      'Language',
      name: 'language',
      desc: '',
      args: [],
    );
  }

  /// `Theme`
  String get theme {
    return Intl.message(
      'Theme',
      name: 'theme',
      desc: '',
      args: [],
    );
  }

  /// `Light`
  String get light {
    return Intl.message(
      'Light',
      name: 'light',
      desc: '',
      args: [],
    );
  }

  /// `Default`
  String get themeDefault {
    return Intl.message(
      'Default',
      name: 'themeDefault',
      desc: '',
      args: [],
    );
  }

  /// `Dark (beta)`
  String get darkBeta {
    return Intl.message(
      'Dark (beta)',
      name: 'darkBeta',
      desc: '',
      args: [],
    );
  }

  /// `Open source licenses`
  String get openSourceLicenses {
    return Intl.message(
      'Open source licenses',
      name: 'openSourceLicenses',
      desc: '',
      args: [],
    );
  }

  /// `Website`
  String get website {
    return Intl.message(
      'Website',
      name: 'website',
      desc: '',
      args: [],
    );
  }

  /// `Change personal data`
  String get changePersonalData {
    return Intl.message(
      'Change personal data',
      name: 'changePersonalData',
      desc: '',
      args: [],
    );
  }

  /// `Keep`
  String get keep {
    return Intl.message(
      'Keep',
      name: 'keep',
      desc: '',
      args: [],
    );
  }

  /// `Reset`
  String get reset {
    return Intl.message(
      'Reset',
      name: 'reset',
      desc: '',
      args: [],
    );
  }

  /// `If you change your personal data, all past certificates expire.`
  String get ifYouChangeYourPersonalDataAllPastCertificatesExpire {
    return Intl.message(
      'If you change your personal data, all past certificates expire.',
      name: 'ifYouChangeYourPersonalDataAllPastCertificatesExpire',
      desc: '',
      args: [],
    );
  }

  /// `Reset required`
  String get resetRequired {
    return Intl.message(
      'Reset required',
      name: 'resetRequired',
      desc: '',
      args: [],
    );
  }

  /// `Advanced (Dangerous)`
  String get advancedDangerous {
    return Intl.message(
      'Advanced (Dangerous)',
      name: 'advancedDangerous',
      desc: '',
      args: [],
    );
  }

  /// `Reset free2**pass**`
  String get resetFree2passFormatted {
    return Intl.message(
      'Reset free2**pass**',
      name: 'resetFree2passFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure?`
  String get areYouSure {
    return Intl.message(
      'Are you sure?',
      name: 'areYouSure',
      desc: '',
      args: [],
    );
  }

  /// `This will reset the app preferences as well as your personal data. Moreover, **all certificates, locations and contacts** will be removed from your device.`
  String get resetAppLongFormatted {
    return Intl.message(
      'This will reset the app preferences as well as your personal data. Moreover, **all certificates, locations and contacts** will be removed from your device.',
      name: 'resetAppLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Edit phone number`
  String get editPhoneNumber {
    return Intl.message(
      'Edit phone number',
      name: 'editPhoneNumber',
      desc: '',
      args: [],
    );
  }

  /// `Error loading certificates. Are you sure you have a valid internet connection?`
  String get certificateRefreshError {
    return Intl.message(
      'Error loading certificates. Are you sure you have a valid internet connection?',
      name: 'certificateRefreshError',
      desc: '',
      args: [],
    );
  }

  /// `Request new code`
  String get requestNewCode {
    return Intl.message(
      'Request new code',
      name: 'requestNewCode',
      desc: '',
      args: [],
    );
  }

  /// `Information`
  String get information {
    return Intl.message(
      'Information',
      name: 'information',
      desc: '',
      args: [],
    );
  }

  /// `Enterprise certificate\nwith **internal**\n**validity** only.`
  String get internalCertificateLongFormatted {
    return Intl.message(
      'Enterprise certificate\nwith **internal**\n**validity** only.',
      name: 'internalCertificateLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Previous certificate`
  String get previousCertificate {
    return Intl.message(
      'Previous certificate',
      name: 'previousCertificate',
      desc: '',
      args: [],
    );
  }

  /// `Next certificate`
  String get nextCertificate {
    return Intl.message(
      'Next certificate',
      name: 'nextCertificate',
      desc: '',
      args: [],
    );
  }

  /// `**Attention:** internal certificate`
  String get internalCertificateFormatted {
    return Intl.message(
      '**Attention:** internal certificate',
      name: 'internalCertificateFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Please manually verify the user's identity card`
  String get noPhotoAvailableLong {
    return Intl.message(
      'Please manually verify the user\'s identity card',
      name: 'noPhotoAvailableLong',
      desc: '',
      args: [],
    );
  }

  /// `No photo available`
  String get noPhotoAvailable {
    return Intl.message(
      'No photo available',
      name: 'noPhotoAvailable',
      desc: '',
      args: [],
    );
  }

  /// `Continue`
  String get continueText {
    return Intl.message(
      'Continue',
      name: 'continueText',
      desc: '',
      args: [],
    );
  }

  /// `If you chose to use your passport instead of a photo, you will need to show your passport every time you check in or do a Covid-19 test.`
  String get ifYouChoseToUseYourPassportInsteadOfA {
    return Intl.message(
      'If you chose to use your passport instead of a photo, you will need to show your passport every time you check in or do a Covid-19 test.',
      name: 'ifYouChoseToUseYourPassportInsteadOfA',
      desc: '',
      args: [],
    );
  }

  /// `Use passport`
  String get usePassport {
    return Intl.message(
      'Use passport',
      name: 'usePassport',
      desc: '',
      args: [],
    );
  }

  /// `In this case, you need to show you passort every time you check in or do a Covid-19 test.`
  String get usePassportLong {
    return Intl.message(
      'In this case, you need to show you passort every time you check in or do a Covid-19 test.',
      name: 'usePassportLong',
      desc: '',
      args: [],
    );
  }

  /// `Error: No expiry provided.`
  String get errorNoExpiryProvided {
    return Intl.message(
      'Error: No expiry provided.',
      name: 'errorNoExpiryProvided',
      desc: '',
      args: [],
    );
  }

  /// `Work`
  String get work {
    return Intl.message(
      'Work',
      name: 'work',
      desc: '',
      args: [],
    );
  }

  /// `New test result pending`
  String get newTestResultPending {
    return Intl.message(
      'New test result pending',
      name: 'newTestResultPending',
      desc: '',
      args: [],
    );
  }

  /// `Grant`
  String get grant {
    return Intl.message(
      'Grant',
      name: 'grant',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get cancel {
    return Intl.message(
      'Cancel',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `You can select to **display you location on the map** and therefore grant us **access to your coarse location**.\nThis can help you to find participating locations close to you. If you choose to grant us this permission, **location access is used additionally** to verify you are **present in locations during a check in**.\nBoth uses of location happen **only locally on your device** and under **no circumstances, you location is transferred** to anyone.`
  String get displayLocationLongFormatted {
    return Intl.message(
      'You can select to **display you location on the map** and therefore grant us **access to your coarse location**.\nThis can help you to find participating locations close to you. If you choose to grant us this permission, **location access is used additionally** to verify you are **present in locations during a check in**.\nBoth uses of location happen **only locally on your device** and under **no circumstances, you location is transferred** to anyone.',
      name: 'displayLocationLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Locate me`
  String get locateMe {
    return Intl.message(
      'Locate me',
      name: 'locateMe',
      desc: '',
      args: [],
    );
  }

  /// `Please show your device to the test station staff.`
  String get showDeviceTestStation {
    return Intl.message(
      'Please show your device to the test station staff.',
      name: 'showDeviceTestStation',
      desc: '',
      args: [],
    );
  }

  /// `Identity check`
  String get identityCheck {
    return Intl.message(
      'Identity check',
      name: 'identityCheck',
      desc: '',
      args: [],
    );
  }

  /// `Please show your\n**identity card** to\nthe test station staff.`
  String get identityCheckLongFormatted {
    return Intl.message(
      'Please show your\n**identity card** to\nthe test station staff.',
      name: 'identityCheckLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `**Never**`
  String get vaccinationStateNone {
    return Intl.message(
      '**Never**',
      name: 'vaccinationStateNone',
      desc: '',
      args: [],
    );
  }

  /// `**Once**`
  String get vaccinationStatePartial {
    return Intl.message(
      '**Once**',
      name: 'vaccinationStatePartial',
      desc: '',
      args: [],
    );
  }

  /// `**Twice**`
  String get vaccinationStateImmunized {
    return Intl.message(
      '**Twice**',
      name: 'vaccinationStateImmunized',
      desc: '',
      args: [],
    );
  }

  /// `I have **not** been vaccinated **yet**`
  String get vaccinationStateNoneLong {
    return Intl.message(
      'I have **not** been vaccinated **yet**',
      name: 'vaccinationStateNoneLong',
      desc: '',
      args: [],
    );
  }

  /// `**Partial** - I have been vaccinated once and **wait for the second** vaccination`
  String get vaccinationStatePartialLong {
    return Intl.message(
      '**Partial** - I have been vaccinated once and **wait for the second** vaccination',
      name: 'vaccinationStatePartialLong',
      desc: '',
      args: [],
    );
  }

  /// `I am **fully immunized**`
  String get vaccinationStateImmunizedLong {
    return Intl.message(
      'I am **fully immunized**',
      name: 'vaccinationStateImmunizedLong',
      desc: '',
      args: [],
    );
  }

  /// `Have you been vaccinated against Covid-19?`
  String get vaccinatedQuestion {
    return Intl.message(
      'Have you been vaccinated against Covid-19?',
      name: 'vaccinatedQuestion',
      desc: '',
      args: [],
    );
  }

  /// `Your response won't give any advantage. We are required to ask you for statistical reasons only.`
  String get vaccinatedQuestionExplanation {
    return Intl.message(
      'Your response won\'t give any advantage. We are required to ask you for statistical reasons only.',
      name: 'vaccinatedQuestionExplanation',
      desc: '',
      args: [],
    );
  }

  /// `Okay, you chose to use your passport. Let's proceed.`
  String get chosePassportVerification {
    return Intl.message(
      'Okay, you chose to use your passport. Let\'s proceed.',
      name: 'chosePassportVerification',
      desc: '',
      args: [],
    );
  }

  /// `Certificate ID`
  String get certificateId {
    return Intl.message(
      'Certificate ID',
      name: 'certificateId',
      desc: '',
      args: [],
    );
  }

  /// `Refresh`
  String get refresh {
    return Intl.message(
      'Refresh',
      name: 'refresh',
      desc: '',
      args: [],
    );
  }

  /// `Request new test result`
  String get requestNewTestResult {
    return Intl.message(
      'Request new test result',
      name: 'requestNewTestResult',
      desc: '',
      args: [],
    );
  }

  /// `Important notice`
  String get positiveComment {
    return Intl.message(
      'Important notice',
      name: 'positiveComment',
      desc: '',
      args: [],
    );
  }

  /// `I understood`
  String get iUnderstood {
    return Intl.message(
      'I understood',
      name: 'iUnderstood',
      desc: '',
      args: [],
    );
  }

  /// `Day`
  String get day {
    return Intl.message(
      'Day',
      name: 'day',
      desc: '',
      args: [],
    );
  }

  /// `Month`
  String get month {
    return Intl.message(
      'Month',
      name: 'month',
      desc: '',
      args: [],
    );
  }

  /// `Year`
  String get year {
    return Intl.message(
      'Year',
      name: 'year',
      desc: '',
      args: [],
    );
  }

  /// `Email address (optional)`
  String get emailAddress {
    return Intl.message(
      'Email address (optional)',
      name: 'emailAddress',
      desc: '',
      args: [],
    );
  }

  /// `Optionally specify`
  String get optional {
    return Intl.message(
      'Optionally specify',
      name: 'optional',
      desc: '',
      args: [],
    );
  }

  /// `Optionally specify - This email address is invalid.`
  String get emailValidator {
    return Intl.message(
      'Optionally specify - This email address is invalid.',
      name: 'emailValidator',
      desc: '',
      args: [],
    );
  }

  /// `I prefer not to answer`
  String get noInformation {
    return Intl.message(
      'I prefer not to answer',
      name: 'noInformation',
      desc: '',
      args: [],
    );
  }

  /// `Multi-User`
  String get multiUser {
    return Intl.message(
      'Multi-User',
      name: 'multiUser',
      desc: '',
      args: [],
    );
  }

  /// `For e. g. family members\nwithout smartphone, you\ncan create user profiles to\nmanage their test results\n and check ins.`
  String get createUsersLongFormatted {
    return Intl.message(
      'For e. g. family members\nwithout smartphone, you\ncan create user profiles to\nmanage their test results\n and check ins.',
      name: 'createUsersLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Create **user**`
  String get createUser {
    return Intl.message(
      'Create **user**',
      name: 'createUser',
      desc: '',
      args: [],
    );
  }

  /// `Go back home`
  String get goBackHome {
    return Intl.message(
      'Go back home',
      name: 'goBackHome',
      desc: '',
      args: [],
    );
  }

  /// `404 - Not found`
  String get notFound {
    return Intl.message(
      '404 - Not found',
      name: 'notFound',
      desc: '',
      args: [],
    );
  }

  /// `The requested page was not found.`
  String get theRequestedPageWasNotFound {
    return Intl.message(
      'The requested page was not found.',
      name: 'theRequestedPageWasNotFound',
      desc: '',
      args: [],
    );
  }

  /// `Save`
  String get add {
    return Intl.message(
      'Save',
      name: 'add',
      desc: '',
      args: [],
    );
  }

  /// `# Create user\nPlease provide the user's personal data`
  String get createUserDataLongFormatted {
    return Intl.message(
      '# Create user\nPlease provide the user\'s personal data',
      name: 'createUserDataLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Import address from initial user`
  String get importAddressFromInitialUser {
    return Intl.message(
      'Import address from initial user',
      name: 'importAddressFromInitialUser',
      desc: '',
      args: [],
    );
  }

  /// `Edit user`
  String get editUser {
    return Intl.message(
      'Edit user',
      name: 'editUser',
      desc: '',
      args: [],
    );
  }

  /// `Current user`
  String get currentUser {
    return Intl.message(
      'Current user',
      name: 'currentUser',
      desc: '',
      args: [],
    );
  }

  /// `Delete user`
  String get deleteUser {
    return Intl.message(
      'Delete user',
      name: 'deleteUser',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure to delete the user and all their certificates? This cannot be undone.`
  String get deleteUserLongFormatted {
    return Intl.message(
      'Are you sure to delete the user and all their certificates? This cannot be undone.',
      name: 'deleteUserLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Initial user`
  String get initialUser {
    return Intl.message(
      'Initial user',
      name: 'initialUser',
      desc: '',
      args: [],
    );
  }

  /// `Other users`
  String get otherUsers {
    return Intl.message(
      'Other users',
      name: 'otherUsers',
      desc: '',
      args: [],
    );
  }

  /// `Please select a photo`
  String get photoValidator {
    return Intl.message(
      'Please select a photo',
      name: 'photoValidator',
      desc: '',
      args: [],
    );
  }

  /// `Select photo`
  String get selectPhoto {
    return Intl.message(
      'Select photo',
      name: 'selectPhoto',
      desc: '',
      args: [],
    );
  }

  /// `Change user`
  String get changeUser {
    return Intl.message(
      'Change user',
      name: 'changeUser',
      desc: '',
      args: [],
    );
  }

  /// `Delete certificates`
  String get deleteCertificates {
    return Intl.message(
      'Delete certificates',
      name: 'deleteCertificates',
      desc: '',
      args: [],
    );
  }

  /// `You changed important user data. If you proceed, all certificates for this user will expire. Are you sure to proceed?`
  String get youChangedImportantUserDataIfYouProceedAllCertificates {
    return Intl.message(
      'You changed important user data. If you proceed, all certificates for this user will expire. Are you sure to proceed?',
      name: 'youChangedImportantUserDataIfYouProceedAllCertificates',
      desc: '',
      args: [],
    );
  }

  /// `Your last certificate has been revoked`
  String get certificateRevokedLong {
    return Intl.message(
      'Your last certificate has been revoked',
      name: 'certificateRevokedLong',
      desc: '',
      args: [],
    );
  }

  /// `Version`
  String get version {
    return Intl.message(
      'Version',
      name: 'version',
      desc: '',
      args: [],
    );
  }

  /// `# No internet connection\nIt seems like there is no internet\nconnection. To proceed, please\nensure you have a valid\ninternet connection.`
  String get noConnectionLongFormatted {
    return Intl.message(
      '# No internet connection\nIt seems like there is no internet\nconnection. To proceed, please\nensure you have a valid\ninternet connection.',
      name: 'noConnectionLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Error saving your user data`
  String get errorSavingUserData {
    return Intl.message(
      'Error saving your user data',
      name: 'errorSavingUserData',
      desc: '',
      args: [],
    );
  }

  /// `A4 (Default)`
  String get formatA4 {
    return Intl.message(
      'A4 (Default)',
      name: 'formatA4',
      desc: '',
      args: [],
    );
  }

  /// `A5`
  String get formatA5 {
    return Intl.message(
      'A5',
      name: 'formatA5',
      desc: '',
      args: [],
    );
  }

  /// `US Letter`
  String get formatUSLetter {
    return Intl.message(
      'US Letter',
      name: 'formatUSLetter',
      desc: '',
      args: [],
    );
  }

  /// `Certificate`
  String get certificate {
    return Intl.message(
      'Certificate',
      name: 'certificate',
      desc: '',
      args: [],
    );
  }

  /// `Share as PDF`
  String get shareAsPdf {
    return Intl.message(
      'Share as PDF',
      name: 'shareAsPdf',
      desc: '',
      args: [],
    );
  }

  /// `Official`
  String get pdfOfficial {
    return Intl.message(
      'Official',
      name: 'pdfOfficial',
      desc: '',
      args: [],
    );
  }

  /// `test certificate`
  String get pdfTestCertificate {
    return Intl.message(
      'test certificate',
      name: 'pdfTestCertificate',
      desc: '',
      args: [],
    );
  }

  /// `PoC antigen test on SARS-CoV-2`
  String get pdfPoCTest {
    return Intl.message(
      'PoC antigen test on SARS-CoV-2',
      name: 'pdfPoCTest',
      desc: '',
      args: [],
    );
  }

  /// `Test used`
  String get pdfTestUsed {
    return Intl.message(
      'Test used',
      name: 'pdfTestUsed',
      desc: '',
      args: [],
    );
  }

  /// `Unknown test type`
  String get pdfUnknownTestType {
    return Intl.message(
      'Unknown test type',
      name: 'pdfUnknownTestType',
      desc: '',
      args: [],
    );
  }

  /// `Birth:`
  String get pdfBirth {
    return Intl.message(
      'Birth:',
      name: 'pdfBirth',
      desc: '',
      args: [],
    );
  }

  /// `No photo\nprovided`
  String get pdfNoPhoto {
    return Intl.message(
      'No photo\nprovided',
      name: 'pdfNoPhoto',
      desc: '',
      args: [],
    );
  }

  /// `Test result: NEGATIVE`
  String get pdfNegativeLong {
    return Intl.message(
      'Test result: NEGATIVE',
      name: 'pdfNegativeLong',
      desc: '',
      args: [],
    );
  }

  /// `Test time: `
  String get pdfTestTime {
    return Intl.message(
      'Test time: ',
      name: 'pdfTestTime',
      desc: '',
      args: [],
    );
  }

  /// `Valid until`
  String get pdfValidUntil {
    return Intl.message(
      'Valid until',
      name: 'pdfValidUntil',
      desc: '',
      args: [],
    );
  }

  /// `A negative test result cannot rule out a COVID-19 infection for sure. This document only certifies the health state at the time of creation.`
  String get pdfNegativeTestLegal {
    return Intl.message(
      'A negative test result cannot rule out a COVID-19 infection for sure. This document only certifies the health state at the time of creation.',
      name: 'pdfNegativeTestLegal',
      desc: '',
      args: [],
    );
  }

  /// `This document hast been automatically created and is valid without signature.`
  String get pdfLegalSignature {
    return Intl.message(
      'This document hast been automatically created and is valid without signature.',
      name: 'pdfLegalSignature',
      desc: '',
      args: [],
    );
  }

  /// `Use of this document is only permitted for the given person. Any transfer or copy of this documents results an immediate expiry of this test certificate.`
  String get pdfLegalNoTransfer {
    return Intl.message(
      'Use of this document is only permitted for the given person. Any transfer or copy of this documents results an immediate expiry of this test certificate.',
      name: 'pdfLegalNoTransfer',
      desc: '',
      args: [],
    );
  }

  /// `free2pass GmbH | Hamburger Allee 2-4 | D-30161 Hannover\nhello@free2pass.de | freepass.de`
  String get pdfLegalNotice {
    return Intl.message(
      'free2pass GmbH | Hamburger Allee 2-4 | D-30161 Hannover\nhello@free2pass.de | freepass.de',
      name: 'pdfLegalNotice',
      desc: '',
      args: [],
    );
  }

  /// `US Legal`
  String get formatUSLegal {
    return Intl.message(
      'US Legal',
      name: 'formatUSLegal',
      desc: '',
      args: [],
    );
  }

  /// `PIN`
  String get pin {
    return Intl.message(
      'PIN',
      name: 'pin',
      desc: '',
      args: [],
    );
  }

  /// `Update available`
  String get updateAvailable {
    return Intl.message(
      'Update available',
      name: 'updateAvailable',
      desc: '',
      args: [],
    );
  }

  /// `This update is mandatory. You won't be able to use free2pass until you update.`
  String get updateMandatoryLongFormatted {
    return Intl.message(
      'This update is mandatory. You won\'t be able to use free2pass until you update.',
      name: 'updateMandatoryLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `We highly recommend to update. If you cannot do this right now, you can come back later and start the update.`
  String get updateOptionalLongFormatted {
    return Intl.message(
      'We highly recommend to update. If you cannot do this right now, you can come back later and start the update.',
      name: 'updateOptionalLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Update now`
  String get updateNow {
    return Intl.message(
      'Update now',
      name: 'updateNow',
      desc: '',
      args: [],
    );
  }

  /// `Later`
  String get later {
    return Intl.message(
      'Later',
      name: 'later',
      desc: '',
      args: [],
    );
  }

  /// `Quit app`
  String get quitApp {
    return Intl.message(
      'Quit app',
      name: 'quitApp',
      desc: '',
      args: [],
    );
  }

  /// `Please wait, we encrypt your user data.`
  String get encryptingUserData {
    return Intl.message(
      'Please wait, we encrypt your user data.',
      name: 'encryptingUserData',
      desc: '',
      args: [],
    );
  }

  /// `Your test result is available!`
  String get notificationResultAvailable {
    return Intl.message(
      'Your test result is available!',
      name: 'notificationResultAvailable',
      desc: '',
      args: [],
    );
  }

  /// `Test results`
  String get notificationChannelTestResults {
    return Intl.message(
      'Test results',
      name: 'notificationChannelTestResults',
      desc: '',
      args: [],
    );
  }

  /// `Information on new Covid-19 test results`
  String get notificationChannelTestResultsDetails {
    return Intl.message(
      'Information on new Covid-19 test results',
      name: 'notificationChannelTestResultsDetails',
      desc: '',
      args: [],
    );
  }

  /// `This silent notification is used to keep an internet connection while the app is closed.`
  String get notificationChannelWebSocketLong {
    return Intl.message(
      'This silent notification is used to keep an internet connection while the app is closed.',
      name: 'notificationChannelWebSocketLong',
      desc: '',
      args: [],
    );
  }

  /// `Server connection`
  String get notificationChannelWebSocket {
    return Intl.message(
      'Server connection',
      name: 'notificationChannelWebSocket',
      desc: '',
      args: [],
    );
  }

  /// `Waiting for your test result...`
  String get notificationSilentContent {
    return Intl.message(
      'Waiting for your test result...',
      name: 'notificationSilentContent',
      desc: '',
      args: [],
    );
  }

  /// `Restart setup`
  String get restartSetup {
    return Intl.message(
      'Restart setup',
      name: 'restartSetup',
      desc: '',
      args: [],
    );
  }

  /// `Duplicate user found`
  String get duplicateUser {
    return Intl.message(
      'Duplicate user found',
      name: 'duplicateUser',
      desc: '',
      args: [],
    );
  }

  /// `For the data you just entered, another user is already present. Please provide other user data or return to the user list.`
  String get duplicateUserLong {
    return Intl.message(
      'For the data you just entered, another user is already present. Please provide other user data or return to the user list.',
      name: 'duplicateUserLong',
      desc: '',
      args: [],
    );
  }

  /// `Abort and view users`
  String get abortAndViewUsers {
    return Intl.message(
      'Abort and view users',
      name: 'abortAndViewUsers',
      desc: '',
      args: [],
    );
  }

  /// `Edit provided data`
  String get editProvidedData {
    return Intl.message(
      'Edit provided data',
      name: 'editProvidedData',
      desc: '',
      args: [],
    );
  }

  /// `Scan the crypto QR code\nto verify whether the\ncertificate is authentic!`
  String get pdfCertificateSigned {
    return Intl.message(
      'Scan the crypto QR code\nto verify whether the\ncertificate is authentic!',
      name: 'pdfCertificateSigned',
      desc: '',
      args: [],
    );
  }

  /// `End`
  String get end {
    return Intl.message(
      'End',
      name: 'end',
      desc: '',
      args: [],
    );
  }

  /// `Tap to show Crypto-QR`
  String get tapToShowQr {
    return Intl.message(
      'Tap to show Crypto-QR',
      name: 'tapToShowQr',
      desc: '',
      args: [],
    );
  }

  /// `Please select the time of the occasion`
  String get validatorOccasionRange {
    return Intl.message(
      'Please select the time of the occasion',
      name: 'validatorOccasionRange',
      desc: '',
      args: [],
    );
  }

  /// `Please create a sound name for your occasion.`
  String get validatorOccasionName {
    return Intl.message(
      'Please create a sound name for your occasion.',
      name: 'validatorOccasionName',
      desc: '',
      args: [],
    );
  }

  /// `Create an occasion`
  String get createAnOccasion {
    return Intl.message(
      'Create an occasion',
      name: 'createAnOccasion',
      desc: '',
      args: [],
    );
  }

  /// `Please name this occasion. The name is displayed for all attendees but never transferred to the health department.`
  String get occasionNameLong {
    return Intl.message(
      'Please name this occasion. The name is displayed for all attendees but never transferred to the health department.',
      name: 'occasionNameLong',
      desc: '',
      args: [],
    );
  }

  /// `Automatically Check-In yourself`
  String get occasionAutoCheckIn {
    return Intl.message(
      'Automatically Check-In yourself',
      name: 'occasionAutoCheckIn',
      desc: '',
      args: [],
    );
  }

  /// `Name`
  String get name {
    return Intl.message(
      'Name',
      name: 'name',
      desc: '',
      args: [],
    );
  }

  /// `Please select the time of your occasion`
  String get occasionTime {
    return Intl.message(
      'Please select the time of your occasion',
      name: 'occasionTime',
      desc: '',
      args: [],
    );
  }

  /// `Error creating your occasion.\nProbably, your community does not\nsupport this feature.`
  String get errorCreatingYourOccasion {
    return Intl.message(
      'Error creating your occasion.\nProbably, your community does not\nsupport this feature.',
      name: 'errorCreatingYourOccasion',
      desc: '',
      args: [],
    );
  }

  /// `We encrypt your data...`
  String get weEncryptYourData {
    return Intl.message(
      'We encrypt your data...',
      name: 'weEncryptYourData',
      desc: '',
      args: [],
    );
  }

  /// `This is a private occasion without address data`
  String get infoPrivateOccasion {
    return Intl.message(
      'This is a private occasion without address data',
      name: 'infoPrivateOccasion',
      desc: '',
      args: [],
    );
  }

  /// `Start`
  String get start {
    return Intl.message(
      'Start',
      name: 'start',
      desc: '',
      args: [],
    );
  }

  /// `Your ongoing occasions:`
  String get ongoingOccasions {
    return Intl.message(
      'Your ongoing occasions:',
      name: 'ongoingOccasions',
      desc: '',
      args: [],
    );
  }

  /// `Create new **Occasion**`
  String get createNewOccasion {
    return Intl.message(
      'Create new **Occasion**',
      name: 'createNewOccasion',
      desc: '',
      args: [],
    );
  }

  /// `Today,`
  String get today {
    return Intl.message(
      'Today,',
      name: 'today',
      desc: '',
      args: [],
    );
  }

  /// `From`
  String get from {
    return Intl.message(
      'From',
      name: 'from',
      desc: '',
      args: [],
    );
  }

  /// `To`
  String get to {
    return Intl.message(
      'To',
      name: 'to',
      desc: '',
      args: [],
    );
  }

  /// `Your data is transferred using advanced encryption.`
  String get advancedEncryption {
    return Intl.message(
      'Your data is transferred using advanced encryption.',
      name: 'advancedEncryption',
      desc: '',
      args: [],
    );
  }

  /// `Your local health department **supports advanced encryption**. Using our unique encryption technology, we can **cryptographically ensure** no one but the **competent health department** is able to access your data in **case of a** Covid-19 **infection**.`
  String get advancedEncryptionLongFormatted {
    return Intl.message(
      'Your local health department **supports advanced encryption**. Using our unique encryption technology, we can **cryptographically ensure** no one but the **competent health department** is able to access your data in **case of a** Covid-19 **infection**.',
      name: 'advancedEncryptionLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Your data is transferred using standard encryption.`
  String get standardEncryption {
    return Intl.message(
      'Your data is transferred using standard encryption.',
      name: 'standardEncryption',
      desc: '',
      args: [],
    );
  }

  /// `Unfortunately, some community's health departments **oppose any kind of data security**. In this case, we cannot use our advanced encryption technology. Even though the communication happens using one **layer of security less** than usually, your data remains **end-to-end encrypted**. Feel free to **appeal to your community** to enable advanced encryption.`
  String get standardEncryptionLongFormatted {
    return Intl.message(
      'Unfortunately, some community\'s health departments **oppose any kind of data security**. In this case, we cannot use our advanced encryption technology. Even though the communication happens using one **layer of security less** than usually, your data remains **end-to-end encrypted**. Feel free to **appeal to your community** to enable advanced encryption.',
      name: 'standardEncryptionLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Okay`
  String get okay {
    return Intl.message(
      'Okay',
      name: 'okay',
      desc: '',
      args: [],
    );
  }

  /// `Learn more`
  String get learnMore {
    return Intl.message(
      'Learn more',
      name: 'learnMore',
      desc: '',
      args: [],
    );
  }

  /// `Encryption`
  String get encryption {
    return Intl.message(
      'Encryption',
      name: 'encryption',
      desc: '',
      args: [],
    );
  }

  /// `Change data`
  String get changeData {
    return Intl.message(
      'Change data',
      name: 'changeData',
      desc: '',
      args: [],
    );
  }

  /// `Ignore`
  String get ignore {
    return Intl.message(
      'Ignore',
      name: 'ignore',
      desc: '',
      args: [],
    );
  }

  /// `Potentially malicious data`
  String get injection {
    return Intl.message(
      'Potentially malicious data',
      name: 'injection',
      desc: '',
      args: [],
    );
  }

  /// `The data you entered looks like a so called injection, an attac commonly used by hackers to perform malicious actions on software. Please be aware of the fact that our software has been pentested and we normalize all data you input.\nIn case you are not a hacker, you can ignore this message.`
  String get injectionLong {
    return Intl.message(
      'The data you entered looks like a so called injection, an attac commonly used by hackers to perform malicious actions on software. Please be aware of the fact that our software has been pentested and we normalize all data you input.\nIn case you are not a hacker, you can ignore this message.',
      name: 'injectionLong',
      desc: '',
      args: [],
    );
  }

  /// `Too many attempts`
  String get tooManyAttempts {
    return Intl.message(
      'Too many attempts',
      name: 'tooManyAttempts',
      desc: '',
      args: [],
    );
  }

  /// `To avoid abuse, registration attempts are limited. Please wait a minute or two and try again.`
  String get tooManyAttemptsLong {
    return Intl.message(
      'To avoid abuse, registration attempts are limited. Please wait a minute or two and try again.',
      name: 'tooManyAttemptsLong',
      desc: '',
      args: [],
    );
  }

  /// `Invalid Crypto QR`
  String get invalidCryptoQr {
    return Intl.message(
      'Invalid Crypto QR',
      name: 'invalidCryptoQr',
      desc: '',
      args: [],
    );
  }

  /// `This does not seem to\nbe a valid free2**pass**\nCrypto QR code. Are you\nsure you scanned the right code?`
  String get invalidCryptoQrLongFormatted {
    return Intl.message(
      'This does not seem to\nbe a valid free2**pass**\nCrypto QR code. Are you\nsure you scanned the right code?',
      name: 'invalidCryptoQrLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Sunday`
  String get sunday {
    return Intl.message(
      'Sunday',
      name: 'sunday',
      desc: '',
      args: [],
    );
  }

  /// `Monday`
  String get monday {
    return Intl.message(
      'Monday',
      name: 'monday',
      desc: '',
      args: [],
    );
  }

  /// `Tuesday`
  String get tuesday {
    return Intl.message(
      'Tuesday',
      name: 'tuesday',
      desc: '',
      args: [],
    );
  }

  /// `Wednesday`
  String get wednesday {
    return Intl.message(
      'Wednesday',
      name: 'wednesday',
      desc: '',
      args: [],
    );
  }

  /// `Thursday`
  String get thursday {
    return Intl.message(
      'Thursday',
      name: 'thursday',
      desc: '',
      args: [],
    );
  }

  /// `Friday`
  String get friday {
    return Intl.message(
      'Friday',
      name: 'friday',
      desc: '',
      args: [],
    );
  }

  /// `Saturday`
  String get saturday {
    return Intl.message(
      'Saturday',
      name: 'saturday',
      desc: '',
      args: [],
    );
  }

  /// `Opening hours`
  String get openingHours {
    return Intl.message(
      'Opening hours',
      name: 'openingHours',
      desc: '',
      args: [],
    );
  }

  /// `Upcoming appointment`
  String get upcomingAppointment {
    return Intl.message(
      'Upcoming appointment',
      name: 'upcomingAppointment',
      desc: '',
      args: [],
    );
  }

  /// `Time`
  String get time {
    return Intl.message(
      'Time',
      name: 'time',
      desc: '',
      args: [],
    );
  }

  /// `Cancel appointment`
  String get cancelAppointment {
    return Intl.message(
      'Cancel appointment',
      name: 'cancelAppointment',
      desc: '',
      args: [],
    );
  }

  /// `closed`
  String get closed {
    return Intl.message(
      'closed',
      name: 'closed',
      desc: '',
      args: [],
    );
  }

  /// `Create appointment`
  String get createAppointment {
    return Intl.message(
      'Create appointment',
      name: 'createAppointment',
      desc: '',
      args: [],
    );
  }

  /// `Previous day`
  String get previousDay {
    return Intl.message(
      'Previous day',
      name: 'previousDay',
      desc: '',
      args: [],
    );
  }

  /// `Next day`
  String get nextDay {
    return Intl.message(
      'Next day',
      name: 'nextDay',
      desc: '',
      args: [],
    );
  }

  /// `Select slot`
  String get selectSlot {
    return Intl.message(
      'Select slot',
      name: 'selectSlot',
      desc: '',
      args: [],
    );
  }

  /// `Select this slot`
  String get selectThisSlot {
    return Intl.message(
      'Select this slot',
      name: 'selectThisSlot',
      desc: '',
      args: [],
    );
  }

  /// `Successfully reserved slot!`
  String get successfullyReservedSlot {
    return Intl.message(
      'Successfully reserved slot!',
      name: 'successfullyReservedSlot',
      desc: '',
      args: [],
    );
  }

  /// `Error creating the appointment.`
  String get errorCreatingAppointment {
    return Intl.message(
      'Error creating the appointment.',
      name: 'errorCreatingAppointment',
      desc: '',
      args: [],
    );
  }

  /// `Other reason`
  String get cancelOtherReason {
    return Intl.message(
      'Other reason',
      name: 'cancelOtherReason',
      desc: '',
      args: [],
    );
  }

  /// `I chose the wrong date or time`
  String get cancelWrongTime {
    return Intl.message(
      'I chose the wrong date or time',
      name: 'cancelWrongTime',
      desc: '',
      args: [],
    );
  }

  /// `I am unexpectedly busy at that time`
  String get cancelUnavailable {
    return Intl.message(
      'I am unexpectedly busy at that time',
      name: 'cancelUnavailable',
      desc: '',
      args: [],
    );
  }

  /// `Why would you like to cancel the appointment?`
  String get cancelReasonLong {
    return Intl.message(
      'Why would you like to cancel the appointment?',
      name: 'cancelReasonLong',
      desc: '',
      args: [],
    );
  }

  /// `Successfully canceled appointment`
  String get appointmentCancelSuccessful {
    return Intl.message(
      'Successfully canceled appointment',
      name: 'appointmentCancelSuccessful',
      desc: '',
      args: [],
    );
  }

  /// `Error canceling this appointment`
  String get appointmentCancelError {
    return Intl.message(
      'Error canceling this appointment',
      name: 'appointmentCancelError',
      desc: '',
      args: [],
    );
  }

  /// `Error loading this location's data`
  String get errorLoadingLocationData {
    return Intl.message(
      'Error loading this location\'s data',
      name: 'errorLoadingLocationData',
      desc: '',
      args: [],
    );
  }

  /// `Test center`
  String get testCenter {
    return Intl.message(
      'Test center',
      name: 'testCenter',
      desc: '',
      args: [],
    );
  }

  /// `Merchant`
  String get merchant {
    return Intl.message(
      'Merchant',
      name: 'merchant',
      desc: '',
      args: [],
    );
  }

  /// `Vaccination center`
  String get vaccinationCenter {
    return Intl.message(
      'Vaccination center',
      name: 'vaccinationCenter',
      desc: '',
      args: [],
    );
  }

  /// `Private occasion`
  String get privateOccasion {
    return Intl.message(
      'Private occasion',
      name: 'privateOccasion',
      desc: '',
      args: [],
    );
  }

  /// `Location type`
  String get locationType {
    return Intl.message(
      'Location type',
      name: 'locationType',
      desc: '',
      args: [],
    );
  }

  /// `Currently closed`
  String get currentlyClosed {
    return Intl.message(
      'Currently closed',
      name: 'currentlyClosed',
      desc: '',
      args: [],
    );
  }

  /// `Currently opened`
  String get currentlyOpened {
    return Intl.message(
      'Currently opened',
      name: 'currentlyOpened',
      desc: '',
      args: [],
    );
  }

  /// `Tap to enlarge.`
  String get tapToEnlarge {
    return Intl.message(
      'Tap to enlarge.',
      name: 'tapToEnlarge',
      desc: '',
      args: [],
    );
  }

  /// `Start test now`
  String get startTestNow {
    return Intl.message(
      'Start test now',
      name: 'startTestNow',
      desc: '',
      args: [],
    );
  }

  /// `The issuer is closed.`
  String get issuerClosed {
    return Intl.message(
      'The issuer is closed.',
      name: 'issuerClosed',
      desc: '',
      args: [],
    );
  }

  /// `Too many appointments.`
  String get tooManyAppointments {
    return Intl.message(
      'Too many appointments.',
      name: 'tooManyAppointments',
      desc: '',
      args: [],
    );
  }

  /// `No more tests available for today.`
  String get noMoreTests {
    return Intl.message(
      'No more tests available for today.',
      name: 'noMoreTests',
      desc: '',
      args: [],
    );
  }

  /// `They could not verify your appointment.`
  String get couldNotVerify {
    return Intl.message(
      'They could not verify your appointment.',
      name: 'couldNotVerify',
      desc: '',
      args: [],
    );
  }

  /// `Unknown reason.`
  String get unknownReason {
    return Intl.message(
      'Unknown reason.',
      name: 'unknownReason',
      desc: '',
      args: [],
    );
  }

  /// `You canceled this appointment.`
  String get youCanceledAppointment {
    return Intl.message(
      'You canceled this appointment.',
      name: 'youCanceledAppointment',
      desc: '',
      args: [],
    );
  }

  /// `The issuer revoked this appointment.`
  String get issuerRevokedAppointment {
    return Intl.message(
      'The issuer revoked this appointment.',
      name: 'issuerRevokedAppointment',
      desc: '',
      args: [],
    );
  }

  /// `Filter locations`
  String get filterLocations {
    return Intl.message(
      'Filter locations',
      name: 'filterLocations',
      desc: '',
      args: [],
    );
  }

  /// `Locations in area:`
  String get locationsInArea {
    return Intl.message(
      'Locations in area:',
      name: 'locationsInArea',
      desc: '',
      args: [],
    );
  }

  /// `Reset filters`
  String get resetFilters {
    return Intl.message(
      'Reset filters',
      name: 'resetFilters',
      desc: '',
      args: [],
    );
  }

  /// `Clear`
  String get clear {
    return Intl.message(
      'Clear',
      name: 'clear',
      desc: '',
      args: [],
    );
  }

  /// `Only show free2pass-enabled locations`
  String get onlyShowFree2passenablesLocations {
    return Intl.message(
      'Only show free2pass-enabled locations',
      name: 'onlyShowFree2passenablesLocations',
      desc: '',
      args: [],
    );
  }

  /// `Vaccination stations`
  String get vaccinationStations {
    return Intl.message(
      'Vaccination stations',
      name: 'vaccinationStations',
      desc: '',
      args: [],
    );
  }

  /// `Merchants`
  String get merchants {
    return Intl.message(
      'Merchants',
      name: 'merchants',
      desc: '',
      args: [],
    );
  }

  /// `Issuers`
  String get issuers {
    return Intl.message(
      'Issuers',
      name: 'issuers',
      desc: '',
      args: [],
    );
  }

  /// `Location types`
  String get locationTypes {
    return Intl.message(
      'Location types',
      name: 'locationTypes',
      desc: '',
      args: [],
    );
  }

  /// `Matching locations`
  String get matchingLocations {
    return Intl.message(
      'Matching locations',
      name: 'matchingLocations',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get search {
    return Intl.message(
      'Search',
      name: 'search',
      desc: '',
      args: [],
    );
  }

  /// `Vaccination Certificate`
  String get vaccinationCertificate {
    return Intl.message(
      'Vaccination Certificate',
      name: 'vaccinationCertificate',
      desc: '',
      args: [],
    );
  }

  /// `Permanently remove`
  String get permanentlyRemove {
    return Intl.message(
      'Permanently remove',
      name: 'permanentlyRemove',
      desc: '',
      args: [],
    );
  }

  /// `Unique certificate identifier`
  String get uniqueCertificateIdentifier {
    return Intl.message(
      'Unique certificate identifier',
      name: 'uniqueCertificateIdentifier',
      desc: '',
      args: [],
    );
  }

  /// `Date of vaccination`
  String get dateOfVaccination {
    return Intl.message(
      'Date of vaccination',
      name: 'dateOfVaccination',
      desc: '',
      args: [],
    );
  }

  /// `Certificate issuer`
  String get certificateIssuer {
    return Intl.message(
      'Certificate issuer',
      name: 'certificateIssuer',
      desc: '',
      args: [],
    );
  }

  /// `Date of birth`
  String get dateOfBirth {
    return Intl.message(
      'Date of birth',
      name: 'dateOfBirth',
      desc: '',
      args: [],
    );
  }

  /// `Warning: This vaccination certificate is only valid in case your digitally verify it.`
  String get vaccinationCertificateVerificationNotice {
    return Intl.message(
      'Warning: This vaccination certificate is only valid in case your digitally verify it.',
      name: 'vaccinationCertificateVerificationNotice',
      desc: '',
      args: [],
    );
  }

  /// `Official COVID-19 vaccination certificate`
  String get vaccinationCertificateTitle {
    return Intl.message(
      'Official COVID-19 vaccination certificate',
      name: 'vaccinationCertificateTitle',
      desc: '',
      args: [],
    );
  }

  /// `Vaccination {x} of {y}`
  String vaccinationNumber(Object x, Object y) {
    return Intl.message(
      'Vaccination $x of $y',
      name: 'vaccinationNumber',
      desc: '',
      args: [x, y],
    );
  }

  /// `Store`
  String get store {
    return Intl.message(
      'Store',
      name: 'store',
      desc: '',
      args: [],
    );
  }

  /// `Don't import`
  String get dontImport {
    return Intl.message(
      'Don\'t import',
      name: 'dontImport',
      desc: '',
      args: [],
    );
  }

  /// `Unsupported version of the digital health certificate:`
  String get unsupportedVaccinationCertificate {
    return Intl.message(
      'Unsupported version of the digital health certificate:',
      name: 'unsupportedVaccinationCertificate',
      desc: '',
      args: [],
    );
  }

  /// `This does not seem to be a valid vaccination certificate.`
  String get notAVaccinationCertificate {
    return Intl.message(
      'This does not seem to be a valid vaccination certificate.',
      name: 'notAVaccinationCertificate',
      desc: '',
      args: [],
    );
  }

  /// `Scan vaccination certificate`
  String get startScanVaccinationCertificate {
    return Intl.message(
      'Scan vaccination certificate',
      name: 'startScanVaccinationCertificate',
      desc: '',
      args: [],
    );
  }

  /// `Scan an official vaccination certificate`
  String get scanVaccinationCertificate {
    return Intl.message(
      'Scan an official vaccination certificate',
      name: 'scanVaccinationCertificate',
      desc: '',
      args: [],
    );
  }

  /// `General information`
  String get generalVaccinationInformation {
    return Intl.message(
      'General information',
      name: 'generalVaccinationInformation',
      desc: '',
      args: [],
    );
  }

  /// `Please note that a digital vaccination certificate does not automatically permit for traveling within the European Union. Further information on the ongoing Covid-19 spread as well as the current traveling restrictions in the European union can be found at [reopen.europa.eu](https://reopen.europa.eu/).  \nInformation on the digital vaccination certificate can be found at [www.digitaler-impfnachweis-app.de](https://www.digitaler-impfnachweis-app.de/).`
  String get vaccinationInformationParagraphs {
    return Intl.message(
      'Please note that a digital vaccination certificate does not automatically permit for traveling within the European Union. Further information on the ongoing Covid-19 spread as well as the current traveling restrictions in the European union can be found at [reopen.europa.eu](https://reopen.europa.eu/).  \nInformation on the digital vaccination certificate can be found at [www.digitaler-impfnachweis-app.de](https://www.digitaler-impfnachweis-app.de/).',
      name: 'vaccinationInformationParagraphs',
      desc: '',
      args: [],
    );
  }

  /// `For technical reasons, the digital vaccination certificate is unfortunately only available in the native mobile app.`
  String get webVaccinationCertificate {
    return Intl.message(
      'For technical reasons, the digital vaccination certificate is unfortunately only available in the native mobile app.',
      name: 'webVaccinationCertificate',
      desc: '',
      args: [],
    );
  }

  /// `Only partially vaccinated yet.`
  String get partiallyVaccinated {
    return Intl.message(
      'Only partially vaccinated yet.',
      name: 'partiallyVaccinated',
      desc: '',
      args: [],
    );
  }

  /// `Fully vaccinated but period of two weeks not passed yet.`
  String get waitingForFullProtection {
    return Intl.message(
      'Fully vaccinated but period of two weeks not passed yet.',
      name: 'waitingForFullProtection',
      desc: '',
      args: [],
    );
  }

  /// `Fully immunized for more than two weeks.`
  String get fullyImmunized {
    return Intl.message(
      'Fully immunized for more than two weeks.',
      name: 'fullyImmunized',
      desc: '',
      args: [],
    );
  }

  /// `Remove vaccination certificate`
  String get removeVaccinationCertificate {
    return Intl.message(
      'Remove vaccination certificate',
      name: 'removeVaccinationCertificate',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure to permanently remove the following vaccination certificate from you device:`
  String get removeVaccinationCertificateLongFormatted {
    return Intl.message(
      'Are you sure to permanently remove the following vaccination certificate from you device:',
      name: 'removeVaccinationCertificateLongFormatted',
      desc: '',
      args: [],
    );
  }

  /// `Remove`
  String get remove {
    return Intl.message(
      'Remove',
      name: 'remove',
      desc: '',
      args: [],
    );
  }

  /// `Valid vaccination certificate found.`
  String get validVaccinationCertificateFound {
    return Intl.message(
      'Valid vaccination certificate found.',
      name: 'validVaccinationCertificateFound',
      desc: '',
      args: [],
    );
  }

  /// `Enable push notifications`
  String get appointmentEnablePush {
    return Intl.message(
      'Enable push notifications',
      name: 'appointmentEnablePush',
      desc: '',
      args: [],
    );
  }

  /// `You can get notifications in case appointments are canceled or test results are available. Please note that this harms your privacy.`
  String get pushPrivacyNotice {
    return Intl.message(
      'You can get notifications in case appointments are canceled or test results are available. Please note that this harms your privacy.',
      name: 'pushPrivacyNotice',
      desc: '',
      args: [],
    );
  }

  /// `Push notifications`
  String get pushNotifications {
    return Intl.message(
      'Push notifications',
      name: 'pushNotifications',
      desc: '',
      args: [],
    );
  }

  /// `Push notifications enabled. Tap to disable.`
  String get pushNotificationsEnabledTapToDisable {
    return Intl.message(
      'Push notifications enabled. Tap to disable.',
      name: 'pushNotificationsEnabledTapToDisable',
      desc: '',
      args: [],
    );
  }

  /// `timezone`
  String get timezone {
    return Intl.message(
      'timezone',
      name: 'timezone',
      desc: '',
      args: [],
    );
  }

  /// `Phone number lost`
  String get phoneNumberLostTitle {
    return Intl.message(
      'Phone number lost',
      name: 'phoneNumberLostTitle',
      desc: '',
      args: [],
    );
  }

  /// `Unfortunately, there was a bug in a previous version of free2pass, which could cause the phone number to be lost. You will need to re-verify your phone number in the next step.`
  String get phoneNumberLostDescription {
    return Intl.message(
      'Unfortunately, there was a bug in a previous version of free2pass, which could cause the phone number to be lost. You will need to re-verify your phone number in the next step.',
      name: 'phoneNumberLostDescription',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'de'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
