import 'dart:typed_data';

import 'package:free2pass/models/appointment.dart';
import 'package:free2pass/models/certificate.dart';
import 'package:free2pass/models/location.dart';
import 'package:free2pass/models/merchant.dart';
import 'package:free2pass/models/occasion.model.dart';
import 'package:free2pass/models/user-data.dart';
import 'package:free2pass/models/vaccination-certificate-model.dart';
import 'package:free2pass/push/exchange/push-subscription.dart';
import 'package:hive/hive.dart';

/// the [Box] containing user preferences
late Box settings;

/// the [Box] containing misc app data
late Box data;

/// the [Box] containing all [UserData]
late Box<UserData> users;

/// the [Box] containing every [Certificate]
late Box<Certificate> certificates;

/// the [Box] containing every [Merchant]
late Box<Merchant> visitedLocations;

/// the [Box] containing every [Location]
late Box<Location> cachedLocations;

/// the [Box] containing the locations' AES ciphers
/// used for encrypted contact tracing
late Box<Map<String, dynamic>> localAesKeys;

/// the [Box] containing every [Occasion] the user created
late Box<Occasion> createdOccasions;

/// the [Box] containing every [Appointment] at locations or issuers
late Box<Appointment> appointments;

/// the [Box] containing every [VaccinationCertificateStorage]
late Box<VaccinationCertificateStorage> vaccinationCertificates;

/// the [Box] containing all every active [PushSubscription]
late Box<PushSubscription> pushSubscriptions;

/// the memory cache of the user images
Map<String, Uint8List> userImages = {};

const kTermsOfServiceUrl = 'https://free2pass.de/terms-of-use/';
const kLegalNoticeUrl = 'https://free2pass.de/impressum/';
const kPrivacyPolicyUrl = 'https://free2pass.de/app-privacy-policy/';

const kScalableAssetBase = 'assets/scalable/';

const String productionApiUrl = 'https://api.free2pass.de';
const String betaApiUrl = 'https://beta-api.free2pass.de';
late String apiUrl;

const debug = false;
