import 'package:free2pass/enums/appointments.enum.dart';
import 'package:json_annotation/json_annotation.dart';

part 'appointment-change.g.dart';

@JsonSerializable()
class AppointmentChange {
  final int? bookingIndex;
  @JsonKey(fromJson: _decodeCancelReason)
  final AppointmentCancelReason? cancelReason;
  final int createDate;
  final int fromTimestamp;
  final String id;
  final String? otherCancelReason;
  final String? otherRevokeReason;
  @JsonKey(fromJson: _decodeRevokeReason)
  final AppointmentRevokeReason? revokeReason;
  @JsonKey(fromJson: _decodeState)
  final AppointmentState state;
  final int? toTimestamp;
  final int? updateDate;
  final String? verifyCode;

  AppointmentChange(
    this.bookingIndex,
    this.cancelReason,
    this.createDate,
    this.fromTimestamp,
    this.id,
    this.otherCancelReason,
    this.otherRevokeReason,
    this.revokeReason,
    this.state,
    this.toTimestamp,
    this.updateDate,
    this.verifyCode,
  );

  factory AppointmentChange.fromJson(Map<String, dynamic> json) =>
      _$AppointmentChangeFromJson(json);

  Map<String, dynamic> toJson() => _$AppointmentChangeToJson(this);
}

AppointmentRevokeReason? _decodeRevokeReason(input) {
  if (input == null) return null;
  return AppointmentRevokeReason.values[input];
}

AppointmentState _decodeState(input) {
  return AppointmentState.values[input];
}

AppointmentCancelReason? _decodeCancelReason(input) {
  if (input == null) return null;
  return AppointmentCancelReason.values[input];
}
