// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appointment-change.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppointmentChange _$AppointmentChangeFromJson(Map<String, dynamic> json) =>
    AppointmentChange(
      json['bookingIndex'] as int?,
      _decodeCancelReason(json['cancelReason']),
      json['createDate'] as int,
      json['fromTimestamp'] as int,
      json['id'] as String,
      json['otherCancelReason'] as String?,
      json['otherRevokeReason'] as String?,
      _decodeRevokeReason(json['revokeReason']),
      _decodeState(json['state']),
      json['toTimestamp'] as int?,
      json['updateDate'] as int?,
      json['verifyCode'] as String?,
    );

Map<String, dynamic> _$AppointmentChangeToJson(AppointmentChange instance) =>
    <String, dynamic>{
      'bookingIndex': instance.bookingIndex,
      'cancelReason': _$AppointmentCancelReasonEnumMap[instance.cancelReason],
      'createDate': instance.createDate,
      'fromTimestamp': instance.fromTimestamp,
      'id': instance.id,
      'otherCancelReason': instance.otherCancelReason,
      'otherRevokeReason': instance.otherRevokeReason,
      'revokeReason': _$AppointmentRevokeReasonEnumMap[instance.revokeReason],
      'state': _$AppointmentStateEnumMap[instance.state],
      'toTimestamp': instance.toTimestamp,
      'updateDate': instance.updateDate,
      'verifyCode': instance.verifyCode,
    };

const _$AppointmentCancelReasonEnumMap = {
  AppointmentCancelReason.OTHER: 'OTHER',
  AppointmentCancelReason.UNAVAILABLE: 'UNAVAILABLE',
  AppointmentCancelReason.WRONG_TIME: 'WRONG_TIME',
};

const _$AppointmentRevokeReasonEnumMap = {
  AppointmentRevokeReason.OTHER: 'OTHER',
  AppointmentRevokeReason.ISSUER_CLOSED: 'ISSUER_CLOSED',
  AppointmentRevokeReason.TOO_MANY_APPOINTMENT: 'TOO_MANY_APPOINTMENT',
  AppointmentRevokeReason.NO_MORE_TESTS_AVAILABLE: 'NO_MORE_TESTS_AVAILABLE',
  AppointmentRevokeReason.COULD_NOT_VERIFY: 'COULD_NOT_VERIFY',
};

const _$AppointmentStateEnumMap = {
  AppointmentState.APPROVED: 'APPROVED',
  AppointmentState.REVOKED: 'REVOKED',
  AppointmentState.CANCELED: 'CANCELED',
};
