import 'package:free2pass/enums/appointments.enum.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/models/location.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:hive/hive.dart';

part 'appointment.g.dart';

@HiveType(typeId: 13)

/// an [Appointment] for a covid 19 test at a [Location]
class Appointment {
  /// the [Location] you have the appointment at
  @HiveField(0)
  final Location issuer;

  /// the Unix time stamp of the appointment's beginning
  @HiveField(1)
  final int startTime;

  /// the signature key used for authentication to avoid you neighbor canceling your covid test
  @HiveField(2)
  final String signature;

  /// the id of the appointment
  @HiveField(3)
  final String id;

  @HiveField(4)
  AppointmentState state;
  @HiveField(5)
  String? otherCancelReason;
  @HiveField(6)
  String? otherRevokeReason;
  @HiveField(7)
  AppointmentRevokeReason? revokeReason;
  @HiveField(8)
  AppointmentCancelReason? cancelReason;

  Appointment({
    required this.issuer,
    required this.startTime,
    required this.signature,
    required this.id,
    this.state = AppointmentState.APPROVED,
    this.otherCancelReason,
    this.otherRevokeReason,
    this.revokeReason,
    this.cancelReason,
  });

  String qrEncode() => id + ';' + Crypto.getSha256Sum("$id;$signature");

  String appointmentRevokeReasonString(S s) {
    switch (revokeReason) {
      case AppointmentRevokeReason.OTHER:
        return otherRevokeReason!;
      case AppointmentRevokeReason.ISSUER_CLOSED:
        return s.issuerClosed;
      case AppointmentRevokeReason.TOO_MANY_APPOINTMENT:
        return s.tooManyAppointments;
      case AppointmentRevokeReason.NO_MORE_TESTS_AVAILABLE:
        return s.noMoreTests;
      case AppointmentRevokeReason.COULD_NOT_VERIFY:
        return s.couldNotVerify;
      default:
        return s.unknownReason;
    }
  }
}
