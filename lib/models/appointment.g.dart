// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appointment.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AppointmentAdapter extends TypeAdapter<Appointment> {
  @override
  final int typeId = 13;

  @override
  Appointment read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Appointment(
      issuer: fields[0] as Location,
      startTime: fields[1] as int,
      signature: fields[2] as String,
      id: fields[3] as String,
      state: fields[4] as AppointmentState,
      otherCancelReason: fields[5] as String?,
      otherRevokeReason: fields[6] as String?,
      revokeReason: fields[7] as AppointmentRevokeReason?,
      cancelReason: fields[8] as AppointmentCancelReason?,
    );
  }

  @override
  void write(BinaryWriter writer, Appointment obj) {
    writer
      ..writeByte(9)
      ..writeByte(0)
      ..write(obj.issuer)
      ..writeByte(1)
      ..write(obj.startTime)
      ..writeByte(2)
      ..write(obj.signature)
      ..writeByte(3)
      ..write(obj.id)
      ..writeByte(4)
      ..write(obj.state)
      ..writeByte(5)
      ..write(obj.otherCancelReason)
      ..writeByte(6)
      ..write(obj.otherRevokeReason)
      ..writeByte(7)
      ..write(obj.revokeReason)
      ..writeByte(8)
      ..write(obj.cancelReason);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppointmentAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
