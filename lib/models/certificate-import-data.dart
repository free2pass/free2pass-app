import 'package:free2pass/models/notification-data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'certificate-import-data.g.dart';

@JsonSerializable()
class CertificateImportData {
  final String id;
  final String signature;
  final String? language;
  final NotificationData? notificationData;

  CertificateImportData(
      this.id, this.signature, this.language, this.notificationData);

  factory CertificateImportData.fromJson(Map<String, dynamic> json) =>
      _$CertificateImportDataFromJson(json);

  Map<String, dynamic> toJson() => _$CertificateImportDataToJson(this);
}
