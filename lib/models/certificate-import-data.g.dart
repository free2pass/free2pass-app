// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'certificate-import-data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CertificateImportData _$CertificateImportDataFromJson(
        Map<String, dynamic> json) =>
    CertificateImportData(
      json['id'] as String,
      json['signature'] as String,
      json['language'] as String?,
      json['notificationData'] == null
          ? null
          : NotificationData.fromJson(
              json['notificationData'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CertificateImportDataToJson(
        CertificateImportData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'signature': instance.signature,
      'language': instance.language,
      'notificationData': instance.notificationData,
    };
