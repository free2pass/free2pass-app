import 'dart:developer';

import 'package:free2pass/enums/certificate-status.enum.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/certificate.dart';
import 'package:free2pass/utils/hive.util.dart';
import 'package:hive/hive.dart';

class CertificateStatus {
  final Certificate? certificate;
  final CertificateStatusValidity validity;
  Certificate? pendingCertificate;

  CertificateStatus._(this.certificate, this.validity, this.pendingCertificate);

  static CertificateStatus empty() =>
      CertificateStatus._(null, CertificateStatusValidity.EMPTY, null);

  static CertificateStatus fromCertificate(Certificate certificate,
          {Certificate? pendingCertificate}) =>
      CertificateStatus._(
          certificate, certificate.expectedValidity!, pendingCertificate);
}

List<CertificateStatus> getWorkCertificateStatuses(Box<Certificate> box) {
  if (box.isEmpty) return [CertificateStatus.empty()];

  final currentHash = data.get(kCurrentUserKey);

  final issuers = box.values
      .where((element) =>
          element.issuer!.isPrivate && element.userHash == currentHash)
      .map((e) => e.issuer!.id)
      .toSet();

  List<CertificateStatus> validCertificates = [];

  issuers.forEach((issuer) {
    Certificate? latestCertificateWithoutExpiry;
    bool foundCertificateForIssuer = false;
    box.values
        .where((element) =>
            element.issuer!.id == issuer && element.userHash == currentHash)
        .forEach((element) {
      // allowing maximally one certificate without expiry (a pending or revoked one hence) in case that there is no currently valid certificate or there is a newer test than the last one with expiry
      /// saving latest pending / revoked certificate for case no other is available
      if ([CertificateStatusValidity.PENDING, CertificateStatusValidity.REVOKED]
              .contains(element.expectedValidity) &&
          element.createDate! >
              (latestCertificateWithoutExpiry?.createDate ?? 0)) {
        latestCertificateWithoutExpiry = element;
        return;
      }

      /// checking positive / negative certificates
      if (element.expiryDate != null &&
          element.expiryDateTime.isAfter(DateTime.now())) {
        /// checking whether it is newer than the last one added
        if (foundCertificateForIssuer) {
          if (element.resultDateTime
              .isAfter(validCertificates.last.certificate!.resultDateTime)) {
            validCertificates.removeLast();
            validCertificates.add(CertificateStatus.fromCertificate(element));
            return;
          } else

            /// skipping older certificates
            return;
        }

        /// adding in case no certificate was found before
        foundCertificateForIssuer = true;
        validCertificates.add(CertificateStatus.fromCertificate(element));
      }
    });
    if (latestCertificateWithoutExpiry != null) {
      /// if pending certificate is newer than latest certificate, adding notice
      if (foundCertificateForIssuer &&
          latestCertificateWithoutExpiry!.resultDateTime
              .isAfter(validCertificates.last.certificate!.resultDateTime) &&
          [CertificateStatusValidity.PENDING, CertificateStatusValidity.REVOKED]
              .contains(latestCertificateWithoutExpiry!.expectedValidity)) {
        validCertificates.last.pendingCertificate =
            latestCertificateWithoutExpiry;
      } else {
        /// returning pending / revoked certificate if no other certificate found
        if (!foundCertificateForIssuer)
          validCertificates.add(CertificateStatus.fromCertificate(
              latestCertificateWithoutExpiry!));
      }
    }
  });

  if (validCertificates.isEmpty)
    validCertificates.add(CertificateStatus.empty());

  return validCertificates;
}

CertificateStatus getDefaultCertificateStatus(Box<Certificate> box) {
  if (box.isEmpty) return CertificateStatus.empty();

  Certificate? validCertificate;
  Certificate? latestCertificateWithoutExpiry;

  final currentHash = data.get(kCurrentUserKey);

  box.values.where((element) =>
      (!element.issuer!.isPrivate) && element.userHash == currentHash)
    ..forEach((element) {
      if (element.issuer!.isPrivate) return;
      // allowing maximally one certificate without expiry (a pending or revoked one hence) in case that there is no currently valid certificate or there is a newer test than the last one with expiry
      if ([CertificateStatusValidity.PENDING]
              .contains(element.expectedValidity) &&
          element.createDate! >
              (latestCertificateWithoutExpiry?.createDate ?? 0)) {
        latestCertificateWithoutExpiry = element;
        return;
      }
      if (element.expiryDate != null &&
          element.expiryDateTime.isAfter(DateTime.now())) {
        if (validCertificate != null &&
            element.expiryDateTime.isBefore(validCertificate!.expiryDateTime))
          return;
        validCertificate = element;
      }
    });

  if (validCertificate == null) {
    if (latestCertificateWithoutExpiry == null) {
      // check if the last cert was invalid and display this instead
      final userCerts = box.values
          .where((element) =>
              (!element.issuer!.isPrivate) && element.userHash == currentHash)
          .toList();
      userCerts.sort((a, b) => b.createDate!.compareTo(a.createDate!));
      // inspect(userCerts.first);6466
      if (userCerts.isNotEmpty &&
          userCerts.first.expectedValidity ==
              CertificateStatusValidity.REVOKED) {
        return CertificateStatus.fromCertificate(userCerts.first);
      }

      return CertificateStatus.empty();
    } else {
      return CertificateStatus.fromCertificate(latestCertificateWithoutExpiry!);
    }
  }
  if (latestCertificateWithoutExpiry != null &&
      latestCertificateWithoutExpiry!.resultDateTime
          .isAfter(validCertificate!.resultDateTime)) {
    if ([CertificateStatusValidity.PENDING]
        .contains(latestCertificateWithoutExpiry!.expectedValidity)) {
      return CertificateStatus.fromCertificate(validCertificate!,
          pendingCertificate: latestCertificateWithoutExpiry);
    } else {
      return CertificateStatus.fromCertificate(latestCertificateWithoutExpiry!);
    }
  }
  // canceling any running certificate import jobs in background if a valid
  // certificate without pending data is found
  /*if (!kIsWeb) {
    cancelWorkManager();
    getCertificateAvailableNotification().then((value) => value.cancelAll());
  }*/
  return CertificateStatus.fromCertificate(validCertificate!);
}
