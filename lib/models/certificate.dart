import 'package:free2pass/enums/certificate-status.enum.dart';
import 'package:free2pass/models/issuer.dart';
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'certificate.g.dart';

@HiveType(typeId: 1)
@JsonSerializable()
class Certificate {
  @HiveField(0)
  int? createDate;

  @HiveField(11)
  int? resultSubmitDate;
  @HiveField(1)
  Issuer? issuer;
  @HiveField(2)
  @JsonKey(name: 'state', fromJson: _decodeValidity)
  CertificateStatusValidity? expectedValidity;
  @HiveField(3)
  int? expiryDate;
  @HiveField(4)
  final String id;
  @HiveField(5)
  String? revokeReasonString;
  @HiveField(6)
  String? name;
  @HiveField(7)
  String? positiveComment;
  @HiveField(8)
  @JsonKey(ignore: true)
  bool? dialogShown;
  @HiveField(9)
  String? userHash;
  @HiveField(10)
  String? testType;

  Certificate({
    this.createDate,
    this.resultSubmitDate,
    this.issuer,
    this.expectedValidity,
    this.expiryDate,
    required this.id,
    this.revokeReasonString,
    this.name,
    this.positiveComment,
    this.dialogShown,
    this.userHash,
    this.testType,
  });

  bool isCertificate(Certificate certificate) {
    return certificate.id == this.id;
  }

  DateTime get expiryDateTime =>
      DateTime.fromMillisecondsSinceEpoch(expiryDate! * 1000);

  DateTime get resultDateTime =>
      DateTime.fromMillisecondsSinceEpoch((resultSubmitDate??createDate!) * 1000);

  bool get shouldShowDialog =>
      ((expectedValidity == CertificateStatusValidity.POSITIVE &&
              positiveComment != null) ||
          (expectedValidity == CertificateStatusValidity.REVOKED &&
              revokeReasonString != null)) &&
      dialogShown != true;

  factory Certificate.fromJson(Map<String, dynamic> json) =>
      _$CertificateFromJson(json);

  Map<String, dynamic> toJson() => _$CertificateToJson(this);
}

CertificateStatusValidity _decodeValidity(data) {
  final names = {
    1: CertificateStatusValidity.NEGATIVE,
    2: CertificateStatusValidity.POSITIVE,
    0: CertificateStatusValidity.PENDING,
    3: CertificateStatusValidity.REVOKED,
  };
  return names[data]!;
}
