// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'certificate.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CertificateAdapter extends TypeAdapter<Certificate> {
  @override
  final int typeId = 1;

  @override
  Certificate read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Certificate(
      createDate: fields[0] as int?,
      issuer: fields[1] as Issuer?,
      expectedValidity: fields[2] as CertificateStatusValidity?,
      expiryDate: fields[3] as int?,
      id: fields[4] as String,
      revokeReasonString: fields[5] as String?,
      name: fields[6] as String?,
      positiveComment: fields[7] as String?,
      dialogShown: fields[8] as bool?,
      userHash: fields[9] as String?,
      testType: fields[10] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, Certificate obj) {
    writer
      ..writeByte(11)
      ..writeByte(0)
      ..write(obj.createDate)
      ..writeByte(1)
      ..write(obj.issuer)
      ..writeByte(2)
      ..write(obj.expectedValidity)
      ..writeByte(3)
      ..write(obj.expiryDate)
      ..writeByte(4)
      ..write(obj.id)
      ..writeByte(5)
      ..write(obj.revokeReasonString)
      ..writeByte(6)
      ..write(obj.name)
      ..writeByte(7)
      ..write(obj.positiveComment)
      ..writeByte(8)
      ..write(obj.dialogShown)
      ..writeByte(9)
      ..write(obj.userHash)
      ..writeByte(10)
      ..write(obj.testType);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CertificateAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Certificate _$CertificateFromJson(Map<String, dynamic> json) => Certificate(
      createDate: json['createDate'] as int?,
      issuer: json['issuer'] == null
          ? null
          : Issuer.fromJson(json['issuer'] as Map<String, dynamic>),
      expectedValidity: _decodeValidity(json['state']),
      expiryDate: json['expiryDate'] as int?,
      id: json['id'] as String,
      revokeReasonString: json['revokeReasonString'] as String?,
      name: json['name'] as String?,
      positiveComment: json['positiveComment'] as String?,
      userHash: json['userHash'] as String?,
      testType: json['testType'] as String?,
    );

Map<String, dynamic> _$CertificateToJson(Certificate instance) =>
    <String, dynamic>{
      'createDate': instance.createDate,
      'issuer': instance.issuer,
      'state': _$CertificateStatusValidityEnumMap[instance.expectedValidity],
      'expiryDate': instance.expiryDate,
      'id': instance.id,
      'revokeReasonString': instance.revokeReasonString,
      'name': instance.name,
      'positiveComment': instance.positiveComment,
      'userHash': instance.userHash,
      'testType': instance.testType,
    };

const _$CertificateStatusValidityEnumMap = {
  CertificateStatusValidity.NEGATIVE: 'NEGATIVE',
  CertificateStatusValidity.POSITIVE: 'POSITIVE',
  CertificateStatusValidity.PENDING: 'PENDING',
  CertificateStatusValidity.EMPTY: 'EMPTY',
  CertificateStatusValidity.REVOKED: 'REVOKED',
};
