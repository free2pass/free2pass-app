import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'checkin-data.g.dart';

@HiveType(typeId: 3)
@JsonSerializable()
class CheckInData {
  @HiveField(0)
  final String publicKey;

  /// latitude
  @HiveField(1)
  final double lat;

  /// longitude
  @HiveField(2)
  final double lng;

  @HiveField(3)
  final int accuracy;

  CheckInData(
      {required this.publicKey,
      required this.lat,
      required this.lng,
      required this.accuracy});

  factory CheckInData.fromJson(Map<String, dynamic> json) =>
      _$CheckInDataFromJson(json);

  Map<String, dynamic> toJson() => _$CheckInDataToJson(this);
}
