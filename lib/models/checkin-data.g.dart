// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'checkin-data.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CheckInDataAdapter extends TypeAdapter<CheckInData> {
  @override
  final int typeId = 3;

  @override
  CheckInData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CheckInData(
      publicKey: fields[0] as String,
      lat: fields[1] as double,
      lng: fields[2] as double,
      accuracy: fields[3] as int,
    );
  }

  @override
  void write(BinaryWriter writer, CheckInData obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.publicKey)
      ..writeByte(1)
      ..write(obj.lat)
      ..writeByte(2)
      ..write(obj.lng)
      ..writeByte(3)
      ..write(obj.accuracy);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CheckInDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CheckInData _$CheckInDataFromJson(Map<String, dynamic> json) => CheckInData(
      publicKey: json['publicKey'] as String,
      lat: (json['lat'] as num).toDouble(),
      lng: (json['lng'] as num).toDouble(),
      accuracy: json['accuracy'] as int,
    );

Map<String, dynamic> _$CheckInDataToJson(CheckInData instance) =>
    <String, dynamic>{
      'publicKey': instance.publicKey,
      'lat': instance.lat,
      'lng': instance.lng,
      'accuracy': instance.accuracy,
    };
