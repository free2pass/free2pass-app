import 'package:json_annotation/json_annotation.dart';

part 'health-department.model.g.dart';

@JsonSerializable()
class HealthDepartment {
  /// the current PGP public key
  @JsonKey(fromJson: _keyFromJson)
  final String? authnPublicPGPKey;

  /// a human-readable name of the health department
  final String name;

  HealthDepartment({this.authnPublicPGPKey, required this.name});

  factory HealthDepartment.fromJson(Map<String, dynamic> json) =>
      _$HealthDepartmentFromJson(json);

  Map<String, dynamic> toJson() => _$HealthDepartmentToJson(this);
}

String? _keyFromJson(dynamic value) {
  if (value is String) return value;
  return null;
}
