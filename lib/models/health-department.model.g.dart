// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'health-department.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HealthDepartment _$HealthDepartmentFromJson(Map<String, dynamic> json) =>
    HealthDepartment(
      authnPublicPGPKey: _keyFromJson(json['authnPublicPGPKey']),
      name: json['name'] as String,
    );

Map<String, dynamic> _$HealthDepartmentToJson(HealthDepartment instance) =>
    <String, dynamic>{
      'authnPublicPGPKey': instance.authnPublicPGPKey,
      'name': instance.name,
    };
