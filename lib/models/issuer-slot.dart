import 'package:json_annotation/json_annotation.dart';

part 'issuer-slot.g.dart';

@JsonSerializable()
class IssuerSlot {
  /// the slot length in minutes
  final int slotLength;

  /// all slots matching the response
  final List<Slot> availableSlots;

  IssuerSlot(this.slotLength, this.availableSlots);

  Duration get slotDuration => Duration(minutes: slotLength);

  factory IssuerSlot.fromJson(Map<String, dynamic> json) =>
      _$IssuerSlotFromJson(json);

  Map<String, dynamic> toJson() => _$IssuerSlotToJson(this);
}

@JsonSerializable()
class Slot {
  /// the beginning timestamp of the slot
  final int fromTimestamp;

  /// the number of concurrent slots
  final int freeSlots;

  Slot(this.fromTimestamp, this.freeSlots);

  DateTime get fromDateTime =>
      DateTime.fromMillisecondsSinceEpoch(fromTimestamp * 1000);

  factory Slot.fromJson(Map<String, dynamic> json) => _$SlotFromJson(json);

  Map<String, dynamic> toJson() => _$SlotToJson(this);
}
