// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'issuer-slot.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IssuerSlot _$IssuerSlotFromJson(Map<String, dynamic> json) => IssuerSlot(
      json['slotLength'] as int,
      (json['availableSlots'] as List<dynamic>)
          .map((e) => Slot.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$IssuerSlotToJson(IssuerSlot instance) =>
    <String, dynamic>{
      'slotLength': instance.slotLength,
      'availableSlots': instance.availableSlots,
    };

Slot _$SlotFromJson(Map<String, dynamic> json) => Slot(
      json['fromTimestamp'] as int,
      json['freeSlots'] as int,
    );

Map<String, dynamic> _$SlotToJson(Slot instance) => <String, dynamic>{
      'fromTimestamp': instance.fromTimestamp,
      'freeSlots': instance.freeSlots,
    };
