import 'package:free2pass/enums/location-types.enum.dart';
import 'package:free2pass/models/location.dart';
import 'package:free2pass/models/opening-hours.dart';
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'issuer.g.dart';

@HiveType(typeId: 3)
@JsonSerializable()
class Issuer extends Location {
  @HiveField(0)
  final String name;
  @HiveField(1)
  final String address;

  /// latitude
  @HiveField(2)
  final double? lat;

  /// longitude
  @HiveField(3)
  final double? lng;
  @HiveField(4)
  final int id;
  @HiveField(7)
  final String? contactPhone;
  @HiveField(6)
  final String? contactWebsite;
  @HiveField(8)
  final String? contactEmail;
  @HiveField(9)
  @JsonKey(defaultValue: false)
  final bool isPrivate;
  @HiveField(10)
  final String? logo;
  @HiveField(18)
  final List<OpeningHours>? openingTimes;
  @HiveField(19)
  final bool? isOpen;
  @HiveField(20)
  final bool? ownsAppointmentModule;
  @HiveField(21)
  final int? utcOffset;

  final LocationType type = LocationType.ISSUER;

  Issuer({
    required this.name,
    required this.address,
    this.lat,
    this.lng,
    required this.id,
    this.contactPhone,
    this.contactWebsite,
    this.contactEmail,
    this.isPrivate = false,
    this.logo,
    this.openingTimes,
    this.isOpen,
    this.ownsAppointmentModule,
    this.utcOffset,
  }) : super(name: name, type: LocationType.ISSUER);

  String get searchString {
    String search = name + ' ' + address;
    return search;
  }

  factory Issuer.fromJson(Map<String, dynamic> json) => _$IssuerFromJson(json);

  Map<String, dynamic> toJson() => _$IssuerToJson(this);
}
