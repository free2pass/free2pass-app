// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'issuer.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class IssuerAdapter extends TypeAdapter<Issuer> {
  @override
  final int typeId = 3;

  @override
  Issuer read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Issuer(
      name: fields[0] as String,
      address: fields[1] as String,
      lat: fields[2] as double?,
      lng: fields[3] as double?,
      id: fields[4] as int,
      contactPhone: fields[7] as String?,
      contactWebsite: fields[6] as String?,
      contactEmail: fields[8] as String?,
      isPrivate: fields[9] as bool,
      logo: fields[10] as String?,
      openingTimes: (fields[18] as List?)?.cast<OpeningHours>(),
      isOpen: fields[19] as bool?,
      ownsAppointmentModule: fields[20] as bool?,
      utcOffset: fields[21] as int?,
    );
  }

  @override
  void write(BinaryWriter writer, Issuer obj) {
    writer
      ..writeByte(16)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.address)
      ..writeByte(2)
      ..write(obj.lat)
      ..writeByte(3)
      ..write(obj.lng)
      ..writeByte(4)
      ..write(obj.id)
      ..writeByte(7)
      ..write(obj.contactPhone)
      ..writeByte(6)
      ..write(obj.contactWebsite)
      ..writeByte(8)
      ..write(obj.contactEmail)
      ..writeByte(9)
      ..write(obj.isPrivate)
      ..writeByte(10)
      ..write(obj.logo)
      ..writeByte(18)
      ..write(obj.openingTimes)
      ..writeByte(19)
      ..write(obj.isOpen)
      ..writeByte(20)
      ..write(obj.ownsAppointmentModule)
      ..writeByte(21)
      ..write(obj.utcOffset)
      ..writeByte(12)
      ..write(obj.count)
      ..writeByte(17)
      ..write(obj.activated);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is IssuerAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Issuer _$IssuerFromJson(Map<String, dynamic> json) => Issuer(
      name: json['name'] as String,
      address: json['address'] as String,
      lat: (json['lat'] as num?)?.toDouble(),
      lng: (json['lng'] as num?)?.toDouble(),
      id: json['id'] as int,
      contactPhone: json['contactPhone'] as String?,
      contactWebsite: json['contactWebsite'] as String?,
      contactEmail: json['contactEmail'] as String?,
      isPrivate: json['isPrivate'] as bool? ?? false,
      logo: json['logo'] as String?,
      openingTimes: (json['openingTimes'] as List<dynamic>?)
          ?.map((e) => OpeningHours.fromJson(e as Map<String, dynamic>))
          .toList(),
      isOpen: json['isOpen'] as bool?,
      ownsAppointmentModule: json['ownsAppointmentModule'] as bool?,
      utcOffset: json['utcOffset'] as int?,
    );

Map<String, dynamic> _$IssuerToJson(Issuer instance) => <String, dynamic>{
      'name': instance.name,
      'address': instance.address,
      'lat': instance.lat,
      'lng': instance.lng,
      'id': instance.id,
      'contactPhone': instance.contactPhone,
      'contactWebsite': instance.contactWebsite,
      'contactEmail': instance.contactEmail,
      'isPrivate': instance.isPrivate,
      'logo': instance.logo,
      'openingTimes': instance.openingTimes,
      'isOpen': instance.isOpen,
      'ownsAppointmentModule': instance.ownsAppointmentModule,
      'utcOffset': instance.utcOffset,
    };
