import 'package:free2pass/enums/location-types.enum.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/models/opening-hours.dart';
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'location.g.dart';

@HiveType(typeId: 8)
@JsonSerializable()
class Location {
  @HiveField(0)
  final String name;
  @HiveField(1)
  final String? address;
  @HiveField(2)
  final double? lat;
  @HiveField(3)
  final double? lng;
  @HiveField(4)
  final int? id;
  @HiveField(7)
  final String? contactPhone;
  @HiveField(6)
  final String? contactWebsite;
  @HiveField(8)
  final String? contactEmail;
  @HiveField(9)
  @JsonKey(name: 'type', fromJson: decodeLocation)
  final LocationType type;
  @HiveField(12)
  final int? count;
  @HiveField(17)
  @JsonKey(defaultValue: true)
  final bool? activated;
  @HiveField(18)
  final List<OpeningHours>? openingTimes;
  @HiveField(19)
  final bool? isOpen;

  Location({
    required this.name,
    this.address,
    this.lat,
    this.lng,
    this.id,
    this.contactPhone,
    this.contactWebsite,
    this.contactEmail,
    required this.type,
    this.count,
    this.activated,
    this.openingTimes,
    this.isOpen,
  });

  String get searchString {
    String search = name;
    if (address != null) search += ' ' + address!;
    return search;
  }

  bool isLocation(Location location) {
    return location.id == this.id;
  }

  factory Location.fromJson(Map<String, dynamic> json) =>
      _$LocationFromJson(json);

  Map<String, dynamic> toJson() => _$LocationToJson(this);

  static String typeToString(LocationType type, S s) {
    final names = {
      LocationType.ISSUER: s.testCenter,
      LocationType.MERCHANT: s.merchant,
      LocationType.VACCINATOR: s.vaccinationCenter,
      LocationType.OCCASION: s.privateOccasion,
    };
    return names[type]!;
  }
}

LocationType decodeLocation(data) {
  final names = {
    0: LocationType.ISSUER,
    1: LocationType.MERCHANT,
    2: LocationType.VACCINATOR,
    3: LocationType.CLUSTER,
    4: LocationType.OCCASION,
  };
  return names[data]!;
}
