// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'location.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class LocationAdapter extends TypeAdapter<Location> {
  @override
  final int typeId = 8;

  @override
  Location read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Location(
      name: fields[0] as String,
      address: fields[1] as String?,
      lat: fields[2] as double?,
      lng: fields[3] as double?,
      id: fields[4] as int?,
      contactPhone: fields[7] as String?,
      contactWebsite: fields[6] as String?,
      contactEmail: fields[8] as String?,
      type: fields[9] as LocationType,
      count: fields[12] as int?,
      activated: fields[17] as bool?,
      openingTimes: (fields[18] as List?)?.cast<OpeningHours>(),
      isOpen: fields[19] as bool?,
    );
  }

  @override
  void write(BinaryWriter writer, Location obj) {
    writer
      ..writeByte(13)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.address)
      ..writeByte(2)
      ..write(obj.lat)
      ..writeByte(3)
      ..write(obj.lng)
      ..writeByte(4)
      ..write(obj.id)
      ..writeByte(7)
      ..write(obj.contactPhone)
      ..writeByte(6)
      ..write(obj.contactWebsite)
      ..writeByte(8)
      ..write(obj.contactEmail)
      ..writeByte(9)
      ..write(obj.type)
      ..writeByte(12)
      ..write(obj.count)
      ..writeByte(17)
      ..write(obj.activated)
      ..writeByte(18)
      ..write(obj.openingTimes)
      ..writeByte(19)
      ..write(obj.isOpen);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LocationAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Location _$LocationFromJson(Map<String, dynamic> json) => Location(
      name: json['name'] as String,
      address: json['address'] as String?,
      lat: (json['lat'] as num?)?.toDouble(),
      lng: (json['lng'] as num?)?.toDouble(),
      id: json['id'] as int?,
      contactPhone: json['contactPhone'] as String?,
      contactWebsite: json['contactWebsite'] as String?,
      contactEmail: json['contactEmail'] as String?,
      type: decodeLocation(json['type']),
      count: json['count'] as int?,
      activated: json['activated'] as bool? ?? true,
      openingTimes: (json['openingTimes'] as List<dynamic>?)
          ?.map((e) => OpeningHours.fromJson(e as Map<String, dynamic>))
          .toList(),
      isOpen: json['isOpen'] as bool?,
    );

Map<String, dynamic> _$LocationToJson(Location instance) => <String, dynamic>{
      'name': instance.name,
      'address': instance.address,
      'lat': instance.lat,
      'lng': instance.lng,
      'id': instance.id,
      'contactPhone': instance.contactPhone,
      'contactWebsite': instance.contactWebsite,
      'contactEmail': instance.contactEmail,
      'type': _$LocationTypeEnumMap[instance.type],
      'count': instance.count,
      'activated': instance.activated,
      'openingTimes': instance.openingTimes,
      'isOpen': instance.isOpen,
    };

const _$LocationTypeEnumMap = {
  LocationType.ISSUER: 'ISSUER',
  LocationType.MERCHANT: 'MERCHANT',
  LocationType.VACCINATOR: 'VACCINATOR',
  LocationType.CLUSTER: 'CLUSTER',
  LocationType.OCCASION: 'OCCASION',
};
