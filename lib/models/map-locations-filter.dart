import 'package:flutter/foundation.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:free2pass/enums/location-types.enum.dart';
import 'package:free2pass/models/location.dart';
import 'package:latlong2/latlong.dart';

/// filters applied to map locations
class MapLocationFilter {
  /// a query string
  final String? query;

  /// [LatLngBounds] in which the location should be
  final LatLngBounds? bounds;

  /// whether to show active (free2pass-enabled) locations only
  /// only show active locations: `true`
  /// only show non-active locations: `false`
  /// show all locations: `null`
  final bool? active;

  /// the [LocationType]s shown on the map
  /// Note: [LocationType.CLUSTER] is always added and fully ignored if
  /// specified
  /// [LocationType.OCCASION] is never added and fully ignored if specified.
  final List<LocationType> types;

  const MapLocationFilter(
      {this.query, this.bounds, this.active, this.types = const []});

  /// copies the instanced [MapLocationFilter] with given new parameters
  MapLocationFilter copyWith(
      {String? query,
      LatLngBounds? bounds,
      bool? active,
      List<LocationType>? types}) {
    return MapLocationFilter(
      query: query ?? this.query,
      bounds: bounds ?? this.bounds,
      active: active ?? this.active,
      types: types ?? this.types,
    );
  }

  bool matches(Location location) {
    bool matches = true;
    matches &= (types.isEmpty || types.contains(location.type));
    matches &= (active == null || location.activated == active);
    matches &= (bounds == null ||
        bounds!.contains(LatLng(location.lat!, location.lng!)));
    matches &= (query == null ||
        location.searchString.toLowerCase().contains(query!.toLowerCase()));
    return matches;
  }

  @nonVirtual
  @override
  bool operator ==(Object other) =>
      other is MapLocationFilter &&
      other.query == query &&
      //other.bounds == bounds && /// TODO: wait for upstream merge
      other.active == active &&
      other.types == types;

  @override
  int get hashCode =>
      query.hashCode + active.hashCode + types.hashCode; //+bounds.hashCode;
}
