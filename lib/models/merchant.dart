import 'package:free2pass/enums/location-types.enum.dart';
import 'package:free2pass/models/opening-hours.dart';
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

import 'location.dart';

part 'merchant.g.dart';

@HiveType(typeId: 7)
@JsonSerializable()
class Merchant extends Location {
  @HiveField(0)
  final String name;
  @HiveField(1)
  final String? address;

  /// latitude
  @HiveField(2)
  final double? lat;

  /// longitude
  @HiveField(3)
  final double? lng;
  @HiveField(4)
  final int? id;
  @HiveField(7)
  final String? contactPhone;
  @HiveField(6)
  final String? contactWebsite;
  @HiveField(8)
  final String? contactEmail;
  @HiveField(9)
  final int checkIn;
  @HiveField(10)
  final int? checkOut;
  @HiveField(11)
  @JsonKey(name: 'type', fromJson: decodeLocation)
  final LocationType type;

  @HiveField(12)
  final int? count;
  @HiveField(18)
  final List<OpeningHours>? openingTimes;
  @HiveField(19)
  final bool? isOpen;

  Merchant({
    required this.name,
    this.address,
    this.lat,
    this.lng,
    this.id,
    this.contactPhone,
    this.contactWebsite,
    this.contactEmail,
    required this.checkIn,
    this.checkOut,
    required this.type,
    this.count,
    this.openingTimes,
    this.isOpen,
  }) : super(name: name, type: type);

  String get searchString {
    String search = name;
    if (address != null) search += ' ' + address!;
    return search;
  }

  factory Merchant.fromJson(Map<String, dynamic> json) =>
      _$MerchantFromJson(json);

  Map<String, dynamic> toJson() => _$MerchantToJson(this);

  DateTime? get checkInDateTime =>
      DateTime.fromMillisecondsSinceEpoch(checkIn * 1000);

  DateTime? get checkOutDateTime => checkOut != null
      ? DateTime.fromMillisecondsSinceEpoch(checkOut! * 1000)
      : null;
}
