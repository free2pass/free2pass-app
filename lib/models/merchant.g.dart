// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'merchant.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MerchantAdapter extends TypeAdapter<Merchant> {
  @override
  final int typeId = 7;

  @override
  Merchant read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Merchant(
      name: fields[0] as String,
      address: fields[1] as String?,
      lat: fields[2] as double?,
      lng: fields[3] as double?,
      id: fields[4] as int?,
      contactPhone: fields[7] as String?,
      contactWebsite: fields[6] as String?,
      contactEmail: fields[8] as String?,
      checkIn: fields[9] as int,
      checkOut: fields[10] as int?,
      type: fields[11] as LocationType,
      count: fields[12] as int?,
      openingTimes: (fields[18] as List?)?.cast<OpeningHours>(),
      isOpen: fields[19] as bool?,
    );
  }

  @override
  void write(BinaryWriter writer, Merchant obj) {
    writer
      ..writeByte(15)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.address)
      ..writeByte(2)
      ..write(obj.lat)
      ..writeByte(3)
      ..write(obj.lng)
      ..writeByte(4)
      ..write(obj.id)
      ..writeByte(7)
      ..write(obj.contactPhone)
      ..writeByte(6)
      ..write(obj.contactWebsite)
      ..writeByte(8)
      ..write(obj.contactEmail)
      ..writeByte(9)
      ..write(obj.checkIn)
      ..writeByte(10)
      ..write(obj.checkOut)
      ..writeByte(11)
      ..write(obj.type)
      ..writeByte(12)
      ..write(obj.count)
      ..writeByte(18)
      ..write(obj.openingTimes)
      ..writeByte(19)
      ..write(obj.isOpen)
      ..writeByte(17)
      ..write(obj.activated);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MerchantAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Merchant _$MerchantFromJson(Map<String, dynamic> json) => Merchant(
      name: json['name'] as String,
      address: json['address'] as String?,
      lat: (json['lat'] as num?)?.toDouble(),
      lng: (json['lng'] as num?)?.toDouble(),
      id: json['id'] as int?,
      contactPhone: json['contactPhone'] as String?,
      contactWebsite: json['contactWebsite'] as String?,
      contactEmail: json['contactEmail'] as String?,
      checkIn: json['checkIn'] as int,
      checkOut: json['checkOut'] as int?,
      type: decodeLocation(json['type']),
      count: json['count'] as int?,
      openingTimes: (json['openingTimes'] as List<dynamic>?)
          ?.map((e) => OpeningHours.fromJson(e as Map<String, dynamic>))
          .toList(),
      isOpen: json['isOpen'] as bool?,
    );

Map<String, dynamic> _$MerchantToJson(Merchant instance) => <String, dynamic>{
      'name': instance.name,
      'address': instance.address,
      'lat': instance.lat,
      'lng': instance.lng,
      'id': instance.id,
      'contactPhone': instance.contactPhone,
      'contactWebsite': instance.contactWebsite,
      'contactEmail': instance.contactEmail,
      'checkIn': instance.checkIn,
      'checkOut': instance.checkOut,
      'type': _$LocationTypeEnumMap[instance.type],
      'count': instance.count,
      'openingTimes': instance.openingTimes,
      'isOpen': instance.isOpen,
    };

const _$LocationTypeEnumMap = {
  LocationType.ISSUER: 'ISSUER',
  LocationType.MERCHANT: 'MERCHANT',
  LocationType.VACCINATOR: 'VACCINATOR',
  LocationType.CLUSTER: 'CLUSTER',
  LocationType.OCCASION: 'OCCASION',
};
