import 'dart:ui';

import 'package:json_annotation/json_annotation.dart';

part 'notification-data.g.dart';

@JsonSerializable()
class NotificationData {
  final String? channelName;
  final String? channelDetails;
  final String? notificationTitle;
  final String? notificationBody;
  @JsonKey(ignore: true)
  final VoidCallback? handler;

  NotificationData({
    this.channelName,
    this.channelDetails,
    this.notificationTitle,
    this.notificationBody,
    this.handler,
  });

  factory NotificationData.fromJson(Map<String, dynamic> json) =>
      _$NotificationDataFromJson(json);

  Map<String, dynamic> toJson() => _$NotificationDataToJson(this);
}
