// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification-data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotificationData _$NotificationDataFromJson(Map<String, dynamic> json) =>
    NotificationData(
      channelName: json['channelName'] as String?,
      channelDetails: json['channelDetails'] as String?,
      notificationTitle: json['notificationTitle'] as String?,
      notificationBody: json['notificationBody'] as String?,
    );

Map<String, dynamic> _$NotificationDataToJson(NotificationData instance) =>
    <String, dynamic>{
      'channelName': instance.channelName,
      'channelDetails': instance.channelDetails,
      'notificationTitle': instance.notificationTitle,
      'notificationBody': instance.notificationBody,
    };
