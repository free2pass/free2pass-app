import 'package:json_annotation/json_annotation.dart';

part 'occasion-create-data.model.g.dart';

@JsonSerializable()
class OccasionCreationData {
  final String id;
  final String? healthDepartmentDailyPublicPGPKey;

  OccasionCreationData(this.id, this.healthDepartmentDailyPublicPGPKey);

  factory OccasionCreationData.fromJson(Map<String, dynamic> json) =>
      _$OccasionCreationDataFromJson(json);

  Map<String, dynamic> toJson() => _$OccasionCreationDataToJson(this);
}
