// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'occasion-create-data.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OccasionCreationData _$OccasionCreationDataFromJson(
        Map<String, dynamic> json) =>
    OccasionCreationData(
      json['id'] as String,
      json['healthDepartmentDailyPublicPGPKey'] as String?,
    );

Map<String, dynamic> _$OccasionCreationDataToJson(
        OccasionCreationData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'healthDepartmentDailyPublicPGPKey':
          instance.healthDepartmentDailyPublicPGPKey,
    };
