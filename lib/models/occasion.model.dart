import 'package:flutter/material.dart';
import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:hive/hive.dart';

part 'occasion.model.g.dart';

@HiveType(typeId: 10)
class Occasion {
  /// the user-selected name of the occasion
  @HiveField(0)
  final String name;

  /// the server-generated id of the occasion
  @HiveField(1)
  final String id;

  /// the timestamp of the start
  @HiveField(2)
  final int start;

  /// the timestamp of the end of the occasion
  @HiveField(3)
  final int end;

  /// the user-generated AES cipher for offline transfer
  @HiveField(4)
  final String cipher;

  /// the AES-cipher (and according to [usesAdvancedEncryption] maybe
  /// health department PGP) encrypted private key of the occasion
  @HiveField(5)
  final String encryptedPrivateKey;

  /// the RSA public key of this occasion
  @HiveField(6)
  final String publicKey;

  /// whether the health department supports advances encryption
  @HiveField(7)
  final bool usesAdvancedEncryption;

  Occasion({
    required this.name,
    required this.id,
    required this.start,
    required this.end,
    required this.cipher,
    required this.encryptedPrivateKey,
    required this.publicKey,
    required this.usesAdvancedEncryption,
  });

  static Future<Occasion> registerFrom({
    required String name,
    required DateTimeRange range,
    required String zip,
  }) async {
    final rsaKeyPair = await Crypto.generateRSAKeyPair(bitStrength: 4096);

    final occasionAESCipher = Crypto.createRandomString();

    String encryptedPrivateKey = await Crypto.aesEncryptData(
        cipher: occasionAESCipher, data: rsaKeyPair.privateKey);

    final response = await F2PApi.instance.occasions.create(zip: zip);

    if (!response.status.success) throw NullThrownError();

    /// using advanced encryption in case it is supported by
    /// the local health department
    bool usesAdvancesEncryption = false;
    if (response.payload!.healthDepartmentDailyPublicPGPKey != null) {
      encryptedPrivateKey = await Crypto.pgpEncryptData(
        base64Key: response.payload!.healthDepartmentDailyPublicPGPKey!,
        data: encryptedPrivateKey,
      );
      usesAdvancesEncryption = true;
    }

    final keyResponse = await F2PApi.instance.occasions.setKey(
        id: response.payload!.id, encryptedPrivateRSAKey: encryptedPrivateKey);
    if (!keyResponse.status.success) throw keyResponse.payload;

    return Occasion(
      name: name,
      start: (range.start.millisecondsSinceEpoch / 1000).round(),
      end: (range.end.millisecondsSinceEpoch / 1000).round(),
      id: response.payload!.id,
      cipher: occasionAESCipher,
      encryptedPrivateKey: encryptedPrivateKey,
      publicKey: rsaKeyPair.publicKey,
      usesAdvancedEncryption: usesAdvancesEncryption,
    );
  }

  DateTime get startDateTime =>
      DateTime.fromMillisecondsSinceEpoch(start * 1000);

  DateTime get endDateTime => DateTime.fromMillisecondsSinceEpoch(end * 1000);

  bool get isOngoing =>
      startDateTime.isBefore(DateTime.now()) &&
      endDateTime.isAfter(DateTime.now());

  String encodeCryptoQr() =>
      'https://go.free2pass.de/#/checkIn/' +
      'PRIV+$id/${Crypto.stringToBase64.encode(name)}/$cipher/$end;$publicKey';
}
