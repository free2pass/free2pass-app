// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'occasion.model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class OccasionAdapter extends TypeAdapter<Occasion> {
  @override
  final int typeId = 10;

  @override
  Occasion read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Occasion(
      name: fields[0] as String,
      id: fields[1] as String,
      start: fields[2] as int,
      end: fields[3] as int,
      cipher: fields[4] as String,
      encryptedPrivateKey: fields[5] as String,
      publicKey: fields[6] as String,
      usesAdvancedEncryption: fields[7] as bool,
    );
  }

  @override
  void write(BinaryWriter writer, Occasion obj) {
    writer
      ..writeByte(8)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.id)
      ..writeByte(2)
      ..write(obj.start)
      ..writeByte(3)
      ..write(obj.end)
      ..writeByte(4)
      ..write(obj.cipher)
      ..writeByte(5)
      ..write(obj.encryptedPrivateKey)
      ..writeByte(6)
      ..write(obj.publicKey)
      ..writeByte(7)
      ..write(obj.usesAdvancedEncryption);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is OccasionAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
