import 'package:free2pass/components/validity-countdown/validity-countdown.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'opening-hours.g.dart';

@JsonSerializable()
@HiveType(typeId: 12)
class OpeningHours {
  @HiveField(0)
  final int day;
  @HiveField(1)
  final int startTime;
  @HiveField(2)
  final int endTime;

  OpeningHours(this.day, this.startTime, this.endTime);

  static Map<DayOfWeek, String> parse(List<OpeningHours> openingHours) {
    Map<DayOfWeek, String> data = {};
    for (DayOfWeek day in DayOfWeek.values) {
      final matchingHours =
          openingHours.where((element) => element.day == day.index).toList();
      // ordering by start time
      matchingHours.sort((a, b) => a.startTime.compareTo(b.startTime));
      String timeString = '';
      matchingHours.forEach((element) {
        timeString += ', ${element.startTimeString} - ${element.endTimeString}';
      });
      if (timeString.isNotEmpty) {
        timeString = timeString.substring(2);
        data[day] = timeString;
      }
    }
    return data;
  }

  static String dayOfWeekName(S s, DayOfWeek day) {
    return {
      DayOfWeek.SUNDAY: s.sunday,
      DayOfWeek.MONDAY: s.monday,
      DayOfWeek.TUESDAY: s.tuesday,
      DayOfWeek.WEDNESDAY: s.wednesday,
      DayOfWeek.THURSDAY: s.thursday,
      DayOfWeek.FRIDAY: s.friday,
      DayOfWeek.SATURDAY: s.saturday,
    }[day]!;
  }

  String get startTimeString {
    final minutes = startTime % 60;
    return "${makeTwoCharacters(((startTime - minutes) ~/ 60).toString())}:${makeTwoCharacters(minutes)}";
  }

  String get endTimeString {
    final minutes = endTime % 60;
    return "${makeTwoCharacters(((endTime - minutes) ~/ 60).toString())}:${makeTwoCharacters(minutes)}";
  }

  factory OpeningHours.fromJson(Map<String, dynamic> json) =>
      _$OpeningHoursFromJson(json);

  Map<String, dynamic> toJson() => _$OpeningHoursToJson(this);
}

enum DayOfWeek {
  SUNDAY,
  MONDAY,
  TUESDAY,
  WEDNESDAY,
  THURSDAY,
  FRIDAY,
  SATURDAY,
}
