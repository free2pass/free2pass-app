import 'package:flutter/foundation.dart';

class ScanRouteData {
  final String? deeplink;
  final ScanType initialScreen;
  final ValueChanged<int>? changePage;

  const ScanRouteData({
    this.deeplink,
    required this.initialScreen,
    this.changePage,
  });

  ScanRouteData copyWith({
    String? deeplink,
    ScanType? screen,
    ValueChanged<int>? changePage,
  }) {
    return ScanRouteData(
      deeplink: deeplink == null ? this.deeplink : deeplink,
      initialScreen: screen == null ? initialScreen : screen,
      changePage: changePage == null ? this.changePage : changePage,
    );
  }
}

enum ScanType { CERTIFICATE, CHECKIN }
