import 'package:json_annotation/json_annotation.dart';

part 'update-data.g.dart';

enum Stores {
  AppleAppStore,
  GooglePlay,
  HuaweiAppGallery,
  FDroid,
  AmazonFireStore,
  GitLab,
  GitLabNightly,
}

const Map<Stores, String> storeUrls = {
  Stores.AppleAppStore: 'https://apps.apple.com/de/app/free2pass/id1561574735',
  Stores.AmazonFireStore: 'https://free2pass.de/download',
  Stores.FDroid: 'https://f-droid.org/de/packages/com.hanntech.free2pass/',
  Stores.GitLab: 'https://gitlab.com/free2pass/free2pass-app/-/tags/',
  Stores.GooglePlay:
      'https://play.google.com/store/apps/details?id=com.hanntech.free2pass',
  Stores.HuaweiAppGallery: 'https://appgallery.huawei.com/#/app/C104190677',
  Stores.GitLabNightly:
      "https://gitlab.com/free2pass/free2pass-app/-/jobs/artifacts/main/browse?job=build%3Aapk",
};

@JsonSerializable()
class UpdateData {
  final int? versionAvailable;
  final bool? forceUpdate;

  UpdateData(this.versionAvailable, this.forceUpdate);

  factory UpdateData.fromJson(Map<String, dynamic> json) =>
      _$UpdateDataFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateDataToJson(this);
}
