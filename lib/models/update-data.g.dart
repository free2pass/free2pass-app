// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update-data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateData _$UpdateDataFromJson(Map<String, dynamic> json) => UpdateData(
      json['versionAvailable'] as int?,
      json['forceUpdate'] as bool?,
    );

Map<String, dynamic> _$UpdateDataToJson(UpdateData instance) =>
    <String, dynamic>{
      'versionAvailable': instance.versionAvailable,
      'forceUpdate': instance.forceUpdate,
    };
