import 'dart:async';
import 'dart:convert';
import 'dart:core';

import 'package:free2pass/enums/vaccination-state.enum.dart';
import 'package:free2pass/models/certificate.dart';
import 'package:free2pass/models/offline-signature-data.dart';
import 'package:free2pass/models/vaccination-certificate-model.dart';
import 'package:free2pass/utils/encode-personal-data.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user-data.g.dart';

@HiveType(typeId: 11)
@JsonSerializable()
class UserData {
  @HiveField(0)
  final String name;
  @HiveField(1)
  final String street;
  @HiveField(2)
  final String zip;
  @HiveField(3)
  final String town;
  @HiveField(4)
  final int birthday;
  @HiveField(5)
  @JsonKey(ignore: true)
  final String? photoLocation;
  @HiveField(6)
  @JsonKey(ignore: true)
  final String? photoCheckSum;
  @HiveField(7)
  final String? phone;
  @HiveField(8)
  final String? email;
  @HiveField(9)
  @JsonKey(ignore: true)

  /// a hash identifying the [UserData] object.
  ///
  /// Used for authentication and comparison.
  String? hash;

  @JsonKey(ignore: true)
  FutureOr<bool>? hashInitialized;

  UserData({
    required this.name,
    required this.street,
    required this.zip,
    required this.town,
    required this.birthday,
    this.photoLocation,
    this.photoCheckSum,
    this.phone,
    this.email,
  }) {
    // in case the user is new, the hash is computed
    if (hash == null) {
      hashInitialized = hashPersonalData(userData: this)
          .then((value) => hash = value)
          .then((value) => true)
          .catchError((e) => false);
    } else
      hashInitialized = true;
  }

  /// JSON encodes [UserData] for contact tracing or certification
  ///
  /// a [VaccinationState] can be included in the data
  ///
  /// [addRandomEntropy] allows to add some random data to avoid equality checks
  /// on the provided user data
  ///
  /// if provided [OfflineSignatureData] can be included to support
  /// cryptographic verification for offline certificates
  ///
  /// you cannot provide both [addRandomEntropy] and [signatureData]
  ///
  /// [minimalize] allows you to remove unnecessary data such as street and
  /// address
  String encodeForApi({
    VaccinationState? vaccinationState,
    bool addRandomEntropy = false,
    OfflineSignatureData? signatureData,
    bool minimalize = false,
  }) {
    Map<String, dynamic> dataMap = this.toJson();

    if (dataMap['phone'] == null) dataMap['phone'] = getDefaultUserData().phone;
    if (vaccinationState != null)
      dataMap['vaccinationState'] = vaccinationStateMap[vaccinationState];
    if (addRandomEntropy)
      dataMap['randomEntropy'] = Crypto.createRandomString();
    if (signatureData != null) {
      dataMap['randomEntropy'] = signatureData.entropy;
      dataMap['expiry'] = signatureData.expiry;
      dataMap['signature'] = signatureData.signature;
      dataMap['salt'] = signatureData.salt;
    }
    if (minimalize) {
      const removals = ['street', 'phone', 'email'];
      removals.forEach((key) => dataMap.remove(key));
    }
    return jsonEncode(dataMap);
  }

  Future<String> makeQrDeeplink(Certificate certificate) async {
    final signatureData = await OfflineSignatureData.generate(
        user: this, expiry: certificate.expiryDateTime);
    final encodedData =
        encodeForApi(signatureData: signatureData, minimalize: true);
    if (signatureData == null) return encodedData;
    final base64String = Crypto.stringToBase64.encode(encodedData);
    return 'https://app.free2pass.de/verify#$base64String';
  }

  DateTime get birthdayDateTime =>
      DateTime.fromMillisecondsSinceEpoch(birthday * 1000);

  bool hasValidVaccinationCertificate(Box<VaccinationCertificateStorage> box) {
    for (final element in box.values) {
      final metadataBirthday =
          DateTime.fromMillisecondsSinceEpoch(element.metadata.birthday * 1000);

      if (element.metadata.isValid &&
          element.metadata.name.toLowerCase().trim() ==
              name.toLowerCase().trim() &&
          (metadataBirthday.year == birthdayDateTime.year &&
              metadataBirthday.month == birthdayDateTime.month &&
              metadataBirthday.day == birthdayDateTime.day)) return true;
    }
    return false;
  }

  factory UserData.fromJson(Map<String, dynamic> json) =>
      _$UserDataFromJson(json);

  Map<String, dynamic> toJson() => _$UserDataToJson(this);
}
