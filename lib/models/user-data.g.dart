// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user-data.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class UserDataAdapter extends TypeAdapter<UserData> {
  @override
  final int typeId = 11;

  @override
  UserData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return UserData(
      name: fields[0] as String,
      street: fields[1] as String,
      zip: fields[2] as String,
      town: fields[3] as String,
      birthday: fields[4] as int,
      photoLocation: fields[5] as String?,
      photoCheckSum: fields[6] as String?,
      phone: fields[7] as String?,
      email: fields[8] as String?,
    )..hash = fields[9] as String?;
  }

  @override
  void write(BinaryWriter writer, UserData obj) {
    writer
      ..writeByte(10)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.street)
      ..writeByte(2)
      ..write(obj.zip)
      ..writeByte(3)
      ..write(obj.town)
      ..writeByte(4)
      ..write(obj.birthday)
      ..writeByte(5)
      ..write(obj.photoLocation)
      ..writeByte(6)
      ..write(obj.photoCheckSum)
      ..writeByte(7)
      ..write(obj.phone)
      ..writeByte(8)
      ..write(obj.email)
      ..writeByte(9)
      ..write(obj.hash);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserData _$UserDataFromJson(Map<String, dynamic> json) => UserData(
      name: json['name'] as String,
      street: json['street'] as String,
      zip: json['zip'] as String,
      town: json['town'] as String,
      birthday: json['birthday'] as int,
      phone: json['phone'] as String?,
      email: json['email'] as String?,
    );

Map<String, dynamic> _$UserDataToJson(UserData instance) => <String, dynamic>{
      'name': instance.name,
      'street': instance.street,
      'zip': instance.zip,
      'town': instance.town,
      'birthday': instance.birthday,
      'phone': instance.phone,
      'email': instance.email,
    };
