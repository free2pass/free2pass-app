import 'dart:io';

import 'package:dart_base45/dart_base45.dart';
import 'package:dart_cose/dart_cose.dart';
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'vaccination-certificate-model.g.dart';

const supportedHCVersions = [1];

@JsonSerializable()
class VaccinationCertificateModel {
  @JsonKey(name: 'v', fromJson: _vaccinationInformationDecoder)
  final VaccinationInformation vaccinationInformation;
  @JsonKey(name: 'dob', fromJson: _decodeEuropeanDate)
  final DateTime birthday;

  @JsonKey(name: 'nam', fromJson: _personalInformationDecoder)
  final VaccinationPersonalInformation person;

  @JsonKey(name: 'ver')
  final String version;

  VaccinationCertificateModel(
      this.vaccinationInformation, this.birthday, this.person, this.version);

  factory VaccinationCertificateModel.fromJson(Map<String, dynamic> json) =>
      _$VaccinationCertificateModelFromJson(json);

  factory VaccinationCertificateModel.fromQr(String qr) {
    // checking HC version
    if (qr.startsWith('HC')) {
      if (!supportedHCVersions.contains(int.parse(qr.substring(2, 3)))) {
        throw UnsupportedHealthCertificateVersion(qr.substring(0, 3));
      }
      qr = qr.substring(4);
    }

    final base45 = Base45.decode(qr);
    final cose = zlib.decode(base45);
    final coseResult = Cose.decodeAndVerify(
      cose,
      {'kid': '''pem'''},
    );

    final Map<String, dynamic> json = Map.from(coseResult.payload[-260][1]);
    return VaccinationCertificateModel.fromJson(json);
  }

  Map<String, dynamic> toJson() => _$VaccinationCertificateModelToJson(this);
}

@JsonSerializable()
class VaccinationPersonalInformation {
  @JsonKey(name: 'fn')
  final String familyName;
  @JsonKey(name: 'gn')
  final String firstName;

  VaccinationPersonalInformation(this.firstName, this.familyName);

  factory VaccinationPersonalInformation.fromJson(Map<String, dynamic> json) =>
      _$VaccinationPersonalInformationFromJson(json);

  Map<String, dynamic> toJson() => _$VaccinationPersonalInformationToJson(this);

  String get fullName => toString();

  @override
  String toString() {
    return '$firstName $familyName';
  }
}

@JsonSerializable()
class VaccinationInformation {
  @JsonKey(name: 'ci')
  final String uniqueIdentifier;
  @JsonKey(name: 'co')
  final String origin;

  @JsonKey(name: 'is')
  final String issuer;

  @JsonKey(name: 'dt', fromJson: _decodeEuropeanDate)
  final DateTime date;

  @JsonKey(name: 'dn')
  final int numVaccinated;

  @JsonKey(name: 'sd')
  final int numFinalVaccination;

  VaccinationInformation(
    this.uniqueIdentifier,
    this.origin,
    this.issuer,
    this.date,
    this.numVaccinated,
    this.numFinalVaccination,
  );

  factory VaccinationInformation.fromJson(Map<String, dynamic> json) =>
      _$VaccinationInformationFromJson(json);

  Map<String, dynamic> toJson() => _$VaccinationInformationToJson(this);

  bool get gotAllVaccinations => numVaccinated == numFinalVaccination;

  bool get twoWeeksPassed =>
      date.isBefore(DateTime.now().subtract(Duration(days: 14)));

  bool get isOptional => numVaccinated >= 3;

  bool get isValid => isOptional || twoWeeksPassed && gotAllVaccinations;
}

/// this object is used as a layer for better performance to quickly access
/// metadata of a [VaccinationCertificateModel]
@HiveType(typeId: 17)
class VaccinationCertificateStorage {
  /// the raw String scanned from the QR code
  @HiveField(1)
  final String rawQr;

  /// the metadata for quick processing
  @HiveField(2)
  final VaccinationCertificateStorageMetadata metadata;

  VaccinationCertificateStorage(this.rawQr, this.metadata);
}

@HiveType(typeId: 18)
class VaccinationCertificateStorageMetadata {
  @HiveField(1)
  final String name;

  @HiveField(2)
  final int birthday;

  @HiveField(3)
  final int vaccinationDate;

  @HiveField(4)
  final int numVaccinated;

  @HiveField(5)
  final int numFinalVaccination;

  VaccinationCertificateStorageMetadata(
      {required this.name,
      required this.birthday,
      required this.vaccinationDate,
      required this.numVaccinated,
      required this.numFinalVaccination});

  factory VaccinationCertificateStorageMetadata.fromModel(
          VaccinationCertificateModel model) =>
      VaccinationCertificateStorageMetadata(
        name: model.person.fullName,
        birthday: (model.birthday.millisecondsSinceEpoch / 1000).round(),
        numFinalVaccination: model.vaccinationInformation.numFinalVaccination,
        numVaccinated: model.vaccinationInformation.numVaccinated,
        vaccinationDate:
            (model.vaccinationInformation.date.millisecondsSinceEpoch / 1000)
                .round(),
      );

  bool get gotAllVaccinations => numVaccinated == numFinalVaccination;

  bool get isOptional => numVaccinated >= 3;

  bool get twoWeeksPassed =>
      DateTime.fromMillisecondsSinceEpoch(vaccinationDate * 1000).isBefore(
        DateTime.now().subtract(Duration(days: 14)),
      );

  bool get isValid => isOptional || twoWeeksPassed && gotAllVaccinations;
}

DateTime _decodeEuropeanDate(String input) {
  final year = input.substring(0, 4);
  final month = input.substring(5, 7);
  final day = input.substring(8, 10);
  return DateTime(int.parse(year), int.parse(month), int.parse(day));
}

VaccinationInformation _vaccinationInformationDecoder(Iterable input) {
  final Map<String, dynamic> vaccinationInformation = Map.from(input.first);
  return _$VaccinationInformationFromJson(vaccinationInformation);
}

VaccinationPersonalInformation _personalInformationDecoder(
    Map<dynamic, dynamic> input) {
  final Map<String, dynamic> personalInformation = Map.from(input);
  return VaccinationPersonalInformation.fromJson(personalInformation);
}

class UnsupportedHealthCertificateVersion extends Error {
  final String version;

  UnsupportedHealthCertificateVersion(this.version);
}
