// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vaccination-certificate-model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class VaccinationCertificateStorageAdapter
    extends TypeAdapter<VaccinationCertificateStorage> {
  @override
  final int typeId = 17;

  @override
  VaccinationCertificateStorage read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return VaccinationCertificateStorage(
      fields[1] as String,
      fields[2] as VaccinationCertificateStorageMetadata,
    );
  }

  @override
  void write(BinaryWriter writer, VaccinationCertificateStorage obj) {
    writer
      ..writeByte(2)
      ..writeByte(1)
      ..write(obj.rawQr)
      ..writeByte(2)
      ..write(obj.metadata);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is VaccinationCertificateStorageAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class VaccinationCertificateStorageMetadataAdapter
    extends TypeAdapter<VaccinationCertificateStorageMetadata> {
  @override
  final int typeId = 18;

  @override
  VaccinationCertificateStorageMetadata read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return VaccinationCertificateStorageMetadata(
      name: fields[1] as String,
      birthday: fields[2] as int,
      vaccinationDate: fields[3] as int,
      numVaccinated: fields[4] as int,
      numFinalVaccination: fields[5] as int,
    );
  }

  @override
  void write(BinaryWriter writer, VaccinationCertificateStorageMetadata obj) {
    writer
      ..writeByte(5)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.birthday)
      ..writeByte(3)
      ..write(obj.vaccinationDate)
      ..writeByte(4)
      ..write(obj.numVaccinated)
      ..writeByte(5)
      ..write(obj.numFinalVaccination);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is VaccinationCertificateStorageMetadataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VaccinationCertificateModel _$VaccinationCertificateModelFromJson(
        Map<String, dynamic> json) =>
    VaccinationCertificateModel(
      _vaccinationInformationDecoder(json['v'] as List),
      _decodeEuropeanDate(json['dob'] as String),
      _personalInformationDecoder(json['nam'] as Map),
      json['ver'] as String,
    );

Map<String, dynamic> _$VaccinationCertificateModelToJson(
        VaccinationCertificateModel instance) =>
    <String, dynamic>{
      'v': instance.vaccinationInformation,
      'dob': instance.birthday.toIso8601String(),
      'nam': instance.person,
      'ver': instance.version,
    };

VaccinationPersonalInformation _$VaccinationPersonalInformationFromJson(
        Map<String, dynamic> json) =>
    VaccinationPersonalInformation(
      json['gn'] as String,
      json['fn'] as String,
    );

Map<String, dynamic> _$VaccinationPersonalInformationToJson(
        VaccinationPersonalInformation instance) =>
    <String, dynamic>{
      'fn': instance.familyName,
      'gn': instance.firstName,
    };

VaccinationInformation _$VaccinationInformationFromJson(
        Map<String, dynamic> json) =>
    VaccinationInformation(
      json['ci'] as String,
      json['co'] as String,
      json['is'] as String,
      _decodeEuropeanDate(json['dt'] as String),
      json['dn'] as int,
      json['sd'] as int,
    );

Map<String, dynamic> _$VaccinationInformationToJson(
        VaccinationInformation instance) =>
    <String, dynamic>{
      'ci': instance.uniqueIdentifier,
      'co': instance.origin,
      'is': instance.issuer,
      'dt': instance.date.toIso8601String(),
      'dn': instance.numVaccinated,
      'sd': instance.numFinalVaccination,
    };
