import 'dart:async';

class WebSocketController {
  final Future? connected;
  final Completer? disconnect;

  WebSocketController({this.connected, this.disconnect});
}
