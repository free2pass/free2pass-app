import 'package:hive/hive.dart';

part 'push-entrypoint.g.dart';

@HiveType(typeId: 20)
enum PushEntrypoint {
  @HiveField(0)
  NONE,
  @HiveField(1)
  CERTIFICATE,
  @HiveField(2)
  APPOINTMENT,
}
