// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'push-entrypoint.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PushEntrypointAdapter extends TypeAdapter<PushEntrypoint> {
  @override
  final int typeId = 20;

  @override
  PushEntrypoint read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return PushEntrypoint.NONE;
      case 1:
        return PushEntrypoint.CERTIFICATE;
      case 2:
        return PushEntrypoint.APPOINTMENT;
      default:
        return PushEntrypoint.NONE;
    }
  }

  @override
  void write(BinaryWriter writer, PushEntrypoint obj) {
    switch (obj) {
      case PushEntrypoint.NONE:
        writer.writeByte(0);
        break;
      case PushEntrypoint.CERTIFICATE:
        writer.writeByte(1);
        break;
      case PushEntrypoint.APPOINTMENT:
        writer.writeByte(2);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PushEntrypointAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
