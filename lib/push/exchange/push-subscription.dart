import 'package:free2pass/push/exchange/push-entrypoint.dart';
import 'package:hive/hive.dart';

part 'push-subscription.g.dart';

@HiveType(typeId: 19)
class PushSubscription {
  @HiveField(0)
  final PushEntrypoint type;
  @HiveField(1)
  final String identifier;

  PushSubscription({required this.type, required this.identifier});
}
