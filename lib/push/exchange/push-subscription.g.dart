// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'push-subscription.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PushSubscriptionAdapter extends TypeAdapter<PushSubscription> {
  @override
  final int typeId = 19;

  @override
  PushSubscription read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PushSubscription(
      type: fields[0] as PushEntrypoint,
      identifier: fields[1] as String,
    );
  }

  @override
  void write(BinaryWriter writer, PushSubscription obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.type)
      ..writeByte(1)
      ..write(obj.identifier);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PushSubscriptionAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
