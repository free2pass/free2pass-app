import 'dart:async';

import 'package:free2pass/models/appointment.dart';
import 'package:free2pass/models/certificate.dart';
import 'package:free2pass/push/exchange/push-entrypoint.dart';
import 'package:free2pass/push/push-adapter.dart';

class NoPushAdapter extends PushAdapter {
  @override
  FutureOr<void> initialize() {
    print('Successfully registered');
  }

  @override
  FutureOr<bool> unregister() => super.unregister();

  @override
  FutureOr<bool> randomize() => true;

  @override
  Future<bool> registerAppointment(Appointment appointment) async => true;

  @override
  Future<bool> registerCertificate(Certificate certificate) async => true;

  @override
  Future<bool> requestPermission() async => true;

  @override
  bool get isActivated => true;

  @override
  bool get isDummy => true;

  @override
  FutureOr<PushEntrypoint> initialMessage() => PushEntrypoint.NONE;

  @override
  Stream<PushEntrypoint> messagesOpeningApp() => Stream.empty();
}
