import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/appointment.dart';
import 'package:free2pass/models/certificate.dart';
import 'package:free2pass/push/exchange/push-entrypoint.dart';

abstract class PushAdapter {
  static late PushAdapter _current;

  static PushAdapter get instance => _current;

  static FutureOr<void> registerAdapter<T extends PushAdapter>(T adapter) {
    _current = adapter;
    return _current.initialize();
  }

  static unsubscribeAppointment(String id) {
    _unsubscribe(PushEntrypoint.APPOINTMENT, id);
  }

  static _unsubscribe(PushEntrypoint type, String identifier) {
    pushSubscriptions.keys.forEach((key) {
      final handle = pushSubscriptions.get(key)!;
      if (handle.type == type && handle.identifier == identifier)
        pushSubscriptions.delete(key);
    });
  }

  FutureOr<void> initialize();

  @mustCallSuper
  FutureOr<bool> unregister() async {
    await pushSubscriptions.clear();
    return true;
  }

  FutureOr<bool> randomize();

  Future<bool> requestPermission();

  Future<bool> registerCertificate(Certificate certificate);

  Future<bool> registerAppointment(Appointment appointment);

  FutureOr<PushEntrypoint> initialMessage();

  Stream<PushEntrypoint> messagesOpeningApp();

  bool get isActivated;

  bool get isDummy;

  bool get isValid => !isDummy && isActivated;
}
