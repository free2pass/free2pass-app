import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/models/scan-route-data.dart';
import 'package:free2pass/screens/create-occasion/create-occasion.dart';
import 'package:free2pass/screens/expired-certificates/expired-certificates.screen.dart';
import 'package:free2pass/screens/home/home.dart';
import 'package:free2pass/screens/introduction/introduction.dart';
import 'package:free2pass/screens/multi-user/multi-user.dart';
import 'package:free2pass/screens/recent-locations/recent-locations.screen.dart';
import 'package:free2pass/screens/scan-flow/scan-flow.dart';
import 'package:free2pass/screens/settings/settings.screen.dart';
import 'package:free2pass/screens/setup/setup.dart';
import 'package:free2pass/screens/splash/splash.dart';
import 'package:free2pass/screens/vaccination-certificate/vaccination-certificate-stub.dart'
    if (dart.library.io) 'package:free2pass/screens/vaccination-certificate/vaccination-certificate.dart';

/// The [RouteGenerator] handles the routing in the application
/// Usually, the [generateRoute] method is accessed.
class RouteGenerator {
  /// The [generateRoute] method is used to build a [Route] from
  /// [RouteSettings] containing a name. This is usually accessed by
  /// ```dart
  /// Navigator.of(context).pushNamed('/routeName');
  /// ```
  ///
  /// Every route name known in our app is switched here.
  /// Route names should be accessible by `MyScreen.routeName`.
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case SplashScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => SplashScreen(), settings: settings);
      case HomeScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => HomeScreen(), settings: settings);
      case SetupScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => SetupScreen(), settings: settings);
      case ScanFlowScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => ScanFlowScreen(
                  data: settings.arguments is ScanRouteData
                      ? settings.arguments as ScanRouteData
                      : ScanRouteData(initialScreen: ScanType.CERTIFICATE),
                ),
            settings: settings);
      case IntroductionScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => IntroductionScreen(), settings: settings);
      case ExpiredCertificatesScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => ExpiredCertificatesScreen(), settings: settings);
      case RecentLocationsScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => RecentLocationsScreen(), settings: settings);
      case CreateOccasionScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => CreateOccasionScreen(), settings: settings);
      case MultiUserScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => MultiUserScreen(), settings: settings);
      case SettingsScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => SettingsScreen(), settings: settings);
      case VaccinationCertificateScreen.routeName:
        return MaterialPageRoute(
            builder: (_) => VaccinationCertificateScreen(), settings: settings);
    }

    return _generateErrorRoute();
  }

  /// In case we don't know the right directions
  static _generateErrorRoute() => MaterialPageRoute(
      builder: (context) => Scaffold(
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    S.of(context).notFound,
                    style: Theme.of(context).textTheme.headline5,
                  ),
                  Text(
                    S.of(context).theRequestedPageWasNotFound,
                  ),
                  OutlinedButton(
                      onPressed: () => Navigator.of(context)
                          .pushReplacementNamed(SplashScreen.routeName),
                      child: Text(S.of(context).goBackHome))
                ],
              ),
            ),
          ));
}
