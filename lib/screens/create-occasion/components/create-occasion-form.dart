import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/models/occasion-for-data.model.dart';
import 'package:free2pass/models/occasion.model.dart';
import 'package:free2pass/utils/encode-personal-data.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:intl/intl.dart';
import 'package:time_range_picker/time_range_picker.dart';

class CreateOccasionForm extends StatefulWidget {
  const CreateOccasionForm({Key? key}) : super(key: key);

  @override
  CreateOccasionFormState createState() => CreateOccasionFormState();
}

class CreateOccasionFormState extends State<CreateOccasionForm> {
  TextEditingController _nameController = TextEditingController();
  GlobalKey<FormState> _formKey = GlobalKey();

  bool _autoCheckIn = true;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            S.of(context).createAnOccasion,
            style: Theme.of(context).textTheme.headline5,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 12, bottom: 8),
            child: Text(S.of(context).occasionNameLong),
          ),
          TextFormField(
            autofocus: true,
            controller: _nameController,
            validator: _nameValidator,
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              labelText: S.of(context).name,
              border: OutlineInputBorder(),
            ),
          ),
          SwitchListTile(
            value: _autoCheckIn,
            onChanged: _setAutoCheckIn,
            title: Text(S.of(context).occasionAutoCheckIn),
          ),
          FormField<DateTimeRange>(
            builder: (foo) {
              return ListTile(
                leading: Icon(Icons.date_range),
                title: _occasionDateTimeRange == null
                    ? Text(S.of(context).occasionTime)
                    : AlignedMarkdown(
                        data: S.of(context).today +
                            ' **' +
                            DateFormat.Hm()
                                .format(_occasionDateTimeRange!.start) +
                            ' - ' +
                            DateFormat.Hm()
                                .format(_occasionDateTimeRange!.end) +
                            '** ' +
                            S.of(context).oClock,
                        alignment: CrossAxisAlignment.start,
                      ),
                subtitle: foo.hasError
                    ? Text(
                        foo.errorText!,
                        style: TextStyle(color: red),
                      )
                    : null,
                onTap: _pickDateTimeRange,
              );
            },
            validator: _notNullValidator,
          )
        ],
      ),
    );
  }

  /// checks whether the form is valid and creates an [Occasion]
  OccasionFormData save() {
    if (!_formKey.currentState!.validate())
      return OccasionFormData(success: false);
    else
      return OccasionFormData(
        success: true,
        autoCheckIn: _autoCheckIn,
        // here, the occasion is being created and registered
        occasionDelegate: Occasion.registerFrom(
          name: _nameController.text,
          zip: getCurrentUser().zip,
          range: _occasionDateTimeRange!,
        ),
      );
  }

  Future<void> _pickDateTimeRange() async {
    final now = DateTime.now();
    final result = await showTimeRangePicker(
      disabledColor: gray,
      fromText: S.of(context).from,
      toText: S.of(context).to,
      context: context,
      selectedColor: blue,
      disabledTime: TimeRange(
        startTime: TimeOfDay(hour: 0, minute: 0),
        endTime: TimeOfDay.now(),
      ),
    );
    if (result is TimeRange) {
      final dateTimeRange = DateTimeRange(
        start: DateTime(
          now.year,
          now.month,
          now.day,
          result.startTime.hour,
          result.startTime.minute,
        ),
        end: DateTime(
          now.year,
          now.month,
          now.day,
          result.endTime.hour,
          result.endTime.minute,
        ),
      );
      setState(() {
        _occasionDateTimeRange = dateTimeRange;
      });
    }
  }

  DateTimeRange? _occasionDateTimeRange;

  void _setAutoCheckIn(bool value) {
    setState(() {
      _autoCheckIn = value;
    });
  }

  String? _nameValidator(String? value) {
    if (value != null && value.trim().length > 3) return null;
    return S.of(context).validatorOccasionName;
  }

  String? _notNullValidator(value) {
    if (_occasionDateTimeRange == null)
      return S.of(context).validatorOccasionRange;
    return null;
  }
}
