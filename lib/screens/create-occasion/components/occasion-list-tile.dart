import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/models/occasion.model.dart';
import 'package:free2pass/screens/create-occasion/components/occasion-qr.dart';
import 'package:free2pass_support/free2pass_support.dart';

class OccasionListTile extends StatelessWidget {
  final Occasion occasion;

  const OccasionListTile({Key? key, required this.occasion}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: OpenContainer(closedBuilder: (context, w) {
        return ListTile(
          title: Text(occasion.name),
          subtitle:
              Text(S.of(context).end + ': ' + occasion.endDateTime.toString()),
          trailing: Tooltip(
              message: S.of(context).tapToShowQr, child: Icon(Icons.crop)),
        );
      }, openBuilder: (context, w) {
        return Scaffold(
          body: ResponsiveBox(
            child: OccasionQrView(
              occasion: occasion,
            ),
          ),
        );
      }),
    );
  }
}
