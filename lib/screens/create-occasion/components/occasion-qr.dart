import 'package:flutter/material.dart';
import 'package:free2pass/dialogs/passport-dialog/passport-dialog.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/models/occasion.model.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:pretty_qr_code/pretty_qr_code.dart';

class OccasionQrView extends StatelessWidget {
  final Occasion occasion;

  const OccasionQrView({Key? key, required this.occasion}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constrains) {
      return Center(
        child: ListView(
          shrinkWrap: true,
          children: [
            ListTile(
              title: Text(
                occasion.name,
                style: Theme.of(context).textTheme.headline4,
              ),
            ),
            Center(
              child: PrettyQr(
                image: AssetImage('assets/logos/icon.png'),
                size: (constrains.maxWidth < constrains.maxHeight
                        ? constrains.maxWidth
                        : constrains.maxHeight) -
                    128,
                data: occasion.encodeCryptoQr(),
                errorCorrectLevel: QrErrorCorrectLevel.Q,
                roundEdges: true,
                elementColor: Theme.of(context).brightness == Brightness.light
                    ? gray.withOpacity(.65)
                    : Colors.white.withOpacity(.65),
              ),
            ),
            ListTile(
              leading: Icon(Icons.event),
              title: Text(
                  localDateTimeString(S.of(context), occasion.startDateTime)),
              subtitle: Text(S.of(context).start),
            ),
            ListTile(
              leading: Icon(Icons.timelapse),
              title: Text(
                  localDateTimeString(S.of(context), occasion.endDateTime)),
              subtitle: Text(S.of(context).end),
            ),
          ],
        ),
      );
    });
  }
}
