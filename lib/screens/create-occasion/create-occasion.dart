import 'dart:async';

import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/components/requires-network-page.dart';
import 'package:free2pass/enums/location-types.enum.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/merchant.dart';
import 'package:free2pass/models/occasion-for-data.model.dart';
import 'package:free2pass/models/occasion.model.dart';
import 'package:free2pass/screens/create-occasion/components/create-occasion-form.dart';
import 'package:free2pass/screens/create-occasion/components/occasion-qr.dart';
import 'package:free2pass/utils/encode-personal-data.dart';
import 'package:free2pass_support/free2pass_support.dart';

class CreateOccasionScreen extends StatefulWidget {
  static const routeName = '/createOccasion';

  @override
  _CreateOccasionScreenState createState() => _CreateOccasionScreenState();
}

class _CreateOccasionScreenState extends State<CreateOccasionScreen> {
  AxisTransitionController _axisController = AxisTransitionController();
  GlobalKey<CreateOccasionFormState> _formKey = GlobalKey();

  bool _isLoading = false;

  Occasion? _occasionData;

  @override
  Widget build(BuildContext context) {
    return RequiresNetworkPage(
      child: Scaffold(
        appBar: DefaultAppBar(),
        body: ResponsiveBox(
          child: Center(
            child: LayoutBuilder(builder: (context, constrains) {
              return AxisTransitionView(
                controller: _axisController,
                direction: SharedAxisTransitionType.vertical,
                children: [
                  Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16),
                    ),
                    margin: const EdgeInsets.all(16),
                    key: ValueKey(0),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: CreateOccasionForm(key: _formKey),
                    ),
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      CircularProgressIndicator(
                        key: ValueKey(1),
                      ),
                      Container(height: 16),
                      Text(S.of(context).weEncryptYourData),
                    ],
                  ),
                  _occasionData != null
                      ? OccasionQrView(
                          key: ValueKey(2), occasion: _occasionData!)
                      : Container(),
                  Text(S.of(context).errorCreatingYourOccasion),
                ],
              );
            }),
          ),
        ),
        floatingActionButton: !_isLoading
            ? _axisController.current > 1
                ? FloatingActionButton.extended(
                    label: Text(S.of(context).close),
                    onPressed: Navigator.of(context).pop,
                  )
                : FloatingActionButton.extended(
                    onPressed: _next,
                    label: Text(S.of(context).next),
                  )
            : null,
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      ),
    );
  }

  void _next() {
    switch (_axisController.current) {
      case 0:
        // the actual occasion si created and registered within the form
        final data = _formKey.currentState!.save();
        if (!data.success) return;

        _registerOccasion(data);
        break;
    }
  }

  Future<void> _registerOccasion(OccasionFormData data) async {
    _showProgress();

    try {
      data.occasionDelegate!.catchError((e) => throw (e));
      _occasionData = await data.occasionDelegate!;

      await createdOccasions.put(_occasionData!.id, _occasionData!);

      if (data.autoCheckIn!) {
        await F2PApi.instance.occasions.setEncryptedUserData(
          id: _occasionData!.id,
          encryptedUserData: await Crypto.rsaEncryptData(
            base64Key: _occasionData!.publicKey,
            data: getCurrentUser().encodeForApi(addRandomEntropy: true),
          ),
        );
        await visitedLocations.add(
          Merchant(
            name: _occasionData!.name,
            checkIn: _occasionData!.start,
            checkOut: _occasionData!.end,
            type: LocationType.OCCASION,
          ),
        );
      }

      _axisController.current = 2;
    } catch (e) {
      _axisController.current = 3;
    }
    setState(() {
      _isLoading = false;
    });
  }

  void _showProgress() {
    _axisController.current = 1;
    setState(() {
      _isLoading = true;
    });
  }
}
