import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:free2pass/enums/certificate-status.enum.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/models/certificate.dart';
import 'package:free2pass/screens/pdf-export/pdf-export.dart';
import 'package:free2pass/utils/contact-issuer.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:intl/intl.dart';

class CertificateDetailTile extends StatefulWidget {
  final Certificate certificate;

  const CertificateDetailTile({Key? key, required this.certificate})
      : super(key: key);

  @override
  _CertificateDetailTileState createState() => _CertificateDetailTileState();
}

class _CertificateDetailTileState extends State<CertificateDetailTile> {
  @override
  Widget build(BuildContext context) {
    final issuer = widget.certificate.issuer;
    String fullTestState;
    Widget icon;

    switch (widget.certificate.expectedValidity) {
      case CertificateStatusValidity.NEGATIVE:
        fullTestState = S.of(context).negativeLong;
        icon = Icon(
          Icons.check,
          color: green,
        );
        break;
      case CertificateStatusValidity.POSITIVE:
        fullTestState = S.of(context).positiveLong;
        icon = Icon(
          Icons.error,
          color: red,
        );
        break;

      case CertificateStatusValidity.PENDING:
        fullTestState = S.of(context).testedLong;
        icon = Icon(
          Icons.access_time,
        );
        break;

      case CertificateStatusValidity.REVOKED:
        fullTestState = S.of(context).testedLong;
        icon = Icon(
          Icons.cancel,
          color: red,
        );
        break;
      default:
        fullTestState = S.of(context).testedLong;
        icon = Icon(
          Icons.help,
        );
        break;
    }

    final canExportPdf = widget.certificate.expectedValidity ==
            CertificateStatusValidity.NEGATIVE &&
        widget.certificate.expiryDateTime.isAfter(DateTime.now()) &&
        !widget.certificate.issuer!.isPrivate;

    return ExpansionTileCard(
      leading: icon,
      title: Text(widget.certificate.issuer!.name),
      subtitle: Text(_dateTimeString(widget.certificate.resultDateTime)),
      children: [
        if (widget.certificate.name != null)
          ListTile(
            leading: Icon(Icons.tag),
            title: Text(
              widget.certificate.name!,
              style: widget.certificate.expectedValidity ==
                      CertificateStatusValidity.PENDING
                  ? Theme.of(context).textTheme.headline3
                  : null,
            ),
            subtitle: Text(S.of(context).certificateId),
          ),
        ListTile(
          title: AlignedMarkdown(data: '## ' + fullTestState),
        ),
        if (issuer!.isPrivate)
          ListTile(
            leading: issuer.logo != null
                ? Image.network(
                    issuer.logo!,
                    width: 64,
                    height: 64,
                  )
                : null,
            title: AlignedMarkdown(
              data: S.of(context).internalCertificateLongFormatted,
              alignment: CrossAxisAlignment.start,
            ),
          ),
        ListTile(
          leading: Icon(Icons.healing),
          title: Text(issuer.name),
          subtitle: Text(S.of(context).issuer),
        ),
        ListTile(
          leading: Icon(Icons.pin_drop),
          title: Text(issuer.address),
        ),
        if (widget.certificate.expiryDate != null &&
            widget.certificate.expiryDate is int)
          ListTile(
            leading: Icon(Icons.calendar_today),
            subtitle: Text(
                widget.certificate.expiryDateTime.isAfter(DateTime.now())
                    ? S.of(context).validity
                    : S.of(context).expiredAt),
            title: Text(
                DateFormat.yMMMMd().format(widget.certificate.expiryDateTime) +
                    ', ' +
                    DateFormat.Hm().format(widget.certificate.expiryDateTime) +
                    ' ' +
                    S.of(context).oClock),
          ),
        ButtonBar(
          children: [
            if (canExportPdf)
              OutlinedButton.icon(
                onPressed: _sharePdf,
                label: Text(S.of(context).shareAsPdf),
                icon: Icon(Icons.picture_as_pdf),
              ),
            OutlinedButton(
              onPressed: _contactIssuer,
              child: Text(S.of(context).contactIssuer),
            ),
          ],
        ),
      ],
    );
  }

  String _dateTimeString(DateTime dateTime) {
    return DateFormat.yMMMMd().format(dateTime) +
        ', ' +
        DateFormat.Hm().format(dateTime) +
        ' ' +
        S.of(context).oClock;
  }

  void _contactIssuer() {
    contactIssuer(context, widget.certificate.issuer!);
  }

  void _sharePdf() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => PdfExportScreen(certificate: widget.certificate),
        fullscreenDialog: true));
  }
}
