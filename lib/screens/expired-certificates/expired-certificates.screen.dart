import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/components/default-drawer/default-drawer.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/certificate.dart';
import 'package:free2pass/screens/expired-certificates/certificate-detail-tile/certificate-detail-tile.dart';
import 'package:free2pass/screens/home/components/views/decoration/badge-decoration.dart';
import 'package:free2pass/utils/encode-personal-data.dart';
import 'package:free2pass/utils/hive.util.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:hive_flutter/hive_flutter.dart';

class ExpiredCertificatesScreen extends StatefulWidget {
  static const routeName = '/recentCertificates';

  @override
  _ExpiredCertificatesScreenState createState() =>
      _ExpiredCertificatesScreenState();
}

class _ExpiredCertificatesScreenState extends State<ExpiredCertificatesScreen> {
  @override
  void initState() {
    _refreshCertificates();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DefaultDrawer(),
      appBar: DefaultAppBar(),
      body: ResponsiveBox(
        child: ValueListenableBuilder(
          valueListenable: certificates.listenable(),
          builder: (c, Box<Certificate> box, w) {
            final currentHash = data.get(kCurrentUserKey);
            List<Certificate> userCertificates = box.values
                .where((element) => element.userHash == currentHash)
                .toList();
            if (userCertificates.isEmpty) {
              // we are using [Stack] instead of [Column] to allow the unnecessary assets to overflow
              return LayoutBuilder(
                builder: (context, constraints) => Stack(
                  fit: StackFit.expand,
                  alignment: Alignment.center,
                  children: [
                    Transform.scale(
                      child: SvgPicture.asset(kPendingCertificateAsset),
                      scale: 1.25,
                    ),
                    Positioned(
                      top: getTopPadding(constraints.maxHeight),
                      child: AlignedMarkdown(
                        data: S.of(context).noExpiredCertificatesLongFormatted,
                      ),
                    ),
                  ],
                ),
              );
            }
            userCertificates
                .sort((a, b) => b.createDate!.compareTo(a.createDate!));

            return RefreshIndicator(
              onRefresh: _refreshCertificates,
              child: ListView.builder(
                padding: EdgeInsets.all(8),
                itemBuilder: (c, i) {
                  final Certificate certificate = userCertificates[i];
                  return CertificateDetailTile(
                    certificate: certificate,
                  );
                },
                itemCount: userCertificates.length,
              ),
            );
          },
        ),
      ),
    );
  }

  Future<void> _refreshCertificates() async {
    if (certificates.isEmpty) return;
    try {
      // workaround typecast
      List<String> uuids = List.from(certificates.keys);

      await F2PApi.instance.certificates.getBulk(
        signature: getCurrentUser().hash!,
        ids: uuids,
      );
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(S.of(context).certificateRefreshError)));
    }
  }
}
