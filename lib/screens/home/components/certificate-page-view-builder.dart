import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/certificate-status.dart';
import 'package:free2pass/models/certificate.dart';
import 'package:free2pass/screens/home/components/views/certificate-status-future.view.dart';
import 'package:hive_flutter/hive_flutter.dart';

class CertificatePageViewBuilder extends StatefulWidget {
  final RefreshCallback? onRefresh;

  const CertificatePageViewBuilder({Key? key, this.onRefresh})
      : super(key: key);

  @override
  _CertificatePageViewBuilderState createState() =>
      _CertificatePageViewBuilderState();
}

class _CertificatePageViewBuilderState
    extends State<CertificatePageViewBuilder> {
  PageController _pageViewController = PageController();
  double? _currentCertificate = 0;

  @override
  void initState() {
    _pageViewController.addListener(_pageViewChanged);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: certificates.listenable(),
      builder: (context, Box<Certificate> box, d) {
        final status = getWorkCertificateStatuses(box);
        return Stack(
          children: [
            PageView.builder(
              controller: _pageViewController,
              itemBuilder: (c, i) {
                return CertificateStatusFutureView(
                  certificateStatus: status[i],
                );
              },
              itemCount: status.length,
            ),
            if (status.length > 1)
              Positioned(
                bottom: 0,
                right: 0,
                left: 0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    _currentCertificate! <
                            1 // should be triggered immediately after beginning of animation
                        ? Container()
                        : IconButton(
                            icon: Icon(Icons.arrow_back),
                            onPressed: _previousCertificate,
                            tooltip: S.of(context).previousCertificate,
                          ),
                    _currentCertificate! >
                            status.length -
                                2 // should be triggered immediately after beginning of animation
                        ? Container()
                        : IconButton(
                            icon: Icon(Icons.arrow_forward),
                            onPressed: _nextCertificate,
                            tooltip: S.of(context).nextCertificate,
                          ),
                  ],
                ),
              )
          ],
        );
      },
    );
  }

  void _previousCertificate() {
    _pageViewController.previousPage(
        duration: Duration(milliseconds: 350), curve: Curves.easeInOut);
  }

  void _nextCertificate() {
    _pageViewController.nextPage(
        duration: Duration(milliseconds: 350), curve: Curves.easeInOut);
  }

  void _pageViewChanged() {
    setState(() {
      _currentCertificate = _pageViewController.page;
    });
  }
}
