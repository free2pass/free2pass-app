import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/vaccination-certificate-model.dart';
import 'package:free2pass/screens/vaccination-certificate/components/general-vaccination-information.dart';
import 'package:free2pass/screens/vaccination-certificate/components/vaccination-certificate-widget.dart';
import 'package:free2pass/utils/encode-personal-data.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:hive_flutter/hive_flutter.dart';

class MaybeUserVaccinationCertificate extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: vaccinationCertificates.listenable(),
      builder: (BuildContext context, Box<VaccinationCertificateStorage> value,
          Widget? child) {
        final user = getCurrentUser();

        for (final element in vaccinationCertificates.values) {
          final metadataBirthday = DateTime.fromMillisecondsSinceEpoch(
              element.metadata.birthday * 1000);

          if (element.metadata.isValid &&
              element.metadata.name.toLowerCase().trim() ==
                  user.name.toLowerCase().trim() &&
              (metadataBirthday.year == user.birthdayDateTime.year &&
                  metadataBirthday.month == user.birthdayDateTime.month &&
                  metadataBirthday.day == user.birthdayDateTime.day)) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: OpenContainer(
                closedShape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16)),
                closedColor: Theme.of(context).cardColor,
                closedBuilder: (c, f) => ListTile(
                  leading: Icon(Icons.check_circle),
                  title: Text(S.of(context).validVaccinationCertificateFound),
                  subtitle: Text(S.of(context).tapToEnlarge),
                  trailing: Icon(Icons.aspect_ratio),
                  onTap: f,
                ),
                closedElevation: 2,
                openBuilder: (c, f) => Scaffold(
                  appBar: DefaultAppBar(),
                  body: Center(
                    child: SafeArea(
                      child: ResponsiveBox(
                        child: ListView(
                          padding: const EdgeInsets.all(8),
                          children: [
                            VaccinationCertificateCard(
                                certificate: element.rawQr, metadata: element.metadata),
                            GeneralVaccinationInformation(),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            );
          }
        }
        return Container();
      },
    );
  }
}
