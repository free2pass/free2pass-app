import 'package:flutter/material.dart';
import 'package:free2pass/screens/home/components/refresh-listener-delegate.dart';

class RefreshButton extends StatefulWidget {
  final RefreshCallback? onRefresh;
  final Color? color;
  final String label;
  final bool isOutlined;

  RefreshButton._(
      {Key? key,
      this.onRefresh,
      this.color,
      required this.label,
      this.isOutlined = false})
      : super(key: key);

  @override
  _RefreshButtonState createState() => _RefreshButtonState();

  static RefreshButton outlined(
      {onRefresh, required String label, Color? color}) {
    return RefreshButton._(
      onRefresh: onRefresh,
      label: label,
      color: color,
      isOutlined: true,
    );
  }

  static RefreshButton raised(
      {onRefresh, required String label, Color? color}) {
    return RefreshButton._(
      onRefresh: onRefresh,
      label: label,
      color: color,
    );
  }
}

class _RefreshButtonState extends State<RefreshButton> {
  @override
  void initState() {
    RefreshListenerDelegate.of()!.addListener(_handleRefresh);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final isRefreshing = RefreshListenerDelegate.of()!.isRefreshing;
    var icon = isRefreshing
        ? Padding(
            padding: const EdgeInsets.all(4.0),
            child: SizedBox(
              width: 16,
              height: 16,
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(widget.color),
                strokeWidth: 3,
              ),
            ),
          )
        : Icon(
            Icons.refresh,
          );
    var onPressed =
        isRefreshing ? () => null : RefreshListenerDelegate.of()!.refresh;
    var label = Text(
      widget.label,
    );
    return widget.isOutlined
        ? OutlinedButton.icon(
            style: OutlinedButton.styleFrom(
              primary: widget.color,
            ),
            onPressed: onPressed,
            icon: icon,
            label: label,
          )
        : ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              primary: Colors.white,
              onPrimary: widget.color,
            ),
            onPressed: onPressed,
            icon: icon,
            label: label,
          );
  }

  @override
  void dispose() {
    RefreshListenerDelegate.of()!.removeListener(_handleRefresh);
    super.dispose();
  }

  _handleRefresh(bool isRefreshing) {
    setState(() {});
  }
}
