import 'package:flutter/material.dart';
import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/enums/certificate-status.enum.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/certificate-status.dart';
import 'package:free2pass/utils/encode-personal-data.dart';

class RefreshListenerDelegate extends StatefulWidget {
  static _RefreshListenerDelegateState? _current;

  const RefreshListenerDelegate({Key? key, required this.child})
      : super(key: key);

  static _RefreshListenerDelegateState? of() {
    return _current;
  }

  final Widget child;

  @override
  _RefreshListenerDelegateState createState() {
    _current = _RefreshListenerDelegateState();
    return _current!;
  }
}

class _RefreshListenerDelegateState extends State<RefreshListenerDelegate>
    with WidgetsBindingObserver {
  List<RefreshNotificationStateCallback> _listeners = [];
  bool isRefreshing = false;

  @override
  void initState() {
    WidgetsBinding.instance?.addObserver(this);
    refresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) refresh();
  }

  void addListener(RefreshNotificationStateCallback callback) {
    _listeners.add(callback);
  }

  void removeListener(RefreshNotificationStateCallback callback) {
    _listeners.remove(callback);
  }

  Future refresh() async {
    isRefreshing = true;
    _callback();
    await _refreshCertificates();
    isRefreshing = false;
    _callback();
  }

  Future<void> _refreshCertificates() async {
    if (certificates.isEmpty) return;

    final status = getWorkCertificateStatuses(certificates)
      ..add(getDefaultCertificateStatus(certificates));

    status.removeWhere(
        (element) => element.validity == CertificateStatusValidity.EMPTY);
    try {
      Set<String> ids = Set();
      status.forEach((e) {
        ids.add(e.certificate!.id);
      });
      certificates.values.forEach((element) {
        if (element.expectedValidity == CertificateStatusValidity.PENDING)
          ids.add(element.id);
      });

      if (ids.isEmpty) return;

      await F2PApi.instance.certificates.getBulk(
        signature: getCurrentUser().hash!,
        ids: ids.toList(),
      );
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(S.of(context).certificateRefreshError)));
    }
  }

  void _callback() {
    _listeners.forEach((callback) => callback(isRefreshing));
  }
}

typedef RefreshNotificationStateCallback(bool isRefreshing);
