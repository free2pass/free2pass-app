import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/models/scan-route-data.dart';
import 'package:free2pass/screens/scan-flow/scan-flow.dart';
import 'package:free2pass_support/free2pass_support.dart';

class ScanCertificateButton extends StatefulWidget {
  final int page;
  final ValueChanged<int> changePage;

  const ScanCertificateButton(
      {Key? key, required this.page, required this.changePage})
      : super(key: key);

  @override
  _ScanCertificateButtonState createState() => _ScanCertificateButtonState();
}

class _ScanCertificateButtonState extends State<ScanCertificateButton> {
  late AxisTransitionController _controller;

  @override
  void initState() {
    _controller = AxisTransitionController();
    //_controller.current = widget.page;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AxisTransitionView(
      controller: _controller,
      direction: SharedAxisTransitionType.vertical,
      children: [
        OpenContainer(
          key: ValueKey(0),
          closedBuilder: (c, f) => FloatingActionButton.extended(
            elevation: 0,
            onPressed: f,
            backgroundColor: Colors.transparent,
            label: AlignedMarkdown(
              data: S.of(context).scanNewTest,
              color: Colors.white,
            ),
          ),
          closedShape: StadiumBorder(),
          closedColor: green,
          openBuilder: (c, f) => ScanFlowScreen(
            data: ScanRouteData(
              changePage: _handleClose,
              initialScreen: ScanType.CERTIFICATE,
            ),
          ),
          routeSettings: RouteSettings(name: ScanFlowScreen.routeName),
        ),
        OpenContainer(
          key: ValueKey(1),
          closedBuilder: (c, f) => FloatingActionButton.extended(
            elevation: 0,
            onPressed: f,
            backgroundColor: Colors.transparent,
            label: AlignedMarkdown(
              data: S.of(context).checkInNow,
              color: Colors.white,
            ),
          ),
          closedShape: StadiumBorder(),
          closedColor: green,
          openBuilder: (c, f) => ScanFlowScreen(
            data: ScanRouteData(
              changePage: _handleClose,
              initialScreen: ScanType.CHECKIN,
            ),
          ),
          routeSettings: RouteSettings(name: ScanFlowScreen.routeName),
        )
      ],
    );
  }

  @override
  void didUpdateWidget(covariant ScanCertificateButton oldWidget) {
    setState(() {
      _controller.current =
          widget.page == 2 ? 0 : widget.page; // no button for "Work" page
    });
    super.didUpdateWidget(oldWidget);
  }

  void _handleClose(int data) {
    Navigator.of(context).pop();
    widget.changePage(data);
  }
}
