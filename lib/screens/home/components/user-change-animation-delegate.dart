import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/certificate-status.dart';
import 'package:free2pass/models/certificate.dart';
import 'package:free2pass/models/vaccination-certificate-model.dart';
import 'package:free2pass/screens/home/components/certificate-page-view-builder.dart';
import 'package:free2pass/screens/home/components/maybe-vaccination-certificate-box.dart';
import 'package:free2pass/screens/home/components/refresh-listener-delegate.dart';
import 'package:free2pass/screens/home/components/views/certificate-status-future.view.dart';
import 'package:free2pass/screens/home/components/views/check-in.view.dart';
import 'package:free2pass/utils/encode-personal-data.dart';
import 'package:free2pass/utils/hive.util.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:hive_flutter/hive_flutter.dart';

class UserChangeAnimationDelegate extends StatelessWidget {
  final AxisTransitionController controller;

  const UserChangeAnimationDelegate({Key? key, required this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ResponsiveBox(
      child: ValueListenableBuilder<Box>(
        valueListenable: data.listenable(),
        builder: (context, Box box, w) {
          // the PageTransitionSwitcher is used to provide a smooth animation when changing the user
          return PageTransitionSwitcher(
            transitionBuilder: (
              Widget child,
              Animation<double> primaryAnimation,
              Animation<double> secondaryAnimation,
            ) {
              return SharedAxisTransition(
                animation: primaryAnimation,
                secondaryAnimation: secondaryAnimation,
                transitionType: SharedAxisTransitionType.vertical,
                child: child,
              );
            },
            child: RefreshListenerDelegate(
              key: ValueKey<String>(box.get(kCurrentUserKey)),
              child: LayoutBuilder(
                builder: (context, constraints) => RefreshIndicator(
                  onRefresh: RefreshListenerDelegate.of()!.refresh,
                  child: SingleChildScrollView(
                    child: Container(
                      height: constraints.maxHeight + 0.000001,
                      // ugly workaround to allow scroll and pull to refresh hence
                      child: ValueListenableBuilder(
                          valueListenable: vaccinationCertificates.listenable(),
                          builder: (context,
                              Box<VaccinationCertificateStorage> box, w) {
                            return Column(
                              children: [
                                if (getCurrentUser()
                                    .hasValidVaccinationCertificate(box))
                                  Container(
                                    height: 96,
                                    child: MaybeUserVaccinationCertificate(),
                                  ),
                                Container(
                                  height: (getCurrentUser()
                                          .hasValidVaccinationCertificate(box))
                                      ? constraints.maxHeight - 96
                                      : constraints.maxHeight,
                                  child: AxisTransitionView(
                                    direction: SharedAxisTransitionType.scaled,
                                    controller: controller,
                                    children: [
                                      ValueListenableBuilder(
                                          valueListenable:
                                              certificates.listenable(),
                                          builder: (context,
                                              Box<Certificate> box, d) {
                                            final status =
                                                getDefaultCertificateStatus(
                                                    box);

                                            // the PageTransitionSwitcher is used to provide a smooth animation on certificate status change
                                            return PageTransitionSwitcher(
                                              transitionBuilder: (
                                                Widget child,
                                                Animation<double>
                                                    primaryAnimation,
                                                Animation<double>
                                                    secondaryAnimation,
                                              ) {
                                                return SharedAxisTransition(
                                                  animation: primaryAnimation,
                                                  secondaryAnimation:
                                                      secondaryAnimation,
                                                  transitionType:
                                                      SharedAxisTransitionType
                                                          .horizontal,
                                                  child: child,
                                                );
                                              },
                                              child:
                                                  CertificateStatusFutureView(
                                                key: ValueKey(status.validity),
                                                certificateStatus: status,
                                              ),
                                            );
                                          }),
                                      // CheckInView(),
                                      CertificatePageViewBuilder(),
                                    ],
                                  ),
                                ),
                              ],
                            );
                          }),
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
