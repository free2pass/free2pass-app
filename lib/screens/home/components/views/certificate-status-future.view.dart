import 'package:flutter/material.dart';
import 'package:free2pass/enums/certificate-status.enum.dart';
import 'package:free2pass/models/certificate-status.dart';
import 'package:free2pass/screens/home/components/views/certificate-status/negative-certificate.dart';
import 'package:free2pass/screens/home/components/views/certificate-status/no-certificate.dart';
import 'package:free2pass/screens/home/components/views/certificate-status/pending-certificate.dart';
import 'package:free2pass/screens/home/components/views/certificate-status/positive-certificate.dart';
import 'package:free2pass/screens/home/components/views/certificate-status/revoked-certificate.dart';

class CertificateStatusFutureView extends StatefulWidget {
  final CertificateStatus certificateStatus;

  const CertificateStatusFutureView({Key? key, required this.certificateStatus})
      : super(key: key);

  @override
  _CertificateStatusFutureViewState createState() =>
      _CertificateStatusFutureViewState();
}

class _CertificateStatusFutureViewState
    extends State<CertificateStatusFutureView> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback(_checkShowDialog);
  }

  @override
  Widget build(BuildContext context) {
    switch (widget.certificateStatus.validity) {
      case CertificateStatusValidity.NEGATIVE:
        if (widget.certificateStatus.certificate!.expiryDateTime
            .isAfter(DateTime.now()))
          return NegativeCertificateView(certificate: widget.certificateStatus);
        else
          return NoCertificateView();

      case CertificateStatusValidity.REVOKED:
        return RevokedCertificateView(certificate: widget.certificateStatus);
      case CertificateStatusValidity.POSITIVE:
        return PositiveCertificateView(certificate: widget.certificateStatus);
      case CertificateStatusValidity.PENDING:
        return PendingCertificateView(certificate: widget.certificateStatus);
      case CertificateStatusValidity.EMPTY:
        return NoCertificateView();
      default:
        return NoCertificateView();
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant CertificateStatusFutureView oldWidget) {
    super.didUpdateWidget(oldWidget);

    WidgetsBinding.instance!.addPostFrameCallback(_checkShowDialog);
  }

  void _checkShowDialog(_) {
    if ((widget.certificateStatus.certificate?.expectedValidity !=
            CertificateStatusValidity.REVOKED) &&
        (widget.certificateStatus.certificate?.shouldShowDialog ?? false))
      showTestNotice(context, widget.certificateStatus.certificate!);
    if (widget.certificateStatus.pendingCertificate?.shouldShowDialog ?? false)
      showTestNotice(context, widget.certificateStatus.pendingCertificate!);
  }
}
