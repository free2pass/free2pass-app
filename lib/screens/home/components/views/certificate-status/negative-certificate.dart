import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:free2pass/components/validity-countdown/validity-countdown.dart';
import 'package:free2pass/dialogs/passport-dialog/passport-dialog.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/models/certificate-status.dart';
import 'package:free2pass/screens/home/components/views/decoration/badge-decoration.dart';
import 'package:free2pass/screens/pdf-export/pdf-export.dart';
import 'package:free2pass_support/free2pass_support.dart';

class NegativeCertificateView extends StatefulWidget {
  final CertificateStatus certificate;

  const NegativeCertificateView({Key? key, required this.certificate})
      : super(key: key);

  @override
  _NegativeCertificateViewState createState() =>
      _NegativeCertificateViewState();
}

class _NegativeCertificateViewState extends State<NegativeCertificateView> {
  Widget build(BuildContext context) {
    var bottom = Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        IconTheme(
          data: bigIconTheme.copyWith(color: blue),
          child: Icon(
            Icons.timer,
          ),
        ),
        ValidityCountdownWidget(
          color: blue,
          milliseconds: widget
              .certificate.certificate!.expiryDateTime.millisecondsSinceEpoch,
        ),
      ],
    );
    var badgeDecoration = BadgeDecoration(
      certificate: widget.certificate.certificate!,
      pendingCertificate: widget.certificate.pendingCertificate,
      bottom: bottom,
    );
    return ResponsiveBox(
      child: widget.certificate.certificate!.issuer!.isPrivate
          ? badgeDecoration
          : CupertinoContextMenu(
              actions: [
                CupertinoContextMenuAction(
                  child: Text(S.of(context).shareAsPdf),
                  trailingIcon: Icons.picture_as_pdf,
                  onPressed: _sharePdf,
                ),
                CupertinoContextMenuAction(
                  child: Text(S.of(context).showPassport),
                  trailingIcon: Icons.badge,
                  onPressed: _showPassport,
                ),
              ],
              previewBuilder: (context, animation, w) {
                return ClipRRect(
                  borderRadius: BorderRadius.circular(64.0 * animation.value),
                  child: BadgeDecoration(
                    certificate: widget.certificate.certificate!,
                    pendingCertificate: widget.certificate.pendingCertificate,
                    animation: animation,
                    bottom: bottom,
                  ),
                );
              },
              child: badgeDecoration,
            ),
    );
  }

  void _sharePdf() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) =>
            PdfExportScreen(certificate: widget.certificate.certificate!),
        fullscreenDialog: true));
  }

  void _showPassport() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => PassportDialog(
            certificate: widget.certificate.certificate!,
            label: S.of(context).negativeLong)));
  }
}
