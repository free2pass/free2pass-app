import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:free2pass/components/dark-mode-overlay.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/screens/introduction/introduction.dart';
import 'package:free2pass_support/free2pass_support.dart';

const kNoCertificateAsset = kScalableAssetBase + 'grafik-kein-test.svg';

class NoCertificateView extends StatefulWidget {
  @override
  _NoCertificateViewState createState() => _NoCertificateViewState();
}

class _NoCertificateViewState extends State<NoCertificateView> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return Stack(
        alignment: Alignment.center,
        fit: StackFit.expand,
        textDirection: TextDirection.ltr,
        children: [
          Padding(
            padding:
                EdgeInsets.only(top: getTopPadding(constraints.maxHeight) / 2),
            child: Transform.scale(
              scale: 1.3725,
              child: AspectRatio(
                aspectRatio: 1,
                child: DarkModeOverlay(
                  child: SvgPicture.asset(
                    kNoCertificateAsset,
                    excludeFromSemantics: true,
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            top: getTopPadding(constraints.maxHeight),
            child: AlignedMarkdown(
                data: S.of(context).noCurrentCertificateLongFormatted),
          ),
          Positioned(
            bottom: getBottomPadding(constraints.maxHeight),
            child: TextButton.icon(
                onPressed: _showTutorial,
                icon: Icon(
                  Icons.info,
                  color: blue,
                ),
                label: MarkdownBody(
                  data: S.of(context).tutorial,
                )),
          )
        ],
      );
    });
  }

  void _showTutorial() {
    Navigator.of(context).pushNamed(IntroductionScreen.routeName);
  }
}
