import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/models/certificate-status.dart';
import 'package:free2pass/screens/home/components/views/decoration/badge-decoration.dart';
import 'package:free2pass_support/free2pass_support.dart';

class PendingCertificateView extends StatelessWidget {
  final CertificateStatus certificate;

  const PendingCertificateView({Key? key, required this.certificate})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ResponsiveBox(
      child: BadgeDecoration(
          certificate: certificate.certificate!,
          pendingCertificate: certificate.pendingCertificate,
          bottom: AlignedMarkdown(
            data: S.of(context).pendingTestLongFormatted,
          )),
    );
  }
}
