import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/certificate-status.dart';
import 'package:free2pass/models/certificate.dart';
import 'package:free2pass/screens/home/components/views/certificate-status/revoked-certificate.dart';
import 'package:free2pass/screens/home/components/views/decoration/badge-decoration.dart';
import 'package:free2pass_support/free2pass_support.dart';

class PositiveCertificateView extends StatefulWidget {
  final CertificateStatus certificate;

  const PositiveCertificateView({Key? key, required this.certificate})
      : super(key: key);

  @override
  _PositiveCertificateViewState createState() =>
      _PositiveCertificateViewState();
}

class _PositiveCertificateViewState extends State<PositiveCertificateView> {
  @override
  Widget build(BuildContext context) {
    var bottomText = AlignedMarkdown(
      data: S.of(context).positiveTestNotice,
    );
    return ResponsiveBox(
      child: BadgeDecoration(
          certificate: widget.certificate.certificate!,
          pendingCertificate: widget.certificate.pendingCertificate,
          bottom: widget.certificate.certificate!.positiveComment == null
              ? bottomText
              : Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    bottomText,
                    Container(
                      height: 16,
                    ),
                    ElevatedButton.icon(
                        style: ElevatedButton.styleFrom(
                          primary: red,
                        ),
                        onPressed: _showPositiveNotice,
                        icon: Icon(Icons.error),
                        label: Text(S.of(context).positiveComment))
                  ],
                )),
    );
  }

  void _showPositiveNotice() {
    showTestNotice(context, widget.certificate.certificate!);
  }
}

showTestNotice(BuildContext context, Certificate certificate) async {
  final isPositiveComment = certificate.positiveComment != null;
  certificate.dialogShown = true;
  certificates.put(certificate.id, certificate);
  showModal(
    context: context,
    builder: (context) => AlertDialog(
      title: Text(isPositiveComment
          ? S.of(context).positiveComment
          : S.of(context).certificateRevokedLong),
      content: AlignedMarkdown(
        data: isPositiveComment
            ? certificate.positiveComment!
            : certificate.revokeReasonString!,
        alignment: CrossAxisAlignment.start,
      ),
      actions: [
        IssuerContactButton(
          issuer: certificate.issuer!,
        ),
        TextButton(
          onPressed: Navigator.of(context).pop,
          child: Text(S.of(context).iUnderstood),
        )
      ],
    ),
  );
}
