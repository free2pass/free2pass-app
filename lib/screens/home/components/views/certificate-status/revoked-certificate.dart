import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/models/certificate-status.dart';
import 'package:free2pass/models/issuer.dart';
import 'package:free2pass/screens/home/components/views/decoration/badge-decoration.dart';
import 'package:free2pass/utils/contact-issuer.dart';
import 'package:free2pass_support/free2pass_support.dart';

class RevokedCertificateView extends StatefulWidget {
  final CertificateStatus certificate;

  const RevokedCertificateView({Key? key, required this.certificate})
      : super(key: key);

  @override
  _RevokedCertificateViewState createState() => _RevokedCertificateViewState();
}

class _RevokedCertificateViewState extends State<RevokedCertificateView> {
  String get _getRevokeString {
    String handle = S.of(context).certificateRevoked;
    if (widget.certificate.certificate!.revokeReasonString != null &&
        widget.certificate.certificate!.revokeReasonString!.trim().isNotEmpty) {
      handle += '''

${S.of(context).reason}:

${widget.certificate.certificate!.revokeReasonString}
''';
    }
    return handle;
  }

  @override
  Widget build(BuildContext context) {
    return BadgeDecoration(
        certificate: widget.certificate.certificate!,
        pendingCertificate: widget.certificate.pendingCertificate,
        bottom: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            AlignedMarkdown(
              data: _getRevokeString,
            ),
            IssuerContactButton(
              issuer: widget.certificate.certificate!.issuer!,
            ),
          ],
        ));
  }
}

class IssuerContactButton extends StatefulWidget {
  final Issuer issuer;

  const IssuerContactButton({Key? key, required this.issuer}) : super(key: key);

  @override
  _IssuerContactButtonState createState() => _IssuerContactButtonState();
}

class _IssuerContactButtonState extends State<IssuerContactButton> {
  @override
  Widget build(BuildContext context) {
    return TextButton.icon(
        onPressed: _contactIssuer,
        icon: Icon(_contactIcon()),
        label: Text(S.of(context).contactIssuer));
  }

  IconData _contactIcon() {
    final uri = makeIssuerContactUri(widget.issuer);
    if (uri.startsWith('tel:'))
      return Icons.phone;
    else if (uri.startsWith('mailto:'))
      return Icons.email;
    else
      return Icons.contact_support;
  }

  void _contactIssuer() {
    contactIssuer(context, widget.issuer);
  }
}
