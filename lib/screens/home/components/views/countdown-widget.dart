import 'package:flutter/material.dart';

class CountdownWidget extends StatefulWidget {
  const CountdownWidget({
    Key? key,
    required this.fomatter,
    required this.duration,
  }) : super(key: key);

  final Duration duration;
  final DurationFormatter fomatter;

  State createState() => new _CountdownWidgetState();
}

typedef String DurationFormatter(Duration? duration);

class _CountdownWidgetState extends State<CountdownWidget>
    with TickerProviderStateMixin {
  late AnimationController _controller;
  Duration? duration;

  String get timerDisplayString {
    return widget.fomatter(duration);
  }

  @override
  void initState() {
    super.initState();
    duration = widget.duration;
    _controller = new AnimationController(
      vsync: this,
      duration: duration,
    );
    _controller.reverse(from: widget.duration.inSeconds.toDouble());
  }

  @override
  void didUpdateWidget(CountdownWidget oldWidget) {
    if (widget.duration != oldWidget.duration) {
      setState(() {
        duration = widget.duration;
        _controller.dispose();
        _controller = new AnimationController(
          vsync: this,
          duration: duration,
        );
        _controller.reverse(from: widget.duration.inSeconds.toDouble());
      });
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: _controller,
        builder: (_, Widget? child) {
          return Text(
            timerDisplayString,
          );
        });
  }
}
