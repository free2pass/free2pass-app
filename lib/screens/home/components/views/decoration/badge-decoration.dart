import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:free2pass/components/dark-mode-overlay.dart';
import 'package:free2pass/components/photo-not-available.dart';
import 'package:free2pass/dialogs/passport-dialog/passport-dialog.dart';
import 'package:free2pass/enums/certificate-status.enum.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/certificate.dart';
import 'package:free2pass/models/vaccination-certificate-model.dart';
import 'package:free2pass/screens/home/components/refresh-button.dart';
import 'package:free2pass/screens/home/components/views/decoration/user-photo-decoration.dart';
import 'package:free2pass/screens/pdf-export/pdf-export.dart';
import 'package:free2pass/utils/encode-personal-data.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:intl/intl.dart';

const kNegativeCertificateAsset = kScalableAssetBase + 'grafik-negativ.svg';
const kNegativeCertificateBackground = kScalableAssetBase + 'BG-negativ.svg';

const kNegativePositiveCertificateAssetInternal =
    kScalableAssetBase + 'grafik-work-negativ-positiv.svg';
const kNegativeCertificateBackgroundInternal =
    kScalableAssetBase + 'BG-work-negativ.svg';

const kPositiveCertificateAsset = kScalableAssetBase + 'grafik-positiv.svg';
const kPositiveCertificateBackground = kScalableAssetBase + 'BG-positiv.svg';

const kPositiveCertificateBackgroundInternal =
    kScalableAssetBase + 'BG-work-positiv.svg';

const kPendingCertificateAsset = kScalableAssetBase + 'grafik-prüfung.svg';
const kPendingCertificateBackground = kScalableAssetBase + 'BG-pruefung.svg';

const kShowPassportAsset = kScalableAssetBase + 'passport.component.svg';

const kPendingCertificateBackgroundInternal =
    kScalableAssetBase + 'BG-work-pruefung.svg';

const kInvalidCertificateAsset = kScalableAssetBase + 'grafik-prüfung.svg';
const kInvalidCertificateBackground = kScalableAssetBase + 'BG-invalid.svg';

class BadgeDecoration extends StatefulWidget {
  final Certificate certificate;
  final Widget bottom;
  final Certificate? pendingCertificate;
  final Animation<double> animation;

  const BadgeDecoration({
    Key? key,
    required this.certificate,
    required this.bottom,
    this.pendingCertificate,
    this.animation = const AlwaysStoppedAnimation(0),
  }) : super(key: key);

  @override
  _BadgeDecorationState createState() => _BadgeDecorationState();
}

class _BadgeDecorationState extends State<BadgeDecoration> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        final user = getCurrentUser();

        String background;
        String asset;
        String title;
        String fullTestState;
        switch (widget.certificate.expectedValidity) {
          case CertificateStatusValidity.NEGATIVE:
            background = widget.certificate.issuer!.isPrivate
                ? kNegativeCertificateBackgroundInternal
                : kNegativeCertificateBackground;
            asset = widget.certificate.issuer!.isPrivate
                ? kNegativePositiveCertificateAssetInternal
                : kNegativeCertificateAsset;
            title = S.of(context).negatie;
            fullTestState = S.of(context).negativeLong;
            break;
          case CertificateStatusValidity.POSITIVE:
            background = widget.certificate.issuer!.isPrivate
                ? kPositiveCertificateBackgroundInternal
                : kPositiveCertificateBackground;
            asset = widget.certificate.issuer!.isPrivate
                ? kNegativePositiveCertificateAssetInternal
                : kPositiveCertificateAsset;
            title = S.of(context).positive;
            fullTestState = S.of(context).positiveLong;
            break;

          case CertificateStatusValidity.PENDING:
            background = widget.certificate.issuer!.isPrivate
                ? kPendingCertificateBackgroundInternal
                : kPendingCertificateBackground;
            asset = kPendingCertificateAsset;
            title = S.of(context).pending;
            fullTestState = S.of(context).testedLong;
            break;

          default:
            background = kInvalidCertificateBackground;
            asset = kInvalidCertificateAsset;
            title = S.of(context).invalid;
            fullTestState = S.of(context).testedLong;
            break;
        }

        var decorationTextColor =
            widget.certificate.issuer!.isPrivate ? null : Colors.white;

        var userColumn = DefaultTextStyle(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                title.toUpperCase(),
                style: Theme.of(context).textTheme.headline4!.copyWith(
                      color: decorationTextColor is Color
                          ? decorationTextColor
                          : widget.certificate.expectedValidity ==
                                  CertificateStatusValidity.NEGATIVE
                              ? green
                              : widget.certificate.expectedValidity ==
                                      CertificateStatusValidity.POSITIVE
                                  ? red
                                  : null,
                    ),
              ),
              Container(
                height: 16,
              ),
              DefaultTextStyle.merge(
                child: Text(user.name),
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text(fullTestState),
              Text(widget.certificate.issuer!.name),
              if (widget.certificate.expectedValidity ==
                      CertificateStatusValidity.PENDING &&
                  widget.certificate.name != null)
                Text(
                  'ID:' + ' ' + widget.certificate.name!,
                  style: Theme.of(context)
                      .textTheme
                      .headline3!
                      .copyWith(color: decorationTextColor),
                ),
              Text(_dateTimeString(widget.certificate.resultDateTime)),
              if (CertificateStatusValidity.PENDING ==
                  widget.certificate.expectedValidity)
                Padding(
                  padding: EdgeInsets.only(
                      right: constraints.maxWidth / 10,
                      top: constraints.maxHeight / 16),
                  child: RefreshButton.raised(
                    label: S.of(context).refresh,
                    color: blue,
                  ),
                ),
              if ([
                CertificateStatusValidity.NEGATIVE,
                CertificateStatusValidity.POSITIVE
              ].contains(widget.certificate.expectedValidity))
                Opacity(
                  opacity: 1 - widget.animation.value,
                  child: Tooltip(
                    child: Padding(
                      padding: const EdgeInsets.all(6.0),
                      child: Material(
                        color: widget.certificate.issuer!.isPrivate
                            ? widget.certificate.expectedValidity ==
                                    CertificateStatusValidity.NEGATIVE
                                ? green
                                : red
                            : Colors.white,
                        elevation: widget.certificate.issuer!.isPrivate ? 4 : 0,
                        borderRadius: BorderRadius.circular(16),
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Icon(
                            Icons.crop,
                            color: widget.certificate.issuer!.isPrivate
                                ? Colors.white
                                : widget.certificate.expectedValidity ==
                                        CertificateStatusValidity.NEGATIVE
                                    ? green
                                    : red,
                          ),
                        ),
                      ),
                    ),
                    message: S.of(context).showPassport,
                  ),
                ),
              if (CertificateStatusValidity.NEGATIVE ==
                      widget.certificate.expectedValidity &&
                  !widget.certificate.issuer!.isPrivate)
                Opacity(
                  opacity: 1 - widget.animation.value,
                  child: Padding(
                    padding: EdgeInsets.only(
                        right: constraints.maxWidth / 10,
                        top: constraints.maxHeight / 48),
                    child: ElevatedButton.icon(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        onPrimary: green,
                      ),
                      onPressed: _sharePdf,
                      icon: Icon(Icons.picture_as_pdf),
                      label: Text(S.of(context).shareAsPdf),
                    ),
                  ),
                ),
            ],
          ),
          style: TextStyle(color: decorationTextColor ?? gray),
        );

        final stepValue = 1 - widget.animation.value;

        return Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          children: [
            Positioned(
              right: (-constraints.maxWidth * 1 / 3) * (.3 + (.7 * stepValue)),
              top: 0,
              child: Container(
                constraints: BoxConstraints(
                    maxHeight:
                        constraints.maxHeight * (4 / 5 + (1 - stepValue) / 5)),
                child: Transform.scale(
                  scale: 1 + .5 * stepValue,
                  child: DarkModeOverlay(
                    child: SvgPicture.asset(
                      background,
                      excludeFromSemantics: true,
                      width: constraints.maxWidth,
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              left: -128 * 6 / (constraints.maxWidth / 24),
              bottom: (getBottomPadding(constraints.maxHeight)) * stepValue,
              child: Opacity(
                opacity: widget.animation.value > .25
                    ? 0
                    : 1 - 4 * widget.animation.value,
                child: Container(
                  constraints:
                      BoxConstraints(maxHeight: constraints.maxHeight * 2 / 3),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 64),
                    child: Transform.scale(
                      scale: stepValue,
                      child: DarkModeOverlay(
                        child: SvgPicture.asset(
                          asset,
                          excludeFromSemantics: true,
                          width: constraints.maxWidth * 3 / 5,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            ValueListenableBuilder(
                valueListenable: vaccinationCertificates.listenable(),
                builder: (context, Box<VaccinationCertificateStorage> box, w) {
                  return Positioned(
                      right: constraints.maxWidth * 1 / 4 +
                          (constraints.maxWidth / 2 -
                              constraints.maxWidth *
                                  constraints.maxWidth /
                                  2000),
                      top: (getCurrentUser().hasValidVaccinationCertificate(box)
                              ? (constraints.maxHeight - 128)
                              : constraints.maxHeight) *
                          1 /
                          6,
                      child: (user.photoLocation != null)
                          ? UserPhotoDecoration(listen: false)
                          : PhotoNotAvailableNotice());
                }),
            Positioned(
                right: 16,
                top: 24 + getTopPadding(constraints.maxHeight) * 2,
                child: DefaultTextStyle.merge(
                    style: TextStyle(color: decorationTextColor, height: 1.5),
                    child: ([
                              CertificateStatusValidity.NEGATIVE,
                              CertificateStatusValidity.POSITIVE
                            ].contains(widget.certificate.expectedValidity) &&
                            widget.animation.value != 1)
                        ? OpenContainer(
                            closedColor: Colors.transparent,
                            closedElevation: 0,
                            openElevation: 0,
                            closedBuilder: (c, f) => userColumn,
                            openColor: Colors.transparent,
                            openBuilder: (c, f) => PassportDialog(
                              certificate: widget.certificate,
                              label: fullTestState,
                            ),
                          )
                        : userColumn)),
            Positioned(
              bottom: getBottomPadding(constraints.maxHeight) *
                  (stepValue * .3 + .7),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  widget.bottom,
                  if ((widget.pendingCertificate?.expectedValidity ==
                          CertificateStatusValidity.PENDING) &&
                      widget.animation.value < .25)
                    Opacity(
                      opacity: 1 - 4 * widget.animation.value,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: RefreshButton.outlined(
                              label: S.of(context).requestNewTestResult,
                              color: blue,
                            ),
                          ),
                          if (widget.pendingCertificate!.name != null)
                            Text(
                              'ID:' + ' ' + widget.pendingCertificate!.name!,
                              style: Theme.of(context).textTheme.headline4,
                            ),
                        ],
                      ),
                    ),
                ],
              ),
            ),
            if (widget.certificate.issuer!.isPrivate)
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                child: Material(
                  color: red,
                  child: ListTile(
                    title: AlignedMarkdown(
                      data: '# ' + S.of(context).internalCertificateFormatted,
                      color: Colors.white,
                    ),
                  ),
                ),
              )
          ],
        );
      },
    );
  }

  String _dateTimeString(DateTime dateTime) {
    return DateFormat.yMMMMd().format(dateTime) +
        ', ' +
        DateFormat.Hm().format(dateTime) +
        ' ' +
        S.of(context).oClock;
  }

  void _sharePdf() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => PdfExportScreen(certificate: widget.certificate),
        fullscreenDialog: true));
  }
}
