import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/components/dark-mode-overlay.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/screens/create-occasion/create-occasion.dart';
import 'package:free2pass/screens/recent-locations/recent-locations.screen.dart';
import 'package:free2pass_support/free2pass_support.dart';

class CheckInDecoration extends StatefulWidget {
  @override
  _CheckInDecorationState createState() => _CheckInDecorationState();
}

class _CheckInDecorationState extends State<CheckInDecoration> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return Stack(
        alignment: Alignment.center,
        fit: StackFit.expand,
        textDirection: TextDirection.ltr,
        children: [
          Padding(
            padding: EdgeInsets.only(
                top: getTopPadding(constraints.maxHeight) * 6.5,
                bottom: getBottomPadding(constraints.maxHeight) * 1.5),
            child: Transform.scale(
              scale: 1.3725,
              child: AspectRatio(
                aspectRatio: 1,
                child: DarkModeOverlay(
                  child: SvgPicture.asset(
                    kScalableAssetBase + 'checkin-grafik.svg',
                    excludeFromSemantics: true,
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            top: getTopPadding(constraints.maxHeight),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                AlignedMarkdown(data: S.of(context).checkInLongFormatted),
                FutureBuilder(
                  future: F2PApi.instance.occasions.available(),
                  builder: (context, AsyncSnapshot<ResponseModel<bool>> data) {
                    if (data.data?.payload == true) {
                      return Container(
                        constraints: BoxConstraints(
                          maxWidth: constraints.maxWidth < 320
                              ? constraints.maxWidth
                              : 320,
                        ),
                        padding: EdgeInsets.only(top: 48),
                        child: ListTile(
                          onTap: _createOccasion,
                          title: AlignedMarkdown(
                            data: S.of(context).createNewOccasion,
                            alignment: CrossAxisAlignment.start,
                          ),
                          trailing: Icon(Icons.arrow_forward),
                        ),
                      );
                    }
                    return Container();
                  },
                ),
              ],
            ),
          ),
          Positioned(
            bottom: getBottomPadding(constraints.maxHeight),
            child: TextButton.icon(
                onPressed: _showLastVisited,
                icon: Icon(
                  Icons.info,
                  color: blue,
                ),
                label: AlignedMarkdown(
                  data: S.of(context).showLastVisitedFormatted,
                )),
          )
        ],
      );
    });
  }

  void _showLastVisited() {
    Navigator.of(context).pushNamed(RecentLocationsScreen.routeName);
  }

  void _createOccasion() {
    Navigator.of(context).pushNamed(CreateOccasionScreen.routeName);
  }
}
