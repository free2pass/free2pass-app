import 'package:flutter/material.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/utils/hive.util.dart';
import 'package:hive_flutter/hive_flutter.dart';

class UserPhotoDecoration extends StatelessWidget {
  final Object tag;

  /// whether to listen on changes of the current user
  final bool listen;

  const UserPhotoDecoration(
      {Key? key, this.tag = kPhotoLocationKey, this.listen = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: tag,
      // using a value listenable only when dynamic changes should be detected
      child: listen
          ? ValueListenableBuilder<Box>(
              valueListenable: data.listenable(),
              builder: (context, Box box, w) => _UserImageFromBox(box: box),
            )
          : _UserImageFromBox(box: data),
    );
  }
}

class _UserImageFromBox extends StatelessWidget {
  final Box box;

  const _UserImageFromBox({Key? key, required this.box}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (userImages.containsKey(box.get(kCurrentUserKey)))
      return Container(
        constraints: BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width * 1 / 3,
            maxHeight: MediaQuery.of(context).size.height * 1 / 3),
        child: AspectRatio(
          aspectRatio: 1,
          child: Container(
            decoration: BoxDecoration(
                border: Border.all(width: 2, color: Colors.white),
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: MemoryImage(userImages[box.get(kCurrentUserKey)]!),
                    fit: BoxFit.cover)),
          ),
        ),
      );
    return Container();
  }
}
