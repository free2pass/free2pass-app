import 'dart:math';

import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass_support/free2pass_support.dart';

class HomePageBottomBar extends StatefulWidget {
  final ValueChanged<int> onNavigate;
  final int? page;

  const HomePageBottomBar({Key? key, required this.onNavigate, this.page})
      : super(key: key);

  @override
  _HomePageBottomBarState createState() => _HomePageBottomBarState();
}

class _HomePageBottomBarState extends State<HomePageBottomBar> {
  int current = 0;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: BottomNavigationBar(
        currentIndex: current,
        selectedItemColor: blue,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.badge), label: S.of(context).pass),
          BottomNavigationBarItem(
              icon: Transform(
                transform: Matrix4.rotationY(pi),
                alignment: Alignment.center,
                child: Transform.rotate(
                  child: Icon(Icons.vpn_key),
                  angle: .25 * pi,
                ),
              ),
              label: S.of(context).work),
        ],
        onTap: _change,
      ),
    );
  }

  @override
  void didUpdateWidget(covariant HomePageBottomBar oldWidget) {
    if (widget.page != null && widget.page != oldWidget.page) {
      setState(() {
        current = widget.page!;
      });
    }
    super.didUpdateWidget(oldWidget);
  }

  void _change(int value) {
    setState(() {
      current = value;
    });
    widget.onNavigate(value);
  }
}
