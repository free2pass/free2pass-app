import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/user-data.dart';
import 'package:free2pass/screens/home/components/views/decoration/user-photo-decoration.dart';
import 'package:free2pass/screens/multi-user/components/user-list-tile.dart';
import 'package:free2pass/utils/hive.util.dart';
import 'package:hive_flutter/hive_flutter.dart';

class UserSelector extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: PopupMenuButton(
        icon: ValueListenableBuilder<Box>(
            valueListenable: data.listenable(),
            builder: (context, Box box, w) {
              return userImages.containsKey(box.get(kCurrentUserKey))
                  ? UserPhotoDecoration(
                      tag: UserSelector,
                    )
                  : Icon(
                      Icons.person,
                      color: Colors.white,
                    );
            }),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        tooltip: S.of(context).changeUser,
        itemBuilder: (context) {
          return users.values
              .map(
                (e) => PopupMenuItem(
                  padding: EdgeInsets.zero,
                  child: UserListTile(
                    user: e,
                    isEditable: false,
                  ),
                  value: e,
                ),
              )
              .toList();
        },
        padding: EdgeInsets.all(2),
        onSelected: (UserData user) async {
          await user.hashInitialized;
          await data.put(kCurrentUserKey, user.hash);
        },
      ),
    );
  }
}
