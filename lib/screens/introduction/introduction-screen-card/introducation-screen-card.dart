import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:free2pass/components/dark-mode-overlay.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass_support/free2pass_support.dart';

class IntroductionScreenCard extends StatelessWidget {
  final String head;
  final String bottom;
  final String asset;
  final VoidCallback onAbort;

  const IntroductionScreenCard(
      {Key? key,
      required this.head,
      required this.bottom,
      required this.asset,
      required this.onAbort})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: LayoutBuilder(
        builder: (context, constraints) => Stack(
          alignment: Alignment.center,
          fit: StackFit.expand,
          children: [
            Padding(
              padding: EdgeInsets.only(
                // calculating the position of the asset relative to the two text's lengths
                top: 56 +
                    getTopPadding(constraints.maxHeight) *
                        (head.split('\n').length),
                bottom: getBottomPadding(constraints.maxHeight) *
                    (bottom.split('\n').length / 2.5),
              ),
              child: Transform.scale(
                scale: 1.25,
                child: DarkModeOverlay(child: SvgPicture.asset(asset)),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height / 12,
              child: AlignedMarkdown(
                data: head,
              ),
            ),
            Positioned(
              bottom: MediaQuery.of(context).size.height / 16,
              child: AlignedMarkdown(
                data: bottom,
              ),
            ),
            Positioned(
              top: 0,
              right: 0,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    IconButton(
                      onPressed: onAbort,
                      icon: Icon(Icons.close),
                      tooltip: S.of(context).close,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
