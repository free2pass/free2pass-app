import 'package:animations/animations.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:flutter/material.dart';
import 'package:free2pass/dialogs/date-picker/date-picker.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/user-data.dart';
import 'package:free2pass/screens/multi-user/multi-user.dart';
import 'package:free2pass/screens/setup/components/setup-card/views/photo.view.dart';
import 'package:free2pass/utils/encode-personal-data.dart';
import 'package:free2pass/utils/hive.util.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:intl/intl.dart';

const _nameRegex = r'^.+\s.+$';
const _streetRegex = r'.+\s';

const _injectionRegex = r'([=\*\+\(\)]|(HYPERLINK|CMD|DDE|SET|SUM)|(.+["' +
    '\'' +
    r':;].*){3,}|(SELECT|UPDATE|DELETE|SHOW|EXEC|DROP TABLE)|(http|ftp|rsync))';

/// A full screen dialog displaying a [UserDataForm]
///
/// once saved, page is popped by the [Navigator]
class EditUserDialog extends StatefulWidget {
  /// the [UserData] to edit
  final UserData? user;

  final bool isSecondaryUser;

  const EditUserDialog({Key? key, this.user, this.isSecondaryUser = true})
      : super(key: key);

  @override
  _EditUserDialogState createState() => _EditUserDialogState();
}

class _EditUserDialogState extends State<EditUserDialog> {
  /// the [GlobalKey] used to communicate with the [UserDataForm]
  GlobalKey<UserDataFormState> _formKey = GlobalKey();

  /// whether to display a progress indicator
  bool _saving = false;

  FocusNode _fabFocus = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(8),
        child: ResponsiveBox(
          child: UserDataForm(
            key: _formKey,
            user: widget.user,
            onSaved: _onSaved,
            isSecondaryUser: widget.isSecondaryUser,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
          focusNode: _fabFocus,
          icon: _saving
              ? SizedBox.fromSize(
                  child: CircularProgressIndicator(
                    color: Colors.white,
                  ),
                  size: Size.square(24))
              : Icon(Icons.person_add),
          onPressed: _saving ? () => null : _trySave,
          label: Text(S.of(context).add)),
    );
  }

  _onSaved(UserData user) {
    Navigator.of(context).pop();
  }

  /// checks whether the input is valid and asynchronously saves the data in case
  Future<void> _trySave() async {
    _fabFocus.requestFocus();

    /// is [false] in case validation was unsuccessful and a [Future]
    /// in  case input is valid
    final result = _formKey.currentState!.saveUser();
    if (result is Future) {
      setState(() {
        _saving = true;
      });
      await result;
      setState(() {
        _saving = false;
      });
    }
  }
}

/// the [Form] used to create users
///
/// requires a [Key] to be passed to enable the form to save the data
class UserDataForm extends StatefulWidget {
  /// some initial [UserData] to edit
  final UserData? user;

  /// a Function returning the just created [UserData] asynchronously
  /// after is has been saved
  final Function(UserData user)? onSaved;

  /// whether the created user is the primary user or not
  final bool isSecondaryUser;

  /// whether to allow the change of the photo or not
  final bool isInitialProcess;

  const UserDataForm(
      {required Key key,
      this.user,
      this.onSaved,
      this.isSecondaryUser = false,
      this.isInitialProcess = false})
      : super(key: key);

  @override
  UserDataFormState createState() => UserDataFormState();
}

class UserDataFormState extends State<UserDataForm> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _addressController = TextEditingController();
  TextEditingController _zipController = TextEditingController();
  TextEditingController _townController = TextEditingController();

  // only used to display a pretty String
  TextEditingController _birthdayController = TextEditingController();

  FilePickerCross? _photo;
  String? _photoPath;
  bool _usePassport = false;

  DateTime? _birthday;

  FocusNode _birthdayFocus = FocusNode();
  FocusNode _userImageFocus = FocusNode();
  FocusNode _addressImportFocus = FocusNode();

  /// used to verify the [Form]
  GlobalKey<FormState> _formKey = GlobalKey();

  @override
  void initState() {
    _importUser();

    _birthdayFocus.addListener(() {
      _birthdayFocus.unfocus();
    });

    super.initState();
  }

  void _importUser() {
    if (widget.user != null) {
      _nameController.text = widget.user!.name;
      _emailController.text = widget.user!.email ?? '';
      _addressController.text = widget.user!.street;
      _zipController.text = widget.user!.zip;
      _townController.text = widget.user!.town;
      _birthdayController.text = _birthdayString(widget.user!.birthdayDateTime);
      _birthday = widget.user!.birthdayDateTime;
      _importPhoto();
    }
  }

  /// either returns [false] in case the data is invalid or a [Future] in case
  /// the data successfully saves
  dynamic saveUser() {
    if (_formKey.currentState!.validate() == false) return;
    String? photoLocation, photoChecksum;
    if (!_usePassport && _photo != null) {
      photoLocation = _photoPath;
      photoChecksum = fileShaSum(_photo!);
    }
    final newUser = UserData(
      phone: widget.isSecondaryUser ? null : widget.user?.phone,
      name: _nameController.text.trim(),
      street: _addressController.text.trim(),
      zip: _zipController.text.trim(),
      town: _townController.text.trim(),
      email: _emailController.text.trim().isNotEmpty
          ? _emailController.text.trim()
          : null,
      birthday: (_birthday!.millisecondsSinceEpoch / 1000).round(),
      photoLocation: photoLocation,
      photoCheckSum: photoChecksum,
    );
    return _returnNewUser(newUser);
  }

  Future _returnNewUser(UserData newUser) async {
    await newUser.hashInitialized;

    // showing a funny hint on injections ;-)
    //
    // and ***NO*** of course we have corresponding (and yes, more advanced)
    // protections on the server and the other parts of the software too
    final injection = RegExp(_injectionRegex, caseSensitive: false);
    if (injection.hasMatch(newUser.name) ||
        injection.hasMatch(newUser.email ?? '') ||
        injection.hasMatch(newUser.street) ||
        injection.hasMatch(newUser.zip) ||
        injection.hasMatch(newUser.photoLocation ?? '') ||
        injection.hasMatch(newUser.town)) {
      final cancel = await showModal(
          context: context,
          builder: (context) => AlertDialog(
                title: Text(S.of(context).injection),
                content: Text(S.of(context).injectionLong),
                actions: [
                  TextButton(
                    onPressed: Navigator.of(context).pop,
                    child: Text(S.of(context).ignore),
                  ),
                  TextButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: Text(S.of(context).changeData),
                  ),
                ],
              ));
      if (cancel == true) return;
    }

    // checking whether user hash already exists but is not the one currently edited
    if (users.keys.contains(newUser.hash) &&
        (widget.user == null || newUser.hash != widget.user!.hash)) {
      final response = await showModal(
          context: context,
          builder: (context) => AlertDialog(
                title: Text(S.of(context).duplicateUser),
                content: Text(S.of(context).duplicateUserLong),
                actions: [
                  TextButton(
                      onPressed: () => Navigator.of(context).pop(true),
                      child: Text(S.of(context).abortAndViewUsers)),
                  TextButton(
                      onPressed: Navigator.of(context).pop,
                      child: Text(S.of(context).editProvidedData)),
                ],
              ));
      if (response == true) {
        Navigator.of(context).pushReplacementNamed(MultiUserScreen.routeName);
      }
      return false;
    }

    // ask whether to delete certificates

    if (widget.user != null) {
      // in case the user hash (name, zip, birth) or the photo checksum change, all certificates expire
      if (widget.user!.hash != newUser.hash ||
          widget.user!.photoCheckSum != newUser.photoCheckSum) {
        final response = await showModal(
            context: context,
            builder: (context) => AlertDialog(
                  title: Text(S.of(context).deleteCertificates),
                  content: Text(S
                      .of(context)
                      .youChangedImportantUserDataIfYouProceedAllCertificates),
                  actions: [
                    TextButton(
                        onPressed: Navigator.of(context).pop,
                        child: Text(S.of(context).cancel)),
                    TextButton(
                        onPressed: () => Navigator.of(context).pop(true),
                        child: Text(S.of(context).deleteCertificates)),
                  ],
                ));
        if (response != true) return false;
        // deleting all certificates
        for (var key in certificates.keys) {
          if (certificates.get(key)!.userHash == widget.user!.hash)
            await certificates.delete(key);
        }
        // in case the old user is active, changing current hash
        if (data.get(kCurrentUserKey) == widget.user!.hash)
          await data.put(kCurrentUserKey, newUser.hash);
        userImages.remove(widget.user!.hash);
        // overwriting default user hash in case it changed
        if (data.get(kDefaultUserKey) == widget.user!.hash)
          await data.put(kDefaultUserKey, newUser.hash);
      }
    }
    // saving user image in case it is set
    if (newUser.photoLocation != null) {
      userImages[newUser.hash!] =
          (await FilePickerCross.fromInternalPath(path: newUser.photoLocation!))
              .toUint8List();
    }

    if (!widget.isInitialProcess) {
      // saving user in database
      if (widget.user != null) {
        await users.delete(widget.user!.hash);
      }
      await users.put(newUser.hash, newUser);
    }

    widget.onSaved!(newUser);
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          AlignedMarkdown(
              data: widget.isSecondaryUser
                  ? widget.user == null
                      ? S.of(context).createUserDataLongFormatted
                      : '# ' + S.of(context).editUser
                  : S.of(context).personalDataLongFormatted),
          TextFormField(
            autofillHints: [AutofillHints.name],
            controller: _nameController,
            autofocus: true,
            keyboardType: Theme.of(context).platform == TargetPlatform.iOS
                ? TextInputType.text // iOS does not allow `-` in name input
                : TextInputType.name,
            textInputAction: TextInputAction.next,
            validator: _nameValidator,
            // inputFormatters: [NameCaseTextFormatter()],
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.person),
              border: OutlineInputBorder(),
              labelText: S.of(context).fullName,
              helperText: S.of(context).fullNameHelper,
            ),
          ),
          if (!widget.isInitialProcess)
            FormField(
              builder: (g) {
                return ListTile(
                  focusNode: _userImageFocus,
                  title: Text(S.of(context).selectPhoto),
                  leading: _photo != null
                      ? Container(
                          height: 56,
                          width: 56,
                          decoration: BoxDecoration(
                              border: Border.all(width: 2, color: Colors.white),
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: MemoryImage(_photo!.toUint8List()),
                                  fit: BoxFit.cover)),
                        )
                      : Icon(Icons.camera),
                  onTap: _selectPhoto,
                  subtitle: g.hasError
                      ? Text(
                          g.errorText!,
                          style: TextStyle(color: Colors.red),
                        )
                      : _usePassport
                          ? Text(S.of(context).usePassport)
                          : null,
                );
              },
              validator: _photoValidator,
            ),
          TextFormField(
            focusNode: _birthdayFocus,
            controller: _birthdayController,
            autofillHints: [AutofillHints.birthday],
            keyboardType: TextInputType.datetime,
            validator: _birthdayValidator,
            //// inputFormatters: [BirthdayFormatter()],
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.card_giftcard),
              border: OutlineInputBorder(),
              labelText: S.of(context).birthday,
            ),
            onTap: _pickBirthday,
          ),
          TextFormField(
            autofillHints: [AutofillHints.email],
            controller: _emailController,
            keyboardType: TextInputType.emailAddress,
            textInputAction: TextInputAction.next,
            validator: _optionalEmailValidator,
            decoration: InputDecoration(
                prefixIcon: Icon(Icons.email),
                border: OutlineInputBorder(),
                labelText: S.of(context).emailAddress,
                helperText: S.of(context).optional),
          ),
          if (widget.isSecondaryUser)
            OutlinedButton(
              focusNode: _addressImportFocus,
              onPressed: _importAddress,
              child: Text(S.of(context).importAddressFromInitialUser),
            ),
          TextFormField(
            autofillHints: [
              AutofillHints.streetAddressLine1,
              AutofillHints.fullStreetAddress,
            ],
            controller: _addressController,
            keyboardType: TextInputType.streetAddress,
            textInputAction: TextInputAction.next,
            validator: _addressValidator,
            // inputFormatters: [NameCaseTextFormatter()],
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.home),
              border: OutlineInputBorder(),
              labelText: S.of(context).address,
              helperText: S.of(context).addressHelper,
            ),
          ),
          TextFormField(
            autofillHints: [AutofillHints.postalCode],
            controller: _zipController,
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.next,
            validator: _zipValidator,
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.dialpad),
              border: OutlineInputBorder(),
              labelText: S.of(context).zipCode,
            ),
          ),
          TextFormField(
            controller: _townController,
            autofillHints: [AutofillHints.addressCity],
            keyboardType: TextInputType.text,
            validator: _townValidator,
            textInputAction: TextInputAction.next,
            // inputFormatters: [NameCaseTextFormatter()],
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.location_city),
              border: OutlineInputBorder(),
              labelText: S.of(context).town,
            ),
          ),
          if (widget.isSecondaryUser && widget.user != null)
            ButtonBar(
              children: [
                TextButton(
                    onPressed: _cancel, child: Text(S.of(context).cancel)),
                TextButton(
                    onPressed: _delete, child: Text(S.of(context).deleteUser)),
              ],
            ),
          Container(
            height: 64,
          )
        ]
            .map((e) => Padding(
                  child: e,
                  padding: EdgeInsets.symmetric(vertical: 8),
                ))
            .toList(),
      ),
    );
  }

  String? _nameValidator(String? value) {
    if (value == null || !RegExp(_nameRegex).hasMatch(value.trim())) {
      return S.of(context).fullNameValidator;
    }
    return null;
  }

  String? _addressValidator(String? value) {
    if (value == null ||
        !RegExp(_streetRegex).hasMatch(value.trim()) ||
        !RegExp(r'\d').hasMatch(value.trim())) {
      return S.of(context).addressValidator;
    }
    return null;
  }

  String? _birthdayValidator(String? value) {
    if (value == null || value.isEmpty) {
      return S.of(context).birthdayValidator;
    }
    return null;
  }

  String? _zipValidator(String? value) {
    if (value == null || int.tryParse(value) == null || 4 > value.length) {
      return S.of(context).pleaseProvideAValidZipCode;
    }
    return null;
  }

  String? _townValidator(String? value) {
    if (value == null || value.length < 3) {
      return S.of(context).pleaseProvideAValidTown;
    }
    return null;
  }

  Future<void> _pickBirthday() async {
    final DateTime? datePick = await showInputDatePicker(
      context: context,
      initialDate: _birthday,
    );
    if (datePick != null && datePick != _birthday) {
      _birthday = datePick;
      _birthdayController.text = _birthdayString(_birthday!);
    }
  }

  String _birthdayString(DateTime dateTime) {
    return DateFormat.yMMMMd().format(dateTime);
  }

  @override
  void dispose() {
    _zipController.dispose();
    _townController.dispose();
    _birthdayController.dispose();
    _addressController.dispose();
    _emailController.dispose();
    _nameController.dispose();
    super.dispose();
  }

  String? _optionalEmailValidator(String? value) {
    if (value == null ||
        value.trim().isEmpty ||
        RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9\-]+\.[a-zA-Z]+")
            .hasMatch(value.trim())) return null;
    return S.of(context).emailValidator;
  }

  void _importAddress() {
    _addressImportFocus.requestFocus();
    final data = getDefaultUserData();
    _zipController.text = data.zip;
    _addressController.text = data.street;
    _townController.text = data.town;
  }

  void _cancel() {
    Navigator.of(context).pop();
  }

  Future<void> _delete() async {
    final result = await showModal(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(S.of(context).areYouSure),
        content: AlignedMarkdown(data: S.of(context).deleteUserLongFormatted),
        actions: [
          TextButton(
              onPressed: Navigator.of(context).pop,
              child: Text(S.of(context).cancel)),
          TextButton(
              onPressed: () => Navigator.of(context).pop(true),
              child: Text(S.of(context).iUnderstood)),
        ],
      ),
    );
    if (result) {
      certificates.keys.forEach((key) {
        if (certificates.get(key)!.userHash == widget.user!.hash)
          certificates.delete(key);
      });
      // changing current profile in case the user to delete is active
      if (data.get(kCurrentUserKey) == widget.user!.hash) {
        final defaultUser = getDefaultUserData();
        await defaultUser.hashInitialized;
        await data.put(kCurrentUserKey, defaultUser.hash);
        userImages.remove(widget.user!.hash);
      }
      await deleteUnusedSelfies();
      await users.delete(widget.user!.hash);
      Navigator.of(context).pop();
    }
  }

  String? _photoValidator(value) {
    if (_photo != null || _usePassport) return null;
    return S.of(context).photoValidator;
  }

  void _selectPhoto() {
    _userImageFocus.requestFocus();
    const radius = BorderRadius.vertical(top: Radius.circular(32));
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(borderRadius: radius),
        builder: (context) => ClipRRect(
              borderRadius: radius,
              child: Material(
                color: Colors.transparent,
                child: SafeArea(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16),
                    child: PhotoSelector(
                      onPassport: _onPassport,
                      onPhoto: _onPhoto,
                    ),
                  ),
                ),
              ),
            ));
  }

  void _onPassport() {
    Navigator.of(context).pop();
    setState(() {
      _usePassport = true;
      _photo = null;
      _photoPath = null;
    });
  }

  _onPhoto(FilePickerCross photo, String path) {
    Navigator.of(context).pop();
    setState(() {
      _usePassport = false;
      _photo = photo;
      _photoPath = path;
    });
  }

  void _importPhoto() async {
    if (widget.user!.photoLocation != null) {
      _photo = await FilePickerCross.fromInternalPath(
          path: widget.user!.photoLocation!);
      setState(() {
        _photoPath = widget.user!.photoLocation;
      });
    }
  }
}
