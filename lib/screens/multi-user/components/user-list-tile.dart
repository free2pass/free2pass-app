import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/user-data.dart';
import 'package:free2pass/screens/multi-user/components/edit-user-dialog.dart';
import 'package:free2pass/utils/hive.util.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:hive_flutter/hive_flutter.dart';

class UserListTile extends StatefulWidget {
  final UserData user;
  final bool isEditable;
  final bool isSecondaryUser;

  const UserListTile({Key? key, required this.user, this.isEditable = true, this.isSecondaryUser = true})
      : super(key: key);

  @override
  _UserListTileState createState() => _UserListTileState();
}

class _UserListTileState extends State<UserListTile> {
  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: data.listenable(),
        builder: (context, Box box, w) {
          return FutureBuilder<bool>(
              future: _checkSelection(),
              initialData: false,
              builder: (context, snapshot) {
                return ListTile(
                  leading: _LeadingImage(hash: widget.user.hash),
                  title: Text(widget.user.name),
                  subtitle:
                      snapshot.data! ? Text(S.of(context).currentUser) : null,
                  selected: snapshot.data ?? false,
                  selectedTileColor: blue.withOpacity(.25),
                  onTap: _activateUser,
                  trailing: widget.isEditable
                      ? IconButton(
                          icon: const Icon(Icons.edit),
                          tooltip: S.of(context).editUser,
                          onPressed: _editUser,
                        )
                      : null,
                );
              });
        });
  }

  void _editUser() {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => EditUserDialog(
        user: widget.user,
        isSecondaryUser: widget.isSecondaryUser,
      ),
      fullscreenDialog: true,
    ));
  }

  Future<bool> _checkSelection() async {
    await widget.user.hashInitialized;
    return widget.user.hash == data.get(kCurrentUserKey);
  }

  Future<void> _activateUser() async {
    await widget.user.hashInitialized;
    await data.put(kCurrentUserKey, widget.user.hash);
  }
}

/// the user image for the given path
class _LeadingImage extends StatelessWidget {
  /// the hash of the photo to be displayed
  final String? hash;

  const _LeadingImage({Key? key, this.hash}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (userImages.containsKey(hash)) {
      return Container(
        height: 56,
        width: 56,
        decoration: BoxDecoration(
            border: Border.all(width: 2, color: Colors.white),
            shape: BoxShape.circle,
            image: DecorationImage(
                image: MemoryImage(userImages[hash!]!), fit: BoxFit.cover)),
      );
    } else
      return SizedBox.fromSize(
        size: Size.square(56),
        child: Icon(
          Icons.person,
          size: 32,
        ),
      );
  }
}
