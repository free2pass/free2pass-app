import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/screens/multi-user/components/user-list-tile.dart';
import 'package:free2pass/utils/encode-personal-data.dart';
import 'package:free2pass/utils/hive.util.dart';

class UserListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) {
        if (index == 0)
          return Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(S.of(context).initialUser),
              UserListTile(
                user: getDefaultUserData(),
                isSecondaryUser: false,
              ),
              Text(S.of(context).otherUsers),
            ],
          );
        final user = users.get(users.keys
            .where((element) => element != data.get(kDefaultUserKey))
            .toList()[index - 1])!;
        return UserListTile(user: user);
      },
      itemCount: users.length,
    );
  }
}
