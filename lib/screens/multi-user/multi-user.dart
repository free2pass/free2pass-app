import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:free2pass/components/dark-mode-overlay.dart';
import 'package:free2pass/components/default-drawer/default-drawer.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/screens/multi-user/components/edit-user-dialog.dart';
import 'package:free2pass/screens/multi-user/components/user-list-view.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:hive_flutter/hive_flutter.dart';

const String _multiUserAsset = kScalableAssetBase + 'grafik-multi-user.svg';

class MultiUserScreen extends StatefulWidget {
  static const routeName = '/user';

  @override
  _MultiUserScreenState createState() => _MultiUserScreenState();
}

class _MultiUserScreenState extends State<MultiUserScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(),
      drawer: DefaultDrawer(),
      body: ResponsiveBox(
        child: ValueListenableBuilder(
          valueListenable: users.listenable(),
          builder: (context, Box box, w) {
            // if only one user available
            if (users.length == 1) {
              return LayoutBuilder(
                builder: (context, constraints) => Stack(
                  alignment: Alignment.center,
                  children: [
                    Transform.scale(
                      scale: 1.25,
                      child: DarkModeOverlay(
                        child: SvgPicture.asset(_multiUserAsset),
                      ),
                    ),
                    Positioned(
                      top: getTopPadding(constraints.maxHeight),
                      child: AlignedMarkdown(
                        data: S.of(context).createUsersLongFormatted,
                      ),
                    ),
                  ],
                ),
              );
            }
            return UserListView();
          },
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: OpenContainer(
        closedBuilder: (c, f) => FloatingActionButton.extended(
            elevation: 0,
            onPressed: null,
            backgroundColor: Colors.transparent,
            label: AlignedMarkdown(
              data: S.of(context).createUser,
              color: Colors.white,
            )),
        closedShape: StadiumBorder(),
        closedColor: green,
        openBuilder: (c, f) => EditUserDialog(),
      ),
    );
  }
}
