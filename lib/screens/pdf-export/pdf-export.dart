import 'dart:math';

import 'package:flutter/material.dart';
import 'package:free2pass/components/requires-network-page.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/models/certificate.dart';
import 'package:free2pass/screens/pdf-export/pdf/generate-poster.dart';
import 'package:free2pass/utils/encode-personal-data.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:pdf/pdf.dart';
import 'package:printing/printing.dart';

class PdfExportScreen extends StatefulWidget {
  final Certificate certificate;

  const PdfExportScreen({Key? key, required this.certificate})
      : super(key: key);

  @override
  _PdfExportScreenState createState() => _PdfExportScreenState();
}

class _PdfExportScreenState extends State<PdfExportScreen> {
  @override
  Widget build(BuildContext context) {
    final user = getCurrentUser();
    var title =
        'free2pass ${S.of(context).certificate} ${widget.certificate.name} ${user.name}';
    return RequiresNetworkPage(
      child: Scaffold(
        appBar: DefaultAppBar(),
        body: PdfPreview(
          build: (format) => generatePoster(
            format: format,
            certificate: widget.certificate,
            user: user,
            s: S.of(context),
            title: title,
          ),
          initialPageFormat: PdfPageFormat.a4,
          pdfFileName: title.replaceAll(' ', '-') + '.pdf',
          canChangeOrientation: false,
          previewPageMargin: EdgeInsets.all(16),
          pageFormats: {
            S.of(context).formatA4: PdfPageFormat.a4,
            S.of(context).formatUSLetter: PdfPageFormat.letter,
            S.of(context).formatUSLegal: PdfPageFormat.legal,
          },
          maxPageWidth: (MediaQuery.of(context).size.height / sqrt2) - 56 * 2,
        ),
      ),
    );
  }
}
