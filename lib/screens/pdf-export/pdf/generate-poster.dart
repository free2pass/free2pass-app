import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/services.dart';
import 'package:free2pass/components/validity-countdown/validity-countdown.dart';
import 'package:free2pass/dialogs/passport-dialog/passport-dialog.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/certificate.dart';
import 'package:free2pass/models/user-data.dart';
import 'package:free2pass_support/free2pass_support.dart' as support;
import 'package:intl/intl.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;

final PdfColor green = PdfColor.fromInt(support.green.value);
final PdfColor gray = PdfColor.fromInt(support.gray.value);
final PdfColor blue = PdfColor.fromInt(support.blue.value);

Future<Uint8List> generatePoster(
    {required PdfPageFormat format,
    required Certificate certificate,
    required UserData user,
    required S s,
    String? title}) async {
  final doc = pw.Document(title: title, author: 'free2pass GmbH');

  final qrData = await user.makeQrDeeplink(certificate);

  doc.addPage(
    pw.MultiPage(
      pageTheme: await _free2passPageTheme(format),
      header: (pw.Context context) {
        return pw.Header(
          level: 2,
          child: pw.RichText(
              text: pw.TextSpan(style: pw.TextStyle(fontSize: 18), children: [
            pw.TextSpan(
                text: s.pdfOfficial.toUpperCase(),
                style: pw.TextStyle(fontWeight: pw.FontWeight.bold)),
            pw.TextSpan(text: ' ' + s.pdfTestCertificate),
          ])),
        );
      },
      build: (pw.Context context) {
        String issuerTimeZoneSuffix = '';
        Duration issuerUtcOffset = Duration();
        if (certificate.issuer!.utcOffset != null) {
          final minutesDec = certificate.issuer!.utcOffset! % 60;
          final hours = (certificate.issuer!.utcOffset! - minutesDec) / 60;
          final minutes = minutesDec / 10 * 6;
          final prefix = hours.isNegative ? '-' : '+';
          issuerTimeZoneSuffix = ', ' +
              s.timezone +
              ': UTC' +
              prefix +
              makeTwoCharacters(hours.toInt().abs()) +
              ':' +
              makeTwoCharacters(minutes.toInt());
          issuerUtcOffset = Duration(
            hours: hours.toInt(),
            minutes: minutes.toInt(),
          );
        }

        final now = DateTime.now().timeZoneOffset.inMinutes;
        final minutesDec = now % 60;
        final hours = (now - minutesDec) / 60;
        final minutes = minutesDec / 10 * 6;
        final prefix = hours.isNegative ? '-' : '+';
        String localTimeZoneSuffix = ' UTC' +
            prefix +
            makeTwoCharacters(hours.toInt().abs()) +
            ':' +
            makeTwoCharacters(minutes.toInt());

        return [
          pw.Text(s.pdfPoCTest),
          pw.Text(s.issuer + ':',
              style: pw.TextStyle(fontWeight: pw.FontWeight.bold)),
          pw.Text(
            certificate.issuer!.name,
          ),
          pw.Text(
            certificate.issuer!.address.replaceAll(r', ', '\n'),
          ),
          pw.Container(height: 8),
          pw.Text(s.pdfTestUsed + ':',
              style: pw.TextStyle(fontWeight: pw.FontWeight.bold)),
          pw.Text(
            certificate.testType ?? s.pdfUnknownTestType,
          ),
          pw.Container(height: 16),
          pw.Row(
            mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
            mainAxisSize: pw.MainAxisSize.max,
            crossAxisAlignment: pw.CrossAxisAlignment.center,
            children: [
              pw.Container(
                  height: 128,
                  width: 128,
                  decoration: pw.BoxDecoration(
                    border: pw.Border.all(color: green, width: 2),
                    shape: pw.BoxShape.circle,
                    image: user.photoLocation == null
                        ? null
                        : pw.DecorationImage(
                            image: pw.MemoryImage(userImages[user.hash]!),
                            fit: pw.BoxFit.cover),
                  ),
                  alignment: pw.Alignment.center,
                  child: user.photoLocation == null
                      ? pw.Text(s.pdfNoPhoto,
                          style: pw.TextStyle(fontSize: 20),
                          textAlign: pw.TextAlign.center)
                      : null),
              pw.Padding(
                padding: pw.EdgeInsets.symmetric(horizontal: 16),
                child: pw.Column(
                  mainAxisSize: pw.MainAxisSize.min,
                  crossAxisAlignment: pw.CrossAxisAlignment.start,
                  children: [
                    pw.Text(user.name,
                        style: pw.TextStyle(
                            fontWeight: pw.FontWeight.bold, fontSize: 14)),
                    pw.Container(height: 8),
                    pw.Text(user.street, style: pw.TextStyle(color: gray)),
                    pw.Text(user.zip + ' ' + user.town,
                        style: pw.TextStyle(color: gray)),
                    pw.Container(height: 8),
                    pw.Text(s.pdfBirth +
                        ' ' +
                        DateFormat.yMMMMd().format(user.birthdayDateTime))
                  ],
                ),
              ),
              pw.Expanded(child: pw.Container()),
              pw.Padding(
                padding: pw.EdgeInsets.all(8),
                child: pw.Column(
                  mainAxisSize: pw.MainAxisSize.min,
                  children: [
                    pw.Flexible(
                      flex: 1,
                      child: pw.Container(height: 24),
                    ),
                    pw.BarcodeWidget(
                      data: qrData,
                      width: 96,
                      height: 96,
                      color: PdfColors.black,
                      barcode: pw.Barcode.qrCode(),
                    ),
                    pw.Flexible(
                      flex: 1,
                      child: pw.Container(
                        height: 24,
                        alignment: pw.Alignment.bottomCenter,
                        child: pw.Padding(
                          padding: pw.EdgeInsets.only(top: 2),
                          child: pw.Text(
                            s.pdfCertificateSigned,
                            textAlign: pw.TextAlign.center,
                            style: pw.TextStyle(fontSize: 8),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          pw.Container(height: 16),
          pw.Container(
            decoration: pw.BoxDecoration(
                borderRadius: pw.BorderRadius.circular(32),
                border: pw.Border.all(
                  color: green,
                  width: 2,
                )),
            child: pw.Padding(
              child: pw.Center(
                child: pw.Column(
                    mainAxisSize: pw.MainAxisSize.min,
                    children: [
                      pw.Text(s.pdfNegativeLong,
                          style: pw.TextStyle(
                              fontSize: 18, fontWeight: pw.FontWeight.bold)),
                      pw.Text(s.pdfTestTime +
                          localDateTimeString(
                            s,
                            certificate.resultDateTime
                                .toUtc()
                                .add(issuerUtcOffset),
                          ) +
                          issuerTimeZoneSuffix),
                      pw.Text(
                          s.pdfValidUntil +
                              ': ' +
                              localDateTimeString(
                                s,
                                certificate.expiryDateTime
                                /*.toUtc()
                                    .add(issuerUtcOffset)*/
                                ,
                              ) +
                              localTimeZoneSuffix,
                          style: pw.TextStyle(
                              fontSize: 16, fontWeight: pw.FontWeight.bold)),
                    ]
                        .map(
                          (e) => pw.Padding(
                            padding: pw.EdgeInsets.all(4),
                            child: e,
                          ),
                        )
                        .toList()),
              ),
              padding: pw.EdgeInsets.all(8),
            ),
          ),
          pw.Padding(
            padding: pw.EdgeInsets.symmetric(vertical: 16, horizontal: 24)
                .copyWith(bottom: 0),
            child: pw.Text(s.pdfNegativeTestLegal,
                style: pw.TextStyle(
                    fontStyle: pw.FontStyle.italic, fontSize: 8.75),
                textAlign: pw.TextAlign.center),
          ),
        ];
      },
      footer: (context) {
        return pw.DefaultTextStyle.merge(
          child: pw.Column(children: [
            pw.Text(s.pdfLegalSignature,
                style:
                    pw.TextStyle(color: gray, fontStyle: pw.FontStyle.italic)),
            pw.Container(height: 16),
            pw.Text(s.pdfLegalNoTransfer,
                style: pw.TextStyle(fontWeight: pw.FontWeight.bold)),
            pw.Container(height: 8),
            pw.Text(s.pdfLegalNotice,
                style: pw.TextStyle(fontSize: 7),
                textAlign: pw.TextAlign.center)
          ]),
          textAlign: pw.TextAlign.center,
          style: pw.TextStyle(fontSize: 8.75),
        );
      },
    ),
  ); //

  return doc.save();
}

Future<pw.PageTheme> _free2passPageTheme(PdfPageFormat format) async {
  final logoShape = await rootBundle.loadString('assets/scalable/logo.svg');

  format = format.applyMargin(
    left: 2.0 * PdfPageFormat.cm,
    top: 4.5 * PdfPageFormat.cm,
    right: 2.0 * PdfPageFormat.cm,
    bottom: 1.0 * PdfPageFormat.cm,
  );
  return pw.PageTheme(
    pageFormat: format,
    theme: pw.ThemeData.withFont(
      base:
          pw.Font.ttf(await rootBundle.load('assets/pdf/OpenSans-Regular.ttf')),
      bold: pw.Font.ttf(await rootBundle.load('assets/pdf/OpenSans-Bold.ttf')),
      boldItalic: pw.Font.ttf(
          await rootBundle.load('assets/pdf/OpenSans-BoldItalic.ttf')),
      italic:
          pw.Font.ttf(await rootBundle.load('assets/pdf/OpenSans-Italic.ttf')),
    ),
    buildBackground: (pw.Context context) {
      return pw.FullPage(
        ignoreMargins: true,
        child: pw.Stack(
          children: [
            pw.Positioned(
              child: pw.SvgImage(svg: logoShape, width: 96, height: 96),
              top: 32,
              right: 32,
            ),
            pw.Positioned(
              left: 1.6 * PdfPageFormat.cm,
              top: 0,
              child: pw.Container(
                width: 4,
                height: 10.5 * PdfPageFormat.cm,
                decoration: pw.BoxDecoration(
                  color: green,
                  borderRadius: pw.BorderRadius.only(
                    bottomRight: pw.Radius.circular(2),
                    bottomLeft: pw.Radius.circular(2),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    },
  );
}
