import 'package:flutter/material.dart';
import 'package:free2pass_support/free2pass_support.dart';

class BottomSheetHandle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ResponsiveBox(
      child: Material(
        //elevation: 12,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(32),
          topRight: Radius.circular(32),
        ),
        child: Center(
          child: Container(
            width: 64,
            height: 8,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: Theme.of(context).primaryColor),
          ),
        ),
      ),
    );
  }
}
