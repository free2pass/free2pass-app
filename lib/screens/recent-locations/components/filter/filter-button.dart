import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';

class ToggleFilterButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (context) => IconButton(
        onPressed: Scaffold.of(context).openEndDrawer,
        tooltip: S.of(context).filterLocations,
        icon: Icon(Icons.filter_alt),
      ),
    );
  }
}
