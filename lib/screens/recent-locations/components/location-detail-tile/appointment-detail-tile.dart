import 'package:animations/animations.dart';
import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/enums/appointments.enum.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/models/appointment.dart';
import 'package:free2pass/screens/recent-locations/components/location-detail-tile/components/appointment-qr.dart';
import 'package:free2pass/utils/geo_url.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

class AppointmentDetailTile extends StatefulWidget {
  final Appointment appointment;
  final VoidCallback onActivated;

  const AppointmentDetailTile(
      {Key? key, required this.appointment, required this.onActivated})
      : super(key: key);

  @override
  State<AppointmentDetailTile> createState() => _AppointmentDetailTileState();
}

class _AppointmentDetailTileState extends State<AppointmentDetailTile> {
  bool _canceling = false;

  @override
  Widget build(BuildContext context) {
    return ExpansionTileCard(
      title: Text(widget.appointment.issuer.name),
      subtitle: Text(S.of(context).upcomingAppointment),
      onExpansionChanged: _handleExpansion,
      children: [
        ListTile(
          leading: Icon(Icons.pin_drop),
          title: Text(widget.appointment.issuer.address!),
        ),
        ListTile(
          leading: Icon(Icons.calendar_today),
          subtitle: Text(S.of(context).time),
          title: Text(DateFormat.yMMMMd().format(
                DateTime.fromMillisecondsSinceEpoch(
                    widget.appointment.startTime * 1000),
              ) +
              ', ' +
              DateFormat.Hm().format(
                DateTime.fromMillisecondsSinceEpoch(
                    widget.appointment.startTime * 1000),
              ) +
              ' ' +
              S.of(context).oClock),
        ),
        if (widget.appointment.state == AppointmentState.APPROVED)
          AppointmentQr(appointment: widget.appointment),
        if (widget.appointment.state == AppointmentState.CANCELED)
          ListTile(
            title: Text(S.of(context).youCanceledAppointment),
          ),
        if (widget.appointment.state == AppointmentState.REVOKED)
          ListTile(
            title: Text(S.of(context).issuerRevokedAppointment),
            subtitle: Text(widget.appointment
                .appointmentRevokeReasonString(S.of(context))),
          ),
        if (widget.appointment.issuer.contactWebsite != null ||
            (widget.appointment.issuer.lat != null &&
                widget.appointment.issuer.lng != null))
          ButtonBar(
            children: [
              if (widget.appointment.state == AppointmentState.APPROVED)
                OutlinedButton.icon(
                  onPressed: !_canceling ? _cancelAppointment : null,
                  icon: !_canceling
                      ? Icon(Icons.cancel)
                      : SizedBox.fromSize(
                          size: Size.square(24),
                          child: CircularProgressIndicator(),
                        ),
                  label: Text(S.of(context).cancelAppointment),
                ),
              if (widget.appointment.issuer.contactWebsite != null)
                OutlinedButton.icon(
                  onPressed: _openWebsite,
                  icon: Icon(Icons.open_in_new),
                  label: Text(S.of(context).openWebsite),
                ),
              if (widget.appointment.issuer.lat != null &&
                  widget.appointment.issuer.lng != null)
                OutlinedButton.icon(
                  onPressed: _directions,
                  icon: Icon(Icons.directions),
                  label: Text(S.of(context).directions),
                )
            ],
          ),
      ],
    );
  }

  void _openWebsite() {
    launch(widget.appointment.issuer.contactWebsite!);
  }

  void _handleExpansion(bool value) {
    if (value) {
      widget.onActivated();
      F2PApi.instance.appointments.check(
        id: widget.appointment.id,
        signature: widget.appointment.signature,
      );
    }
  }

  void _directions() {
    GeoUrl.open(
      context,
      widget.appointment.issuer.lat!,
      widget.appointment.issuer.lng!,
    );
  }

  Future<void> _cancelAppointment() async {
    setState(() {
      _canceling = true;
    });
    var _textController = TextEditingController();
    final selection = await showModal<AppointmentCancelReason>(
        context: context,
        builder: (context) => SimpleDialog(
              title: Text(S.of(context).cancelReasonLong),
              children: [
                ListTile(
                  title: Text(S.of(context).cancelUnavailable),
                  trailing: Icon(Icons.navigate_next),
                  onTap: () => Navigator.of(context)
                      .pop(AppointmentCancelReason.UNAVAILABLE),
                ),
                ListTile(
                  title: Text(S.of(context).cancelWrongTime),
                  trailing: Icon(Icons.navigate_next),
                  onTap: () => Navigator.of(context)
                      .pop(AppointmentCancelReason.WRONG_TIME),
                ),
                ListTile(
                  title: TextField(
                    controller: _textController = TextEditingController(),
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: S.of(context).cancelOtherReason,
                        suffixIcon: IconButton(
                            onPressed: () => Navigator.of(context)
                                .pop(AppointmentCancelReason.OTHER),
                            icon: Icon(Icons.navigate_next))),
                  ),
                  onTap: () =>
                      Navigator.of(context).pop(AppointmentCancelReason.OTHER),
                ),
              ],
            ));
    if (selection == null) {
      setState(() {
        _canceling = false;
      });
      return;
    }
    final response = await F2PApi.instance.appointments.cancel(
      id: widget.appointment.id,
      reason: selection,
      reasonString: selection == AppointmentCancelReason.OTHER
          ? _textController.text
          : null,
      signature: widget.appointment.signature,
    );
    setState(() {
      _canceling = false;
    });
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(response.status.success
            ? S.of(context).appointmentCancelSuccessful
            : S.of(context).appointmentCancelError),
      ),
    );
  }
}
