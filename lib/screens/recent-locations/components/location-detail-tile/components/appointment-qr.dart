import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/models/appointment.dart';
import 'package:free2pass/screens/scan-flow/scan-flow.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:pretty_qr_code/pretty_qr_code.dart';

class AppointmentQr extends StatefulWidget {
  final Appointment appointment;

  const AppointmentQr({Key? key, required this.appointment}) : super(key: key);

  @override
  State<AppointmentQr> createState() => _AppointmentQrState();
}

class _AppointmentQrState extends State<AppointmentQr> {
  @override
  Widget build(BuildContext context) {
    final qr = LayoutBuilder(
      builder: (context, constrains) {
        constrains = constrains.copyWith(
            maxHeight: MediaQuery.of(context).size.height -
                (MediaQuery.of(context).viewPadding.top +
                    MediaQuery.of(context).viewPadding.bottom),
            maxWidth: MediaQuery.of(context).size.width -
                (MediaQuery.of(context).viewPadding.left +
                    MediaQuery.of(context).viewPadding.right));
        return PrettyQr(
          image: AssetImage('assets/logos/icon.png'),
          size: (constrains.maxWidth < constrains.maxHeight
                  ? constrains.maxWidth
                  : constrains.maxHeight) -
              128,
          data: widget.appointment.qrEncode(),
          errorCorrectLevel: QrErrorCorrectLevel.Q,
          roundEdges: true,
          elementColor: Theme.of(context).brightness == Brightness.light
              ? gray.withOpacity(.65)
              : Colors.white.withOpacity(.65),
        );
      },
    );
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: OpenContainer(
        closedBuilder: (context, f) {
          return Column(
            children: [
              Center(
                child: qr,
              ),
              Container(
                height: 16,
              ),
              Text(S.of(context).tapToEnlarge),
            ],
          );
        },
        closedColor: Colors.transparent,
        closedElevation: 0,
        openColor: Colors.transparent,
        openElevation: 0,
        openBuilder: (BuildContext context,
            void Function({Object? returnValue}) action) {
          return Scaffold(
            body: Center(
              child: ResponsiveBox(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    qr,
                    ButtonBar(
                      children: [
                        ElevatedButton(
                          onPressed: _openScanPage,
                          child: Text(S.of(context).startTestNow),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  void _openScanPage() {
    Navigator.of(context).pushReplacementNamed(ScanFlowScreen.routeName);
  }
}
