import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:free2pass/enums/location-types.enum.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/models/merchant.dart';
import 'package:free2pass/utils/geo_url.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

class LocationDetailTile extends StatefulWidget {
  final Merchant location;
  final VoidCallback? onActivated;

  const LocationDetailTile(
      {Key? key, required this.location, required this.onActivated})
      : super(key: key);

  @override
  _LocationDetailTileState createState() => _LocationDetailTileState();
}

class _LocationDetailTileState extends State<LocationDetailTile> {
  @override
  Widget build(BuildContext context) {
    return ExpansionTileCard(
      onExpansionChanged: _handleExpansion,
      leading: Icon(widget.location.type == LocationType.OCCASION
          ? Icons.celebration
          : Icons.landscape),
      title: Text(widget.location.name),
      subtitle: Text(_dateTimeString(widget.location.checkInDateTime!)),
      children: [
        widget.location.type != LocationType.OCCASION
            ? ListTile(
                leading: Icon(Icons.pin_drop),
                title: Text(widget.location.address!),
              )
            : ListTile(
                leading: Icon(Icons.info),
                title: Text(S.of(context).infoPrivateOccasion),
              ),
        ListTile(
          leading: Icon(Icons.calendar_today),
          subtitle: Text(S.of(context).checkOut),
          title: Text(
              DateFormat.yMMMMd().format(widget.location.checkOutDateTime!) +
                  ', ' +
                  DateFormat.Hm().format(widget.location.checkOutDateTime!) +
                  ' ' +
                  S.of(context).oClock),
        ),
        if (widget.location.contactWebsite != null ||
            (widget.location.lat != null && widget.location.lng != null))
          ButtonBar(
            children: [
              if (widget.location.contactWebsite != null)
                OutlinedButton.icon(
                  onPressed: _openWebsite,
                  icon: Icon(Icons.open_in_new),
                  label: Text(S.of(context).openWebsite),
                ),
              if (widget.location.lat != null && widget.location.lng != null)
                OutlinedButton.icon(
                  onPressed: _directions,
                  icon: Icon(Icons.directions),
                  label: Text(S.of(context).directions),
                )
            ],
          ),
      ],
    );
  }

  String _dateTimeString(DateTime dateTime) {
    return DateFormat.yMMMMd().format(dateTime) +
        ', ' +
        DateFormat.Hm().format(dateTime) +
        ' ' +
        S.of(context).oClock;
  }

  void _openWebsite() {
    launch(widget.location.contactWebsite!);
  }

  void _handleExpansion(bool value) {
    if (value && widget.location.type != LocationType.OCCASION)
      widget.onActivated!();
  }

  void _directions() {
    GeoUrl.open(context, widget.location.lat!, widget.location.lng!);
  }
}
