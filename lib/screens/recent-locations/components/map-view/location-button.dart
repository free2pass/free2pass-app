import 'package:flutter/material.dart';
import 'package:flutter_map_location/flutter_map_location.dart';
import 'package:free2pass/generated/l10n.dart';

class LocationButton extends StatefulWidget {
  final ValueNotifier<LocationServiceStatus> statusListenable;
  final VoidCallback onPressed;

  const LocationButton(
      {Key? key, required this.statusListenable, required this.onPressed})
      : super(key: key);

  @override
  _LocationButtonState createState() => _LocationButtonState();
}

class _LocationButtonState extends State<LocationButton> {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomRight,
      child: Padding(
        padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).size.height / 10 + 56 + 16,
            right: 16.0),
        child: FloatingActionButton(
            mini: true,
            tooltip: S.of(context).locateMe,
            child: ValueListenableBuilder<LocationServiceStatus>(
                valueListenable: widget.statusListenable,
                builder: (BuildContext context, LocationServiceStatus value,
                    Widget? child) {
                  switch (value) {
                    case LocationServiceStatus.disabled:
                    case LocationServiceStatus.permissionDenied:
                    case LocationServiceStatus.unsubscribed:
                      return const Icon(
                        Icons.location_disabled,
                        color: Colors.white,
                      );
                    default:
                      return const Icon(
                        Icons.location_searching,
                        color: Colors.white,
                      );
                  }
                }),
            onPressed: widget.onPressed),
      ),
    );
  }
}
