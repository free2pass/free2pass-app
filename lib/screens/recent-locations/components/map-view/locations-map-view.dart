import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_location/flutter_map_location.dart';
import 'package:flutter_map_marker_cluster/flutter_map_marker_cluster.dart';
import 'package:free2pass/components/dark-mode-overlay.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/location.dart';
import 'package:free2pass/models/map-locations-filter.dart';
import 'package:free2pass/screens/recent-locations/components/map-view/location-button.dart';
import 'package:free2pass/screens/recent-locations/components/map-view/map-location-marker.dart';
import 'package:free2pass/utils/hive.util.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:latlong2/latlong.dart';
import 'package:url_launcher/url_launcher.dart';

class LocationsMapView extends StatefulWidget {
  final MapController? controller;
  final MapLocationFilter filter;

  const LocationsMapView({Key? key, this.controller, required this.filter})
      : super(key: key);

  @override
  _LocationsMapViewState createState() => _LocationsMapViewState();
}

class _LocationsMapViewState extends State<LocationsMapView> {
  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: data.listenable(),
      builder: (c, Box box, w) {
        final useLocation =
            box.get(kLocationAccessGrantedKey, defaultValue: false);
        return Stack(
          children: [
            DarkModeOverlay(
              child: FlutterMap(
                mapController: widget.controller!,
                options: MapOptions(
                  center: LatLng(51.1634, 10.4477),
                  zoom: 6.0,
                  maxZoom: 20,
                  plugins: <MapPlugin>[
                    if (useLocation) LocationPlugin(),
                    MarkerClusterPlugin(),
                  ],
                  interactiveFlags:
                      InteractiveFlag.all & ~InteractiveFlag.rotate,
                ),
                layers: [
                  TileLayerOptions(
                    urlTemplate:
                        "https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png",
                    subdomains: ['a', 'b'],
                    maxZoom: 20,
                  ),
                  MarkerClusterLayerOptions(
                    maxClusterRadius: 64,
                    size: Size(32, 32),
                    fitBoundsOptions: FitBoundsOptions(
                      padding: EdgeInsets.fromLTRB(64, 64, 64, 192),
                    ),
                    markers: cachedLocations.values
                        // filter the locations according to the provided filters
                        .where(
                          (element) =>
                              element.lng != null &&
                              element.lat != null &&
                              widget.filter.matches(element),
                        )
                        .map((Location e) => MapLocationMarker(location: e))
                        .toList(),
                    polygonOptions: PolygonOptions(
                        borderColor: Theme.of(context).primaryColor,
                        color: Colors.black12,
                        borderStrokeWidth: 3),
                    builder: (context, markers) {
                      return MapLocationMarkerButtonWidget(
                        icon: Center(
                            child: Text(
                          markers.length.toString(),
                          style: TextStyle(color: Colors.white),
                        )),
                        name: S.of(context).locationsInArea +
                            ' ' +
                            markers.length.toString(),
                      );
                    },
                  ),
                  /*MarkerLayerOptions(
                    markers: ,
                  ),*/
                  if (useLocation)
                    LocationOptions(
                      (BuildContext context,
                          ValueNotifier<LocationServiceStatus> status,
                          Function onPressed) {
                        return LocationButton(
                            statusListenable: status,
                            onPressed: onPressed as void Function());
                      },
                      onLocationUpdate: (LatLngData? ld) {},
                      onLocationRequested: (LatLngData? ld) {
                        if (ld == null) {
                          return;
                        }
                        widget.controller?.move(ld.location, 12.0);
                      },
                    ),
                ],
              ),
            ),
            Positioned(
              child: Padding(
                padding: EdgeInsets.all(4),
                child: Text(
                  S.of(context).poweredByOpenstreetmap,
                  style:
                      TextStyle(color: Theme.of(context).colorScheme.secondary),
                ),
              ),
              bottom: MediaQuery.of(context).size.height / 10 + 56,
            ),
            if (!useLocation)
              Positioned(
                child: FloatingActionButton(
                  mini: true,
                  tooltip: S.of(context).locateMe,
                  onPressed: _requestLocationAccess,
                  child: const Icon(
                    Icons.location_disabled,
                    color: Colors.white,
                  ),
                ),
                bottom: MediaQuery.of(context).size.height / 10 + 56 + 16,
                right: 16.0,
              )
          ],
        );
      },
    );
  }

  @override
  void didUpdateWidget(covariant LocationsMapView oldWidget) {
    if (oldWidget.filter != widget.filter) setState(() {});
    super.didUpdateWidget(oldWidget);
  }

  void _requestLocationAccess() {
    showModal(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(S.of(context).locationRequired),
        content: AlignedMarkdown(
          data: S.of(context).displayLocationLongFormatted,
          alignment: CrossAxisAlignment.start,
        ),
        actions: [
          TextButton(
              onPressed: _openPrivacyPolicy,
              child: Text(S.of(context).privacyPolicy)),
          TextButton(
              onPressed: Navigator.of(context).pop,
              child: Text(S.of(context).cancel)),
          TextButton(onPressed: _grantAccess, child: Text(S.of(context).grant)),
        ],
      ),
    );
  }

  Future<void> _grantAccess() async {
    await data.put(kLocationAccessGrantedKey, true);
    Navigator.of(context).pop();
  }

  void _openPrivacyPolicy() => launch(kPrivacyPolicyUrl);
}
