import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/dialogs/appointment-dialog/appointment-dialog.dart';
import 'package:free2pass/enums/location-types.enum.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/models/issuer.dart';
import 'package:free2pass/models/location.dart';
import 'package:free2pass/models/opening-hours.dart';
import 'package:free2pass/utils/contact-issuer.dart';
import 'package:free2pass/utils/geo_url.dart';
import 'package:free2pass_support/free2pass_support.dart';

class MapLocationDialog extends StatefulWidget {
  final Location location;

  const MapLocationDialog({Key? key, required this.location}) : super(key: key);

  @override
  State<MapLocationDialog> createState() => _MapLocationDialogState();
}

class _MapLocationDialogState extends State<MapLocationDialog> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _fetchLocation(),
      builder: (BuildContext context,
          AsyncSnapshot<ResponseModel<Location>> snapshot) {
        late Widget child;
        List<Widget> actions = [];

        if (snapshot.hasData && snapshot.data!.status.success) {
          final location = snapshot.data!.payload!;
          actions.add(
            TextButton(
              onPressed: _directions,
              child: Text(S.of(context).directions),
            ),
          );
          if (location.type == LocationType.ISSUER && location is Issuer) {
            actions.add(
              OutlinedButton(
                onPressed: () => contactIssuer(context, location),
                child: Text(S.of(context).contactIssuer),
              ),
            );
            if (location.ownsAppointmentModule == true)
              actions.add(
                ElevatedButton.icon(
                  onPressed: () => _createAppointment(location),
                  icon: Icon(Icons.calendar_today),
                  label: Text(S.of(context).createAppointment),
                ),
              );
          }
          child = Container(
            width: double.maxFinite,
            child: ListView(
              key: ValueKey(0),
              shrinkWrap: true,
              children: [
                ListTile(
                  leading: Icon(Icons.location_city),
                  title: Text(
                    Location.typeToString(location.type, S.of(context)),
                  ),
                  subtitle: Text(S.of(context).locationType),
                ),
                ListTile(
                  title: Text(
                    location.address!,
                  ),
                  leading: Icon(Icons.map),
                  onTap: _directions,
                ),
                if (location.openingTimes != null &&
                    location.openingTimes!.isNotEmpty)
                  ExpansionTile(
                    leading: Icon(Icons.schedule),
                    title: Text(
                      location.isOpen != null
                          ? location.isOpen!
                              ? S.of(context).currentlyOpened
                              : S.of(context).currentlyClosed
                          : S.of(context).openingHours,
                    ),
                    children: OpeningHours.parse(location.openingTimes!)
                        .keys
                        .map((key) => ListTile(
                              subtitle: Text(OpeningHours.dayOfWeekName(
                                  S.of(context), key)),
                              title: Text(OpeningHours.parse(
                                      location.openingTimes!)[key] ??
                                  S.of(context).closed),
                            ))
                        .toList(),
                  ),
              ],
            ),
          );
        } else {
          if (!snapshot.hasData && !snapshot.hasError)
            child = Container(
                key: ValueKey(1),
                alignment: Alignment.center,
                height: 64,
                width: 64,
                child: CircularProgressIndicator());
          else
            child = Text(
              S.of(context).errorLoadingLocationData,
              key: ValueKey(2),
            );
        }
        return AlertDialog(
          title: Text(widget.location.name),
          content: PageTransitionSwitcher(
            transitionBuilder: (Widget child,
                Animation<double> primaryAnimation,
                Animation<double> secondaryAnimation) {
              return SharedAxisTransition(
                fillColor: Colors.transparent,
                animation: primaryAnimation,
                secondaryAnimation: secondaryAnimation,
                transitionType: SharedAxisTransitionType.vertical,
                child: child,
              );
            },
            child: child,
          ),
          actions: [
            TextButton(
              onPressed: Navigator.of(context).pop,
              child: Text(S.of(context).back),
            ),
          ]..addAll(actions),
        );
      },
    );
  }

  void _directions() {
    GeoUrl.open(context, widget.location.lat!, widget.location.lng!);
  }

  Future<void> _createAppointment(Issuer location) async {
    final DateTime? data = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime.now().add(Duration(days: 14)));
    if (data == null) return;
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AppointmentsDialog(
            issuer: location,
            initialDate: data,
          );
        });
  }

  Future<ResponseModel<Location>> _fetchLocation() {
    switch (widget.location.type) {
      case LocationType.ISSUER:
        return F2PApi.instance.issuers.getData(id: widget.location.id!);
      case LocationType.MERCHANT:
        return F2PApi.instance.merchants.getData(id: widget.location.id!);
      default:
        throw NullThrownError();
    }
  }
}
