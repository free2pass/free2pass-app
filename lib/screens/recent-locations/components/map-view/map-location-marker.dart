import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:free2pass/enums/location-types.enum.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/location.dart';
import 'package:free2pass/screens/recent-locations/components/map-view/map-location-dialog.dart';
import 'package:latlong2/latlong.dart';

class MapLocationMarker extends Marker {
  final Location location;

  MapLocationMarker({required this.location})
      : super(
          point: LatLng(location.lat!, location.lng!),
          builder: (context) => MapLocationMarkerWidget(location: location),
          height: 32,
          width: 32,
        );
}

class MapLocationMarkerWidget extends StatefulWidget {
  final Location location;

  const MapLocationMarkerWidget({Key? key, required this.location})
      : super(key: key);

  @override
  _MapLocationMarkerWidgetState createState() =>
      _MapLocationMarkerWidgetState();
}

class _MapLocationMarkerWidgetState extends State<MapLocationMarkerWidget> {
  @override
  Widget build(BuildContext context) {
    Widget icon;
    switch (widget.location.type) {
      case LocationType.ISSUER:
        icon = Icon(Icons.receipt_long);
        break;
      case LocationType.MERCHANT:
        icon = Icon(Icons.store);
        break;
      default:
        icon = Icon(Icons.location_pin);
        break;
    }
    return MapLocationMarkerButtonWidget(
      isVisited: _isVisited,
      icon: icon,
      name: widget.location.name,
      onTap: _handleActivation,
    );
  }

  void _handleActivation() {
    showModal(
        context: context,
        builder: (c) {
          return MapLocationDialog(location: widget.location);
        });
  }

  bool get _isVisited {
    bool visited = false;
    visitedLocations.values.forEach((element) {
      if (element.isLocation(widget.location)) visited = true;
    });
    return visited;
  }
}

class MapLocationMarkerButtonWidget extends StatelessWidget {
  final bool isVisited;
  final Widget icon;
  final String name;
  final VoidCallback? onTap;
  final bool isActivated;

  const MapLocationMarkerButtonWidget(
      {Key? key,
      this.isVisited = false,
      required this.icon,
      required this.name,
      this.onTap,
      this.isActivated = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Tooltip(
      message: name,
      child: GestureDetector(
        onTap: onTap,
        child: Material(
          color: isActivated
              ? isVisited
                  ? Theme.of(context).colorScheme.secondary
                  : Theme.of(context).primaryColor
              : Colors.deepOrange,
          borderRadius: BorderRadius.circular(24),
          type: MaterialType.button,
          child: OverflowBox(
            child: IconTheme(
              child: icon,
              data: IconThemeData(color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
