import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/components/default-drawer/default-drawer.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/location.dart';
import 'package:free2pass/models/map-locations-filter.dart';
import 'package:free2pass/screens/recent-locations/components/bottom-sheet-handle.dart';
import 'package:free2pass/screens/recent-locations/components/map-view/locations-map-view.dart';
import 'package:free2pass/screens/recent-locations/components/recent-locations-list.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:latlong2/latlong.dart';
import 'package:snapping_sheet/snapping_sheet.dart';

import 'components/filter/filter-button.dart';
import 'components/filter/map-location-filter-tile.dart';

class RecentLocationsScreen extends StatefulWidget {
  static const routeName = '/recentLocations';

  @override
  _ExpiredCertificatesState createState() => _ExpiredCertificatesState();
}

class _ExpiredCertificatesState extends State<RecentLocationsScreen> {
  ScrollController _scrollController = ScrollController();
  MapController _mapController = MapController();

  MapLocationFilter filter = MapLocationFilter();

  @override
  void initState() {
    _refreshLocations();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DefaultDrawer(),
      appBar: DefaultAppBar(
        actions: [ToggleFilterButton()],
      ),
      endDrawer: MapLocationFilterTile(
        filter: filter,
        onChanged: _handleFilters,
      ),
      body: ValueListenableBuilder(
        valueListenable: cachedLocations.listenable(),
        builder: (c, Box<Location> box, w) {
          return SnappingSheet(
            snappingPositions: const [
              SnappingPosition.factor(
                positionFactor: 0.1,
                grabbingContentOffset: GrabbingContentOffset.top,
              ),
              SnappingPosition.factor(positionFactor: 0.375),
              SnappingPosition.factor(
                positionFactor: 1.0,
                grabbingContentOffset: GrabbingContentOffset.bottom,
              ),
            ],
            child: LocationsMapView(
              controller: _mapController,
              filter: filter,
            ),
            grabbing: BottomSheetHandle(),
            grabbingHeight: 64,
            lockOverflowDrag: true,
            sheetBelow: SnappingSheetContent(
                childScrollController: _scrollController,
                draggable: true,
                child: ResponsiveBox(
                  child: RecentLocationsList(
                    controller: _scrollController,
                    onCardActivation: _handleCardActivation,
                  ),
                )),
          );
        },
      ),
    );
  }

  void _handleCardActivation(LatLng location) {
    _mapController.move(location, 15);
  }

  void _refreshLocations() {
    /// parameters:
    /// search: string
    /// skip: int
    /// take: int
    /// types: [issuer,merchant]
    /// bounds: [
    ///   {
    ///     lat: double
    ///     long: double
    ///   },
    ///   {
    ///     lat: double
    ///     long: double
    ///   },
    /// ] list(2)
    F2PApi.instance.locations.getAll();
  }

  void _handleFilters(MapLocationFilter value) {
    setState(() {
      filter = value;
    });
  }
}
