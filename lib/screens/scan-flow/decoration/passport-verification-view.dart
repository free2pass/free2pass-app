import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:free2pass/components/dark-mode-overlay.dart';
import 'package:free2pass/components/photo-not-available.dart';
import 'package:free2pass/dialogs/image-preview/full-screen-user-image-preview.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/screens/home/components/views/decoration/badge-decoration.dart';
import 'package:free2pass/screens/home/components/views/decoration/user-photo-decoration.dart';
import 'package:free2pass/utils/hive.util.dart';
import 'package:free2pass_support/free2pass_support.dart';

class PassportVerificationView extends StatelessWidget {
  final String code;
  final VoidCallback? onNavigate;
  final bool manualNavigation;

  const PassportVerificationView(
      {Key? key,
      required this.code,
      this.onNavigate,
      required this.manualNavigation})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: ResponsiveBox(
        child: LayoutBuilder(
          builder: (context, constraints) => Stack(
            fit: StackFit.expand,
            alignment: Alignment.center,
            children: [
              Transform.scale(
                scale: constraints.maxHeight < constraints.maxWidth ? 1.5 : 2,
                child: DarkModeOverlay(
                  child: SvgPicture.asset(
                    kPendingCertificateBackground,
                    excludeFromSemantics: true,
                    width: constraints.maxWidth,
                  ),
                ),
              ),
              Positioned(
                child: Transform.scale(
                  child: userImages.containsKey(data.get(kCurrentUserKey))
                      ? OpenContainer(
                          closedBuilder: (c, f) => UserPhotoDecoration(),
                          closedElevation: 0,
                          closedColor: Colors.transparent,
                          openColor: Colors.transparent,
                          openElevation: 0,
                          openBuilder: (c, f) => FullScreenUserImagePreview(),
                        )
                      : PhotoNotAvailableNotice(),
                  scale: 2,
                ),
                right: constraints.maxHeight < constraints.maxWidth
                    ? constraints.maxWidth * 2 / 3
                    : null,
                top: 128,
              ),
              Positioned(
                top: constraints.maxHeight < constraints.maxWidth
                    ? null
                    : 32 + 128 + constraints.maxWidth / 2,
                left: constraints.maxHeight < constraints.maxWidth
                    ? constraints.maxWidth / 3 + 128
                    : null,
                bottom: constraints.maxHeight < constraints.maxWidth ? null : 0,
                child: Transform.scale(
                  scale: 1.25,
                  alignment: constraints.maxHeight < constraints.maxWidth
                      ? Alignment.centerRight
                      : Alignment.center,
                  child: Center(
                    child: Container(
                      //color: Colors.pink,
                      width: constraints.maxWidth * 2 / 3,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          ListTile(
                            leading: Icon(
                              Icons.info,
                              color: Colors.white,
                            ),
                            title: Text(
                              S.of(context).showDeviceTestStation,
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                          Text(
                            code,
                            style:
                                Theme.of(context).textTheme.headline2!.copyWith(
                              color: Colors.white,
                              shadows: [
                                Shadow(color: blue, blurRadius: 12),
                              ],
                            ),
                          ),
                          Container(height: 92), // fixes scrolling
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              if (manualNavigation)
                Positioned(
                  bottom: 0,
                  right: 0,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 32, horizontal: 24),
                    child: ElevatedButton.icon(
                      icon: Icon(Icons.navigate_next),
                      onPressed: onNavigate,
                      label: AlignedMarkdown(
                        data: S.of(context).issuerVerifiedFormatted,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
