import 'dart:async';
import 'dart:convert';

import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/api/websocket.api.dart';
import 'package:free2pass/components/invalid-qr-code-view.dart';
import 'package:free2pass/components/requires-network-page.dart';
import 'package:free2pass/enums/vaccination-state.enum.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/scan-route-data.dart';
import 'package:free2pass/models/update-data.dart';
import 'package:free2pass/models/web-socket-controller.dart';
import 'package:free2pass/push/push-adapter.dart';
import 'package:free2pass/screens/home/home.dart';
import 'package:free2pass/screens/scan-flow/decoration/passport-verification-view.dart';
import 'package:free2pass/screens/scan-flow/decoration/request-vaccination-state-view.dart';
import 'package:free2pass/screens/scan-flow/decoration/scan-result-view.dart';
import 'package:free2pass/utils/app_config.dart';
import 'package:free2pass/utils/encode-personal-data.dart';
import 'package:free2pass_support/free2pass_support.dart';

/// allows users to scan a new covid 19 test
class ScanCertificateFlow extends StatefulWidget {
  final ScanRouteData data;
  final ValueChanged<ScanRouteData> changeToCheckIn;

  const ScanCertificateFlow(
      {Key? key,
      this.data = const ScanRouteData(initialScreen: ScanType.CERTIFICATE),
      required this.changeToCheckIn})
      : super(key: key);

  @override
  _ScanCertificateFlowState createState() => _ScanCertificateFlowState();
}

const kNewCertificatePendingBackground =
    kScalableAssetBase + 'grafik-info-test-ausstehend.svg';

class _ScanCertificateFlowState extends State<ScanCertificateFlow>
    with AfterLayoutMixin {
  AxisTransitionController _axisController = AxisTransitionController();

  String? _verificationCode;

  VaccinationState? _vaccinationState;

  late CryptoQrData _currentCode;

  /// a web socket is used to perform live communication
  late WebSocketController _socketController;

  /// is set to [true] in case the web socket accidentally disconnects
  bool _useManualProcess = false;

  /// used to cancel web socket later
  final StreamController<bool> _disconnectionStream = StreamController<bool>();
  late StreamSubscription<bool> _disconnectionSubscription;

  /// whether the user wants to subscribe to push notifications
  bool? _receivePush;

  @override
  void initState() {
    // we put the disconnection event in a stream to properly dispose it
    _createDisconnectionStream();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RequiresNetworkPage(
        child: WillPopScope(
          onWillPop: _checkAbort,
          child: AxisTransitionView(
            controller: _axisController,
            children: [
              // users should scan a QR code first
              widget.data.deeplink == null
                  ? QrScannerWidget(
                      onData: _showVaccinationStateForm,
                      onAbort: _abort,
                      pattern: CryptoQrData.regex,
                    )
                  : Container(),
              // they are asked whether they are vaccinated for some statistics
              RequestVaccinationStateView(
                onSelect: _vaccinationStateSelected,
              ),
              // start crypto and web socket
              Center(
                child: CircularProgressIndicator(),
              ),
              // in case any error occurs
              InvalidQrCodeView(onRetry: _retryScan),
              // displays the user image and a verification PIN
              PassportVerificationView(
                code: _verificationCode ?? '',
                onNavigate: _showPendingScreen,
                manualNavigation: _useManualProcess,
              ),
              // a page telling the user their test result is pending. Only used
              // in case the web socket communication is not properly working
              ScanResultView(
                children: [
                  AlignedMarkdown(
                    alignment: CrossAxisAlignment.end,
                    data: '# ' +
                        S.of(context).pendingTestResult +
                        '\n' +
                        S.of(context).pendingTestResultLong,
                    color: gray,
                  ),
                  ButtonBar(
                    children: [
                      if (_useManualProcess)
                        TextButton(
                          onPressed: _showDataScreen,
                          child: Text(S.of(context).back),
                        ),
                      ElevatedButton(
                        onPressed: _returnHomeWithCorrectPage,
                        child: Text(S.of(context).finish),
                      ),
                    ],
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    // avoid [setState] on disposed States
    _disconnectionSubscription.cancel();
    super.dispose();
  }

  void _showVaccinationStateForm(String data) {
    _currentCode = CryptoQrData.fromRaw(data);
    _axisController.current = 1;
  }

  void _verifyQrCode() async {
    _axisController.current = 2;

    try {
      final verificationCode = await _sendData(code: _currentCode);

      if (verificationCode == null)
        throw NullThrownError(); // TODO: custom errors
      setState(() {
        _verificationCode = verificationCode;
      });
      _showDataScreen();
    } catch (e) {
      // if scanned a qr (not redirected by deeplink), trying to check in instead
      if (widget.data.deeplink == null) {
        widget.changeToCheckIn(widget.data.copyWith(
          deeplink: _currentCode.toString(),
          screen: ScanType.CHECKIN,
        ));
      } else {
        _axisController.current = 3;
      }
    }
  }

  void _retryScan() {
    _axisController.current = 0;
  }

  void _showPendingScreen() {
    _axisController.current = 5;
  }

  /// on pages where a pop in Navigator might invalidate test results, showing
  /// a confirmation dialog
  Future<bool> _checkAbort() async {
    if ([1, 2, 4, 5].contains(_axisController.current)) {
      return await showDialog<bool>(
            context: context,
            builder: (context) => AlertDialog(
              title: Text(S.of(context).abortTestLong),
              content: Text(S.of(context).abortTestHelper),
              actions: [
                TextButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: Text(S.of(context).abortTest),
                ),
                TextButton(
                  onPressed: () => Navigator.of(context).pop(false),
                  child: Text(S.of(context).continueTest),
                ),
              ],
            ),
          ) ??
          false;
    } else
      return true;
  }

  void _abort() {
    if (Navigator.of(context).canPop())
      Navigator.of(context).pop();
    else
      Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
  }

  Future<String?> _sendData({required CryptoQrData code}) async {
    final certificatePublicKeyResponse =
        await F2PApi.instance.certificates.getPublicKey(
      id: code.id,
    );

    if (debug) print(certificatePublicKeyResponse.payload);

    if (!certificatePublicKeyResponse.status.success) return null;

    final String userData = getCurrentUser().encodeForApi(
      vaccinationState: _vaccinationState,
      addRandomEntropy: true,
    );

    final encryptedUserData = await Crypto.rsaEncryptData(
      base64Key: certificatePublicKeyResponse.payload!,
      data: userData,
    );

    Map<String, Map<String, dynamic>?> aesKeys = {};
    if (localAesKeys.isNotEmpty) {
      localAesKeys.keys.forEach((locationId) {
        aesKeys[locationId.toString()] = localAesKeys.get(locationId);
      });
    }

    final cipher = Crypto.createRandomString(32);

    final encryptedAesKeysList = await Crypto.aesEncryptData(
      cipher: cipher,
      data: jsonEncode(aesKeys),
    );
    final encryptedAesCipher = await Crypto.rsaEncryptData(
      data:
          cipher, // the aes cipher es being encrypted with the session rsa key
      base64Key: code.key,
    );

    final verificationCode = Crypto.createRandomCode();

    final signature = getCurrentUser().hash!;

    final userDataResponse =
        await F2PApi.instance.certificates.setEncryptedData(
      id: code.id,
      encryptedUserData: encryptedUserData,
      verificationCode: verificationCode,
      locationCodes: encryptedAesKeysList,
      aesCipher: encryptedAesCipher,
      signature: signature,
    );

    if (debug) print(userDataResponse.payload);

    if (!userDataResponse.status.success) return null;

    // starting WebSocket to automate navigation process
    final server = AppConfig.of(context)!.source == Stores.GitLabNightly
        ? 'https://beta-push.free2pass.de'
        : 'https://push.free2pass.de';

    _socketController = await WebSocketClient.certificateProcess(
      server,
      userDataResponse.payload!,
      onPinVerified: _returnHomeWithCorrectPage,
      onDisconnect: () => _disconnectionStream.add(true),
      onError: _socketDisconnected,
      //notificationData: notificationData,
      s: S.of(context),
    );

    await _socketController.connected!.timeout(Duration(seconds: 7),
        onTimeout: () => setState(() => _useManualProcess = true));

    await F2PApi.instance.certificates.getStatus(
      id: code.id,
      signature: signature,
    );

    if (_receivePush == true)
      await PushAdapter.instance
          .registerCertificate(certificates.get(code.id)!);

    return verificationCode;
  }

  void _showDataScreen() {
    _axisController.current = 4;
  }

  _vaccinationStateSelected(VaccinationRequestData data) {
    _vaccinationState = data.vaccinationState;
    _receivePush = data.receivePush;
    _verifyQrCode();
  }

  void _socketDisconnected() {
    setState(() => _useManualProcess = true);
  }

  void _createDisconnectionStream() {
    _disconnectionSubscription = _disconnectionStream.stream.listen((event) {
      _socketDisconnected();
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    if (widget.data.deeplink != null) {
      _showVaccinationStateForm(widget.data.deeplink!);
    }
  }

  void _returnHomeWithCorrectPage() {
    if (widget.data.changePage != null)
      widget.data.changePage!(
          certificates.get(_currentCode.id)?.issuer?.isPrivate ?? false
              ? 2
              : 0);
    else
      Navigator.of(context).pop();
  }
}
