import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/components/invalid-qr-code-view.dart';
import 'package:free2pass/components/requires-network-page.dart';
import 'package:free2pass/enums/location-types.enum.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/merchant.dart';
import 'package:free2pass/models/scan-route-data.dart';
import 'package:free2pass/screens/home/home.dart';
import 'package:free2pass/screens/scan-flow/decoration/scan-result-view.dart';
import 'package:free2pass/utils/encode-personal-data.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:geolocator/geolocator.dart';
import 'package:url_launcher/url_launcher.dart';

class CheckInScanFlow extends StatefulWidget {
  final ScanRouteData data;
  final ValueChanged<ScanRouteData> changeToCertificate;

  const CheckInScanFlow(
      {Key? key,
      this.data = const ScanRouteData(initialScreen: ScanType.CHECKIN),
      required this.changeToCertificate})
      : super(key: key);

  @override
  _CheckInScanFlowState createState() => _CheckInScanFlowState();
}

class _CheckInScanFlowState extends State<CheckInScanFlow>
    with AfterLayoutMixin {
  late String _currentResult;

  AxisTransitionController _axisController = AxisTransitionController();

  bool _usesAdvancedEncryption = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RequiresNetworkPage(
        child: AxisTransitionView(
          controller: _axisController,
          children: [
            widget.data.deeplink == null
                ? QrScannerWidget(
                    onData: _verifyQrCode,
                    onAbort: _abortCheckIn,
                    pattern: CryptoQrData.regex,
                  )
                : Container(),
            Center(
              child: CircularProgressIndicator(),
            ),
            InvalidQrCodeView(onRetry: _retryScan),
            ScanResultView(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: AlignedMarkdown(
                    data: '# ' +
                        S.of(context).locationRequired +
                        '\n' +
                        S.of(context).locationRequiredLongFormatted,
                    color: gray,
                    alignment: CrossAxisAlignment.end,
                  ),
                ),
                ElevatedButton(
                    onPressed: _giveLocationAccess,
                    child: Text(S.of(context).grant))
              ],
            ),
            ScanResultView(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: AlignedMarkdown(
                    data: '# ' +
                        S.of(context).invalidLocation +
                        '\n' +
                        S.of(context).invalidLocationLongFormatted,
                    color: gray,
                    alignment: CrossAxisAlignment.end,
                  ),
                ),
                ElevatedButton(
                    onPressed: _abortCheckIn,
                    child: Text(S.of(context).returnHome))
              ],
            ),
            ScanResultView(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: AlignedMarkdown(
                    data: '# ' +
                        S.of(context).checkInSuccessful +
                        '\n' +
                        '' + // TODO: placeholder for check-in data
                        '\n' +
                        S.of(context).checkinSuccessfulLong,
                    color: gray,
                    alignment: CrossAxisAlignment.end,
                  ),
                ),
                ElevatedButton(
                  onPressed: _abortCheckIn,
                  child: Text(S.of(context).returnHome),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      _usesAdvancedEncryption == true
                          ? S.of(context).advancedEncryption
                          : S.of(context).standardEncryption,
                      textAlign: TextAlign.end,
                    ),
                    IconButton(
                      color: blue,
                      iconSize: 18,
                      onPressed: _showEncryptionInformation,
                      icon: Icon(Icons.help),
                      tooltip: S.of(context).learnMore,
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future<bool> _verifyQrCode(String data) async {
    _currentResult = data;
    _axisController.current = 1;

    final CryptoQrData code = CryptoQrData.fromRaw(data);

    try {
      // handle private occasions
      if (code.id.startsWith('PRIV+')) {
        await _savePrivateOccasionData(code: code);
      } else {
        final checkInDataResponse =
            await F2PApi.instance.merchants.getCheckInData(id: code.id);

        final locationStatus = await _checkLocation(
          latitude: checkInDataResponse.payload!.lat,
          longitude: checkInDataResponse.payload!.lng,
          accuracy: checkInDataResponse.payload!.accuracy,
        );

        if (!locationStatus) return false;

        await _saveMerchantCheckInData(code: code);

        await _setEncryptedPersonalData(
            code: code,
            locationPublicKey: checkInDataResponse.payload!.publicKey);
      }
      _axisController.current = 5;
      return true;
    } catch (e) {
      // if scanned a qr (not redirected by deeplink), trying to check in instead
      if (widget.data.deeplink == null) {
        widget.changeToCertificate(widget.data.copyWith(
          deeplink: data,
          screen: ScanType.CERTIFICATE,
        ));
      } else {
        _axisController.current = 2;
      }
      return false;
    }
  }

  void _retryScan() {
    _axisController.current = 0;
  }

  Future _giveLocationAccess() async {
    _axisController.current = 1;
    LocationPermission permission = await Geolocator.requestPermission();

    if (permission == LocationPermission.denied) {
      _axisController.current = 3;
    } else if (permission == LocationPermission.deniedForever) {
      Geolocator.openAppSettings();
      _axisController.current = 3;
    } else {
      final enabled = await Geolocator.isLocationServiceEnabled();
      if (!enabled) {
        Geolocator.openLocationSettings();
        _axisController.current = 3;
      } else
        _verifyQrCode(_currentResult);
    }
  }

  void _abortCheckIn() {
    if (Navigator.of(context).canPop())
      Navigator.of(context).pop();
    else
      Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
  }

  Future<bool> _checkLocation(
      {double? latitude, double? longitude, int? accuracy}) async {
    final permission = await Geolocator.checkPermission();
    if ([LocationPermission.denied, LocationPermission.denied]
        .contains(permission)) {
      _axisController.current = 3;
      return false;
    }
    final enabled = await Geolocator.isLocationServiceEnabled();
    if (!enabled) {
      _axisController.current = 3;
      return false;
    }

    /// this *unnecessary* cast allows us to set a timeout
    // ignore: unnecessary_cast
    Position? position = await (Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.medium) as Future<Position?>)
        .timeout(Duration(seconds: 15), onTimeout: (() => null));
    if (position == null) {
      _axisController.current = 3;
      return false;
    }

    final distance = Geolocator.distanceBetween(
        position.latitude, position.longitude, latitude!, longitude!);

    if (distance >
        (accuracy! + (position.accuracy > 500 ? 500 : position.accuracy))) {
      _axisController.current = 4;
      return false;
    }
    return true;
  }

  Future _saveMerchantCheckInData({required CryptoQrData code}) async {
    final data = {
      "in": (DateTime.now().millisecondsSinceEpoch / 1000).round(),
      "out": null, // TODO: process check-out time
      "cipher": code.key,
    };
    await localAesKeys.put(code.id, data);
    await F2PApi.instance.merchants.getData(
      id: int.parse(code.id),
      addToVisitedLocations: true,
    );
  }

  Future _setEncryptedPersonalData(
      {required CryptoQrData code, required String locationPublicKey}) async {
    final String userData = getCurrentUser().encodeForApi(
      addRandomEntropy: true,
    );

    final encryptedUserData = await Crypto.rsaEncryptData(
      base64Key: locationPublicKey,
      data: userData,
    );

    /// {
    ///   "data": "string" // the encrypted user data following our default encryption implementation
    /// }

    /// adding the data to the **encrypted** merchant id on the server to
    /// minimize metadata
    final response = await F2PApi.instance.merchants.setEncryptedUserData(
      encryptedId: await Crypto.aesEncryptData(cipher: code.key, data: code.id),
      encryptedUserData: encryptedUserData,
    );

    if (response.payload['usesAdvancedEncryption']) {
      setState(() {
        _usesAdvancedEncryption = true;
      });
    }

    return response.status.success;
  }

  Future<void> _savePrivateOccasionData({required CryptoQrData code}) async {
    // parsing the private occasion code following this pattern
    // `PRIV+$id/${Crypto.stringToBase64.encode(name)}/$cipher/$end;$publicKey`
    final idData = code.id.replaceFirst('PRIV+', '').split('/');
    final id = idData[0];
    final name = idData[1];
    final cipher = idData[2];
    final end = int.parse(idData[3]);
    final publicKey = code.key;
    final response = await F2PApi.instance.occasions.setEncryptedUserData(
      id: id,
      encryptedUserData: await Crypto.rsaEncryptData(
          base64Key: publicKey,
          data: getCurrentUser().encodeForApi(addRandomEntropy: true)),
    );
    if (!response.status.success) throw response.status;
    if (response.payload['usesAdvancedEncryption']) {
      setState(() {
        _usesAdvancedEncryption = true;
      });
    }

    final now = (DateTime.now().millisecondsSinceEpoch / 1000).round();
    final data = {
      "in": now,
      "out": end,
      "cipher": cipher,
    };
    await localAesKeys.put('PRIV+' + code.id, data);

    visitedLocations.add(Merchant(
      type: LocationType.OCCASION,
      name: name,
      checkIn: now,
      checkOut: end,
    ));
  }

  void _showEncryptionInformation() {
    showModal(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(S.of(context).encryption),
        content: AlignedMarkdown(
          data: _usesAdvancedEncryption == true
              ? S.of(context).advancedEncryptionLongFormatted
              : S.of(context).standardEncryptionLongFormatted,
          alignment: CrossAxisAlignment.start,
        ),
        actions: [
          TextButton(
              onPressed: _openSecurityInfo,
              child: Text(S.of(context).learnMore)),
          TextButton(
              onPressed: _returnHomeWithPage, child: Text(S.of(context).okay))
        ],
      ),
    );
  }

  void _openSecurityInfo() {
    launch('https://free2pass.de/verschluesselung');
  }

  @override
  void afterFirstLayout(BuildContext context) {
    if (widget.data.deeplink != null) {
      _verifyQrCode(widget.data.deeplink!);
    }
  }

  void _returnHomeWithPage() {
    if (widget.data.changePage != null) widget.data.changePage!(1);
    Navigator.of(context).pop();
  }
}
