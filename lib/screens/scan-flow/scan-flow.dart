import 'package:flutter/material.dart';
import 'package:free2pass/models/scan-route-data.dart';
import 'package:free2pass/screens/scan-flow/flows/certificate.dart';
import 'package:free2pass/screens/scan-flow/flows/check-in.dart';

class ScanFlowScreen extends StatefulWidget {
  static const routeName = '/scan';
  final ScanRouteData data;

  const ScanFlowScreen({Key? key, required this.data}) : super(key: key);

  @override
  State<ScanFlowScreen> createState() => _ScanFlowScreenState();
}

class _ScanFlowScreenState extends State<ScanFlowScreen> {
  late ScanRouteData currentRouteData;

  @override
  void initState() {
    currentRouteData = widget.data;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    switch (currentRouteData.initialScreen) {
      case ScanType.CERTIFICATE:
        return ScanCertificateFlow(
          data: currentRouteData,
          changeToCheckIn: _setRouteData,
        );
      case ScanType.CHECKIN:
        return CheckInScanFlow(
          data: currentRouteData,
          changeToCertificate: _setRouteData,
        );
    }
  }

  void _setRouteData(ScanRouteData value) {
    setState(() {
      currentRouteData = value;
    });
  }
}
