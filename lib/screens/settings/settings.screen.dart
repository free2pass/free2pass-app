import 'package:animations/animations.dart';
import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:free2pass/components/default-drawer/default-drawer.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/screens/multi-user/components/edit-user-dialog.dart';
import 'package:free2pass/screens/splash/splash.dart';
import 'package:free2pass/utils/app_config.dart';
import 'package:free2pass/utils/encode-personal-data.dart';
import 'package:free2pass/utils/hive.util.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingsScreen extends StatefulWidget {
  static const routeName = '/settings';

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(),
      drawer: DefaultDrawer(),
      body: ResponsiveBox(
        child: ListView(
          padding: EdgeInsets.all(8),
          children: [
            Container(
              padding: EdgeInsets.only(top: 32),
              child: SvgPicture.asset(
                kLogoImage,
                width: 128,
              ),
            ),
            ListTile(
              title: AlignedMarkdown(data: '# free2**pass**'),
            ),
            ExpansionTileCard(
              leading: Icon(Icons.info),
              title: Text(S.of(context).information),
              children: [
                ButtonBar(
                  alignment: MainAxisAlignment.center,
                  children: [
                    OutlinedButton(
                      onPressed: _licenses,
                      child: Text(S.of(context).openSourceLicenses),
                    ),
                    OutlinedButton(
                      onPressed: _website,
                      child: Text(S.of(context).website),
                    ),
                    TextButton(
                      onPressed: _openPrivacyPolicy,
                      child: Text(S.of(context).privacyPolicy),
                    ),
                    TextButton(
                      onPressed: _openUsagePolicy,
                      child: Text(S.of(context).termsOfService),
                    ),
                    TextButton(
                      onPressed: _openLegalNotice,
                      child: Text(S.of(context).legalNotice),
                    ),
                    FutureBuilder(
                        future: PackageInfo.fromPlatform(),
                        builder:
                            (context, AsyncSnapshot<PackageInfo> snapshot) {
                          if (snapshot.hasData)
                            return TextButton(
                              onPressed: null,
                              child: Text(S.of(context).version +
                                  ' ' +
                                  snapshot.data!.version +
                                  '+' +
                                  snapshot.data!.buildNumber +
                                  '-' +
                                  AppConfig.of(context)!.source.toString()),
                            );
                          return Container();
                        })
                  ],
                ),
              ],
            ),
            Divider(),
            ExpansionTileCard(
              title: Text(S.of(context).language),
              leading: Icon(Icons.translate),
              children: [
                RadioListTile(
                  value: 'de',
                  groupValue: settings.get(kLocaleKey,
                      defaultValue: _getDefaultLanguageValue()),
                  onChanged: _setLocale,
                  title: Text('Deutsch / German'),
                ),
                RadioListTile(
                  value: 'en',
                  groupValue: settings.get(kLocaleKey,
                      defaultValue: _getDefaultLanguageValue()),
                  onChanged: _setLocale,
                  title: Text('English / Englisch'),
                ),
              ],
            ),
            ExpansionTileCard(
              title: Text(S.of(context).theme),
              leading: Icon(Icons.lightbulb),
              children: [
                RadioListTile(
                  value: 1,
                  groupValue:
                      settings.get(kThemeKey, defaultValue: kDefaultTheme),
                  onChanged: _setTheme,
                  title: Text(S.of(context).light),
                ),
                RadioListTile(
                  value: 2,
                  groupValue: settings.get(kThemeKey),
                  onChanged: _setTheme,
                  title: Text(S.of(context).darkBeta),
                ),
              ],
            ),
            if (AppConfig.of(context)!.pushAdapter.isValid) Divider(),
            if (AppConfig.of(context)!.pushAdapter.isValid)
              ListTile(
                title: Text(S.of(context).pushNotifications),
                subtitle:
                    Text(S.of(context).pushNotificationsEnabledTapToDisable),
                onTap: _disablePush,
                leading: Icon(Icons.notifications),
              ),
            Divider(),
            ExpansionTileCard(
              title: Text(S.of(context).advancedDangerous),
              children: [
                ListTile(
                  leading: Icon(Icons.person),
                  title: Text(S.of(context).changePersonalData),
                  onTap: _changePersonalData,
                ),
                ListTile(
                  leading: Icon(Icons.settings_backup_restore),
                  title: AlignedMarkdown(
                      alignment: CrossAxisAlignment.start,
                      data: S.of(context).resetFree2passFormatted),
                  onTap: _resetApp,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  void _openPrivacyPolicy() => launch(kPrivacyPolicyUrl);

  void _openUsagePolicy() => launch(kTermsOfServiceUrl);

  void _openLegalNotice() => launch(kLegalNoticeUrl);

  void _setTheme(value) {
    settings.put(kThemeKey, value);
  }

  void _setLocale(value) {
    settings.put(kLocaleKey, value);
  }

  void _licenses() {
    showLicensePage(context: context);
  }

  void _website() {
    launch('https://free2pass.de');
  }

  void _changePersonalData() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => EditUserDialog(
              user: getDefaultUserData(),
              isSecondaryUser: false,
            ),
        fullscreenDialog: true));
  }

  void _resetApp() {
    showModal(
      context: context,
      builder: (c) => AlertDialog(
        title: Text(S.of(context).areYouSure),
        content: AlignedMarkdown(
          alignment: CrossAxisAlignment.start,
          data: S.of(context).resetAppLongFormatted,
        ),
        actions: [
          TextButton(
              onPressed: Navigator.of(context).pop,
              child: Text(S.of(context).keep)),
          TextButton(
              onPressed: _continueFullReset, child: Text(S.of(context).reset)),
        ],
      ),
    );
  }

  Future<void> _continueFullReset() async {
    Navigator.of(context).pop();
    await data.clear();
    await certificates.clear();
    await localAesKeys.clear();
    await settings.clear();
    await visitedLocations.clear();
    await cachedLocations.clear();
    await users.clear();

    Navigator.pushNamedAndRemoveUntil(
        context, SplashScreen.routeName, (r) => false);
  }

  String _getDefaultLanguageValue() {
    final systemLanguage = Localizations.localeOf(context).languageCode;
    if (S.delegate.supportedLocales
        .map((e) => e.languageCode)
        .contains(systemLanguage)) return systemLanguage;
    return 'en';
  }

  void _disablePush() async {
    final success = await AppConfig.of(context)!.pushAdapter.unregister();
    setState(() {});
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(success
            ? 'Push notifications disabled. Re-enable by scanning a test certificate.'
            : 'Error disabling push notifications.'),
      ),
    );
  }
}
