import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass_support/free2pass_support.dart';

typedef AxisNavigationCallback({required bool forward});
typedef bool ShouldNavigateAxisCallback();

class SetupCard extends StatefulWidget {
  final Widget child;
  final AxisNavigationCallback? onPageChange;
  final bool isFirst;
  final bool isLast;
  final bool canGoForward;
  final VoidCallback? onFinish;
  final ShouldNavigateAxisCallback? canNavigate;

  const SetupCard(
      {Key? key,
      required this.child,
      this.onPageChange,
      this.isFirst = false,
      this.isLast = false,
      this.onFinish,
      this.canGoForward = true,
      this.canNavigate})
      : super(key: key);

  @override
  _SetupCardState createState() => _SetupCardState();
}

class _SetupCardState extends State<SetupCard> {
  @override
  Widget build(BuildContext context) {
    return ResponsiveBox(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          widget.child,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              !widget.isFirst
                  ? OutlinedButton(
                      onPressed: () => widget.onPageChange!(forward: false),
                      child: Text(S.of(context).back))
                  : Container(),
              !widget.isLast
                  ? OutlinedButton(
                      onPressed: widget.canGoForward
                          ? () => (widget.canNavigate != null &&
                                  !widget.canNavigate!())
                              ? null
                              : widget.onPageChange!(forward: true)
                          : null,
                      child: Text(S.of(context).next))
                  : OutlinedButton(
                      onPressed: widget.canGoForward ? widget.onFinish : null,
                      child: Text(S.of(context).finish))
            ],
          )
        ],
      ),
    );
  }

  @override
  void didUpdateWidget(covariant SetupCard oldWidget) {
    setState(() {});
    super.didUpdateWidget(oldWidget);
  }
}
