import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/user-data.dart';
import 'package:free2pass/screens/multi-user/components/edit-user-dialog.dart';
import 'package:free2pass/screens/setup/components/setup-card/setup-card.component.dart';
import 'package:free2pass/utils/hive.util.dart';
import 'package:free2pass_support/free2pass_support.dart';

class DataCard extends StatefulWidget {
  @required
  final AxisNavigationCallback onPageChange;

  const DataCard({Key? key, required this.onPageChange}) : super(key: key);

  @override
  _DataCardState createState() => _DataCardState();
}

class _DataCardState extends State<DataCard> {
  GlobalKey<UserDataFormState> _formKey = GlobalKey();

  bool _submissionInProcess = false;

  @override
  Widget build(BuildContext context) {
    return SetupCard(
      key: ValueKey(DataCard),
      onPageChange: widget.onPageChange,
      isFirst: true,
      child: !_canSkipPage()
          ? UserDataForm(
              key: _formKey,
              onSaved: _saveName,
              isInitialProcess: true,
            )
          : AlignedMarkdown(data: S.of(context).nameAlreadySaved),
      canNavigate: _canNavigate,
      canGoForward: !_submissionInProcess,
    );
  }

  bool _canSkipPage() {
    return (data.get(kBirthdayKey) != null) &&
        (data.get(kFullNameKey) != null) &&
        (data.get(kStreetKey) != null) &&
        (data.get(kTownKey) != null) &&
        (data.get(kZipKey) != null) &&
        (data.get(kBirthdayKey) != null);
  }

  Future _saveName(UserData user) async {
    await data.put(kFullNameKey, user.name);
    if (user.email != null) await data.put(kEmailKey, user.email);
    await data.put(kStreetKey, user.street);
    await data.put(kBirthdayKey, user.birthday);
    await data.put(kTownKey, user.town);
    await data.put(kZipKey, user.zip);
    widget.onPageChange(forward: true);
  }

  bool _canNavigate() {
    if (_submissionInProcess) return false;
    setState(() => _submissionInProcess = true);

    if (_canSkipPage()) return true;

    if (_formKey.currentState != null) {
      final result = _formKey.currentState!.saveUser();
      if (result is Future) {
        final controller = ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(S.of(context).encryptingUserData),
          duration: Duration(
              days: 1), // okay, one day should be enough to compute a B-Crypt
        ));
        result.then((value) {
          setState(() => _submissionInProcess = false);
          controller.close();
        });
      } else
        setState(() => _submissionInProcess = false);
    }

    return false;
  }
}
