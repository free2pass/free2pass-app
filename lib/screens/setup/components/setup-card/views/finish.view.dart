import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/screens/setup/components/setup-card/setup-card.component.dart';
import 'package:free2pass_support/free2pass_support.dart';

class FinishCard extends StatelessWidget {
  @required
  final AxisNavigationCallback onPageChange;
  @required
  final VoidCallback? onFinish;

  const FinishCard({Key? key, this.onFinish, required this.onPageChange})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SetupCard(
      key: ValueKey(FinishCard),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Icon(
              Icons.check,
              color: green,
              size: 64,
            ),
          ),
          AlignedMarkdown(data: S.of(context).readyToGo),
        ],
      ),
      isFirst: true,
      isLast: true,
      onPageChange: onPageChange,
      onFinish: onFinish,
    );
  }
}
