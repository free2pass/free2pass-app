import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/screens/setup/components/setup-card/intro-card-tile.dart';
import 'package:free2pass/screens/setup/components/setup-card/setup-card.component.dart';
import 'package:free2pass/utils/constants.style.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:url_launcher/url_launcher.dart';

class IntroductionCard extends StatefulWidget {
  @required
  final AxisNavigationCallback onPageChange;

  const IntroductionCard({Key? key, required this.onPageChange})
      : super(key: key);

  @override
  _IntroductionCardState createState() => _IntroductionCardState();
}

class _IntroductionCardState extends State<IntroductionCard> {
  bool _privacyPolicyAccepted = false;

  bool _usagePolicyAccepted = false;

  bool _correctDataAccepted = false;

  @override
  Widget build(BuildContext context) {
    return SetupCard(
      key: ValueKey(IntroductionCard),
      onPageChange: widget.onPageChange,
      isFirst: true,
      child: Column(
        //mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            child: SvgPicture.asset(kLogoCirclePath),
            width: MediaQuery.of(context).size.width / 3,
            height: MediaQuery.of(context).size.height / 3,
            constraints: BoxConstraints(
              maxWidth: 256,
              maxHeight: 256,
            ),
          ),
          AlignedMarkdown(
            data: '# ' +
                S.of(context).welcomeToFree2pass +
                '\n' +
                /*PlaceholderGenerator.cupcakeIpsum() +
                '\n' +*/
                S.of(context).keepAvailable,
            alignment: CrossAxisAlignment.start,
          ),
          ListView(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            primary: false,
            children: [
              ListTile(
                leading: Icon(Icons.phone),
                title: Text(S.of(context).yourPhoneNumber),
              ),
              ListTile(
                leading: Icon(Icons.portrait),
                title: Text(S.of(context).aPhotoDepictingYourFaceYourCamera),
              ),
              IntroCardTile(
                child: CheckboxListTile(
                  value: _correctDataAccepted,
                  onChanged: _toggleCorrectData,
                  title: MarkdownBody(
                    data: S.of(context).acceptDataCorrectFormatted,
                  ),
                ),
              ),
              IntroCardTile(
                child: CheckboxListTile(
                  secondary: IconButton(
                    tooltip: S.of(context).openPrivacyPolicy,
                    icon: Icon(Icons.open_in_new),
                    onPressed: _openPrivacyPolicy,
                  ),
                  title: MarkdownBody(data: S.of(context).acceptPrivacyPolicy),
                  value: _privacyPolicyAccepted,
                  onChanged: _togglePrivacyPolicy,
                ),
              ),
              IntroCardTile(
                child: CheckboxListTile(
                  secondary: IconButton(
                    tooltip: S.of(context).openUsagePolicy,
                    icon: Icon(Icons.open_in_new),
                    onPressed: _openUsagePolicy,
                  ),
                  title: MarkdownBody(
                      data: S.of(context).acceptTermsOfUseFormatted),
                  value: _usagePolicyAccepted,
                  onChanged: _toggleUsagePolicy,
                ),
              ),
            ],
          ),
          ButtonBar(
            children: [
              TextButton(
                  onPressed: () => launch(kLegalNoticeUrl),
                  child: Text(S.of(context).legalNotice)),
            ],
          )
        ],
      ),
      canGoForward: _canNavigate(),
    );
  }

  void _togglePrivacyPolicy(bool? value) {
    setState(() {
      _privacyPolicyAccepted = value ?? false;
    });
  }

  void _toggleUsagePolicy(bool? value) {
    setState(() {
      _usagePolicyAccepted = value ?? false;
    });
  }

  void _toggleCorrectData(bool? value) {
    setState(() {
      _correctDataAccepted = value ?? false;
    });
  }

  void _openPrivacyPolicy() => launch(kPrivacyPolicyUrl);

  void _openUsagePolicy() => launch(kTermsOfServiceUrl);

  bool _canNavigate() {
    return _correctDataAccepted &&
        _privacyPolicyAccepted &&
        _usagePolicyAccepted;
  }
}
