import 'dart:async';

import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/screens/setup/components/setup-card/setup-card.component.dart';
import 'package:free2pass/utils/hive.util.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:pin_input_text_field/pin_input_text_field.dart';

const _phoneNumberRegex =
    r'^((\+|00)(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1))?\d{1,14}$';

class PhoneCard extends StatefulWidget {
  @required
  final AxisNavigationCallback onPageChange;

  const PhoneCard({Key? key, required this.onPageChange}) : super(key: key);

  @override
  _PhoneCardState createState() => _PhoneCardState();
}

class _PhoneCardState extends State<PhoneCard> {
  final _formKey = GlobalKey<FormState>();
  late TextEditingController _phoneNumberController;
  late TextEditingController _codeController;

  late AxisTransitionController _axisController;

  String _phoneNumber = '';

  bool _allowRequestNewCode = false;

  FocusNode _verificationFocusNode = FocusNode();

  StreamController _backToPhoneStream = StreamController();
  late StreamSubscription _backToPhoneStreamSubscription;

  @override
  void initState() {
    _phoneNumberController = TextEditingController();
    _codeController = TextEditingController();
    _axisController = AxisTransitionController();
    _backToPhoneStreamSubscription = _backToPhoneStream.stream.listen((event) {
      _backToPhoneNumber();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SetupCard(
      key: ValueKey(PhoneCard),
      child: !_canSkipPage()
          ? Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  AlignedMarkdown(data: S.of(context).phoneNumberLongFormatted),
                  Container(
                    height: 16,
                  ),
                  TextFormField(
                    enabled: _phoneNumber.isEmpty,
                    validator: _phoneNumberValidator,
                    controller: _phoneNumberController,
                    keyboardType: TextInputType.phone,
                    autofillHints: _phoneNumber.isEmpty
                        ? [AutofillHints.telephoneNumber]
                        : null,
                    autofocus: true,
                    textInputAction: TextInputAction.done,
                    onFieldSubmitted: _phoneSubmitted,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      prefixIcon: Icon(Icons.phone),
                      labelText: S.of(context).yourPhoneNumber,
                      helperText: S.of(context).phoneNumberInputHelper,
                    ),
                  ),
                  if (_phoneNumber.isNotEmpty)
                    ButtonBar(
                      children: [
                        /*IconButton(
                          icon: Icon(Icons.edit),
                          tooltip: S.of(context).editPhoneNumber,
                          onPressed: _backToPhoneNumber,
                        ),*/
                        TextButton.icon(
                          icon: Icon(Icons.edit),
                          label: Text(S.of(context).editPhoneNumber),
                          onPressed: _backToPhoneNumber,
                        ),
                      ],
                    ),
                  AnimatedContainer(
                    duration: Duration(milliseconds: 150),
                    height: _bottomHeight(),
                    child: AxisTransitionView(
                      controller: _axisController,
                      direction: SharedAxisTransitionType.vertical,
                      children: [
                        Container(
                          key: ValueKey(0),
                        ),
                        CircularProgressIndicator(
                          key: ValueKey(1),
                        ),
                        OverflowBox(
                          // Fixing a bottom overflow during the expansion animation
                          key: ValueKey(2),
                          maxHeight: double.maxFinite,
                          child: Column(
                            children: [
                              Text(S.of(context).enterVerificationCodeLong),
                              Container(
                                height: 16,
                              ),
                              Center(
                                child: SizedBox(
                                  height: 48,
                                  width: 320,
                                  child: Container(
                                    child: PinInputTextField(
                                      autoFocus: true,
                                      controller: _codeController,
                                      focusNode: _verificationFocusNode,
                                      onSubmit: _shouldVerifyCode,
                                      keyboardType: TextInputType.number,
                                      pinLength: 6,
                                      decoration: BoxLooseDecoration(
                                        strokeColorBuilder:
                                            PinListenColorBuilder(
                                          green,
                                          Colors.grey,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              ButtonBar(
                                children: [
                                  TextButton(
                                    onPressed: _allowRequestNewCode
                                        ? _requestVerificationCode
                                        : null,
                                    child: TweenAnimationBuilder(
                                      duration: Duration(minutes: 1),
                                      tween: Tween<double>(begin: 59, end: 0),
                                      onEnd: _setAllowRequestNewCode,
                                      builder: (c, dynamic v, w) => v <= 0
                                          ? Text(S.of(context).requestNewCode)
                                          : Text(S.of(context).requestNewCode +
                                              ' (' +
                                              (v + 1).round().toString() +
                                              ')'),
                                    ),
                                  )
                                ],
                              )
                            ],
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                          ),
                        ),
                        CircularProgressIndicator(
                          key: ValueKey(3),
                        ),
                        Text(
                          S.of(context).verificationSuccessful,
                          key: ValueKey(4),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          : AlignedMarkdown(data: S.of(context).phoneAlreadyVerifiedLong),
      canNavigate: _onNavigateStart,
      onPageChange: widget.onPageChange,
    );
  }

  bool _canSkipPage() {
    return (data.get(kPhoneNumberKey) != null) && _phoneNumber.isEmpty;
  }

  String? _phoneNumberValidator(String? value) {
    if (value == null ||
        !RegExp(_phoneNumberRegex)
            .hasMatch(value.replaceAll(RegExp(r'[\s-]'), ''))) {
      return S.of(context).phoneNumberValidator;
    }
    return null;
  }

  Future<void> _requestVerificationCode() async {
    if (_formKey.currentState!.validate()) {
      _axisController.next();
      setState(() {
        _allowRequestNewCode = false;
      });

      _phoneNumber = _phoneNumberController.text;

      // removing whitespace
      _phoneNumber = _phoneNumber.replaceAll(RegExp(r'[\s-]'), '');

      if (_phoneNumber.startsWith("00")) {
        _phoneNumber = "+" + _phoneNumber.substring(2);
      } else if (_phoneNumber.startsWith("0")) {
        _phoneNumber = "+49" + _phoneNumber.substring(1);
      }

      String? appHash;

      try {
        final response = (await F2PApi.instance.auth
            .verifySMS(phoneNumber: _phoneNumber.trim(), appHash: appHash));

        if (!response.status.success) {
          print(response.status.code);
          // showing a waiting dialog in case there were too many attempts
          if (response.status.code == ErrorCodes.MaxSendAttemptsReached) {
            showDialog(
                context: context,
                builder: (context) {
                  return TweenAnimationBuilder(
                      tween: IntTween(begin: 30, end: 0),
                      duration: Duration(seconds: 30),
                      builder: (context, int value, w) {
                        return WillPopScope(
                          onWillPop: () async => value != 0 ? false : true,
                          child: AlertDialog(
                            title: Text(S.of(context).tooManyAttempts),
                            content: Text(S.of(context).tooManyAttemptsLong),
                            actions: [
                              TextButton(
                                child: Text((value != 0 ? '($value) ' : '') +
                                    S.of(context).okay),
                                onPressed: value != 0
                                    ? null
                                    : Navigator.of(context).pop,
                              )
                            ],
                          ),
                        );
                      });
                },
                barrierDismissible: false);
          }
          throw NullThrownError();
        } // TODO: custom errors
        _axisController.next();

        Future.delayed(Duration(minutes: 10)).then((value) {
          _backToPhoneStream.add(true);
        });
        return;
      } catch (e) {}

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(S.of(context).errorRequestingVerificationCode)));

      setState(() {
        _phoneNumber = '';

        _axisController.current = 0;
      });
    }
  }

  _shouldVerifyCode(String p1) {
    _onNavigateStart();
  }

  void _verifyVerificationCode() async {
    if (_formKey.currentState!.validate()) {
      _axisController.next();

      try {
        if (_codeController.text.length != 6) throw NullThrownError();

        final response = await F2PApi.instance.auth.register.user(
            phoneNumber: _phoneNumber.toString(),
            verifyCode: _codeController.text.toString());

        if (!response.status.success)
          throw NullThrownError(); // TODO: custom errors

        _axisController.next();
        await data.put(kPhoneNumberKey, _phoneNumber.trim());
      } catch (e) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(S
                .of(context)
                .errorWringVerificationCode))); // TODO: separate between mismatch and connection error
        setState(() {
          _codeController.clear();
          _phoneNumber = '';

          _axisController.current = 0;
        });
      }
    }
  }

  bool _onNavigateStart() {
    if (_canSkipPage()) return true;
    switch (_axisController.current) {
      case 0:
        _requestVerificationCode();
        break;
      case 2:
        _verifyVerificationCode();
        break;
      case 4:
        return true;
    }

    return false;
  }

  double _bottomHeight() {
    switch (_axisController.current) {
      case 0:
        return 0;
      case 1:
        return 70;
      case 2:
        return 192;
      case 3:
        return 70;
      case 4:
        return 24;
      default:
        return 0;
    }
  }

  @override
  void dispose() {
    _codeController.dispose();
    _phoneNumberController.dispose();
    _backToPhoneStreamSubscription.cancel();
    super.dispose();
  }

  void _phoneSubmitted(String value) {
    _onNavigateStart();
  }

  void _backToPhoneNumber() {
    setState(() {
      _codeController.clear();
      _phoneNumber = '';

      _axisController.current = 0;
    });
  }

  void _setAllowRequestNewCode() {
    setState(() {
      _allowRequestNewCode = true;
    });
  }
}
