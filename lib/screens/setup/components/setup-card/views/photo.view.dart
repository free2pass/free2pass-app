import 'package:animations/animations.dart';
import 'package:camera_camera/camera_camera.dart';
import 'package:crypto/crypto.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/screens/setup/components/setup-card/setup-card.component.dart';
import 'package:free2pass/utils/encode-personal-data.dart';
import 'package:free2pass/utils/hive.util.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:image_picker/image_picker.dart';

class PhotoCard extends StatefulWidget {
  @required
  final AxisNavigationCallback onPageChange;

  const PhotoCard({Key? key, required this.onPageChange}) : super(key: key);

  @override
  _PhotoCardState createState() => _PhotoCardState();
}

class _PhotoCardState extends State<PhotoCard> {
  GlobalKey<FormState> _formKey = GlobalKey();

  bool _canGoForward = false;

  @override
  void initState() {
    _canSkipPage();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SetupCard(
      key: ValueKey(PhotoCard),
      onPageChange: ({required bool forward}) async {
        try {
          if (data.get(kPhoneNumberKey) != null) {
            //migrate
            await migrateTemporaryUserDataToDatabase();
          }

          widget.onPageChange(forward: true);
        } catch (e) {
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text(S.of(context).errorSavingUserData)));
        }
      },
      isFirst: true,
      child: !_canGoForward
          ? Form(
              key: _formKey,
              child: Column(
                children: [
                  AlignedMarkdown(data: S.of(context).selectPhotoLongFormatted),
                  Container(
                    height: 16,
                  ),
                  PhotoSelector(
                    onPhoto: _onPhoto,
                    onPassport: _onPassport,
                  ),
                ],
              ),
            )
          : AlignedMarkdown(data: S.of(context).photoAlreadySaved),
      canGoForward: _canGoForward,
    );
  }

  Future _canSkipPage() async {
    if (users.isEmpty) return;
    // checking whether image has been manipulated
    final image = await FilePickerCross.fromInternalPath(
        path: data.get(kPhotoLocationKey));
    if (fileShaSum(image) == (data.get(kPhotoChecksumKey))) {
      setState(() {
        _canGoForward = true;
      });
    }
  }

  _onPhoto(FilePickerCross photo, String path) async {
    await data.put(kPhotoLocationKey, path);
    final checksum = fileShaSum(photo);
    await data.put(kPhotoChecksumKey, checksum);
    // finally saving user in database

    try {
      await migrateTemporaryUserDataToDatabase();
      userImages[data.get(kCurrentUserKey)] = photo.toUint8List();
      _canGoForward = true;
      widget.onPageChange(forward: true);
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(S.of(context).errorSavingUserData)));
    }
  }

  Future<void> _onPassport() async {
    data.put(kPhotoLocationKey, false);
    // finally saving user in database
    try {
      await migrateTemporaryUserDataToDatabase();
      _canGoForward = true;
      widget.onPageChange(forward: true);
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(S.of(context).errorSavingUserData)));
    }
  }
}

String fileShaSum(FilePickerCross image) {
  final bytes = image.toUint8List();
  return sha256.convert(bytes).toString();
}

class PhotoSelector extends StatefulWidget {
  final Function(FilePickerCross photo, String path)? onPhoto;
  final VoidCallback? onPassport;

  const PhotoSelector({Key? key, this.onPhoto, this.onPassport})
      : super(key: key);

  @override
  _PhotoSelectorState createState() => _PhotoSelectorState();
}

class _PhotoSelectorState extends State<PhotoSelector> {
  bool _enabled = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ListTile(
            enabled: _enabled,
            leading: Icon(Icons.collections),
            onTap: _selectPhotoFromGallery,
            title: Text(S.of(context).selectSelfie)),
        if (!kIsWeb &&
            [TargetPlatform.android, TargetPlatform.iOS]
                .contains(Theme.of(context).platform))
          ListTile(
              enabled: _enabled,
              leading: Icon(Icons.camera),
              onTap: _takeSelfie,
              title: Text(S.of(context).takeSelfie)),
        ListTile(
          enabled: _enabled,
          leading: Icon(Icons.badge),
          onTap: _usePassport,
          title: Text(S.of(context).usePassport),
          subtitle: Text(S.of(context).usePassportLong),
        ),
      ],
    );
  }

  Future _selectPhotoFromGallery() async {
    setState(() {
      _enabled = false;
    });
    FilePickerCross image;
    try {
      image =
          await FilePickerCross.importFromStorage(type: FileTypeCross.image);
    } catch (e) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(S.of(context).imagePickError)));
      setState(() {
        _enabled = true;
      });
      return;
    }

    final response = await showDialog(
        context: context,
        builder: (context) => ImagePreviewDialog(
              image: image,
            ));
    if (response != true) {
      setState(() {
        _enabled = true;
      });
      return;
    }
    _savePhoto(image);
  }

  Future _takeSelfie() async {
    setState(() {
      _enabled = false;
    });
    FilePickerCross? image;
    try {
      // using a native camera dialog by default
      final file = await ImagePicker().pickImage(
        source: ImageSource.camera,
        preferredCameraDevice: CameraDevice.front,
      );
      if (file != null) image = FilePickerCross(await file.readAsBytes());
    } on PlatformException {
      // falling back to an ugly but always available dialog
      image = await Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => CameraDialog(
            side: CameraSide.back,
          ),
        ),
      );
    }
    if (image == null) {
      setState(() {
        _enabled = true;
      });
      return;
    }
    final response = await showDialog(
        context: context,
        builder: (context) => ImagePreviewDialog(
              image: image!,
            ));
    if (response != true) {
      setState(() {
        _enabled = true;
      });
      return;
    }
    _savePhoto(image);
  }

  Future _usePassport() async {
    setState(() {
      _enabled = false;
    });
    final response = await showModal(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(S.of(context).areYouSure),
        content: Text(S.of(context).ifYouChoseToUseYourPassportInsteadOfA),
        actions: [
          TextButton(
              onPressed: Navigator.of(context).pop,
              child: Text(S.of(context).back)),
          TextButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: Text(S.of(context).continueText),
          )
        ],
      ),
    );
    if (response == true) {
      widget.onPassport!();
    } else {
      setState(() {
        _enabled = true;
      });
    }
  }

  Future _savePhoto(FilePickerCross image) async {
    setState(() {
      _enabled = false;
    });
    await deleteUnusedSelfies();
    final path =
        '/selfie/${DateTime.now().millisecondsSinceEpoch}.${image.fileExtension}';

    final success = await image.saveToPath(path: path);

    if (!success) {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(S.of(context).couldNotSavePhoto)));
      setState(() {
        _enabled = true;
      });
      return;
    }
    widget.onPhoto!(image, path);
  }
}

/// deletes all photos except of the ones used in current user profiles
Future deleteUnusedSelfies() async {
  final usersImages = users.values.map((e) => e.photoLocation).toList();
  if (data.get(kPhotoLocationKey) is String)
    usersImages.add(data.get(kPhotoLocationKey));
  usersImages.removeWhere((element) => element == null);
  final oldImages = await FilePickerCross.listInternalFiles(at: '/selfie/');
  for (int i = 0; i < oldImages.length; i++) {
    if (!usersImages.contains(oldImages[i]))
      await FilePickerCross.delete(path: oldImages[i]);
  }
}
