import 'package:flutter/material.dart';
import 'package:free2pass/components/requires-network-page.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/screens/home/home.dart';
import 'package:free2pass/screens/setup/components/setup-card/views/data.view.dart';
import 'package:free2pass/screens/setup/components/setup-card/views/finish.view.dart';
import 'package:free2pass/screens/setup/components/setup-card/views/introduction.view.dart';
import 'package:free2pass/screens/setup/components/setup-card/views/phone.view.dart';
import 'package:free2pass/screens/setup/components/setup-card/views/photo.view.dart';
import 'package:free2pass/screens/splash/splash.dart';
import 'package:free2pass/utils/encode-personal-data.dart';
import 'package:free2pass/utils/hive.util.dart';
import 'package:free2pass_support/free2pass_support.dart';

class SetupScreen extends StatefulWidget {
  static const routeName = '/setup';

  @override
  _SetupScreenState createState() => _SetupScreenState();
}

class _SetupScreenState extends State<SetupScreen> {
  /// the [AxisTransitionController] used for the axis animation
  late AxisTransitionController _controller;

  @override
  void initState() {
    _controller = AxisTransitionController();
    // marking migration as obsolete
    data.put(kUsesOldSignatureMechanism, false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        actions: [
          PopupMenuButton(
            itemBuilder: (c) => [
              PopupMenuItem(
                child: Text(S.of(context).restartSetup),
                value: 0,
              ),
            ],
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            onSelected: _restartSetup,
          ),
        ],
      ),
      body: RequiresNetworkPage(
        child: Container(
          color: introBackground,
          child: LayoutBuilder(
            builder: (context, constraints) => ListView(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 16,
                    vertical: getTopPadding(constraints.maxHeight),
                  ),
                  child: AxisTransitionView(
                    backgroundColor: Colors.transparent,
                    controller: _controller,
                    children: [
                      IntroductionCard(
                        onPageChange: _pageChanged,
                      ),
                      PhoneCard(
                        onPageChange: _pageChanged,
                      ),
                      DataCard(
                        onPageChange: _pageChanged,
                      ),
                      PhotoCard(
                        onPageChange: _pageChanged,
                      ),
                      FinishCard(
                        onPageChange: _pageChanged,
                        onFinish: _finish,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _pageChanged({required bool forward}) {
    if (forward)
      _controller.next();
    else
      _controller.previous();
  }

  void _finish() {
    // saving the created user data in the [users] [Box]
    getDefaultUserData();
    Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
  }

  Future<void> _restartSetup(value) async {
    switch (value) {
      case 0:
        await data.clear();
        await users.clear();
        Navigator.of(context)
            .pushNamedAndRemoveUntil(SplashScreen.routeName, (route) => false);
        break;
    }
  }
}
