import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass_support/free2pass_support.dart';

class GeneralVaccinationInformation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            title: Text(
              S.of(context).generalVaccinationInformation,
              style: Theme.of(context).textTheme.headline5,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16),
            child: AlignedMarkdown(
              data: S.of(context).vaccinationInformationParagraphs,
              alignment: CrossAxisAlignment.start,
            ),
          )
        ],
      ),
    );
  }
}
