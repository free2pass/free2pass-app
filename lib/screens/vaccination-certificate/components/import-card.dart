import 'package:animations/animations.dart';
import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/screens/vaccination-certificate/components/vaccination-certificate-preview.dart';
import 'package:free2pass_support/free2pass_support.dart';

class VaccinationCertificateImportCard extends StatefulWidget {
  @override
  State<VaccinationCertificateImportCard> createState() =>
      _VaccinationCertificateImportCardState();
}

class _VaccinationCertificateImportCardState
    extends State<VaccinationCertificateImportCard> {
  AxisTransitionController _cameraAxisController = AxisTransitionController();

  String? _data;

  GlobalKey<ExpansionTileCardState> _cardKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return ExpansionTileCard(
      key: _cardKey,
      initiallyExpanded: vaccinationCertificates.isEmpty,
      leading: Icon(
        Icons.add_circle,
        color: Theme.of(context).colorScheme.secondary,
      ),
      title: Text(S.of(context).scanVaccinationCertificate),
      finalPadding: EdgeInsets.all(16),
      borderRadius: BorderRadius.circular(32),
      elevation: 4,
      initialElevation: 2,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(32),
          child: AspectRatio(
            aspectRatio: 1,
            child: AxisTransitionView(
              controller: _cameraAxisController,
              direction: SharedAxisTransitionType.scaled,
              children: [
                Center(
                  key: ValueKey(0),
                  child: ElevatedButton(
                    onPressed: _cameraAxisController.current == 0
                        ? _scanCertificate
                        : null,
                    child: Text(S.of(context).startScanVaccinationCertificate),
                  ),
                ),
                QrScannerWidget(
                  key: ValueKey(1),
                  onData: _handleCode,
                  onAbort: _abortScan,
                  pattern:
                      RegExp(r'^(HC1:)?[A-Z0-9:%\.\/ \$\+\-\*]+$'), // base45
                ),
                DefaultTextStyle.merge(
                  key: ValueKey(2),
                  textAlign: TextAlign.center,
                  child: VaccinationCertificatePreview(
                    data: _data,
                    onFinished: _importFinished,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  void _scanCertificate() {
    setState(() {
      _cameraAxisController.next();
    });
  }

  _handleCode(String data) {
    setState(() {
      _cameraAxisController.next();
      _data = data;
    });
  }

  void _abortScan() {
    setState(() {
      _cameraAxisController.previous();
    });
  }

  void _importFinished() {
    setState(() {
      _cameraAxisController.current = 0;
    });
    _toggleCard(false);
  }

  void _toggleCard(bool open) {
    if (open)
      _cardKey.currentState!.expand();
    else
      _cardKey.currentState!.collapse();
  }
}
