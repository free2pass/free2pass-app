import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/vaccination-certificate-model.dart';
import 'package:intl/intl.dart';

class VaccinationCertificatePreview extends StatelessWidget {
  final String? data;
  final VoidCallback onFinished;

  const VaccinationCertificatePreview(
      {Key? key, required this.data, required this.onFinished})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    String? processedData = data;
    if (processedData == null) return Container();
    try {
      final certificate = VaccinationCertificateModel.fromQr(processedData);

      return Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ListTile(
                leading: Icon(Icons.person),
                title: Text(certificate.person.fullName),
              ),
              ListTile(
                leading: Icon(Icons.calendar_today),
                title: Text(DateFormat.yMMMMd()
                    .format(certificate.vaccinationInformation.date)),
              ),
              ListTile(
                leading: Icon(Icons.gesture),
                title: Text(certificate.vaccinationInformation.issuer),
              ),
              ButtonBar(
                children: [
                  TextButton(
                      onPressed: _cancelImport,
                      child: Text(S.of(context).dontImport)),
                  ElevatedButton(
                    onPressed: () => _import(
                      key: certificate.vaccinationInformation.uniqueIdentifier,
                      data: data!,
                      certificate: certificate,
                    ),
                    child: Text(S.of(context).store),
                  ),
                ],
              )
            ],
          ),
        ),
      );
    } on UnsupportedHealthCertificateVersion catch (e) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(S.of(context).unsupportedVaccinationCertificate +
              ' ' +
              e.version),
          ElevatedButton(
            onPressed: onFinished,
            child: Text(S.of(context).back),
          ),
        ],
      );
    } catch (e) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(S.of(context).notAVaccinationCertificate),
          ElevatedButton(
            onPressed: onFinished,
            child: Text(S.of(context).back),
          ),
        ],
      );
    }
  }

  void _import({
    required String key,
    required String data,
    required VaccinationCertificateModel certificate,
  }) {
    vaccinationCertificates.put(
      key,
      VaccinationCertificateStorage(
        data,
        VaccinationCertificateStorageMetadata.fromModel(certificate),
      ),
    );
    onFinished();
  }

  void _cancelImport() {
    onFinished();
  }
}
