import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/vaccination-certificate-model.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:intl/intl.dart';
import 'package:pretty_qr_code/pretty_qr_code.dart';

class VaccinationCertificateCard extends StatelessWidget {
  final String certificate;
  final VaccinationCertificateStorageMetadata metadata;

  const VaccinationCertificateCard({Key? key, required this.certificate, required this.metadata})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      child: VaccinationCertificate(certificate: certificate, metadata: metadata),
    );
  }
}

class VaccinationCertificate extends StatefulWidget {
  final String certificate;
  final VaccinationCertificateStorageMetadata metadata;

  const VaccinationCertificate({Key? key, required this.certificate, required this.metadata})
      : super(key: key);

  @override
  State<VaccinationCertificate> createState() => _VaccinationCertificateState();
}

class _VaccinationCertificateState extends State<VaccinationCertificate> {
  late VaccinationCertificateModel certificate;

  @override
  void initState() {
    certificate = VaccinationCertificateModel.fromQr(widget.certificate);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ListTile(
          title: Text(
            S.of(context).vaccinationCertificateTitle,
            style: Theme.of(context).textTheme.headline5,
          ),
        ),
        SizedBox(height: 8.0),
        ListTile(
          leading: Icon(Icons.error, color: blue),
          title: Text(S.of(context).vaccinationCertificateVerificationNotice),
        ),
        SizedBox(height: 16.0),
        LayoutBuilder(
          builder: (context, constraints) {
            return PrettyQr(
              size: constraints.maxWidth - 16,
              data: widget.certificate,
              elementColor: Theme.of(context).brightness == Brightness.light
                  ? Colors.black
                  : Colors.white,
            );
          },
        ),
        ListTile(
          title: Text(S.of(context).vaccinationNumber(widget.metadata.numVaccinated, widget.metadata.numFinalVaccination), textAlign: TextAlign.center,),
        ),
        _ValidityTile(data: certificate.vaccinationInformation),
        ListTile(
          leading: Icon(Icons.person),
          title: Text(certificate.person.fullName),
          subtitle: Text(S.of(context).name),
        ),
        ListTile(
          leading: Icon(Icons.card_giftcard),
          title: Text(DateFormat.yMMMMd().format(certificate.birthday)),
          subtitle: Text(S.of(context).dateOfBirth),
        ),
        ListTile(
          leading: Icon(Icons.gesture),
          title: Text(certificate.vaccinationInformation.issuer),
          subtitle: Text(S.of(context).certificateIssuer),
        ),
        ListTile(
          leading: Icon(Icons.calendar_today),
          title: Text(DateFormat.yMMMMd()
              .format(certificate.vaccinationInformation.date)),
          subtitle: Text(S.of(context).dateOfVaccination),
        ),
        ListTile(
          leading: Icon(Icons.approval),
          title: Text(certificate.vaccinationInformation.uniqueIdentifier),
          subtitle: Text(S.of(context).uniqueCertificateIdentifier),
        ),
        ButtonBar(
          children: [
            TextButton(
                onPressed: _deleteCertificate,
                child: Text(S.of(context).permanentlyRemove))
          ],
        ),
      ],
    );
  }

  Future<void> _deleteCertificate() async {
    final response = await showModal(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(S.of(context).removeVaccinationCertificate),
        content: Text(S.of(context).removeVaccinationCertificateLongFormatted +
            '\n' +
            certificate.person.fullName +
            ', ' +
            DateFormat.yMMMMd().format(certificate.birthday)),
        actions: [
          TextButton(
              onPressed: Navigator.of(context).pop,
              child: Text(S.of(context).keep)),
          TextButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: Text(S.of(context).remove),
          ),
        ],
      ),
    );
    if (response != true) return;

    await vaccinationCertificates
        .delete(certificate.vaccinationInformation.uniqueIdentifier);
  }
}

class _ValidityTile extends StatelessWidget {
  final VaccinationInformation data;

  const _ValidityTile({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: data.isValid
          ? Icon(Icons.info)
          : Icon(
              Icons.error,
              color: red,
            ),
      title: Text(!data.gotAllVaccinations
          ? S.of(context).partiallyVaccinated
          : !data.isValid
              ? S.of(context).waitingForFullProtection
              : S.of(context).fullyImmunized),
    );
  }
}
