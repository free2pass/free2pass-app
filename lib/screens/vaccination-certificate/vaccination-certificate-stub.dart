import 'package:flutter/material.dart';
import 'package:free2pass/components/default-drawer/default-drawer.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass_support/free2pass_support.dart';

class VaccinationCertificateScreen extends StatefulWidget {
  static const routeName = '/vaccinationCertificateImport';

  @override
  State<VaccinationCertificateScreen> createState() =>
      _VaccinationCertificateScreenState();
}

class _VaccinationCertificateScreenState
    extends State<VaccinationCertificateScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(),
      drawer: DefaultDrawer(),
      body: ResponsiveBox(
          child: Center(
        child: Card(
          margin: EdgeInsets.all(16),
          child: ListTile(
            leading: Icon(Icons.info),
            title: Text(S.of(context).webVaccinationCertificate),
          ),
        ),
      )),
    );
  }
}
