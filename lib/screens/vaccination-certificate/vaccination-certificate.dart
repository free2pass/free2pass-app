import 'package:flutter/material.dart';
import 'package:free2pass/components/default-drawer/default-drawer.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/vaccination-certificate-model.dart';
import 'package:free2pass/screens/vaccination-certificate/components/general-vaccination-information.dart';
import 'package:free2pass/screens/vaccination-certificate/components/import-card.dart';
import 'package:free2pass/screens/vaccination-certificate/components/vaccination-certificate-widget.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:hive_flutter/hive_flutter.dart';

class VaccinationCertificateScreen extends StatefulWidget {
  static const routeName = '/vaccinationCertificateImport';

  @override
  State<VaccinationCertificateScreen> createState() =>
      _VaccinationCertificateScreenState();
}

class _VaccinationCertificateScreenState
    extends State<VaccinationCertificateScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(),
      drawer: DefaultDrawer(),
      body: ResponsiveBox(
        child: ValueListenableBuilder(
          valueListenable: vaccinationCertificates.listenable(),
          builder: (BuildContext context,
              Box<VaccinationCertificateStorage> box, Widget? child) {
            // Sort certificates
            final certList = box.values.toList();
            certList.sort((a, b) =>
                b.metadata.numVaccinated.compareTo(a.metadata.numVaccinated));
            return ListView.separated(
              padding: EdgeInsets.all(16),
              itemBuilder: (context, index) {
                if (index == 0) {
                  return VaccinationCertificateImportCard();
                }
                if (index == certList.length + 1) {
                  return GeneralVaccinationInformation();
                }
                final currentCertificate = certList[index - 1];

                return VaccinationCertificateCard(
                  certificate: currentCertificate.rawQr,
                  metadata: currentCertificate.metadata,
                );
              },
              itemCount: certList.length + 2,
              separatorBuilder: (BuildContext context, int index) =>
                  Container(height: 16),
            );
          },
        ),
      ),
    );
  }
}
