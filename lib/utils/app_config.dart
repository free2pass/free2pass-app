import 'package:flutter/material.dart';
import 'package:free2pass/models/update-data.dart';
import 'package:free2pass/push/push-adapter.dart';

class AppConfig extends InheritedWidget {
  final Stores source;
  final PushAdapter pushAdapter;

  AppConfig({
    required this.source,
    required Widget child,
    required this.pushAdapter,
  }) : super(child: child);

  static AppConfig? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<AppConfig>();
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;
}
