import 'package:free2pass/globals.dart';

const kLogoCirclePath = kScalableAssetBase + 'logo.svg';
const kLogoSquarePath = 'assets/logos/square.jpg';
