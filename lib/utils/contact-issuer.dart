import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/models/issuer.dart';
import 'package:url_launcher/url_launcher.dart';

String makeIssuerContactUri(Issuer issuer) {
  if (issuer.contactPhone != null)
    return 'tel:' + issuer.contactPhone!;
  else if (issuer.contactEmail != null)
    return 'mailto:' + issuer.contactEmail!;
  else if (issuer.contactWebsite != null)
    return issuer.contactWebsite!.startsWith('http')
        ? issuer.contactWebsite!
        : 'http://' + issuer.contactWebsite!;
  else
    return '';
}

Future<void> contactIssuer(BuildContext context, Issuer issuer) async {
  final success =
      await launch(makeIssuerContactUri(issuer)).catchError((e) => false);
  if (!success)
    ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text(S.of(context).errorContactingIssuer)));
}
