import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/user-data.dart';
import 'package:free2pass/utils/hive.util.dart';
import 'package:free2pass_support/free2pass_support.dart';

/// returns the [UserData] of the initial user
UserData getDefaultUserData() {
  return users.get(data.get(kDefaultUserKey))!;
}

/// returns the [UserData] of the currently active user
UserData getCurrentUser() {
  if (data.containsKey(kCurrentUserKey) &&
      users.containsKey(data.get(kCurrentUserKey))) {
    return users.get(data.get(kCurrentUserKey))!;
  } else {
    return getDefaultUserData();
  }
}

/// saves user in db as soon as hashed and deletes temporary data
Future migrateTemporaryUserDataToDatabase() async {
  final user = UserData(
    name: data.get(kFullNameKey),
    phone: data.get(kPhoneNumberKey),
    birthday: data.get(kBirthdayKey),
    street: data.get(kStreetKey),
    zip: data.get(kZipKey),
    town: data.get(kTownKey),
    email: data.get(kEmailKey),
    photoCheckSum: data.get(kPhotoChecksumKey),
    photoLocation: data.get(kPhotoLocationKey) == false
        ? null
        : data.get(kPhotoLocationKey),
  );
  await user.hashInitialized;
  await users.put(user.hash, user);
  await data.put(kCurrentUserKey, user.hash);
  await data.put(kDefaultUserKey, user.hash);
  await data.delete(kFullNameKey);
  await data.delete(kPhoneNumberKey);
  await data.delete(kBirthdayKey);
  await data.delete(kStreetKey);
  await data.delete(kZipKey);
  await data.delete(kTownKey);
  await data.delete(kEmailKey);
  await data.delete(kPhotoLocationKey);
  await data.delete(kPhotoChecksumKey);
}

/// cryptographically hashes [UserData] for comparison or authentication
///
/// in case the given [userData] is [null], [getDefaultUserData] is used
/// [rounds] is default to 11 or 13 in case the authentication has not
/// been migrated yet
Future<String> hashPersonalData({int rounds = 11, UserData? userData}) async {
  // TODO: remove transition workaround for next version
  if (userData == null) userData = getDefaultUserData();
  final userDataString =
      userData.name + ';' + userData.zip + ';' + userData.birthday.toString();
  final salt = r"$2b$" +
      rounds.toString() +
      r"$" +
      base64Encode(
          Crypto.getSha256Sum(userDataString).codeUnits.sublist(0, 16));

  List<String> params = [userDataString, salt];

  // the `compute` function is not available on web
  return await (!kIsWeb
      ? compute<List<String>, String>(asyncHash, params)
      : asyncHash(params));
}

/// performs a hash using built-in types only. Used for [Isolate] communication
Future<String> asyncHash(List<String> data) async {
  return Crypto.getBCryptChecksum(data[0], data[1]);
}
