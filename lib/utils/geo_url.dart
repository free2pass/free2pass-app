import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

abstract class GeoUrl {
  static void open(BuildContext context, double lat, double long) {
    // handling unofficial Android format:
    // https://en.wikipedia.org/wiki/Geo_URI_scheme#Unofficial_extensions
    if (Theme.of(context).platform == TargetPlatform.android)
      launch('geo:0,0?q=$lat,$long');
    else
      launch('geo:$lat,$long');
  }
}
