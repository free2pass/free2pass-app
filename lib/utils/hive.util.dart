import 'package:free2pass/enums/appointments.enum.dart';
import 'package:free2pass/enums/certificate-status.enum.dart';
import 'package:free2pass/enums/location-types.enum.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/appointment.dart';
import 'package:free2pass/models/certificate.dart';
import 'package:free2pass/models/issuer.dart';
import 'package:free2pass/models/location.dart';
import 'package:free2pass/models/merchant.dart';
import 'package:free2pass/models/occasion.model.dart';
import 'package:free2pass/models/opening-hours.dart';
import 'package:free2pass/models/user-data.dart';
import 'package:free2pass/models/vaccination-certificate-model.dart';
import 'package:free2pass/push/exchange/push-entrypoint.dart';
import 'package:free2pass/push/exchange/push-subscription.dart';
import 'package:hive_flutter/hive_flutter.dart';

const _settingsBoxName = 'settings';
const _appDataBoxName = 'data';
const _usersBoxName = 'users';
const _certificatesBoxName = 'certificates';
const _visitedLocationsBoxName = 'visitedLocations';
const _cachedLocationsBoxName = 'cachedLocations';
const _localAesKeysBoxName = 'aesKeys';
const _createdOccasionsBox = 'createdOccasions';
const _appointmentsBox = 'appointments';
const _vaccinationCertificatesBox = 'vaccinationCertificates';
const _pushSubscriptionsBox = 'pushSubscriptions';

// deprecated user storage for setup
const kPhoneNumberKey = 'phoneNumber';
const kFullNameKey = 'fullName';
const kEmailKey = 'email';
const kBirthdayKey = 'birthday';
const kStreetKey = 'street';
const kZipKey = 'zip';
const kTownKey = 'town';
const kPhotoLocationKey = 'photoLocation';
const kPhotoChecksumKey = 'photoChecksum';

const kIntroductionShown = 'introductionShown';

const kDefaultUserKey = 'defaultUser';
const kCurrentUserKey = 'currentUser';

const kLocationAccessGrantedKey = 'locationAccessGranted';

const kUsesOldSignatureMechanism = 'useOldSignature';

const kThemeKey = 'theme';
const kLocaleKey = 'locale';

const kDefaultTheme = 1;
const kDefaultLocale = 'en';

Future<void> initHive() async {
  await Hive.initFlutter();
  Hive.registerAdapter(CertificateAdapter());
  Hive.registerAdapter(IssuerAdapter());
  Hive.registerAdapter(MerchantAdapter());
  Hive.registerAdapter(LocationAdapter());
  Hive.registerAdapter(LocationTypeAdapter());
  Hive.registerAdapter(CertificateStatusValidityAdapter());
  Hive.registerAdapter(UserDataAdapter());
  Hive.registerAdapter(OccasionAdapter());
  Hive.registerAdapter(OpeningHoursAdapter());
  Hive.registerAdapter(AppointmentAdapter());
  Hive.registerAdapter(AppointmentCancelReasonAdapter());
  Hive.registerAdapter(AppointmentStateAdapter());
  Hive.registerAdapter(AppointmentRevokeReasonAdapter());
  Hive.registerAdapter(VaccinationCertificateStorageAdapter());
  Hive.registerAdapter(VaccinationCertificateStorageMetadataAdapter());
  Hive.registerAdapter(PushSubscriptionAdapter());
  Hive.registerAdapter(PushEntrypointAdapter());
  settings = await Hive.openBox(_settingsBoxName);
  data = await Hive.openBox(_appDataBoxName);
  users = await Hive.openBox(_usersBoxName);
  certificates = await Hive.openBox(_certificatesBoxName);
  visitedLocations = await Hive.openBox(_visitedLocationsBoxName);
  cachedLocations = await Hive.openBox(_cachedLocationsBoxName);
  localAesKeys = await Hive.openBox(_localAesKeysBoxName);
  createdOccasions = await Hive.openBox(_createdOccasionsBox);
  appointments = await Hive.openBox(_appointmentsBox);
  vaccinationCertificates = await Hive.openBox(_vaccinationCertificatesBox);
  pushSubscriptions = await Hive.openBox(_pushSubscriptionsBox);
  // await cachedLocations.clear();
  // await settings.clear();
  // await certificates.clear();
  // await visitedLocations.clear();
  // await data.clear();
  // await localAesKeys.clear();
  // await appointments.clear();
  // await vaccinationCertificates.clear();
  /*certificates.add(Certificate(
      createDate: 1619269585,
      issuer: Issuer(
        name: 'Firma GmbH',
        isPrivate: true,
        logo:
            'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/F-Droid_Logo_4.svg/480px-F-Droid_Logo_4.svg.png',
        id: 6329856,
        address: 'Brückenstraße',
        contactWebsite: 'https://testzentrum.de',
      ),
      expectedValidity: CertificateStatusValidity.NEGATIVE,
      revokeReasonString: 'Some lemon drops fell into the solution.',
      expiryDate: 1619615185,
      id: 'uuid'));
  certificates.put(
      '78678697',
      Certificate(
          createDate: 1619517196,
          issuer: Issuer(
            name: 'Firma GmbH',
            isPrivate: true,
            logo:
                'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/F-Droid_Logo_4.svg/480px-F-Droid_Logo_4.svg.png',
            id: 6329856,
            address: 'Brückenstraße',
            contactWebsite: 'https://testzentrum.de',
          ),
          expectedValidity: CertificateStatusValidity.PENDING,
          expiryDate: 1619689996,
          id: '78678697'));*/
  /*certificates.add(Certificate(
      createDate: 1619269585,
      issuer: Issuer(
        name: 'Andere Firma GmbH',
        isPrivate: true,
        id: 89437,
        address: 'Brückenstraße',
        contactWebsite: 'https://testzentrum.de',
      ),
      expectedValidity: CertificateStatusValidity.PENDING,
      expiryDate: 1619615185,
      id: 'uuid'));*/
  /*certificates.put(
      'uuid',
      Certificate(
          createDate: 1619854551,
          issuer: Issuer(
            name: 'Testzentrum Brückenstraße',
            id: 6329856,
            address: 'Brückenstraße',
            contactWebsite: 'https://testzentrum.de',
          ),
          expiryDate: 1619940951,
          expectedValidity: CertificateStatusValidity.POSITIVE,
          positiveComment: PlaceholderGenerator.cupcakeIpsum(),
          id: 'uuid'));*/
  /*visitedLocations.add(
    VisitedLocation(
      name: 'Nienburg Markt',
      id: 69687,
      street: 'Lange Straße',
      //website: 'https://testzentrum.de',
      checkIn: 1617645904,
      checkOut: 1617732304,
      latitude: 52.64181,
      longitude: 9.20811,
    ),
  );*/

  /*cachedLocations.add(
    Location(
      name: 'Nienburg Markt',
      id: 69687,
      street: 'Lange Straße',
      streetNumber: '23',
      latitude: 52.64181,
      longitude: 9.20811,
      type: LocationType.MERCHANT,
    ),
  );
  cachedLocations.add(
    Location(
      name: 'Sparkasse',
      id: 24435,
      street: 'Goetheplatz',
      streetNumber: '4',
      website: 'https://testzentrum.de',
      latitude: 52.64173,
      longitude: 9.20957,
      type: LocationType.MERCHANT,
    ),
  );
  cachedLocations.add(
    Location(
      name: 'Testzentrum Marienstraße',
      id: 24435,
      street: 'Marienstraße',
      streetNumber: '43',
      website: 'https://testzentrum.de',
      latitude: 52.64088,
      longitude: 9.21260,
      type: LocationType.ISSUER,
    ),
  );*/
  /*appointments.put(89437,Appointment(
      Issuer(
          name: 'Andere Firma GmbH',
          isPrivate: true,
          id: 89437,
          address: 'Brückenstraße',
          contactWebsite: 'https://testzentrum.de',
          openingTimes: [
            OpeningHours(1, 750, 900),
          ]),
      1622981770));*/
}
