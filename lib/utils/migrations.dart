import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/utils/encode-personal-data.dart';
import 'package:free2pass/utils/hive.util.dart';

Future runMigrationJobs() async {
  if (data.get(kCurrentUserKey) == null) {
    data.put(kCurrentUserKey, getDefaultUserData().hash);
  }

  // migrating to new signature mechanism
  if (data.get(kUsesOldSignatureMechanism, defaultValue: true) &&
      certificates.isNotEmpty) {
    try {
      final response = await F2PApi.instance.certificates.swapSignature(
        oldSignature: await hashPersonalData(rounds: 13),
        newSignature: await hashPersonalData(rounds: 11),
      );
      if (response.status.success) data.put(kUsesOldSignatureMechanism, false);
    } catch (e) {
      data.put(kUsesOldSignatureMechanism, true);
    }
  } else {
    data.put(kUsesOldSignatureMechanism, false);
  }
}

Future defaultUserMigration() async {
  // migrating old default user key
  if (users.isEmpty) return;
  if (users.containsKey(kDefaultUserKey)) {
    final defaultUser = users.get(kDefaultUserKey)!;
    await users.put(defaultUser.hash, defaultUser);
    await data.put(kDefaultUserKey, defaultUser.hash);
    await users.delete(kDefaultUserKey);
  }
  if (!data.containsKey(kDefaultUserKey)) {
    await data.put(kDefaultUserKey, users.keys.first);
  }
  if (!data.containsKey(kCurrentUserKey)) {
    await data.put(kCurrentUserKey, data.get(kDefaultUserKey));
  }
}
